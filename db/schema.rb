# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140219193234) do

  create_table "accountabilities", :force => true do |t|
    t.integer  "account_id"
    t.integer  "carer_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "accounts", :force => true do |t|
    t.integer  "user_id"
    t.string   "invoice_type"
    t.string   "sage_account_reference"
    t.datetime "created_at",             :null => false
    t.datetime "updated_at",             :null => false
    t.string   "account_no"
    t.integer  "nursery_id"
    t.string   "account_name"
  end

  create_table "active_admin_comments", :force => true do |t|
    t.string   "resource_id",   :null => false
    t.string   "resource_type", :null => false
    t.integer  "author_id"
    t.string   "author_type"
    t.text     "body"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.string   "namespace"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], :name => "index_active_admin_comments_on_author_type_and_author_id"
  add_index "active_admin_comments", ["namespace"], :name => "index_active_admin_comments_on_namespace"
  add_index "active_admin_comments", ["resource_type", "resource_id"], :name => "index_admin_notes_on_resource_type_and_resource_id"

  create_table "admin_users", :force => true do |t|
    t.string   "username",               :default => "", :null => false
    t.string   "first_name",             :default => "", :null => false
    t.string   "last_name",              :default => "", :null => false
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "failed_attempts",        :default => 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "admin_users", ["email"], :name => "index_admin_users_on_email", :unique => true
  add_index "admin_users", ["reset_password_token"], :name => "index_admin_users_on_reset_password_token", :unique => true
  add_index "admin_users", ["unlock_token"], :name => "index_admin_users_on_unlock_token", :unique => true

  create_table "billing_periods", :force => true do |t|
    t.integer  "nursery_id"
    t.date     "start_date"
    t.date     "end_date"
    t.string   "status"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "bookings", :force => true do |t|
    t.integer  "nursery_id"
    t.integer  "child_id"
    t.integer  "period_id"
    t.date     "start_date"
    t.date     "end_date"
    t.string   "every"
    t.text     "days_of_week"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
  end

  create_table "carers", :force => true do |t|
    t.string   "title"
    t.string   "first_name"
    t.string   "surname"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "nursery_id"
  end

  create_table "charges", :force => true do |t|
    t.integer  "invoice_line_item_id"
    t.integer  "chargeable_id"
    t.string   "chargeable_type"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "child_relationship_types", :force => true do |t|
    t.string   "relationship_type"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
  end

  create_table "child_relationships", :force => true do |t|
    t.integer  "child_id"
    t.integer  "carer_id"
    t.integer  "child_relationship_type_id"
    t.datetime "created_at",                 :null => false
    t.datetime "updated_at",                 :null => false
    t.boolean  "pick_up_flag"
    t.boolean  "drop_off_flag"
  end

  create_table "children", :force => true do |t|
    t.integer  "nursery_id"
    t.integer  "account_id"
    t.integer  "staff_member_id"
    t.integer  "room_id"
    t.integer  "carer_id"
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "surname"
    t.string   "preferred_name"
    t.string   "gender_id"
    t.date     "date_of_birth"
    t.date     "start_date"
    t.date     "leave_date"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
    t.string   "AEN"
    t.integer  "hair_id"
    t.integer  "eye_color_id"
    t.integer  "religion_id"
    t.integer  "ethnicity_id"
    t.integer  "nationality_id"
    t.integer  "language_id"
    t.integer  "status_type_id"
  end

  create_table "contact_infos", :force => true do |t|
    t.string   "address_line_1"
    t.string   "address_line_2"
    t.string   "address_line_3"
    t.string   "town"
    t.string   "county"
    t.string   "country"
    t.string   "post_code"
    t.string   "telephone_1"
    t.string   "telephone_2"
    t.string   "telephone_3"
    t.string   "telephone_4"
    t.string   "skype"
    t.string   "email_1"
    t.string   "email_2"
    t.string   "website"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.integer  "addressable_id"
    t.string   "addressable_type"
  end

  create_table "currencies", :force => true do |t|
    t.integer  "currency_type_id"
    t.integer  "nursery_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "currency_types", :force => true do |t|
    t.string   "currency_name"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  create_table "discounts", :force => true do |t|
    t.integer  "nursery_id"
    t.string   "name"
    t.string   "discount_type"
    t.decimal  "amount",            :precision => 6, :scale => 3
    t.datetime "created_at",                                      :null => false
    t.datetime "updated_at",                                      :null => false
    t.integer  "discountable_id"
    t.string   "discountable_type"
    t.date     "start_date"
    t.date     "end_date"
    t.string   "applies_to"
  end

  create_table "employments", :force => true do |t|
    t.integer  "nursery_id"
    t.integer  "user_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "ethnicities", :force => true do |t|
    t.string   "race"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "eye_colors", :force => true do |t|
    t.string   "shade"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "hairs", :force => true do |t|
    t.string   "color"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "invoice_line_items", :force => true do |t|
    t.integer  "invoice_id"
    t.integer  "account_id"
    t.string   "description"
    t.decimal  "cost",        :precision => 5, :scale => 2
    t.decimal  "total_cost",  :precision => 6, :scale => 2
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.decimal  "quantity",    :precision => 5, :scale => 2
    t.integer  "child_id"
    t.boolean  "editable"
    t.integer  "issue"
    t.decimal  "fe_amount",   :precision => 5, :scale => 2
  end

  create_table "invoices", :force => true do |t|
    t.integer  "nursery_id"
    t.integer  "carer_id"
    t.integer  "account_id"
    t.integer  "issue"
    t.string   "state"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.integer  "billing_period_id"
    t.integer  "number"
  end

  create_table "languages", :force => true do |t|
    t.string   "tongue"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "local_authorities", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "nationalities", :force => true do |t|
    t.string   "country"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "nurseries", :force => true do |t|
    t.integer  "nursery_group_id"
    t.string   "name"
    t.datetime "created_at",              :null => false
    t.datetime "updated_at",              :null => false
    t.integer  "region_id"
    t.integer  "billing_cycle_start_day"
    t.integer  "local_authority_id"
  end

  create_table "nursery_groups", :force => true do |t|
    t.string   "name"
    t.date     "billing_period_start_date"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "open_hours", :force => true do |t|
    t.integer  "nursery_id"
    t.string   "day_of_week"
    t.time     "open_time"
    t.time     "close_time"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "period_automations", :force => true do |t|
    t.integer  "period_id"
    t.integer  "preset_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "period_prices", :force => true do |t|
    t.integer  "period_id"
    t.datetime "start_timestamp"
    t.datetime "end_timestamp"
    t.decimal  "price",           :precision => 5, :scale => 2
    t.datetime "created_at",                                    :null => false
    t.datetime "updated_at",                                    :null => false
  end

  create_table "periodations", :force => true do |t|
    t.integer  "period_id"
    t.integer  "room_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "periods", :force => true do |t|
    t.integer  "nursery_id"
    t.string   "period_name"
    t.time     "period_start_time"
    t.time     "period_end_time"
    t.decimal  "period_hourly_cost", :precision => 5, :scale => 2
    t.datetime "created_at",                                       :null => false
    t.datetime "updated_at",                                       :null => false
    t.decimal  "period_cost",        :precision => 5, :scale => 2
    t.text     "days_of_week"
    t.decimal  "fe_limit",           :precision => 6, :scale => 3
  end

  create_table "permissions", :force => true do |t|
    t.integer  "role_id"
    t.string   "action"
    t.string   "condition"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "presets", :force => true do |t|
    t.integer  "nursery_id"
    t.string   "preset_name"
    t.time     "start_time"
    t.time     "end_time"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "regions", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "religions", :force => true do |t|
    t.string   "name"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
  end

  add_index "roles", ["name", "resource_type", "resource_id"], :name => "index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], :name => "index_roles_on_name"

  create_table "rooms", :force => true do |t|
    t.integer  "nursery_id"
    t.integer  "staff_member_id"
    t.string   "name"
    t.integer  "capacity"
    t.integer  "age_from"
    t.integer  "age_to"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "staff_members", :force => true do |t|
    t.integer  "nursery_id"
    t.integer  "contact_info_id"
    t.string   "first_name"
    t.string   "middle_name"
    t.string   "surname"
    t.string   "title"
    t.date     "date_of_birth"
    t.text     "medical_conditions"
    t.date     "start_date"
    t.date     "leave_date"
    t.string   "payroll_number"
    t.integer  "hours_per_week_contracted"
    t.float    "hourly_rate"
    t.date     "start_of_holiday_year"
    t.float    "holiday_entitlement"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  create_table "status_types", :force => true do |t|
    t.string   "status_name"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "terms", :force => true do |t|
    t.integer  "local_authority_id"
    t.string   "description"
    t.date     "start_date"
    t.date     "end_date"
    t.datetime "created_at",         :null => false
    t.datetime "updated_at",         :null => false
  end

  create_table "timesheets", :force => true do |t|
    t.integer  "nursery_id"
    t.integer  "child_id"
    t.integer  "period_id"
    t.date     "date_attended"
    t.integer  "duration"
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
    t.decimal  "hourly_cost",   :precision => 5, :scale => 2
    t.decimal  "period_cost",   :precision => 5, :scale => 2
    t.boolean  "full_period"
    t.integer  "fe_time"
    t.integer  "term_id"
  end

  create_table "users", :force => true do |t|
    t.string   "username",               :default => "", :null => false
    t.string   "first_name",             :default => "", :null => false
    t.string   "last_name",              :default => "", :null => false
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.integer  "failed_attempts",        :default => 0
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
  end

  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true
  add_index "users", ["unlock_token"], :name => "index_users_on_unlock_token", :unique => true

  create_table "users_roles", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], :name => "index_users_roles_on_user_id_and_role_id"

end
