module ActiveModel 
  module MassAssignmentSecurity 
    class Sanitizer 
      def sanitize(attributes, authorizer) 
        attributes 
      end 
    end 
  end 
end

d = Date.new(2012,10,15)



# GROUP



g = NurseryGroup.find_or_create_by_name(name: "The Old Station Nursery Ltd", billing_period_start_date: Date.new(2012,10,1)).id

# ADDRESS DETAILS

nc1 = ContactInfo.find_or_create_by_address_line_1(address_line_1: "Brayford Pool", town: "Lincoln", post_code: "LN1 RD1").id

nc2 = ContactInfo.find_or_create_by_address_line_1(address_line_1: "Park Road", town: "Faringdon", county: "Oxon", post_code: "SN7 7BP").id

nc3 = ContactInfo.find_or_create_by_address_line_1(address_line_1: "Wallingford" ,town: "Wallingford", county: "Oxon", post_code: "OX10 6AA").id



andreaaddress = ContactInfo.find_or_create_by_address_line_1(address_line_1: "82 Sewell Rd", town: "Lincoln", post_code: "LN2 5LY").id

sandraaddress = ContactInfo.find_or_create_by_address_line_1(address_line_1: "47 Lichfield Rd", town: "Lincoln", post_code: "LN4 8SP").id

kelliejoaddress = ContactInfo.find_or_create_by_address_line_1(address_line_1: "187 Marks Rd", town: "Lincoln", post_code: "LN5 7SY").id

kellyaddress = ContactInfo.find_or_create_by_address_line_1(address_line_1: "24 Lechlade Rd", town: "Faringdon", county: "Oxon", post_code: "SN7 8AQ").id

hannahaddress = ContactInfo.find_or_create_by_address_line_1(address_line_1: "23 Market Place", town: "Faringdon", county: "Oxon", post_code: "SN7 7HU").id

traceyaddress = ContactInfo.find_or_create_by_address_line_1(address_line_1: "15 High St", town: "Faringdon", county: "Oxon", post_code: "SN7 8AE").id

caroladdress = ContactInfo.find_or_create_by_address_line_1(address_line_1: "10 Sands Way", town: "Benson", county: "Oxon", post_code: "OX10 6NG").id

shelinaaddress = ContactInfo.find_or_create_by_address_line_1(address_line_1: "1 Castle Square", town: "Benson", county: "Oxon", post_code: "OX10 6SD").id

taranaaddress = ContactInfo.find_or_create_by_address_line_1(address_line_1: "25 High Street", town: "Benson", county: "Oxon", post_code: "OX10 6RP").id



marinacarercont1 = ContactInfo.find_or_create_by_address_line_1(address_line_1: "22 Albany Terrace", town: "Lincoln", post_code: "LN5 7SY").id

marinacarercont2 = ContactInfo.find_or_create_by_address_line_1(address_line_1: "46 Portland St", town: "Lincoln", post_code: "LN5 8HG").id

marinacarercont3 = ContactInfo.find_or_create_by_address_line_1(address_line_1: "263 Skellingthorpe Rd", town: "Lincoln", post_code: "LN6 0PA").id

faringdoncarercont1 = ContactInfo.find_or_create_by_address_line_1(address_line_1: "7 Park Rd", town: "Faringdon", post_code: "SN7 7BP").id

faringdoncarercont2 = ContactInfo.find_or_create_by_address_line_1(address_line_1: "23 Market Place", town: "Faringdon", post_code: "SN7 7HU").id

faringdoncarercont3 = ContactInfo.find_or_create_by_address_line_1(address_line_1: "3 Marlborough St", town: "Faringdon", post_code: "SN7 7JE").id

bensoncarercont1 = ContactInfo.find_or_create_by_address_line_1(address_line_1: "26 St Marys St", town: "Wallingford", post_code: "OX10 0ET").id

bensoncarercont2 = ContactInfo.find_or_create_by_address_line_1(address_line_1: "12 Saint Marys", town: "Wallingford", post_code: "OX10 0EW").id

bensoncarercont3 = ContactInfo.find_or_create_by_address_line_1(address_line_1: "85 Wallingford St", town: "Wallingford", post_code: "OX10 0BW").id

# NURSERIES 

n1 = Nursery.find_or_create_by_name(name: "The Old Station Nursery Ltd - The Marina", nursery_group_id: g).id

n2 = Nursery.find_or_create_by_name(name: "The Old Station Nursery Ltd - Park Road", nursery_group_id: g).id

n3 = Nursery.find_or_create_by_name(name: "The Old Station Nursery Ltd - RAF Benson", nursery_group_id: g).id

# STAFF

staffandrea=StaffMember.find_or_create_by_first_name(first_name: "Andrea", nursery_id: n1,  surname: "Goldacre", date_of_birth: Date.new(1987,01,07), start_date: Date.new(2011,10,03), payroll_number: "7970", hours_per_week_contracted: 0, hourly_rate: 1.30, start_of_holiday_year: Date.new(2012,1,1), holiday_entitlement: 20).id

staffsandra=StaffMember.find_or_create_by_first_name(first_name: "Sandra", nursery_id: n1,  surname: "Bureck", date_of_birth: Date.new(1991,12,23), start_date: Date.new(2012,2,27), payroll_number: "8350", hours_per_week_contracted: 12, hourly_rate: 1.50, start_of_holiday_year: Date.new(2012,1,1), holiday_entitlement: 20).id

staffkelliejo=StaffMember.find_or_create_by_first_name(first_name: "Kellie Jo", nursery_id: n1,  surname: "Loughlin", date_of_birth: Date.new(1977,06,19), start_date: Date.new(2007,3,19), payroll_number: "1820", hours_per_week_contracted: 42.5, hourly_rate: 1.49, start_of_holiday_year: Date.new(2012,1,1), holiday_entitlement: 25).id

staffkelly=StaffMember.find_or_create_by_first_name(first_name: "Kelly", nursery_id: n2,  surname: "Bowman", date_of_birth: Date.new(1989,05,27), start_date: Date.new(2010,5,13), payroll_number: "6450", hours_per_week_contracted: 42.5, hourly_rate: 1.08, start_of_holiday_year: Date.new(2012,1,1), holiday_entitlement: 21).id

staffhannah=StaffMember.find_or_create_by_first_name(first_name: "Hannah", nursery_id: n2,  surname: "Becket", date_of_birth: Date.new(1978,10,15), start_date: Date.new(2007,9,03), payroll_number: "3490", hours_per_week_contracted: 42.5, hourly_rate: 1.43, start_of_holiday_year: Date.new(2012,1,1), holiday_entitlement: 24).id

stafftracey=StaffMember.find_or_create_by_first_name(first_name: "Tracey", nursery_id: n2,  surname: "Gill", date_of_birth: Date.new(1965,10,02), start_date: Date.new(20012,2,13), payroll_number: "8220", hours_per_week_contracted: 42.5, hourly_rate: 1.80, start_of_holiday_year: Date.new(2012,1,1), holiday_entitlement: 20).id

staffcarol=StaffMember.find_or_create_by_first_name(first_name: "Carol", nursery_id: n3,  surname: "Beattie", date_of_birth: Date.new(1986,04,10), start_date: Date.new(2008,6,30), payroll_number: "4690", hours_per_week_contracted: 40, hourly_rate: 1.28, start_of_holiday_year: Date.new(2012,1,1), holiday_entitlement: 23).id

staffshelina=StaffMember.find_or_create_by_first_name(first_name: "Shelina", middle_name: "Begum", nursery_id: n3,  surname: "Salman", date_of_birth: Date.new(1966,10,11), start_date: Date.new(2010,3,8), payroll_number: "6260", hours_per_week_contracted: 25, hourly_rate: 1.24, start_of_holiday_year: Date.new(2012,1,1), holiday_entitlement: 21).id

stafftarana=StaffMember.find_or_create_by_first_name(first_name: "Tarana", middle_name: "Kibria", nursery_id: n3,  surname: "Chowdhury", date_of_birth: Date.new(1983,02,10), start_date: Date.new(2009,3,9), payroll_number: "5330", hours_per_week_contracted: 25, hourly_rate: 1.24, start_of_holiday_year: Date.new(2012,1,1), holiday_entitlement: 22).id

# ROOMS

marinacaterpillers = Room.find_or_create_by_name(name: "Caterpillers", nursery_id: n1, staff_member_id: staffkelliejo, capacity: 14, age_from: 0, age_to: 23).id

marinabutterflies = Room.find_or_create_by_name(name: "Butterlies", nursery_id: n1, staff_member_id: staffsandra, capacity: 23, age_from: 24, age_to: 35).id

marinaladybirds = Room.find_or_create_by_name(name: "Ladybirds", nursery_id: n1, staff_member_id: staffandrea, capacity: 30, age_from: 36, age_to: 60).id

faringdonbabies = Room.find_or_create_by_name(name: "Babies", nursery_id: n2, staff_member_id: staffhannah, capacity: 10, age_from: 0, age_to: 23).id

faringdontoddlers = Room.find_or_create_by_name(name: "Toddlers", nursery_id: n2, staff_member_id: staffkelly, capacity: 25, age_from: 24, age_to: 35).id

faringdonpreschool = Room.find_or_create_by_name(name: "Pre school", nursery_id: n2, staff_member_id: stafftracey, capacity: 25, age_from: 36, age_to: 60).id

faringdonbeforeafterschool = Room.find_or_create_by_name(name: "before after school", nursery_id: n2, staff_member_id: stafftracey, capacity: 20, age_from: 48, age_to: 98).id

bensonbabies = Room.find_or_create_by_name(name: "Babies", nursery_id: n3, staff_member_id: staffcarol, capacity: 10, age_from: 0, age_to: 12).id

bensonr2 = Room.find_or_create_by_name(name: "1-2", nursery_id: n3, staff_member_id: staffshelina, capacity: 20, age_from: 13, age_to: 23).id

bensonr3 = Room.find_or_create_by_name(name: "2-3", nursery_id: n3, staff_member_id: stafftarana, capacity: 20, age_from: 24, age_to: 35).id

bensonpreschool1= Room.find_or_create_by_name(name: "Pre-school", nursery_id: n3, staff_member_id: staffcarol, capacity: 20, age_from: 36, age_to: 60).id

bensonpreschool2 = Room.find_or_create_by_name(name: "Pre-school", nursery_id: n3, staff_member_id: staffshelina, capacity: 20, age_from: 36, age_to: 60).id

bensonoutofschool = Room.find_or_create_by_name(name: "out of school", nursery_id: n3, staff_member_id: stafftarana, capacity: 25, age_from: 48, age_to: 132).id

bensonbreakfastandafterschool = Room.find_or_create_by_name(name: "Before and After School", nursery_id: n3, staff_member_id: staffshelina, capacity: 20, age_from: 48, age_to: 132).id

bensonholidayclub = Room.find_or_create_by_name(name: "Holiday club", nursery_id: n3, staff_member_id: staffshelina, capacity: 36, age_from: 0, age_to: 132).id



# USERS

user1 = User.find_or_create_by_email(email: "user1@example.com", username: "Gray", first_name: "Paul", last_name: "Gray", password: "password", password_confirmation: "password").id
marinauser1 = User.find_or_create_by_email(email: "graham@example.com", username: "Graham", first_name: "Wendy", last_name: "Graham", password: "password", password_confirmation: "password").id
marinauser2 = User.find_or_create_by_email(email: "fraser@example.com", username: "Fraser", first_name: "Linda", last_name: "Fraser", password: "password", password_confirmation: "password").id
marinauser3 = User.find_or_create_by_email(email: "davis@example.com", username: "Thomppho", first_name: "Ben", last_name: "Davis-Todd", password: "password", password_confirmation: "password").id
faringdonuser1 = User.find_or_create_by_email(email: "adu@example.com", username: "Adu", first_name: "Susanna", last_name: "Adu", password: "password", password_confirmation: "password").id
faringdonuser2 = User.find_or_create_by_email(email: "bannerman@example.com", username: "Bannerman", first_name: "MarkH", last_name: "Bannerman", password: "password", password_confirmation: "password").id
faringdonuser3 = User.find_or_create_by_email(email: "halbert@example.com", username: "Halbert", first_name: "Micheal", last_name: "Halbert", password: "password", password_confirmation: "password").id
bensonuser1 = User.find_or_create_by_email(email: "algar@example.com", username: "Algar", first_name: "Mairi", last_name: "Algar", password: "password", password_confirmation: "password").id
bensonuser2 = User.find_or_create_by_email(email: "williams@example.com", username: "Williams", first_name: "Paul", last_name: "Williams", password: "password", password_confirmation: "password").id
bensonuser3 = User.find_or_create_by_email(email: "clements@example.com", username: "Clements", first_name: "Sarah", last_name: "Clements", password: "password", password_confirmation: "password").id

# ACCOUNTS

marinaacct1 = Account.find_or_create_by_account_no(account_no: "GRAHAM1", user_id: marinauser1, sage_account_reference: "GRAHAM1").id

marinaacct2 = Account.find_or_create_by_account_no(account_no: "FRAZERE", user_id: marinauser2, sage_account_reference: "FRAZERE").id

marinaacct3 = Account.find_or_create_by_account_no(account_no: "THOMPPHO", user_id: marinauser3, sage_account_reference: "THOMPPHO").id

faringdonacct1 = Account.find_or_create_by_account_no(account_no: "ADUJOSHJ", user_id: faringdonuser1, sage_account_reference: "ADUJOSHJ").id

faringdonacct2 = Account.find_or_create_by_account_no(account_no: "BANERMA", user_id: faringdonuser2, sage_account_reference: "BANERMA").id

faringdonacct3 = Account.find_or_create_by_account_no(account_no: "HALBERTA", user_id: faringdonuser3, sage_account_reference: "HALBERTA").id

bensonacct1 = Account.find_or_create_by_account_no(account_no: "ALGARFRA", user_id: bensonuser1, sage_account_reference: "ALGARFRA").id

bensonacct2 = Account.find_or_create_by_account_no(account_no: "WILLIAMS", user_id: bensonuser2, sage_account_reference: "WILLIAMS").id

bensonacct3 = Account.find_or_create_by_account_no(account_no: "CLEMENTS", user_id: bensonuser3, sage_account_reference: "CLEMENTS").id

# CARERS

marinacarer1 = Carer.find_or_create_by_surname(surname: "Graham", first_name: "Wendy", title: "Mrs").id 
marinacarer2 = Carer.find_or_create_by_surname(surname: "Fraser", first_name: "Linda", title: "Mrs").id
marinacarer3 = Carer.find_or_create_by_surname(surname: "Davies-Todd", first_name: "Ben", title: "Mr").id
maringdoncarer1 = Carer.find_or_create_by_surname(surname: "ADU", first_name: "Susanna", title: "Mrs").id
maringdoncarer2 = Carer.find_or_create_by_surname(surname: "Bannerman", first_name: "MarkH", title: "Mr").id
maringdoncarer3 = Carer.find_or_create_by_surname(surname: "Halbert", first_name: "Micheal", title: "Mr").id
mensoncarer1 = Carer.find_or_create_by_surname(surname: "Algar", first_name: "Mairi", title: "Mrs").id
mensoncarer2 = Carer.find_or_create_by_surname(surname: "Williams", first_name: "Paul", title: "Mr").id
mensoncarer3 = Carer.find_or_create_by_surname(surname: "Clements", first_name: "Sarah", title: "Mrs").id

# CHILDREN

marinachild1 = Child.find_or_create_by_surname_and_first_name(surname: "Fraser", first_name: "Joseph", nursery_id: n1, account_id: marinaacct2, staff_member_id: staffandrea, room_id: marinacaterpillers, carer_id: marinacarer2, preferred_name: "Joseph", date_of_birth: Date.new(2011,7,11), start_date: Date.new(2012,03,28), leave_date: Date.new(2015,8,31)).id

marinachild2 = Child.find_or_create_by_surname_and_first_name(surname: "Burton-Graham", first_name: "Ava", nursery_id: n1, account_id: marinaacct1, staff_member_id: staffsandra, room_id: marinabutterflies, carer_id: marinacarer1,  preferred_name: "Ava", date_of_birth: Date.new(2012,3,30), start_date: Date.new(2012,10,29), leave_date: Date.new(2016,8,31)).id

marinachild3 = Child.find_or_create_by_surname_and_first_name(surname: "Fraser", first_name: "Eleanor", nursery_id: n1, account_id: marinaacct2, staff_member_id: staffkelliejo, room_id: marinacaterpillers, carer_id: marinacarer2,  preferred_name: "Eleanor", date_of_birth: Date.new(2009,9,23), start_date: Date.new(2010,8,18), leave_date: Date.new(2014,8,31)).id

marinachild4 = Child.find_or_create_by_surname_and_first_name(surname: "Burton-Graham", first_name: "Patrick", nursery_id: n1, account_id: marinaacct1, staff_member_id: staffsandra, room_id: marinabutterflies, carer_id: marinacarer1,  preferred_name: "Patrick", date_of_birth: Date.new(2009,7,27), start_date: Date.new(2010,4,12), leave_date: Date.new(2013,8,31)).id

marinachild5 = Child.find_or_create_by_surname_and_first_name(surname: "How", first_name: "Alfie", nursery_id: n1, account_id: marinaacct3, staff_member_id: staffkelliejo, room_id: marinacaterpillers, carer_id: marinacarer3,  preferred_name: "Alfie", date_of_birth: Date.new(2011,10,29), start_date: Date.new(2012,7,02), leave_date: Date.new(2016,8,31)).id

marinachild6 = Child.find_or_create_by_surname_and_first_name(surname: "Thompson", first_name: "Phoebe", nursery_id: n1, account_id: marinaacct3, staff_member_id: staffandrea, room_id: marinaladybirds, carer_id: marinacarer3,  preferred_name: "Phoebe", date_of_birth: Date.new(2009,3,8), start_date: Date.new(2011,10,3), leave_date: Date.new(2013,8,31)).id

faringdonchild1 = Child.find_or_create_by_surname_and_first_name(surname: "Adu", first_name: "Jessica", nursery_id: n2, account_id: faringdonacct1, staff_member_id: staffkelly, room_id: faringdontoddlers, preferred_name: "Jessica", date_of_birth: Date.new(2010,1,29), start_date: Date.new(2012,10,3), leave_date: Date.new(2014,8,31)).id

faringdonchild2 = Child.find_or_create_by_surname_and_first_name(surname: "Halbert", first_name: "Alexander", nursery_id: n2, account_id: faringdonacct3, staff_member_id: staffhannah, room_id: faringdonbabies, preferred_name: "Alexander", date_of_birth: Date.new(2010,7,22), start_date: Date.new(2010,2,28), leave_date: Date.new(2014,8,31)).id

faringdonchild3 = Child.find_or_create_by_surname_and_first_name(surname: "Adu", first_name: "Joshua", nursery_id: n2, account_id: faringdonacct1, staff_member_id: stafftracey, room_id: faringdonbeforeafterschool, preferred_name: "Joshua", date_of_birth: Date.new(2008,10,20), start_date: Date.new(2012,9,04), leave_date: Date.new(2013,8,31)).id

faringdonchild4 = Child.find_or_create_by_surname_and_first_name(surname: "Bannerman", first_name: "Ruby", nursery_id: n2, account_id: faringdonacct2, staff_member_id: staffkelly, room_id: faringdontoddlers, preferred_name: "Ruby", date_of_birth: Date.new(2010,3,17), start_date: Date.new(2011,7,5), leave_date: Date.new(2014,8,31)).id

faringdonchild5 = Child.find_or_create_by_surname_and_first_name(surname: "Bannerman", first_name: "Libby", nursery_id: n2, account_id: faringdonacct2, staff_member_id: staffhannah, room_id: faringdonbabies, preferred_name: "Libby", date_of_birth: Date.new(2010,3,17), start_date: Date.new(2011,7,5), leave_date: Date.new(2014,8,31)).id

faringdonchild6 = Child.find_or_create_by_surname_and_first_name(surname: "Halbert", first_name: "Jessica", nursery_id: n2, account_id: faringdonacct3, staff_member_id: stafftracey, room_id: faringdontoddlers, preferred_name: "Jessica", date_of_birth: Date.new(2012,4,3), start_date: Date.new(2012,9,3), leave_date: Date.new(2016,8,31)).id

bensonchild1 = Child.find_or_create_by_surname_and_first_name(surname: "Algar", first_name: "Fraser", nursery_id: n3, account_id: bensonacct1, staff_member_id: staffcarol, room_id: bensonbabies, preferred_name: "Fraser", date_of_birth: Date.new(2009,4,10), start_date: Date.new(2011,11,21), leave_date: Date.new(2016,8,31)).id

bensonchild2 = Child.find_or_create_by_surname_and_first_name(surname: "Clements", first_name: "Monty", nursery_id: n3, account_id: bensonacct3, staff_member_id: staffshelina, room_id: bensonr2, preferred_name: "Monty", date_of_birth: Date.new(2006,12,27), start_date: Date.new(2010,10,5), leave_date: Date.new(2014,8,31)).id

bensonchild3 = Child.find_or_create_by_surname_and_first_name(surname: "Algar", first_name: "Finlay", nursery_id: n3, account_id: bensonacct1, staff_member_id: staffshelina, room_id: bensonholidayclub, preferred_name: "Finlay", date_of_birth: Date.new(2008,2,18), start_date: Date.new(2010,11,21), leave_date: Date.new(2012,8,31)).id

bensonchild4 = Child.find_or_create_by_surname_and_first_name(surname: "Clements", first_name: "Flora", nursery_id: n3, account_id: bensonacct3, staff_member_id: stafftarana, room_id: bensonr3, preferred_name: "Flora", date_of_birth: Date.new(2010,10,14), start_date: Date.new(2011,5,4), leave_date: Date.new(2015,8,31)).id

bensonchild5 = Child.find_or_create_by_surname_and_first_name(surname: "Williams", first_name: "Keira", nursery_id: n3, account_id: bensonacct2, staff_member_id: staffshelina, room_id: bensonbreakfastandafterschool, preferred_name: "Keira", date_of_birth: Date.new(2011,12,16), start_date: Date.new(2012,9,17), leave_date: Date.new(2016,8,31)).id

bensonchild6 = Child.find_or_create_by_surname_and_first_name(surname: "Williams", first_name: "Luke", nursery_id: n3, account_id: bensonacct2, staff_member_id: stafftarana, room_id: bensonr3, preferred_name: "Luke", date_of_birth: Date.new(2006,5,21), start_date: Date.new(2009,8,19), leave_date: Date.new(2017,8,31)).id
