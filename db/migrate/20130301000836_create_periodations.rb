class CreatePeriodations < ActiveRecord::Migration
  def change
    create_table :periodations do |t|
      t.integer :period_id
      t.integer :room_id

      t.timestamps
    end
  end
end
