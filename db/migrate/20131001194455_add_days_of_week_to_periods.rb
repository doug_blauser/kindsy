class AddDaysOfWeekToPeriods < ActiveRecord::Migration
  def change
    add_column :periods, :days_of_week, :text
  end
end
