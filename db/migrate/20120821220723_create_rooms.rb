class CreateRooms < ActiveRecord::Migration
  def change
    create_table :rooms do |t|
      t.integer :nursery_id
      t.integer :staff_member_id
      t.string :name
      t.integer :capacity
      t.integer :age_from
      t.integer :age_to

      t.timestamps
    end
  end
end
