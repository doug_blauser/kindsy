class AddFeCostToInvoiceLineItem < ActiveRecord::Migration
  def change
    add_column :invoice_line_items, :fe_amount, :decimal, :precision => 5, :scale => 2
  end
end
