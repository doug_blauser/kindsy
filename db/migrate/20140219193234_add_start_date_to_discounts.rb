class AddStartDateToDiscounts < ActiveRecord::Migration
  def change
    add_column :discounts, :start_date, :date
    add_column :discounts, :end_date, :date
    add_column :discounts, :applies_to, :string
  end
end
