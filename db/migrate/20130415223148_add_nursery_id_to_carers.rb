class AddNurseryIdToCarers < ActiveRecord::Migration
  def change
    add_column :carers, :nursery_id, :integer
  end
end
