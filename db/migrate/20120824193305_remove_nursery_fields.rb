class RemoveNurseryFields < ActiveRecord::Migration
  change_table :nurseries do |t|
    t.remove :address_line_1, :address_line_2, :address_line_3, :county, :country, :post_code, :telephone_1, :telephone_2, :email_1, :email_2, :website
  end
end
