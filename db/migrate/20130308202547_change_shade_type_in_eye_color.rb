class ChangeShadeTypeInEyeColor < ActiveRecord::Migration
  def change
    change_column :eye_colors, :shade, :string
  end
end
