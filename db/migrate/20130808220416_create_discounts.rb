class CreateDiscounts < ActiveRecord::Migration
  def change
    create_table :discounts do |t|
      t.integer :nursery_id
      t.string :name
      t.string :discount_type
      t.decimal :amount, :precision => 6, :scale => 3

      t.timestamps
    end
  end
end
