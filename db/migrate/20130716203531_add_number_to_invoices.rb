class AddNumberToInvoices < ActiveRecord::Migration
  def change
    add_column :invoices, :number, :integer
    remove_column :invoices, :amount_due
  end
end
