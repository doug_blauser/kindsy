class AddRegionToNurseryGroup < ActiveRecord::Migration
  def change
    add_column :nursery_groups, :region_id, :integer
  end
end
