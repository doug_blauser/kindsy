class CreatePeriods < ActiveRecord::Migration
  def change
    create_table :periods do |t|
      t.integer :nursery_id
      t.string :period_name
      t.time :period_start_time
      t.time :period_end_time
      t.decimal :period_hourly_cost, :precision => 5, :scale => 2

      t.timestamps
    end
  end
end
