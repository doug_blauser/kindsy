class AddEditableToInvoiceLineItems < ActiveRecord::Migration
  def change
    add_column :invoice_line_items, :editable, :boolean
  end
end
