class AddFeLimitToPeriods < ActiveRecord::Migration
  def change
    add_column :periods, :fe_limit, :decimal, :precision => 6, :scale => 3
  end
end
