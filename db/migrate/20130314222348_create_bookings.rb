class CreateBookings < ActiveRecord::Migration
  def change
    create_table :bookings do |t|
      t.integer :nursery_id
      t.integer :child_id
      t.integer :period_id
      t.date :start_date
      t.date :end_date
      t.string :every
      t.text :days_of_week

      t.timestamps
    end
  end
end
