class RemoveContactInfoIdFromCarer < ActiveRecord::Migration
  def up
    remove_column :carers, :contact_info_id
  end

  def down
    add_column :carers, :contact_info_id, :integer
  end
end
