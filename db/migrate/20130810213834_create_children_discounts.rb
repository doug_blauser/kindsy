class CreateChildrenDiscounts < ActiveRecord::Migration
  def change
    create_table 'children_discounts', :id => false do |t|
      t.integer :child_id
      t.integer :discount_id
    end
  end
end
