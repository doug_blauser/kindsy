class CreateInvoiceLineItems < ActiveRecord::Migration
  def change
    create_table :invoice_line_items do |t|
      t.integer :invoice_id
      t.integer :account_id
      t.string :description
      t.decimal :cost, :precision => 5, :scale => 2
      t.decimal :total_cost, :precision => 6, :scale => 2

      t.timestamps
    end
  end
end
