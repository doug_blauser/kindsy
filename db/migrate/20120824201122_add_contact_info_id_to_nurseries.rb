class AddContactInfoIdToNurseries < ActiveRecord::Migration
  def change
    add_column :nurseries, :contact_info_id, :integer
  end
end
