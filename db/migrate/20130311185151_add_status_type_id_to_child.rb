class AddStatusTypeIdToChild < ActiveRecord::Migration
  def change
    add_column :children, :status_type_id, :integer
  end
end
