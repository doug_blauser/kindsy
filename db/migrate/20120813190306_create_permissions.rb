class CreatePermissions < ActiveRecord::Migration
  def change
    create_table :permissions do |t|
      t.integer :role_id
      t.string :action
      t.string :condition

      t.timestamps
    end
  end
end
