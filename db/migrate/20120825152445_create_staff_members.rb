class CreateStaffMembers < ActiveRecord::Migration
  def change
    create_table :staff_members do |t|
      t.integer :nursery_id
      t.integer :contact_info_id
      t.string :first_name
      t.string :middle_name
      t.string :surname
      t.string :title
      t.date :date_of_birth
      t.text :medical_conditions
      t.date :start_date
      t.date :leave_date
      t.string :payroll_number
      t.integer :hours_per_week_contracted
      t.float :hourly_rate
      t.date :start_of_holiday_year
      t.float :holiday_entitlement
      
      t.timestamps
    end
  end
end
