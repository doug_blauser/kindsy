class DropCurrencyTable < ActiveRecord::Migration
  def up
    drop_table :currencies
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
