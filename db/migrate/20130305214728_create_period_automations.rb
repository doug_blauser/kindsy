class CreatePeriodAutomations < ActiveRecord::Migration
  def change
    create_table :period_automations do |t|
      t.integer :period_id
      t.integer :preset_id

      t.timestamps
    end
  end
end
