class CreateCharges < ActiveRecord::Migration
  def change
    create_table :charges do |t|
      t.integer :invoice_line_item_id
      t.integer :chargeable_id
      t.string :chargeable_type

      t.timestamps
    end
  end
end
