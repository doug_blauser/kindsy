class AddBillingPeriodIdToInvoices < ActiveRecord::Migration
  def change
    add_column :invoices, :billing_period_id, :integer
  end
end
