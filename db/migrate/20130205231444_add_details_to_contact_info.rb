class AddDetailsToContactInfo < ActiveRecord::Migration
  def change
    add_column :contact_infos, :addressable_id, :integer
    add_column :contact_infos, :addressable_type, :string
  end
end
