class DropAccountsDiscounts < ActiveRecord::Migration
  def up
    drop_table :accounts_discounts
    drop_table :children_discounts
  end

  def down
    create_table :accounts_discounts
    create_table :children_discounts    
  end
end
