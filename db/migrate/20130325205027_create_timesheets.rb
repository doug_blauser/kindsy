class CreateTimesheets < ActiveRecord::Migration
  def change
    create_table :timesheets do |t|
      t.integer :nursery_id
      t.integer :child_id
      t.integer :period_id
      t.date :date_attended
      t.integer :duration

      t.timestamps
    end
  end
end
