class CreateAccountsDiscounts < ActiveRecord::Migration
  def change
    create_table 'accounts_discounts', :id => false do |t|
      t.integer :account_id
      t.integer :discount_id
    end
  end
end
