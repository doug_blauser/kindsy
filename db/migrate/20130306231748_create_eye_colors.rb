class CreateEyeColors < ActiveRecord::Migration
  def change
    create_table :eye_colors do |t|
      t.integer :shade

      t.timestamps
    end
  end
end
