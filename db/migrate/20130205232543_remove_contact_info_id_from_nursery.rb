class RemoveContactInfoIdFromNursery < ActiveRecord::Migration
  def up
    remove_column :nurseries, :contact_info_id
  end

  def down
    add_column :nurseries, :contact_info_id, :integer
  end
end
