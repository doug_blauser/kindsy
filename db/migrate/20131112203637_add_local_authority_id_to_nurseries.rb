class AddLocalAuthorityIdToNurseries < ActiveRecord::Migration
  def change
    add_column :nurseries, :local_authority_id, :integer
  end
end
