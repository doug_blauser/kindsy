class CreateContactInfos < ActiveRecord::Migration
  def change
    create_table :contact_infos do |t|
      t.string :address_line_1 #bank_account, carer, child, client, contact, enquiry, local_authority, nursery, staff
      t.string :address_line_2 #bank_account, carer, child, client, contact, enquiry, local_authority, nursery, staff
      t.string :address_line_3 #bank_account, carer, child, client, contact, enquiry, local_authority, nursery, staff
      t.string :town #carer, child, client(town/city), contact, enquiry(City or Town), local_authority(City or Town), staff(City or Town)
      t.string :county #carer, child, client(state/county), contact, enquiry, local_authority, nursery, staff
      t.string :country #carer, child, client, contact, enquiry, local_authority, nursery, staff
      t.string :post_code #bank_account, carer, child, client(postal/zip), contact, enquiry, local_authority, nursery, staff
      t.string :telephone_1 #carer(home), child(home), client(phone), contact, enquiry(contact phone), local_authority(phone_1), nursery, staff
      t.string :telephone_2 #carer(mobile), child(mobile), client(PA), contact, enquiry(home phone), local_authority(phone_2), nursery, staff(Mobile)
      t.string :telephone_3 #carer(other), child(work), client(Fax), contact(Fax), enquiry(mobile), local_authority(fax)
      t.string :telephone_4 #client(DDI)
      t.string :skype #client
      t.string :email_1 #carer, child, client, contact, local_authority, nursery, staff(Email Company)
      t.string :email_2 #child, contact, local_authority, nursery, staff(Email Personal)
      t.string :website #contact, nursery

      t.timestamps
    end
  end
end
