class CreateNurseries < ActiveRecord::Migration
  def change
    create_table :nurseries do |t|
      t.integer :nursery_group_id    
      t.string :name
      t.string :address_line_1
      t.string :address_line_2
      t.string :address_line_3
      t.string :county
      t.string :country
      t.string :post_code
      t.string :telephone_1
      t.string :telephone_2
      t.string :email_1
      t.string :email_2
      t.string :website

      t.timestamps
    end
  end
end
