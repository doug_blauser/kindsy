class CreateAccounts < ActiveRecord::Migration
  def change
    create_table :accounts do |t|
      t.integer :user_id
      t.string :invoice_type
      t.string :sage_account_reference

      t.timestamps
    end
  end
end
