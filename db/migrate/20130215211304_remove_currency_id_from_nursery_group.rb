class RemoveCurrencyIdFromNurseryGroup < ActiveRecord::Migration
  def up
    remove_column :nursery_groups, :currency_id
  end

  def down
    add_column :nursery_groups, :currency_id, :integer
  end
end
