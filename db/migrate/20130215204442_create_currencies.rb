class CreateCurrencies < ActiveRecord::Migration
  def change
    create_table :currencies do |t|
      t.integer :currency_type_id
      t.integer :nursery_id

      t.timestamps
    end
  end
end
