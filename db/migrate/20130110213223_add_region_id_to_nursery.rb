class AddRegionIdToNursery < ActiveRecord::Migration
  def change
    add_column :nurseries, :region_id, :integer
  end
end
