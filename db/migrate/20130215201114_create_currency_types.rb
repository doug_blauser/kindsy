class CreateCurrencyTypes < ActiveRecord::Migration
  def change
    create_table :currency_types do |t|
      t.string :currency_name

      t.timestamps
    end
  end
end
