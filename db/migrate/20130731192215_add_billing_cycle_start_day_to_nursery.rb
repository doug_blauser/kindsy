class AddBillingCycleStartDayToNursery < ActiveRecord::Migration
  def change
    add_column :nurseries, :billing_cycle_start_day, :integer
  end
end
