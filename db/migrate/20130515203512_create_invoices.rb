class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.integer :nursery_id
      t.integer :carer_id
      t.integer :account_id
      t.integer :issue      
      t.string :state
      t.decimal :amount_due, :precision => 7, :scale => 2

      t.timestamps
    end
  end
end
