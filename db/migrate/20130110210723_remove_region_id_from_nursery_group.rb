class RemoveRegionIdFromNurseryGroup < ActiveRecord::Migration
  def up
    remove_column :nursery_groups, :region_id
  end

  def down
    add_column :nursery_groups, :region_id, :integer
  end
end
