class CreateChildRelationshipTypes < ActiveRecord::Migration
  def change
    create_table :child_relationship_types do |t|
      t.string :relationship_type

      t.timestamps
    end
  end
end
