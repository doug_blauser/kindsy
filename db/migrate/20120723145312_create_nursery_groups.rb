class CreateNurseryGroups < ActiveRecord::Migration
  def change
    create_table :nursery_groups do |t|
      t.string :name
      t.integer :currency_id
      t.date :billing_period_start_date

      t.timestamps
    end
  end
end
