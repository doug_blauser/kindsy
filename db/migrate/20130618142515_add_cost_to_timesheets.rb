class AddCostToTimesheets < ActiveRecord::Migration
  def change
    add_column :timesheets, :hourly_cost, :decimal, :precision => 5, :scale => 2
    add_column :timesheets, :period_cost, :decimal, :precision => 5, :scale => 2
    add_column :timesheets, :full_period, :boolean
  end
end
