class RemoveContactInfoIdFromChildren < ActiveRecord::Migration
  def up
    remove_column :children, :contact_info_id
  end

  def down
    add_column :children, :contact_info_id, :integer
  end
end
