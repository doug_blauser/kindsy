class CreateEmployments < ActiveRecord::Migration
  def change
    create_table :employments do |t|
      t.integer :nursery_id
      t.integer :user_id

      t.timestamps
    end
  end
end
