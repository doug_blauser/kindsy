class AddTermIdToTimesheet < ActiveRecord::Migration
  def change
    add_column :timesheets, :term_id, :integer
  end
end
