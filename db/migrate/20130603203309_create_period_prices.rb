class CreatePeriodPrices < ActiveRecord::Migration
  def change
    create_table :period_prices do |t|
      t.integer :period_id
      t.datetime :start_timestamp
      t.datetime :end_timestamp
      t.decimal :price, :precision => 5, :scale => 2

      t.timestamps
    end
  end
end
