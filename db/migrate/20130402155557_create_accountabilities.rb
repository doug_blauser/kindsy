class CreateAccountabilities < ActiveRecord::Migration
  def change
    create_table :accountabilities do |t|
      t.integer :account_id
      t.integer :carer_id

      t.timestamps
    end
  end
end
