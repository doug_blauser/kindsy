class AddNurseryIdAndAccountNameToAccounts < ActiveRecord::Migration
  def change
    add_column :accounts, :nursery_id, :integer
    add_column :accounts, :account_name, :string
  end
end
