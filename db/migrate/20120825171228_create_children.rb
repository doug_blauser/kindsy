class CreateChildren < ActiveRecord::Migration
  def change
    create_table :children do |t|
      t.integer :nursery_id
      t.integer :account_id
      t.integer :staff_member_id
      t.integer :room_id
      t.integer :carer_id
      t.integer :contact_info_id
      t.string :first_name
      t.string :middle_name
      t.string :surname
      t.string :preferred_name
      t.string :gender_id
      t.date :date_of_birth
      t.date :start_date
      t.date :leave_date

      t.timestamps
    end
  end
end
