class AddIssueToInvoiceLineItems < ActiveRecord::Migration
  def change
    add_column :invoice_line_items, :issue, :integer
  end
end
