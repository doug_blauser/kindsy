class AddQuantityToInvoiceLineItems < ActiveRecord::Migration
  def change
    add_column :invoice_line_items, :quantity, :decimal, :precision => 5, :scale => 2
  end
end
