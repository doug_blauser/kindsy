class AddAenToChild < ActiveRecord::Migration
  def change
    add_column :children, :AEN, :string
    add_column :children, :hair_id, :integer
    add_column :children, :eye_color_id, :integer
    add_column :children, :religion_id, :integer
    add_column :children, :ethnicity_id, :integer
    add_column :children, :nationality_id, :integer
    add_column :children, :language_id, :integer
  end
end
