class CreateOpenHours < ActiveRecord::Migration
  def change
    create_table :open_hours do |t|
      t.integer :nursery_id
      t.string :day_of_week
      t.time :open_time
      t.time :close_time

      t.timestamps
    end
  end
end
