class CreatePresets < ActiveRecord::Migration
  def change
    create_table :presets do |t|
      t.integer :nursery_id
      t.string :preset_name
      t.time :start_time
      t.time :end_time

      t.timestamps
    end
  end
end
