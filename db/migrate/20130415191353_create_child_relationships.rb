class CreateChildRelationships < ActiveRecord::Migration
  def change
    create_table :child_relationships do |t|
      t.integer :child_id
      t.integer :carer_id
      t.integer :child_relationship_type_id

      t.timestamps
    end
  end
end
