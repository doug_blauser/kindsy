class CreateBillingPeriods < ActiveRecord::Migration
  def change
    create_table :billing_periods do |t|
      t.integer :nursery_id
      t.date :start_date
      t.date :end_date
      t.string :status

      t.timestamps
    end
  end
end
