class AddPeriodCostToPeriod < ActiveRecord::Migration
  def change
    add_column :periods, :period_cost, :decimal, :precision => 5, :scale => 2
  end
end
