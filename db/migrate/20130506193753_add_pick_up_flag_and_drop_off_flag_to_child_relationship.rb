class AddPickUpFlagAndDropOffFlagToChildRelationship < ActiveRecord::Migration
  def change
    add_column :child_relationships, :pick_up_flag, :boolean
    add_column :child_relationships, :drop_off_flag, :boolean
  end
end
