class CreateCarers < ActiveRecord::Migration
  def change
    create_table :carers do |t|
      t.integer :contact_info_id
      t.string :title
      t.string :first_name
      t.string :surname

      t.timestamps
    end
  end
end
