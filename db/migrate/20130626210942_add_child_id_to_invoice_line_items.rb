class AddChildIdToInvoiceLineItems < ActiveRecord::Migration
  def change
    add_column :invoice_line_items, :child_id, :integer
  end
end
