class CreateHairs < ActiveRecord::Migration
  def change
    create_table :hairs do |t|
      t.string :color

      t.timestamps
    end
  end
end
