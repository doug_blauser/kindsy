Feature: Create Invoice
	In order to generate revenue
	As a nursery administrator
	I want to generate an invoice
	
	Scenario: Generate an invoice
		Given I exist as a user
		When I sign in with valid credentials
		Then I see a successful sign in message
		When I go to related_carers_account(account)
		And I click the 'Create Invoice' link 
		Then I should see a notice message "Invoice was added for this bill payer" 		