def create_carer
  @nursery = @account.nursery
  @carer = FactoryGirl.create(:carer, nursery_id: @nursery.id) 
  @accountability = FactoryGirl.create(:accountability, account_id: @account.id, carer_id: @carer.id)   
end

def create_billing_period
  @employment = FactoryGirl.create(:employment, nursery_id: @nursery.id, user_id: @user.id)
  @billing_period = FactoryGirl.create(:billing_period, nursery_id: @nursery.id, start_date: Date.today.beginning_of_month, end_date: Date.today.end_of_month)
end

When(/^I go to related_carers_account\(account\)$/) do
  create_carer
  visit related_carers_account_path(@account.id) 
end

And(/^I click the 'Create Invoice' link$/) do
  create_billing_period
  click_link('Create Invoice')
end

Then(/^I should see a notice message "(.*?)"$/) do |arg1|
  page.should have_content "Invoice was added for this bill payer"
end


