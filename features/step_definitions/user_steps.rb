def create_user
  @account = FactoryGirl.create(:account) 
  @user = @account.user  
end

def sign_in
  @role = FactoryGirl.create(:role)
  @user_role = @user.roles << @role
  @permission = FactoryGirl.create(:permission, role_id: @role.id)
  visit new_user_session_path
  fill_in "user[username]",        :with => @user.username
  fill_in "user[password]",        :with => @user.password
  click_button "Sign in"  
end


Given(/^I am logged in$/) do
  create_user
  sign_in
end

Given(/^I exist as a user$/) do
  create_user
end

Given(/^I am not logged in$/) do
  visit destroy_user_session_path
end

When(/^I sign in with valid credentials$/) do
  sign_in
end

Then(/^I see a successful sign in message$/) do
  page.should have_content "Signed in successfully."
end