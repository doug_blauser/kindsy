Kindsy::Application.routes.draw do

  ActiveAdmin.routes(self)

  devise_for :admin_users, ActiveAdmin::Devise.config
  
  resources :dashboards, :nursery_groups, :nurseries, :currencies, :rooms, :open_hours, :periods, :period_prices, :billing_periods, :presets, :discounts, :local_authorities, :terms

  resources :carers do
    member do
      get 'detail'
      get 'relationships'
      put 'remove_relationship'
      get 'search_children' 
      put 'add_children'
      put 'assign_accounts'
      get 'search_accounts'          
    end
  end 
  
  resources :accounts do
    member do
      get 'related_children'
      put 'remove_child'
      get 'search_children'
      put 'add_children'
      get 'related_carers'
      get 'search_carers'
      put 'add_carers'
      put 'remove_carer'
    end
  end
  
  resources :children do
    member do
      get 'detail'
    end
    resources :bookings
    resources :timesheets
  end
  
  resources :invoices do
    resources :invoice_line_items
  end
  
  get "static_pages/home"
  

  devise_for :users, :controllers => {:registrations => "registrations"}


  devise_scope :user do
    root :to => 'devise/sessions#new'
  end
  
  match '/static_pages_home' => "static_pages#home", as: :user_root


 
end
