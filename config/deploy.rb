set :rvm_type, :system

require "bundler/capistrano"
require "rvm/capistrano"

server "beta.kindsy.co.uk", :web, :app, :db, primary: true

set :application, "kindsy"
set :domain,      "kindsy.co.uk"
set :scm,         :git
set :repository,  "ssh://git@redmine.kindsy.co.uk:7999/ksy/kindsy_v2.git"
set :branch,      "master"
set :deploy_via, :remote_cache
set :use_sudo,    false
set :deploy_to,   "/home/deployer/apps/#{application}"  
set :user,        "deployer" 

default_run_options[:pty] = true
ssh_options[:forward_agent] = true

namespace :deploy do
  %w[start stop restart].each do |command|
    desc "#{command} unicorn server"
    task command, roles: :app, except: {no_release: true} do
      run "/etc/init.d/unicorn_#{application} #{command}"
    end
  end

  after "deploy", "deploy:restart"

 end