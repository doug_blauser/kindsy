require 'spec_helper'

describe "DiscountPages" do
  let(:group) { FactoryGirl.create(:nursery_group) }
  let(:user) { FactoryGirl.create(:user) }
  let(:role) { FactoryGirl.create(:role) } 
  let(:user_roles) { user.roles << role }
  let(:permission) { FactoryGirl.create(:permission, role_id: role.id) }
  let(:region) { FactoryGirl.create(:region) }
  let(:contact_info) { FactoryGirl.create(:contact_info) }
  let!(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: group.id, region_id: region.id) } 
  let!(:employment) { FactoryGirl.create(:employment, nursery_id: nursery.id, user_id: user.id) }
  let!(:room) { FactoryGirl.create(:room, nursery_id: nursery.id) }
  let!(:period) { FactoryGirl.create(:period, nursery_id: nursery.id) }
  let!(:period2) { FactoryGirl.create(:period, nursery_id: nursery.id, period_name: "PM") }
  let!(:preset) { FactoryGirl.create(:preset, nursery_id: nursery.id) }
  let!(:preset2) { FactoryGirl.create(:preset, nursery_id: nursery.id, preset_name: "Afternoon Sessions") }
  let!(:period_presets1) { FactoryGirl.create(:period_automation, period_id: period.id, preset_id: preset.id) }
  let!(:period_presets2) { FactoryGirl.create(:period_automation, period_id: period2.id, preset_id: preset.id) }
  let!(:period_presets3) { FactoryGirl.create(:period_automation, period_id: period.id, preset_id: preset2.id) }
  let!(:carer) { FactoryGirl.create(:carer, nursery_id: nursery.id) }
  let!(:account) { FactoryGirl.create(:account, nursery_id: nursery.id, user_id: user.id) } 
  let!(:accountability) { FactoryGirl.create(:accountability, account_id: account.id, carer_id: carer.id) }       
  let!(:discount) { FactoryGirl.create(:discount, nursery_id: nursery.id, discountable_id: period.id, discountable_type: "Period") }
  
  let(:submit) { "Save" }
  
  subject { page }
  
  describe "Discounts Index Page" do
    describe "for non-signed-in users" do
      before { get discounts_path }
      specify { response.should redirect_to(root_path) }     
    end
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit discounts_path
      end 
      
      it { should have_selector('h1', text: 'Discounts') }   
      it { should have_selector('title', text: full_title('Discounts')) }  
      it { should have_link('+ Attendance Discount', href: new_discount_path(partial: "attendance")) }
      it { should have_link('+ Relationship Discount', href: new_discount_path(partial: "relationship")) }      
      it { should have_selector('th', text: 'Name') }      
      it { should have_selector('th', text: 'Type') } 
      it { should have_selector('th', text: 'Applies To') }
      it { should have_selector('th', text: 'Percentage or Fixed') }
      it { should have_selector('th', text: 'Value') }
      it { should have_selector('th', text: '...') }        
      it { should have_selector('td', text: discount.name) }   
      it { should have_selector('td', text: "#{discount.discountable_type == "Period"? "Attendance":"Relationship"}") }   
      it { should have_selector('td', text: "#{discount.discountable.period_name} #{'Period'}") }
      it { should have_selector('td', text: discount.discount_type) }
      it { should have_link('View', href: discount_path(discount)) }   
      it { should have_link('Edit', href: edit_discount_path(discount, partial: discount.discountable_type == "Period"? "attendance":"relationship")) }      
      it { should have_link('Delete', href: discount_path(discount)) }               
      it { should have_selector('td', text: discount.amount.to_s) }         
    end
  end

  describe "Discounts New Page" do
    describe "for non-signed-in users" do
      before { get new_discount_path }
      specify { response.should redirect_to(root_path) }     
    end
    
    describe "for signed-in users" do      
      context "with valid information for attendance period discounts" do
        before do
          sign_in_user(user, user_roles, permission)
          visit new_discount_path(partial: "attendance")       
        end
      
        it { should have_selector('h1', text: 'Add Attendance Discount') }
        it { should have_selector('title', text: full_title('Add Attendance Discount')) }        

        context "should create new discount" do
          before do
            fill_in 'discount[name]',                       with: "Full Day"
            fill_in 'discount[amount]',                     with: 3.25
            select "Percent",                               from: 'discount[discount_type]'
            select "AM",                                    from: 'discount[discountable_id]'             
            select "June",                                  from: 'discount[start_date(2i)]'
            select "15",                                    from: 'discount[start_date(3i)]'
            select "2013",                                  from: 'discount[start_date(1i)]'   
            select "June",                                  from: 'discount[end_date(2i)]'
            select "15",                                    from: 'discount[end_date(3i)]'
            select "2016",                                  from: 'discount[end_date(1i)]'
            select "Session",                               from: 'discount[applies_to]'              
          end
          
          it "should create a new discount record for the nursery" do
            expect { click_button submit }.to change(Discount, :count).by(1)
          end                              
        end
      end
    
      context "with valid information for relationship discounts" do
        before do
          sign_in_user(user, user_roles, permission)
          visit new_discount_path(partial: "relationship")       
        end
      
        it { should have_selector('h1', text: 'Add Relationship Discount') }
        it { should have_selector('title', text: full_title('Add Relationship Discount')) }        
      
        context "should create new discount" do
          before do
            fill_in 'discount[name]',                       with: "Full Day"
            fill_in 'discount[amount]',                     with: 15.00
            select "Fixed",                                 from: 'discount[discount_type]'
            select account.account_name,                    from: 'discount[discountable_id]' 
            select "June",                                  from: 'discount[start_date(2i)]'
            select "15",                                    from: 'discount[start_date(3i)]'
            select "2013",                                  from: 'discount[start_date(1i)]'   
            select "June",                                  from: 'discount[end_date(2i)]'
            select "15",                                    from: 'discount[end_date(3i)]'
            select "2016",                                  from: 'discount[end_date(1i)]'
            select "Session",                               from: 'discount[applies_to]'                        
          end
          
          it "should create a new discount record for the nursery" do
            expect { click_button submit }.to change(Discount, :count).by(1)
          end                              
        end      
      end
    
      context "with invalid information for attendance period discounts" do
        before do
          sign_in_user(user, user_roles, permission)
          visit new_discount_path(partial: "attendance")       
        end
      
        it { should have_selector('h1', text: 'Add Attendance Discount') }
        it { should have_selector('title', text: full_title('Add Attendance Discount')) }   
        
        context "should not create a new discount" do
          let(:missing_name) { "" }
          let(:missing_amount) { 500.00 }
          let(:missing_type) { "" }
          let(:missing_discountable_id) { "" }
          before do
            fill_in 'discount[name]',                       with: missing_name
            fill_in 'discount[amount]',                     with: missing_amount
            select missing_type,                            from: 'discount[discount_type]'
            select missing_discountable_id,                 from: 'discount[discountable_id]'             
          end 
          
          it "should not create a new discount record for the nursery" do
            expect { click_button submit }.not_to change(Discount, :count)
          end        
          
          it "should render new discount page" do
            click_button submit
            current_path.should == discounts_path
          end                      
        end              
      end
      
      context "with invalid information for relationship discounts" do
        before do
          sign_in_user(user, user_roles, permission)
          visit new_discount_path(partial: "relationship")       
        end
      
        it { should have_selector('h1', text: 'Add Relationship Discount') }
        it { should have_selector('title', text: full_title('Add Relationship Discount')) }   
        
        context "should not create a new discount" do
          let(:missing_name) { "" }
          let(:missing_amount) { "" }
          let(:missing_type) { "" }
          let(:missing_discountable_id) { "" }
          before do
            fill_in 'discount[name]',                       with: missing_name
            fill_in 'discount[amount]',                     with: missing_amount
            select missing_type,                            from: 'discount[discount_type]'
            select missing_discountable_id,                 from: 'discount[discountable_id]'             
          end 
          
          it "should not create a new discount record for the nursery" do
            expect { click_button submit }.not_to change(Discount, :count)
          end        
          
          it "should render new discount page" do
            click_button submit
            current_path.should == discounts_path
          end                      
        end              
      end      
    end              
  end

  describe "Discounts Edit Page" do
    describe "for non-signed-in users" do
      before { get edit_discount_path(discount) }
      specify { response.should redirect_to(root_path) }     
    end        
    
    describe "for signed-in users" do
      context "with valid information for attendance period discounts" do
        before do
          sign_in_user(user, user_roles, permission)
          visit edit_discount_path(discount, partial: "attendance")       
        end   
        
        it { should have_selector('h1', text: 'Edit Attendance Discount') }
        it { should have_selector('title', text: full_title('Edit Attendance Discount')) }             

        context "should change discount values" do
          let(:revised_name) { "Revised Discount Name" }
          let(:revised_amount) { 5.00 }
          let(:revised_type) { "Percent" }
          let(:revised_discountable_id) { period2.period_name }         
          before do
            fill_in 'discount[name]',                       with: revised_name
            fill_in 'discount[amount]',                     with: revised_amount
            select revised_type,                            from: 'discount[discount_type]'
            select revised_discountable_id,                 from: 'discount[discountable_id]'   
            click_button submit          
          end   
          
          specify { discount.reload.name == revised_name }
          specify { discount.reload.amount == revised_amount }
          specify { discount.reload.discount_type == revised_type }
          specify { discount.reload.discountable_id == period2.id }                  

          it "should redirect to discount index page" do
            current_path.should == discounts_path
          end                
        end
      end      

      context "with invalid information for attendance period discounts" do 
        before do
          sign_in_user(user, user_roles, permission)
          visit edit_discount_path(discount, partial: "attendance")       
        end   
        
        it { should have_selector('h1', text: 'Edit Attendance Discount') }
        it { should have_selector('title', text: full_title('Edit Attendance Discount')) }
        
        context "should redirect to edit page" do                          
          let(:missing_name) { "" }
          let(:missing_amount) { "" }
          let(:missing_type) { "" }
          let(:missing_discountable_id) { "" }
          before do
            fill_in 'discount[name]',                       with: missing_name
            fill_in 'discount[amount]',                     with: missing_amount
            select missing_type,                            from: 'discount[discount_type]'
            select missing_discountable_id,                 from: 'discount[discountable_id]'             
          end 
          
          context "it should not change the values of the discount record" do
            specify { discount.reload.name != missing_name }
            specify { discount.reload.amount != missing_amount }
            specify { discount.reload.discount_type != missing_type }
            specify { discount.reload.discountable_id != preset2.id }
          end            
          
          it "should redirect to edit discount page" do
            click_button submit          
            current_path.should == discount_path(discount)
          end     
          
          it { should have_selector('div', :id => "error_explanation") }
          it { should have_selector('div', :class => "control-group string required error") } 
                                          
        end     
      end 
      
      context "with invalid information for relationship discounts" do 
        before do
          sign_in_user(user, user_roles, permission)
          visit edit_discount_path(discount, partial: "relationship")       
        end   
        
        it { should have_selector('h1', text: 'Edit Relationship Discount') }
        it { should have_selector('title', text: full_title('Edit Relationship Discount')) }
        
        context "should redirect to edit page" do                          
          let(:missing_name) { "" }
          let(:missing_amount) { "" }
          let(:missing_type) { "" }
          let(:missing_discountable_id) { "" }
          before do
            fill_in 'discount[name]',                       with: missing_name
            fill_in 'discount[amount]',                     with: missing_amount
            select missing_type,                            from: 'discount[discount_type]'
            select missing_discountable_id,                 from: 'discount[discountable_id]'             
          end 

          context "it should not change the values of the discount record" do
            specify { discount.reload.name != missing_name }
            specify { discount.reload.amount != missing_amount }
            specify { discount.reload.discount_type != missing_type }
            specify { discount.reload.discountable_id != preset2.id }
          end
          
          it "should redirect to edit discount page" do
            click_button submit          
            current_path.should == discount_path(discount)
          end     
          
          it { should have_selector('div', :id => "error_explanation") }
          it { should have_selector('div', :class => "control-group string required error") } 
                                          
        end     
      end        
    end
  end  

  describe "Discounts Show Page" do
    describe "for non-signed-in users" do
      before { get discount_path(discount) }
      specify { response.should redirect_to(root_path) }     
    end  
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit discount_path(discount)        
      end
      
        it { should have_selector('h1', text: 'Discount') }
        it { should have_selector('title', text: full_title('Discount')) }   
        it { should have_link('Edit', href: edit_discount_path(discount, partial: "attendance")) }
        it { should have_content(discount.name) }
        it { should have_content(discount.amount) }
        it { should have_content(discount.discount_type) } 
        it { should have_content(discount.discountable_type == "Period"? "Attendance":"Relationship") }        
        it { should have_content(discount.discountable.period_name) }        
        it { should have_content(discount.start_date) }
        it { should have_content(discount.end_date) }
        it { should have_content(discount.applies_to) }
    end  
  end

  describe "Discount delete Page" do
    describe "for non-signed-in users" do
      before { delete discount_path(discount) }
      specify { response.should redirect_to(root_path) } 
    end  
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit discounts_path
      end       
      
      it "should display discounts index page" do
        current_path.should == discounts_path
      end            
      
      it { should have_link('Delete', href: discount_path(discount)) }
    
      it "should delete the discount record" do
        expect { click_link('Delete'.to change(Discount, :count)).by(-1) } 
        current_path.should == discounts_path
      end    
    end       
  end
end
