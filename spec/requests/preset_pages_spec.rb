require 'spec_helper'

describe "PresetPages" do
  let(:group) { FactoryGirl.create(:nursery_group) }
  let(:user) { FactoryGirl.create(:user) }
  let(:role) { FactoryGirl.create(:role) } 
  let(:user_roles) { user.roles << role }
  let(:permission) { FactoryGirl.create(:permission, role_id: role.id) }
  let(:region) { FactoryGirl.create(:region) }
  let(:contact_info) { FactoryGirl.create(:contact_info) }
  let!(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: group.id, region_id: region.id) } 
  let!(:employment) { FactoryGirl.create(:employment, nursery_id: nursery.id, user_id: user.id) } 
  let!(:period) { FactoryGirl.create(:period, nursery_id: nursery.id) }
  let!(:period2) { FactoryGirl.create(:period, nursery_id: nursery.id, period_name: "PM") }
  let!(:preset) { FactoryGirl.create(:preset, nursery_id: nursery.id) }
  let!(:period_presets1) { FactoryGirl.create(:period_automation, period_id: period.id, preset_id: preset.id) }
  let!(:period_presets2) { FactoryGirl.create(:period_automation, period_id: period2.id, preset_id: preset.id) }
  
  let(:submit) { "Save" }    
  
  subject { page }
  
  describe "Preset New Page" do
    describe "for non-signed-in users" do
      before { get new_preset_path }
      specify { response.should redirect_to(root_path) }
    end
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit new_preset_path        
      end
      
      it { should have_selector('h1', text: 'Add Preset') }
      it { should have_selector('title', text: full_title('Add Preset')) } 
      
      context "with valid information" do
        before do
          fill_in 'preset[preset_name]',    with: "Full Day"
          select "AM",                      from: 'preset[period_ids][]'          
        end
        
        it "should create a new preset record for the nursery" do
          expect { click_button submit }.to change(Preset, :count).by(1)
        end
        
        it "should create an new period_automations record" do
          expect { click_button submit }.to change(PeriodAutomation, :count).by(1)
        end
      end 
      
      context "with multiple periods" do
        before do
          fill_in 'preset[preset_name]',    with: "Full Day"
          select "AM",                      from: 'preset[period_ids][]'
          select "PM",                      from: 'preset[period_ids][]'          
        end 
        
        it "should create a new preset record for the nursery" do
          expect { click_button submit }.to change(Preset, :count).by(1)
        end
        
        it "should create an new period_automations record" do
          expect { click_button submit }.to change(PeriodAutomation, :count).by(2)
        end        
      end 
      
      context "with invalid information" do
        before do
          fill_in 'preset[preset_name]',    with: ""         
        end   
        
        it "should not create a new preset record for the nursery" do
          expect { click_button submit }.not_to change(Preset, :count)
        end 
        
        it "should not create a new period_automations record" do
          expect { click_button submit }.not_to change(PeriodAutomation, :count)
        end            
      end        
    end
  end

  describe "Preset Index Page" do
    describe "for non-signed-in users" do
      before { get new_preset_path }
      specify { response.should redirect_to(root_path) }
    end   
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit presets_path        
      end      
      
      it { should have_selector('h1', text: 'Presets') }
      it { should have_selector('title', text: full_title('Presets')) }    
      it { should have_link('+ Preset', href: new_preset_path) } 
      it { should have_selector('th', text: 'Preset Name') }
      it { should have_selector('th', text: '...') }
      it { should have_selector('td', text: preset.preset_name) }
      it { should have_link('View', href: preset_path(preset)) }
      it { should have_link('Edit', href: edit_preset_path(preset)) }
      it { should have_link('Delete', href: preset_path(preset)) }              
    end 
  end

  describe "Preset Edit Page" do
    describe "for non-signed-in users" do
      before { get edit_preset_path(preset) }
      specify { response.should redirect_to(root_path) }
    end    
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit edit_preset_path(preset)        
      end        
      
      it { should have_selector('h1', text: 'Edit Preset') }
      it { should have_selector('title', text: full_title('Edit Preset')) }
      
      context "with valid information" do
        let(:revised_preset_name) { "Revised Preset Name" }
        let(:revised_preset_period) { "PM" }
        before do
          fill_in 'preset[preset_name]',    with: revised_preset_name
          select revised_preset_period,     from: 'preset[period_ids][]'   
          click_button submit                
        end
        
        specify { preset.reload.preset_name == revised_preset_name }
        specify { preset.reload.periods.each.map { |a| a.period_name } == ["PM"] }
      end
    end
  end

  describe "Preset Show Page" do
    describe "for non-signed-in users" do
      before { get preset_path(preset) }
      specify { response.should redirect_to(root_path) }
    end 
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit preset_path(preset)        
      end  
      
      it { should have_selector('h1', text: 'Preset') }
      it { should have_selector('title', text: full_title('Preset')) } 
      it { should have_link('Edit', href: edit_preset_path(preset)) }
      it { should have_content(preset.preset_name) }     
      it { should have_content(preset.periods.pluck(:period_name).join(", ")) }           
    end       
  end

  describe "Preset Destroy Page" do
    describe "for non-signed-in users" do
      before { delete preset_path(preset) }
      specify { response.should redirect_to(root_path) } 
    end
    
    describe "for signed-in users" do
      before do 
        sign_in_user(user, user_roles, permission)
        visit presets_path
      end
      
      it "should display nursery presets index page" do
        current_path.should == presets_path
      end
      
      it { should have_link('Delete', href: preset_path(preset)) }
      
      it "should delete the preset record" do
        expect { click_link('Delete'.to change(Preset, :count)).by(-1) }
        current_path.should == presets_path
      end
      
      it "should delete the period_automation record" do
        expect { click_link('Delete'.to change(PeriodAutomation, :count)).by(-1) }
      end
    end     
  end
end
