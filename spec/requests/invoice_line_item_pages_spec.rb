require 'spec_helper'

describe "InvoiceLineItemPages" do
  let(:group) { FactoryGirl.create(:nursery_group) }
  let(:user) { FactoryGirl.create(:user) }
  let(:role) { FactoryGirl.create(:role) } 
  let(:user_roles) { user.roles << role }
  let(:permission) { FactoryGirl.create(:permission, role_id: role.id) }  
  let(:region) { FactoryGirl.create(:region) }  
  let!(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: group.id, region_id: region.id) }
  let!(:employment) { FactoryGirl.create(:employment, nursery_id: nursery.id, user_id: user.id) }  
  let!(:account) { FactoryGirl.create(:account, nursery_id: nursery.id, user_id: user.id) }  
  let!(:carer) { FactoryGirl.create(:carer, nursery_id: nursery.id) } 
  let!(:accountability) { FactoryGirl.create(:accountability, account_id: account.id, carer_id: carer.id) }   
  let!(:status) { FactoryGirl.create(:status_type) }    
  let!(:child) { FactoryGirl.create(:child, nursery_id: nursery.id, status_type_id: status.id, account_id: account.id) } 
  let!(:child2) { FactoryGirl.create(:child, nursery_id: nursery.id, status_type_id: status.id, account_id: account.id, first_name: "Duce") }
  let!(:child_relationship_type) { FactoryGirl.create(:child_relationship_type) }
  let!(:child_relationship) { FactoryGirl.create(:child_relationship, child_id: child.id, carer_id: carer.id, child_relationship_type_id: child_relationship_type.id, pick_up_flag: true, drop_off_flag: true) }       
  let!(:child_relationship2) { FactoryGirl.create(:child_relationship, child_id: child2.id, carer_id: carer.id, child_relationship_type_id: child_relationship_type.id, pick_up_flag: true, drop_off_flag: true) }
  let!(:period) { FactoryGirl.create(:period, nursery_id: nursery.id) }
  let!(:period2) { FactoryGirl.create(:period, nursery_id: nursery.id, period_name: "PM") } 
  let!(:booking) { FactoryGirl.create(:booking, nursery_id: nursery.id, child_id: child.id, period_id: period.id, start_date: Date.today.beginning_of_month-1.month, end_date: Date.today.end_of_month+3.months, days_of_week: ["Monday", "Wednesday", "Tuesday"]) }
  let!(:child1_am_hourly_price1) { FactoryGirl.create(:timesheet, nursery_id: nursery.id,
                                                                child_id: child.id,
                                                                period_id: period.id,
                                                                date_attended: Date.today,
                                                                hourly_cost: period.period_hourly_cost,
                                                                period_cost: period.period_cost,
                                                                full_period: false) }                                                    
  let!(:child1_pm_hourly_price1) { FactoryGirl.create(:timesheet, nursery_id: nursery.id,
                                                                 child_id: child.id,
                                                                 period_id: period2.id,
                                                                 date_attended: Date.today,
                                                                 hourly_cost: period.period_hourly_cost,
                                                                 period_cost: period.period_cost, 
                                                                 full_period: false) }                                                     
  let!(:child2_am_hourly_price1) { FactoryGirl.create(:timesheet, nursery_id: nursery.id,
                                                                 child_id: child2.id,
                                                                 period_id: period.id,
                                                                 date_attended: Date.today,
                                                                 hourly_cost: period.period_hourly_cost,
                                                                 period_cost: period.period_cost,
                                                                 full_period: false) }
  let!(:child2_am_full_price1) { FactoryGirl.create(:timesheet, nursery_id: nursery.id,
                                                               child_id: child2.id,
                                                               period_id: period.id,
                                                               duration: 23400,
                                                               date_attended: Date.today,
                                                               hourly_cost: period.period_hourly_cost,
                                                               period_cost: period.period_cost, 
                                                               full_period: true) }  
  let!(:child1_am_hourly_price2) { FactoryGirl.create(:timesheet, nursery_id: nursery.id,
                                                                child_id: child.id,
                                                                period_id: period.id,
                                                                date_attended: Date.today,
                                                                hourly_cost: 7.75,
                                                                period_cost: 35.00,
                                                                full_period: false) } 
  let!(:child1_am_full_price2) { FactoryGirl.create(:timesheet, nursery_id: nursery.id,
                                                                       child_id: child.id,
                                                                       period_id: period.id,
                                                                       duration: 23400,
                                                                       date_attended: Date.today,
                                                                       hourly_cost: 7.75,
                                                                       period_cost: 35.00,
                                                                       full_period: true) }                                                                               
                                                                                                                                                                            
  let!(:billing_period) { FactoryGirl.create(:billing_period, start_date: Date.today.beginning_of_month, end_date: Date.today.end_of_month) }                                                    
  let!(:invoice) { FactoryGirl.create(:invoice, nursery_id: nursery.id, carer_id: carer.id, account_id: account.id, billing_period_id: billing_period.id) }   
  let!(:line_item) { FactoryGirl.create(:invoice_line_item, invoice_id: invoice.id, account_id: account.id, child_id: child.id, total_cost: 7777.77) }
  let!(:balance) { FactoryGirl.create(:invoice_line_item, invoice_id: invoice.id, account_id: account.id, child_id: nil, description: "Balance Brought Forward", quantity: 0, cost: 0.00, total_cost: 173.50) }
      
  let(:submit) { "Save" }                                               
                                                
  subject { page }
  
  describe "Invoice Line Items New Page" do
    describe "for non-signed-in users" do
      before { get new_invoice_invoice_line_item_path(invoice) }
      specify { response.should redirect_to(root_path) }
    end
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit new_invoice_invoice_line_item_path(invoice)
      end        
      
      it { should have_selector('h1', text: "Add Invoice Line Item") }
      it { should have_selector('title', text: full_title('Add Invoice Line Item')) }
      
      
      describe "with valid information" do
        let(:original_amount_due) { 7951.27 }        
        before do
          select 'First',                                     from: 'invoice_line_item[child_id]'
          fill_in 'invoice_line_item[description]',           with: "Additional Charge"
          fill_in 'invoice_line_item[quantity]',              with: 1
          fill_in 'invoice_line_item[cost]',                  with: 40.00
        end
        
        it "should create a new invoice line item record for the invoice" do
          expect { click_button submit}.to change(InvoiceLineItem, :count).by(1)
        end
        
        it "should redirect to booking index page" do
          click_button submit
          current_path.should == invoice_path(invoice)
        end      
        
        it "should change invoice amount due to include new line_item total_amount" do
          click_button submit
          invoice.reload.amount_due.should eql original_amount_due + 40.00
        end
        
        it "should add true to invoice_line_item.editable attribute" do
          click_button submit
          invoice.invoice_line_items.last.reload.editable.should eql true          
        end        
      end

      describe "with missing information" do
        let(:missing_description) { "" }
        let(:missing_quantity) { "" }
        let(:missing_cost) { "" }
        
        before do
          fill_in 'invoice_line_item[description]',           with: missing_description
          fill_in 'invoice_line_item[quantity]',              with: missing_quantity
          fill_in 'invoice_line_item[cost]',                  with: missing_cost
        end        
        
        it "should not create a new invoice line item record" do
          expect { click_button submit}.not_to change(InvoiceLineItem, :count) 
        end       
        
        it "should redirect to invoice line item new page" do
          click_button submit
          current_path.should == invoice_invoice_line_items_path(invoice)
        end 
        
        it { should have_selector('div', :id => "error_explanation") }
        it { should have_selector('div', :class => "control-group string required error") }                  
      end
    end
  end    

  describe "Invoice Line Items Edit Page" do
  let!(:editable_line_item) { FactoryGirl.create(:invoice_line_item, invoice_id: invoice.id, account_id: account.id, issue: invoice.issue, child_id: child.id, total_cost: 7777.77, editable: true) }
    
    describe "for non-signed-in users" do
      before { get edit_invoice_invoice_line_item_path(invoice, editable_line_item) }
      specify { response.should redirect_to(root_path) }
    end       
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit edit_invoice_invoice_line_item_path(invoice, editable_line_item)
      end      
      
      it { should have_selector('title', text: full_title('Edit Invoice Line Item')) }
      it { should have_selector('h1', text: 'Edit Invoice Line Item') }
      
      
      describe "with valid information" do
        let(:revised_description) { "Revised Additional Charge" }
        let(:revised_quantity) { 2 }
        let(:revised_cost) { 30.00 }
       
        before do
          fill_in 'invoice_line_item[description]',           with: revised_description
          fill_in 'invoice_line_item[quantity]',              with: revised_quantity
          fill_in 'invoice_line_item[cost]',                  with: revised_cost
          click_button submit
        end
        
        specify { editable_line_item.reload.description.should == revised_description }
        specify { editable_line_item.reload.quantity.should == revised_quantity }
        specify { editable_line_item.reload.cost.should == revised_cost }
        
        it "should redirect to invoice line item new page" do
          current_path.should == invoice_path(invoice)
        end        
      end
     
      describe "with invalid information" do
        let(:revised_description_missing) { "" }
        let(:revised_quantity_missing) { "" }
        let(:revised_cost_missing) { "" }
       
        before do
          fill_in 'invoice_line_item[description]',           with: revised_description_missing
          fill_in 'invoice_line_item[quantity]',              with: revised_quantity_missing
          fill_in 'invoice_line_item[cost]',                  with: revised_cost_missing
          click_button submit
        end      
        
        specify { editable_line_item.reload.description.should == editable_line_item.description }
        specify { editable_line_item.reload.quantity.should == editable_line_item.quantity }
        specify { editable_line_item.reload.cost.should == editable_line_item.cost }
        
        it { should have_selector('div', :id => "error_explanation") }
        it { should have_selector('div', :class => "control-group string required error") }        
      end
    end

    describe "for signed-in users editing non-editable line_item" do
      before do
        sign_in_user(user, user_roles, permission)
        visit edit_invoice_invoice_line_item_path(invoice, line_item)
      end      
      
      it "should redirect to invoice_path" do
        current_path.should == invoice_path(invoice)
      end
    end
  end
end
