require 'spec_helper'

describe "PeriodPages" do
  let(:group) { FactoryGirl.create(:nursery_group) }
  let(:user) { FactoryGirl.create(:user) }
  let(:role) { FactoryGirl.create(:role) } 
  let(:user_roles) { user.roles << role }
  let(:permission) { FactoryGirl.create(:permission, role_id: role.id) }
  let(:region) { FactoryGirl.create(:region) }
  let(:contact_info) { FactoryGirl.create(:contact_info) }
  let!(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: group.id, region_id: region.id) } 
  let!(:employment) { FactoryGirl.create(:employment, nursery_id: nursery.id, user_id: user.id) }   
  let!(:room) { FactoryGirl.create(:room, nursery_id: nursery.id) }
  let!(:room2) { FactoryGirl.create(:room, nursery_id: nursery.id, name: "red room") }
  let!(:open_hour) { FactoryGirl.create(:open_hour, nursery_id: nursery.id) }
  let!(:open_hour2) { FactoryGirl.create(:open_hour, nursery_id: nursery.id, day_of_week: "Tuesday") }  
  let!(:period) { FactoryGirl.create(:period, nursery_id: nursery.id) }
  let!(:period_room) { FactoryGirl.create(:periodation, period_id: period.id, room_id: room.id) }
  let(:submit) { "Save" }
  
  subject { page }
  
  describe "Period Index Page" do
    describe "for non-signed-in users" do
      before { get periods_path }
      specify { response.should redirect_to(root_path) }  
    end    
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit periods_path       
      end
      
      it { should have_selector('h1', text: 'Session Periods') }
      it { should have_selector('title', text: full_title('Nursery Sessions')) }
      it { should have_link('+ Session', href: new_period_path)}
      it { should have_content('Session Name') }
      it { should have_content('Time Range') }
      it { should have_content('Rooms') }
      it { should have_content('Cost: Session') }
      it { should have_content('Cost: Hour') }
      it { should have_content('...') }
      it { should have_content(nursery.periods[0].period_name) }
      it { should have_content("#{nursery.periods[0].period_start_time.strftime("%I:%M %P")} #{'-'} #{nursery.periods[0].period_end_time.strftime("%I:%M %P")}") }
      it { should have_content(nursery.periods[0].rooms[0].name) }
      it { should have_content(nursery.periods[0].period_hourly_cost) }
      it { should have_content(nursery.periods[0].period_cost) }      
      it { should have_link('View', href: period_path(period)) }
      it { should have_link('Edit', href: edit_period_path(period)) }
      it { should have_link('Delete', href: period_path(period)) }    
    end
  end

  describe "Period New Page" do
    describe "for non-signed-in users" do
      before { get new_period_path }
      specify { response.should redirect_to(root_path) } 
    end    
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit new_period_path  
      end

      it { should have_selector('h1', text: 'Session Period') }
      it { should have_selector('title', text: full_title('Add Session Period')) }            

      describe "with valid information" do
        before do
          fill_in 'period[period_name]',          with: "PM"
          select "blue room",                     from: 'period[room_ids][]'
          select "06",                            from: 'period[period_start_time(4i)]'
          select "45",                            from: 'period[period_start_time(5i)]'
          select "18",                            from: 'period[period_end_time(4i)]'
          select "30",                            from: 'period[period_end_time(5i)]'
          fill_in 'period[period_hourly_cost]',   with: 6.50     
          fill_in 'period[period_cost]',          with: 34.50 
          check('period_days_of_week_monday')   
          fill_in 'period[fe_limit]',             with: 2.5  
        end
        
        it "should create a new period record for the nursery" do
          expect { click_button submit}.to change(Period, :count).by(1)
        end 
        
        it "should redirect to nursery periods index page" do
          click_button submit
          current_path.should == periods_path
        end       
      end
      
      describe "with missing information" do
        let(:missing_period_name) { "" }
        let(:missing_period_start_time) { "" }
        let(:missing_period_end_time) { "" }
        let(:missing_period_hourly_cost) { "" }
        let(:missing_period_cost) { "" }         
      
        before do
          fill_in 'period[period_name]',          with: missing_period_name
          fill_in 'period[period_hourly_cost]',   with: missing_period_hourly_cost
          fill_in 'period[period_cost]',          with: missing_period_cost          
        end
        
        it "should not create a new period record for the nursery" do
          expect {click_button submit}.not_to change(Period, :count)
        end  
        
        it "should redirect to nursery period new page" do
          click_button submit
          current_path.should == periods_path
        end
        
        it { should have_selector('div', :id => "error_explanation") }
        it { should have_selector('div', :class => "control-group string required error") }        
      end
    end
  end
  
  describe "Period Edit Page" do
    describe "for non-signed-in users" do
      before { get edit_period_path(period) }
      specify { response.should redirect_to(root_path) } 
    end    
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit edit_period_path(period)      
      end
      
      it { should have_selector('h1', text: 'Edit Session Period') }
      it { should have_selector('title', text: full_title('Edit Session Period')) }

      describe "with valid information" do
        let(:revised_period_name) { "Revised Period Name" }
        let(:revised_period_start_time) { Time.new(2000,01,01,13,00,00) }
        let(:revised_period_end_time) { Time.new(2000,01,01,19,00,00) }
        let(:revised_period_hourly_cost) { 7.25 }
        let(:revised_period_cost) { 37.50 }        
        before do
          fill_in 'period[period_name]',                  with: revised_period_name
          select "red room",                              from: 'period[room_ids][]'
          select revised_period_start_time.hour.to_s,     from: 'period[period_start_time(4i)]'
          select revised_period_start_time.min.to_s,      from: 'period[period_start_time(5i)]'
          select revised_period_end_time.hour.to_s,       from: 'period[period_end_time(4i)]'
          select revised_period_end_time.min.to_s,        from: 'period[period_end_time(5i)]'
          fill_in 'period[period_hourly_cost]',           with: revised_period_hourly_cost
          fill_in 'period[period_cost]',                  with: revised_period_cost          
          click_button submit
        end
        
        specify { period.reload.period_name == revised_period_name }
        specify { period.reload.rooms.each.map { |a| a.name } == ["red_room"] }
        specify { period.reload.period_start_time == revised_period_start_time }
        specify { period.reload.period_end_time == revised_period_end_time }
        specify { period.reload.period_hourly_cost == revised_period_hourly_cost }  
        specify { period.reload.period_cost == revised_period_cost }              

        it "should redirect to nursery period index page" do
          current_path.should == periods_path
        end
      end 
      
      describe "with missing information" do
        let(:missing_period_name) { "" }
        let(:missing_period_hourly_cost) { "" }  
        let(:missing_period_cost) { "" }              
        before do
          fill_in 'period[period_name]',          with: missing_period_name
          fill_in 'period[period_hourly_cost]',   with: missing_period_hourly_cost
          fill_in 'period[period_cost]',          with: missing_period_cost          
        end 
        
        it "should redirect to edit nursery period page" do
          click_button submit
          current_path.should == period_path(period)
        end
        
        it { should have_selector('div', :id => "error_explanation") }
        it { should have_selector('div', :class => "control-group string required error") }                
               
      end     
    end
  end

  describe "Period Show Page" do
    describe "for non-signed-in users" do
      before { get period_path(period) }
      specify { response.should redirect_to(root_path) } 
    end 
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit period_path(period)      
      end      
      
      it { should have_selector('h1', text: 'Session Period') }
      it { should have_selector('title', text: full_title('Session Period')) }   
      it { should have_link('Edit', href: edit_period_path(period)) }              
      it { should have_content(nursery.periods[0].period_name) }
      it { should have_content("#{nursery.periods[0].period_start_time.strftime("%I:%M %P")} #{'-'} #{nursery.periods[0].period_end_time.strftime("%I:%M %P")}") }
      it { should have_content(nursery.periods[0].rooms[0].name) }
      it { should have_content(nursery.periods[0].period_cost) }       
      it { should have_content(nursery.periods[0].period_hourly_cost) }
      it { should have_content(nursery.periods[0].fe_limit) }       
    end      
  end
  
  describe "Period delete Page" do
    describe "for non-signed-in users" do
      before { delete period_path(period) }
      specify { response.should redirect_to(root_path) } 
    end      
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit periods_path
      end       
      
      it "should display nursery periods index page" do
        current_path.should == periods_path
      end            
      
      it { should have_link('Delete', href: period_path(period)) }
    
      it "should delete the period record" do
        expect { click_link('Delete'.to change(Period, :count)).by(-1) } 
        current_path.should == periods_path
      end    
    end
  end
end
