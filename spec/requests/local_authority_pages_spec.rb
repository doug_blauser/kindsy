require 'spec_helper'

describe "LocalAuthorityPages" do
  let(:user) { FactoryGirl.create(:user) }
  let(:role) { FactoryGirl.create(:role) } 
  let(:user_roles) { user.roles << role }
  let(:permission) { FactoryGirl.create(:permission, role_id: role.id) }
  let!(:local_authority) { FactoryGirl.create(:local_authority) }

  let(:submit) { "Save" }
  
  subject { page }  
  
  describe "New Local Authority Page" do
    context "for non-signed_in users" do
      before { get new_local_authority_path }
      specify { response.should redirect_to(root_path) }       
    end
    
    context "for signed-in user" do
      before do
        sign_in_user(user, user_roles, permission)
        visit new_local_authority_path
      end
      
      it { should have_selector('h1', text: "Add a Local Authority") }
      it { should have_selector('title', text: full_title('Add a Local Authority')) }
      
      context "with valid information" do
        before do
          fill_in 'local_authority[name]',                                      with: "Lincolnshire"
          fill_in 'local_authority[contact_info_attributes][address_line_1]',   with: "Local Authority Address Line 1" 
          fill_in 'local_authority[contact_info_attributes][address_line_2]',   with: "Local Authority Address Line 2"
          fill_in 'local_authority[contact_info_attributes][address_line_3]',   with: "Local Authority Address Line 3" 
          fill_in 'local_authority[contact_info_attributes][town]',             with: "Faringdon"
          fill_in 'local_authority[contact_info_attributes][county]',           with: "Oxon"
          fill_in 'local_authority[contact_info_attributes][post_code]',        with: "SN7 7BP"
          fill_in 'local_authority[contact_info_attributes][telephone_1]',      with: "01522511444"
          fill_in 'local_authority[contact_info_attributes][email_1]',          with: "localauthority@example.com"          
        end
        
        it "should create a new local authority record" do
          expect { click_button submit }.to change(LocalAuthority, :count).by(1) 
        end
        
        it "should create a new contact_info record" do
          expect { click_button submit }.to change(ContactInfo, :count).by(1)          
        end                 
      end      

      context "with missing information" do
        let(:missing_name) { "" }
        let(:missing_add1) { "" }
        let(:missing_town) { "" }
        let(:missing_post_code) { "" }   
        before do
          fill_in 'local_authority[name]',                                      with: missing_name
          fill_in 'local_authority[contact_info_attributes][address_line_1]',   with: missing_add1
          fill_in 'local_authority[contact_info_attributes][address_line_2]',   with: "Local Authority Address Line 2"
          fill_in 'local_authority[contact_info_attributes][address_line_3]',   with: "Local Authority Address Line 3" 
          fill_in 'local_authority[contact_info_attributes][town]',             with: missing_town
          fill_in 'local_authority[contact_info_attributes][county]',           with: "Oxon"
          fill_in 'local_authority[contact_info_attributes][post_code]',        with: missing_post_code
          fill_in 'local_authority[contact_info_attributes][telephone_1]',      with: "01522511444"
          fill_in 'local_authority[contact_info_attributes][email_1]',          with: "localauthority@example.com"          
        end        

        it "should not create a new Local Authority record" do
          expect { click_button submit }.not_to change(LocalAuthority, :count)
        end

        it { should have_selector('div', :id => "error_explanation") }
        it { should have_selector('div', :class => "control-group string required error") }
                
        it "should redirect to new Local Authority  page" do
          current_path.should == new_local_authority_path
        end                
      end
    end    
  end

  describe "Local Authorities Index Page" do
    context "for non-signed_in users" do
      before { get local_authorities_path }
      specify { response.should redirect_to(root_path) }       
    end   
    
    context "for signed-in user" do
      before do
        sign_in_user(user, user_roles, permission)
        visit local_authorities_path
      end
      
      it { should have_selector('h1', text: "Local Authorities") }
      it { should have_selector('title', text: full_title('Local Authorities')) }
      it { should have_link('+ Local Authority', href: new_local_authority_path) }      
      it { should have_selector('th', text: 'Name') }
      it { should have_selector('td', text: local_authority.name) }
      it { should have_selector('th', text: 'Terms') }
      it { should have_link('Terms', href: terms_path(local_authority: local_authority)) }
      it { should have_link('View', href: local_authority_path(local_authority)) }
      it { should have_link('Edit', href: edit_local_authority_path(local_authority)) }
      it { should have_link('Delete', href: local_authority_path(local_authority)) }                 
    end 
  end

  describe "Local Authorities Edit Page" do
    context "for non-signed_in users" do
      before { get edit_local_authority_path(local_authority) }
      specify { response.should redirect_to(root_path) }       
    end       
    
    context "for signed-in user" do
      before do
        sign_in_user(user, user_roles, permission)
        visit edit_local_authority_path(local_authority)
      end
      
      it { should have_selector('h1', text: "Edit Local Authority") }
      it { should have_selector('title', text: full_title('Edit Local Authority')) } 
      
      context "with valid information" do
        let(:revised_name) { "Revised Local Authority Name" }
        let(:revised_add_1) { "Revised Address 1" }
        let(:revised_add_2) { "Revised Address 2" }
        let(:revised_add_3) { "Revised Address 3" }
        let(:revised_town) { "Revised Town" }
        let(:revised_county) { "Revised County" }
        let(:revised_post_code) { "Revised PostCode" }
        let(:revised_telephone) { "01522511445" }
        let(:revised_email) { "revisedlocalauthority@example.com" }   
        before do
          fill_in 'local_authority[name]',                                      with: revised_name
          fill_in 'local_authority[contact_info_attributes][address_line_1]',   with: revised_add_1
          fill_in 'local_authority[contact_info_attributes][address_line_2]',   with: revised_add_2
          fill_in 'local_authority[contact_info_attributes][address_line_3]',   with: revised_add_3 
          fill_in 'local_authority[contact_info_attributes][town]',             with: revised_town
          fill_in 'local_authority[contact_info_attributes][county]',           with: revised_county
          fill_in 'local_authority[contact_info_attributes][post_code]',        with: revised_post_code
          fill_in 'local_authority[contact_info_attributes][telephone_1]',      with: revised_telephone
          fill_in 'local_authority[contact_info_attributes][email_1]',          with: revised_email 
          click_button submit                   
        end        
        
        specify { local_authority.reload.name.should == revised_name }  
        specify { local_authority.reload.contact_info.address_line_1.should == revised_add_1 } 
        specify { local_authority.reload.contact_info.address_line_2.should == revised_add_2 }
        specify { local_authority.reload.contact_info.address_line_3.should == revised_add_3 }
        specify { local_authority.reload.contact_info.town.should == revised_town }      
        specify { local_authority.reload.contact_info.county.should == revised_county }
        specify { local_authority.reload.contact_info.post_code.should == revised_post_code }   
        specify { local_authority.reload.contact_info.telephone_1.should == revised_telephone } 
        specify { local_authority.reload.contact_info.email_1.should == revised_email }                    


        it "should redirect to local authority index page" do
          current_path.should == local_authorities_path
        end                                                       
      end      

      context "with missing information" do
        let(:missing_name) { "" }
        let(:missing_add_1) { "" }
        let(:missing_add_2) { "" }
        let(:missing_add_3) { "" }
        let(:missing_town) { "" }
        let(:missing_county) { "" }
        let(:missing_post_code) { "" }
        let(:missing_telephone) { "" }
        let(:missing_email) { "" }   
        before do
          fill_in 'local_authority[name]',                                      with: missing_name
          fill_in 'local_authority[contact_info_attributes][address_line_1]',   with: missing_add_1
          fill_in 'local_authority[contact_info_attributes][address_line_2]',   with: missing_add_2
          fill_in 'local_authority[contact_info_attributes][address_line_3]',   with: missing_add_3 
          fill_in 'local_authority[contact_info_attributes][town]',             with: missing_town
          fill_in 'local_authority[contact_info_attributes][county]',           with: missing_county
          fill_in 'local_authority[contact_info_attributes][post_code]',        with: missing_post_code
          fill_in 'local_authority[contact_info_attributes][telephone_1]',      with: missing_telephone
          fill_in 'local_authority[contact_info_attributes][email_1]',          with: missing_email 
          click_button submit                   
        end     
        
        specify { local_authority.reload.name.should_not == missing_name }  
        specify { local_authority.reload.contact_info.address_line_1.should_not == missing_add_1 } 
        specify { local_authority.reload.contact_info.address_line_2.should_not == missing_add_2 }
        specify { local_authority.reload.contact_info.address_line_3.should_not == missing_add_3 }
        specify { local_authority.reload.contact_info.town.should_not == missing_town }      
        specify { local_authority.reload.contact_info.county.should_not == missing_county }
        specify { local_authority.reload.contact_info.post_code.should_not == missing_post_code }   
        specify { local_authority.reload.contact_info.telephone_1.should_not == missing_telephone } 
        specify { local_authority.reload.contact_info.email_1.should_not == missing_email }          

        it { should have_selector('div', :id => "error_explanation") }
        it { should have_selector('div', :class => "control-group string required error") }
                
        it "should redirect to edit local_authority page" do
          current_path.should == local_authority_path(local_authority)
        end                    
      end
    end   
  end

  describe "Local Authority Show Page" do
    context "for non-signed_in users" do
      before { get local_authority_path(local_authority) }
      specify { response.should redirect_to(root_path) }       
    end     
    
    context "for signed-in user" do
      before do
        sign_in_user(user, user_roles, permission)
        visit local_authority_path(local_authority)
      end
      
      it { should have_selector('h1', text: "Local Authority") }
      it { should have_selector('title', text: full_title('Local Authority')) } 
      it { should have_link('Edit', href: edit_local_authority_path(local_authority)) }
      it { should have_content(local_authority.name) }
      it { should have_content(local_authority.contact_info.address_line_1) }
      it { should have_content(local_authority.contact_info.address_line_2) } 
      it { should have_content(local_authority.contact_info.address_line_3) }
      it { should have_content(local_authority.contact_info.town) }
      it { should have_content(local_authority.contact_info.county) }
      it { should have_content(local_authority.contact_info.post_code)  }
      it { should have_content(local_authority.contact_info.telephone_1)  }
      it { should have_content(local_authority.contact_info.email_1) }            
    end    
  end

  describe "Local Authority Delete Page" do
    context "for non-signed-in users" do
      before { delete local_authority_path(local_authority) }
      specify { response.should redirect_to(root_path) } 
    end      
    
    context "for signed-in user" do
      before do
        sign_in_user(user, user_roles, permission)
        visit local_authorities_path
      end   
      
      it "should display local authorities index page" do
        current_path.should == local_authorities_path
      end  
      
      it { should have_link('Delete', href: local_authority_path(local_authority)) } 
      
      it "should delete the discount record" do
        expect { click_link('Delete'.to change(LocalAuthority, :count)).by(-1) } 
        current_path.should == local_authorities_path
      end                   
    end
  end  
end
