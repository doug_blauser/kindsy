require 'spec_helper'

describe "PeriodPricePages" do
  let(:group) { FactoryGirl.create(:nursery_group) }
  let(:user) { FactoryGirl.create(:user) }
  let(:role) { FactoryGirl.create(:role) } 
  let(:user_roles) { user.roles << role }
  let(:permission) { FactoryGirl.create(:permission, role_id: role.id) }
  let(:region) { FactoryGirl.create(:region) }
  let!(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: group.id, region_id: region.id) }
  let!(:employment) { FactoryGirl.create(:employment, nursery_id: nursery.id, user_id: user.id) }  
  let!(:period) { FactoryGirl.create(:period, nursery_id: nursery.id) }
  let!(:period_price) { FactoryGirl.create(:period_price, period_id: period.id) }
  
  let(:submit) { "Save" }
  
  subject { page } 
  
  describe "Period Price New Page" do
    describe "for non-signed-in users" do
      before { get new_period_price_path }
      specify { response.should redirect_to(root_path) }  
    end    
    
    describe "for signed-in users" do
      before do        
        sign_in_user(user, user_roles, permission) 
        visit new_period_price_path(:period_id => period.id) 
      end 
      
      it { should have_selector('h1', text: 'Period Price') }
      it { should have_selector('title', text: full_title('Period Price')) }
      
      describe "with valid information" do
        before do
          fill_in 'period_price[price]',       with: 18.75
        end
        
        it "should create new period_price record" do
          expect { click_button submit}.to change(PeriodPrice, :count).by(1)
        end
        
        it "should redirect to periods index page" do
          click_button submit
          current_path.should == periods_path          
        end
      end
      
      describe "with missing information" do
        let(:missing_period_price_cost) { "" }
        
        before do
          fill_in 'period_price[price]',       with: missing_period_price_cost
        end
        
        it "should not create a new period_price record" do
          expect { click_button submit }.not_to change(PeriodPrice, :count)
        end 
        
        it "should redirect to period price new page" do
          click_button submit
          current_path.should == period_prices_path
        end        
        
        it { should have_selector('div', :id => "error_explanation") }
        it { should have_selector('div', :class => "control-group string required error") } 
                      
      end       
    end        
  end

  describe "Period Price Edit Page" do
    describe "for non-signed-in users" do
      before { get edit_period_price_path(period_price) }
      specify { response.should redirect_to(root_path) }  
    end    
    
    describe "for signed-in users" do
      before do        
        sign_in_user(user, user_roles, permission) 
        visit edit_period_price_path(period_price) 
      end 
      
      it { should have_selector('h1', text: 'Edit Period Price') }
      it { should have_selector('title', text: full_title('Edit Period Price')) }
      
      describe "with valid information" do
        let(:revised_start_timestamp) { Time.now + 1.month + 1.day }
        let(:revised_end_timestamp) { Time.now + 2.months + 3.days }
        let(:revised_price) { 20.25 }
        
        before do
          select revised_start_timestamp.strftime("%B"),    from: "period_price[start_timestamp(2i)]"
          select revised_start_timestamp.day.to_s,          from: "period_price[start_timestamp(3i)]"
          select revised_end_timestamp.strftime("%B"),      from: "period_price[end_timestamp(2i)]"
          select revised_end_timestamp.day.to_s,            from: "period_price[end_timestamp(3i)]"
          fill_in "period_price[price]",                    with: revised_price
          click_button submit
        end

        specify { period_price.reload.start_timestamp == revised_start_timestamp }
        specify { period_price.reload.end_timestamp == revised_end_timestamp }    
        specify { period_price.reload.price == revised_price }                    
      end
      
      describe "with missing information" do
        let(:missing_period_price_cost) { "" }
        
        before do
          fill_in 'period_price[price]',       with: missing_period_price_cost
        end
        
        specify { period_price.reload.price != missing_period_price_cost }         
        
        it "should redirect to period price edit page" do
          click_button submit
          current_path.should == period_price_path(period_price)
        end        
        
        it { should have_selector('div', :id => "error_explanation") }
        it { should have_selector('div', :class => "control-group string required error") } 
                      
      end             
    end      
  end
end
