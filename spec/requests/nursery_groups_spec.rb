require 'spec_helper'

describe "NurseryGroups" do
  subject { page }
  
  describe "Nursery Group Index Page" do
    let(:group) { FactoryGirl.create(:nursery_group) }
    let(:user) { FactoryGirl.create(:user) }
    let(:role) { FactoryGirl.create(:role) } 
    let(:user_roles) { user.roles << role }
    let(:permission) { FactoryGirl.create(:permission, role_id: role.id) }
    let(:region) { FactoryGirl.create(:region) }
    let(:contact_info) { FactoryGirl.create(:contact_info) }
    let(:nursery) { FactoryGirl.create(:nursery) }
    
    describe "for non-signed-in users" do
      before { get nursery_groups_path }
      specify { response.should redirect_to(root_path) } 
    end   
    
    describe "for signed-in user" do
      before do        
        sign_in_user(user, user_roles, permission) 
        visit nursery_groups_path 
      end
      
      describe "of correct account" do 
        it { should have_selector('h1', text: 'Our Nurseries') }
        it { should have_selector('title', text: full_title('Nursery List')) }
      end
    end  

  end
end
