require 'spec_helper'

describe "CarerPages" do
  let(:group) { FactoryGirl.create(:nursery_group) }
  let(:user) { FactoryGirl.create(:user) }
  let(:role) { FactoryGirl.create(:role) } 
  let(:user_roles) { user.roles << role }
  let(:permission) { FactoryGirl.create(:permission, role_id: role.id) }
  let(:region) { FactoryGirl.create(:region) }
  let!(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: group.id, region_id: region.id) }
  let!(:employment) { FactoryGirl.create(:employment, nursery_id: nursery.id, user_id: user.id) }
  let!(:carer) { FactoryGirl.create(:carer, nursery_id: nursery.id) } 
  let!(:account) { FactoryGirl.create(:account, nursery_id: nursery.id, user_id: user.id) } 
  let!(:accountability) { FactoryGirl.create(:accountability, account_id: account.id, carer_id: carer.id) } 
  let!(:status) { FactoryGirl.create(:status_type) }    
  let!(:child) { FactoryGirl.create(:child, nursery_id: nursery.id, status_type_id: status.id, account_id: account.id) }
  let!(:child_relationship_type) { FactoryGirl.create(:child_relationship_type) }
  let!(:child_relationship) { FactoryGirl.create(:child_relationship, child_id: child.id, carer_id: carer.id, child_relationship_type_id: child_relationship_type.id, pick_up_flag: true, drop_off_flag: true) }  
  
  let(:submit) { "Save" }
  
  subject { page }
  
  describe "Carers Index Page" do
    describe "for non-signed-in users" do
      before { get carers_path }
      specify { response.should redirect_to(root_path) }
    end
  
    describe "for signed-in users" do
      before do        
        sign_in_user(user, user_roles, permission) 
        visit carers_path 
      end  
      
      it { should have_selector('h1', text: 'Nursery Carers') }
      it { should have_selector('title', text: full_title('Carers')) } 
      it { should have_link('+ Carer', href: new_carer_path) }
      it { should have_selector('th', text: 'Name') }       
      it { should have_selector('th', text: 'Primary Telephone') } 
      it { should have_selector('th', text: 'Accounts') }
      it { should have_selector('th', text: 'Relationship') }
      it { should have_selector('th', text: '...') } 
      it { should have_selector('td', text: carer.display_name) } 
      it { should have_selector('td', text: carer.contact_info.telephone_1) }  
      it { should have_selector('td', text: account.account_no) }   
      it { should have_selector('td', text: carer.display_relations(carer, carer.child_relationships).join(", ")) }
      it { should have_link('View', href: carer_path(carer)) }
      it { should have_link('Edit', href: edit_carer_path(carer)) }
      it { should have_link('Delete', href: carer_path(carer)) }                                      
    end
  end

  describe "Carers New Page" do
    describe "for non-signed-in users" do
      before { get new_carer_path }
      specify { response.should redirect_to(root_path) } 
    end     
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit new_carer_path        
      end
      
      it { should have_selector('h1', text: 'Add a carer') }
      it { should have_selector('title', text: full_title('Add Carer')) }
      
      describe "with valid information" do
        before do
          fill_in 'carer[title]',                                            with: "Mrs"
          fill_in 'carer[first_name]',                                       with: "Firstname"
          fill_in 'carer[surname]',                                          with: "Lastname"
          fill_in 'carer[contact_info_attributes][address_line_1]',   with: "Address Line 1" 
          fill_in 'carer[contact_info_attributes][address_line_2]',   with: "Address Line 2"
          fill_in 'carer[contact_info_attributes][address_line_3]',   with: "Address Line 3" 
          fill_in 'carer[contact_info_attributes][town]',             with: "Faringdon"
          fill_in 'carer[contact_info_attributes][county]',           with: "Oxon"
          fill_in 'carer[contact_info_attributes][post_code]',        with: "SN7 7BP"
          fill_in 'carer[contact_info_attributes][telephone_1]',      with: "01522511444"
          fill_in 'carer[contact_info_attributes][email_1]',          with: "mynursery@example.com"          
        end
        
        it "should create a new carer" do
          expect { click_button submit }.to change(Carer, :count).by(1) 
        end
        
        it "should create a new carer contact_info record" do
          expect { click_button submit }.to change(ContactInfo, :count).by(1)          
        end        
      end
    
      describe "with missing information" do
        let(:missing_first_name) { "" }
        let(:missing_surname) { "" }
        let(:missing_add_1) { "" }
        let(:missing_add_2) { "" }
        let(:missing_add_3) { "" }
        let(:missing_town) { "" }
        let(:missing_county) { "" }
        let(:missing_post_code) { "" }
        let(:missing_telephone) { "" }
        let(:missing_email) { "" }        
        before do
          fill_in 'carer[title]',                                            with: "Mrs"
          fill_in 'carer[first_name]',                                       with: missing_first_name
          fill_in 'carer[surname]',                                          with: missing_surname
          fill_in 'carer[contact_info_attributes][address_line_1]',          with: missing_add_1 
          fill_in 'carer[contact_info_attributes][address_line_2]',          with: missing_add_2
          fill_in 'carer[contact_info_attributes][address_line_3]',          with: missing_add_3 
          fill_in 'carer[contact_info_attributes][town]',                    with: missing_town
          fill_in 'carer[contact_info_attributes][county]',                  with: missing_county
          fill_in 'carer[contact_info_attributes][post_code]',               with: missing_post_code
          fill_in 'carer[contact_info_attributes][telephone_1]',             with: missing_telephone
          fill_in 'carer[contact_info_attributes][email_1]',                 with: missing_email       
        end       
      
        it "should not create a new carer" do
          expect { click_button submit}.not_to change(Carer, :count)
        end
        
        it "should not create a new contact info record" do
          expect { click_button submit }.not_to change(ContactInfo, :count)
        end
        
        it { should have_selector('div', :id => "error_explanation") }
        it { should have_selector('div', :class => "control-group string required error") }
        
        it "should redirect to new carer page" do
          current_path.should == new_carer_path
        end
      end
    end  
  end

  describe "Carers Edit Page" do
    describe "for non-signed-in users" do
      before { get edit_carer_path(carer) }
      specify { response.should redirect_to(root_path) } 
    end  
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit edit_carer_path(carer)        
      end 
      
      it { should have_selector('h1', text: 'Edit Carer') }
      it { should have_selector('title', text: full_title('Edit Carer')) } 
      
      describe "with valid information" do
        let(:revised_title) { "Revised Title" }
        let(:revised_first_name) { "RevisedFirstName" }
        let(:revised_surname) { "RevisedSurname" }
        let(:revised_preferred_name) { "RevisedPreferredName" }
        let(:revised_add_1) { "Revised Address 1" }
        let(:revised_add_2) { "Revised Address 2" }
        let(:revised_add_3) { "Revised Address 3" }
        let(:revised_town) { "Revised Town" }
        let(:revised_county) { "Revised County" }
        let(:revised_post_code) { "Revised PostCode" }
        let(:revised_telephone) { "01522511445" }
        let(:revised_email) { "revisednursery@example.com" }
        before do
          fill_in 'carer[title]',                                            with: revised_title
          fill_in 'carer[first_name]',                                       with: revised_first_name
          fill_in 'carer[surname]',                                          with: revised_surname
          fill_in 'carer[contact_info_attributes][address_line_1]',          with: revised_add_1 
          fill_in 'carer[contact_info_attributes][address_line_2]',          with: revised_add_2
          fill_in 'carer[contact_info_attributes][address_line_3]',          with: revised_add_3 
          fill_in 'carer[contact_info_attributes][town]',                    with: revised_town
          fill_in 'carer[contact_info_attributes][county]',                  with: revised_county
          fill_in 'carer[contact_info_attributes][post_code]',               with: revised_post_code
          fill_in 'carer[contact_info_attributes][telephone_1]',             with: revised_telephone
          fill_in 'carer[contact_info_attributes][email_1]',                 with: revised_email
          click_button submit       
        end   
        
        specify { carer.reload.title == revised_title }
        specify { carer.reload.first_name == revised_first_name } 
        specify { carer.reload.surname == revised_surname }
        specify { carer.contact_info.reload.address_line_1 == revised_add_1 }
        specify { carer.contact_info.reload.address_line_2 == revised_add_2 } 
        specify { carer.contact_info.reload.address_line_3 == revised_add_3 }     
        specify { carer.contact_info.reload.town == revised_town } 
        specify { carer.contact_info.reload.county == revised_county }                                                
        specify { carer.contact_info.reload.post_code == revised_post_code }   
        specify { carer.contact_info.reload.telephone_1 == revised_telephone }
        specify { carer.contact_info.reload.email_1 == revised_email }
        
        it "should redirect to carer index page" do
          current_path.should == carers_path
        end                                                        
      end          
    
      describe "with missing information" do
        let(:missing_first_name) { "" }
        let(:missing_surname) { "" }
        let(:missing_add_1) { "" }
        let(:missing_add_2) { "" }
        let(:missing_add_3) { "" }
        let(:missing_town) { "" }
        let(:missing_county) { "" }
        let(:missing_post_code) { "" }
        let(:missing_telephone) { "" }
        let(:missing_email) { "" }        
        before do
          fill_in 'carer[title]',                                            with: "Mrs"
          fill_in 'carer[first_name]',                                       with: missing_first_name
          fill_in 'carer[surname]',                                          with: missing_surname
          fill_in 'carer[contact_info_attributes][address_line_1]',          with: missing_add_1 
          fill_in 'carer[contact_info_attributes][address_line_2]',          with: missing_add_2
          fill_in 'carer[contact_info_attributes][address_line_3]',          with: missing_add_3 
          fill_in 'carer[contact_info_attributes][town]',                    with: missing_town
          fill_in 'carer[contact_info_attributes][county]',                  with: missing_county
          fill_in 'carer[contact_info_attributes][post_code]',               with: missing_post_code
          fill_in 'carer[contact_info_attributes][telephone_1]',             with: missing_telephone
          fill_in 'carer[contact_info_attributes][email_1]',                 with: missing_email       
        end       
        
        specify { carer.reload.first_name != missing_first_name } 
        specify { carer.reload.surname != missing_surname }
        specify { carer.contact_info.reload.address_line_1 != missing_add_1 }
        specify { carer.contact_info.reload.address_line_2 != missing_add_2 } 
        specify { carer.contact_info.reload.address_line_3 != missing_add_3 }     
        specify { carer.contact_info.reload.town != missing_town } 
        specify { carer.contact_info.reload.county != missing_county }                                                
        specify { carer.contact_info.reload.post_code != missing_post_code }   
        specify { carer.contact_info.reload.telephone_1 != missing_telephone }
        specify { carer.contact_info.reload.email_1 != missing_email }    
        
        it { should have_selector('div', :id => "error_explanation") }
        it { should have_selector('div', :class => "control-group string required error") } 
        
        it "should redirect to edit carer page" do
          current_path.should == edit_carer_path(carer)
        end                           
      end
    end    
  end

  describe "Carers Show Page" do
     describe "for non-signed-in users" do
      before { get carer_path(carer) }
      specify { response.should redirect_to(root_path) } 
    end    
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit carer_path(carer)        
      end      
      
      it { should have_selector('h1', text: "#{'Carer: '}#{carer.display_name}") }
      it { should have_selector('title', text: full_title('Carer')) }    
      it { should have_link('Back to List', href: carers_path) }     
      it { should have_link('Summary', href: carer_path(carer)) }  
      it { should have_link('Personal Information', href: detail_carer_path(carer)) }
      it { should have_link('Relationships', href: relationships_carer_path(carer)) }  
      it { should have_link('Bank Information', href: '#') }
      it { should have_link('Regular Payments', href: '#') }
      it { should have_link('Invoices', href: invoices_path(carer_id: carer.id)) }
      it { should have_link('Payments Paid', href: "#") }
      it { should have_selector('h2', text: 'Carer Summary View') }  
      it { should have_link('+ Add Child', href: search_children_carer_path(carer)) } 
      it { should have_link('+ Add Accounts', href: search_accounts_carer_path(carer)) }                   
      it { should have_content("#{carer.display_name}") } 
      it { should have_content("#{carer.contact_info.email_1}") }    
      it { should have_content("#{carer.contact_info.telephone_1}") } 
      it { should have_content("#{'This is where the outstanding balance for this carer goes'}") }  
      it { should have_content("#{carer.children.first.full_name}") }  
      it { should have_content("#{carer.accounts[0].account_no}") }   
      it { should have_content("#{'Balance for this account goes here'}") }
    end
  end

  describe "Carers Detail Page" do
    describe "for non-signed-in users" do
      before { get detail_carer_path(carer) }
      specify { response.should redirect_to(root_path) } 
    end    
    
    describe "for signed-in users" do
       before do
        sign_in_user(user, user_roles, permission)
        visit detail_carer_path(carer)  
      end      
      
      it { should have_selector('h1', text: "#{'Carer: '}#{carer.display_name}") }
      it { should have_selector('title', text: full_title('Carer Details')) }
      it { should have_link('Back to List', href: carers_path) }     
      it { should have_link('Summary', href: carer_path(carer)) }  
      it { should have_link('Personal Information', href: detail_carer_path(carer)) }
      it { should have_link('Relationships', href: relationships_carer_path(carer)) }  
      it { should have_link('Bank Information', href: '#') }
      it { should have_link('Regular Payments', href: '#') }
      it { should have_link('Invoices', href: invoices_path(carer_id: carer.id)) }
      it { should have_link('Payments Paid', href: "#") }
      it { should have_selector('h2', text: 'Carer Personal Information') } 
      it { should have_selector('h3', text: "#{carer.display_name}") }
      it { should have_content("#{carer.contact_info.telephone_1}") }
      it { should have_content("#{carer.contact_info.telephone_2}") }  
      it { should have_content("#{carer.contact_info.email_1}") }
      it { should have_content("#{carer.contact_info.address_line_1}") } 
      it { should have_content("#{carer.contact_info.address_line_2}") }
      it { should have_content("#{carer.contact_info.address_line_3}") }
      it { should have_content("#{carer.contact_info.town}")  }  
      it { should have_content("#{carer.contact_info.post_code}") }
      it { should have_link('Edit', href: edit_carer_path(carer)) }                          
    end    
  end

  describe "Carer Relationship Page" do
    describe "for non-signed-in users" do
      before { get relationships_carer_path(carer) }
      specify { response.should redirect_to(root_path) } 
    end     
    
    describe "for sign-in users" do
       before do
        sign_in_user(user, user_roles, permission)
        visit relationships_carer_path(carer)  
      end   
      
      it { should have_selector('h1', text: "#{'Carer: '}#{carer.display_name}") }
      it { should have_selector('title', text: full_title('Carer Relationships')) } 
      it { should have_link('Back to List', href: carers_path) }
      it { should have_link('Summary', href: carer_path(carer)) }  
      it { should have_link('Personal Information', href: detail_carer_path(carer)) }
      it { should have_link('Relationships', href: relationships_carer_path(carer)) }  
      it { should have_link('Bank Information', href: '#') }
      it { should have_link('Regular Payments', href: '#') }
      it { should have_link('Invoices', href: invoices_path(carer_id: carer.id)) }
      it { should have_link('Payments Paid', href: "#") } 
      it { should have_selector('h2', text: 'Carer Relationships') }
      it { should have_link('Edit', href: edit_carer_path(carer)) } 
      it { should have_link('+ Add Child', href: search_children_carer_path(carer)) }  
      it { should have_link('+ Add Accounts', href: search_accounts_carer_path(carer)) }           
      it { should have_selector('th', text: 'Child') }
      it { should have_selector('th', text: 'Relationship') }
      it { should have_selector('th', text: 'Pick Up') }
      it { should have_selector('th', text: 'Drop Off') }
      it { should have_selector('th', text: 'Payment') }
      it { should have_selector('th', text: '%') }
      it { should have_selector('th', text: '...') }      
      it { should have_selector('td', text: carer.children[0].full_name) }
      it { should have_selector('td', text: carer.child_relationships[0].child_relationship_type.relationship_type) }
      it { should have_selector('i', :class => "icon-flag icon-large") }             
      it { should have_css('#pickup') }  
      it { should have_css('#dropoff') }  
      it { should have_css('#payment') } 
      it { should have_content('100%') }  
      it { should have_link('Delete', href: remove_relationship_carer_path(carer, :child_id => child.id)) }       
    end   
  end

  describe "Carer Search Children Page" do
    describe "for non-signed-in users" do
      before { get search_children_carer_path(carer) }
      specify { response.should redirect_to(root_path) } 
    end  
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit search_children_carer_path(carer)        
      end
      
      it { should have_selector('h1', text: 'Carer Detail') }  
      it { should have_selector('title', text: full_title('Add Related Children')) } 
      it { should have_link('Back to List', href: carers_path) }      
      it { should have_selector('h2', text: 'Add Related Children') } 
      
      describe "with valid information" do
        before do
          select "surname",                 from: 'field'
          fill_in 'query',                  with: 'sur' 
          click_button 'Search'         
        end
        
        it { should have_content(child.full_name) }
        it { should have_css('input#add_children', type: 'submit') }
      end                              
    end  
  end

  describe "Carer Add Children Page" do
    describe "for non-signed-in users" do
      before { put add_children_carer_path(carer) }
      specify { response.should redirect_to(root_path) } 
    end   
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit search_children_carer_path(carer)
      end      
      
      describe "with valid information" do
        before do
          select "surname",                 from: 'field'
          fill_in 'query',                  with: 'sur' 
          click_button 'Search'              
          check('child_ids[]')   
          select "Mother",                  from: 'child_relationship_type_ids[]'
          select "true",                      from: 'pick_up_flag[]'
          select "true",                      from: 'drop_off_flag[]'
        end                
      
        it "should create a new child relationship record" do
          expect { click_button 'Add Children' }.to change(ChildRelationship, :count).by(1)
        end    
        
        it "should redirect to relationships tab" do 
          click_button 'Add Children'        
          current_path.should == relationships_carer_path(carer)
        end  
      end
    end  
  end

  describe "Carer Remove Related Children Page" do
    describe "for non-signed-in users" do
      before { put remove_relationship_carer_path(carer, child) }
      specify { response.should redirect_to(root_path) } 
    end         
    
    describe "for signed-in users" do
       before do
        sign_in_user(user, user_roles, permission)
        visit relationships_carer_path(carer)  
      end   
      
      it { should have_selector('h1', text: "#{'Carer: '}#{carer.display_name}") }
      it { should have_selector('title', text: full_title('Carer Relationships')) }  
      it { should have_link('Delete', href: remove_relationship_carer_path(carer, :child_id => child.id)) }  
      
      it "should remove child_relationship record when delete link is clicked" do
        expect { click_link 'Delete' }.to change(ChildRelationship, :count).by(-1)
      end              
    end
  end
  
  describe "Carer Search Accounts Page" do
    describe "for non-signed-in users" do
      before { get search_accounts_carer_path(carer) }
      specify { response.should redirect_to(root_path) } 
    end     
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit search_accounts_carer_path(carer)        
      end
      
      it { should have_selector('h1', text: 'Carer Detail') }  
      it { should have_selector('title', text: full_title('Add Carer Account')) } 
      it { should have_link('Back to List', href: carers_path) }      
      it { should have_selector('h2', text: 'Add Carer to Account') } 
      
      describe "with valid information" do
        before do
          select "account_name",            from: 'field'
          fill_in 'query',                  with: 'ccou' 
          click_button 'Search'         
        end
        
        it { should have_content(account.account_name) }
        it { should have_css('input#assign_accounts', type: 'submit') }        
      end        
    end     
  end

  describe "Carer Assign Account Page" do
    describe "for non-signed-in users" do
      before { put assign_accounts_carer_path(carer) }
      specify { response.should redirect_to(root_path) } 
    end        
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit search_accounts_carer_path(carer)
      end      
      
      describe "with valid information" do
        before do
          select "account_name",            from: 'field'
          fill_in 'query',                  with: 'Acc' 
          click_button 'Search' 
          check('account_ids[]')            
        end
        
        it "should add a new accountability record" do
          expect { click_button 'Assign Accounts' }.to change(Accountability, :count).by(1)          
        end
        
        it "should redirect to relationships tab" do 
          click_button 'Assign Accounts'        
          current_path.should == carer_path(carer)
        end          
      end          
    end   
  end
end
