require 'spec_helper'

describe "InvoicePages" do
  describe "Invoice Free Entitlement" do
    describe "for non-signed-in users" do
      before { post invoices_path }
      specify { response.should redirect_to(root_path) }
    end       
    
    describe "for signed-in users" do
      new_time = Time.local(2013,9,30,12,0,0) 
      Timecop.freeze(new_time)        
      context "with term & non-term time 1 yr old" do       
        before do
          nursery = FactoryGirl.create(:nursery)
          local_authority = nursery.local_authority
          term = FactoryGirl.create(:term, :term_1, local_authority_id: local_authority.id)
          account = FactoryGirl.create(:account, nursery_id: nursery.id)
          user = account.user
          role = FactoryGirl.create(:role)
          user_roles = user.roles << role
          permission = FactoryGirl.create(:permission, role_id: role.id)
          employment = FactoryGirl.create(:employment, nursery_id: nursery.id, user_id: user.id)
          carer = FactoryGirl.create(:carer, :shehzad, nursery_id: nursery.id)
          accountability = FactoryGirl.create(:accountability, account_id: account.id, carer_id: carer.id)
          child = FactoryGirl.create(:child, :zaynab, carer_id: carer.id, nursery_id: nursery.id, account_id: account.id)
          child_relationship_type = FactoryGirl.create(:child_relationship_type, relationship_type: "Father")
          child_relationship = FactoryGirl.create(:child_relationship, child_id: child.id, carer_id: carer.id, child_relationship_type_id: child_relationship_type.id, pick_up_flag: true, drop_off_flag: true)
          billing_period = FactoryGirl.create(:billing_period, nursery_id: nursery.id, start_date: Date.new(2013,9,1), end_date: Date.new(2013,9,30))
          full_day_period = FactoryGirl.create(:period, :full_day_1_yr_olds, nursery_id: nursery.id)
          half_day_am_period = FactoryGirl.create(:period, :half_day_am_1_yr_olds, nursery_id: nursery.id)
          half_day_pm_period = FactoryGirl.create(:period, :half_day_pm_1_yr_olds, nursery_id: nursery.id)
          full_day_dates = [Date.new(2013,9,2),Date.new(2013,9,9),Date.new(2013,9,16),Date.new(2013,9,23),Date.new(2013,9,30)]
          full_day_dates.each do |date|
            timesheet = FactoryGirl.create(:timesheet, :full_day_1_yr_olds, nursery_id: nursery.id, child_id: child.id, period_id: full_day_period.id, term_id: term.id, date_attended: date)
          end
          half_day_am_dates = [Date.new(2013,9,3),Date.new(2013,9,10),Date.new(2013,9,17),Date.new(2013,9,24)]
          half_day_am_dates.each do |date|
            timesheet = FactoryGirl.create(:timesheet, :half_day_1_yr_olds, nursery_id: nursery.id, child_id: child.id, period_id: half_day_am_period.id, term_id: term.id, date_attended: date)  
          end
          half_day_pm_dates = [Date.new(2013,9,4),Date.new(2013,9,11),Date.new(2013,9,18),Date.new(2013,9,25)]
          half_day_pm_dates.each do |date|
            timesheet = FactoryGirl.create(:timesheet, :half_day_1_yr_olds, nursery_id: nursery.id, child_id: child.id, period_id: half_day_pm_period.id, term_id: term.id, date_attended: date)
          end           
          children = carer.children
          session_discount = FactoryGirl.create(:discount, discountable_id: full_day_period.id, discountable_type: "Period") 
          account_discount = FactoryGirl.create(:discount, name: "Account Discount", amount: 10.00, discountable_id: account.id, discountable_type: "Account", applies_to: "Whole Invoice")         
          @invoice =  account.invoices.build(nursery_id: nursery.id, carer_id: carer.id, number: Invoice.next_number, issue: 1, state: "new", billing_period_id: billing_period.id)
          @line_items = @invoice.populate_line_items(account, children, billing_period)
          @invoice.save   
          @items = @invoice.invoice_line_items              
        end
        
        describe "timesheets" do
          subject { Timesheet.count }
          it { should eq(13) }
        end
        
        describe "line items" do      
          subject { InvoiceLineItem.count }
          it { should eq(7) }
        end
        
        describe "charges" do
          subject { Charge.count }
          it { should eq(13) }
        end
        
        describe "line item" do
          subject { Invoice.last.invoice_line_items.order("id ASC") }
          context "1" do
            it "description" do            
              expect(subject[0].description).to eq("Full Day 1 Year Olds Full Sessions")
            end
            it "quantity" do              
              expect(subject[0].quantity).to eq(5)
            end
            it "cost" do               
              expect(subject[0].cost).to eq(38.00)
            end
            it "total cost" do
              expect(subject[0].total_cost).to eq(190.00)
            end
          end
          context "2" do
            it "description" do            
              expect(subject[1].description).to eq("Half Day - AM 1 Year Olds Full Sessions")
            end
            it "quantity" do              
              expect(subject[1].quantity).to eq(4)
            end
            it "cost" do               
              expect(subject[1].cost).to eq(20.25)
            end
            it "total cost" do
              expect(subject[1].total_cost).to eq(81.00)
            end
          end
          context "3" do
            it "description" do            
              expect(subject[2].description).to eq("Half Day - PM 1 Year Olds Full Sessions")
            end
            it "quantity" do              
              expect(subject[2].quantity).to eq(4)
            end
            it "cost" do               
              expect(subject[2].cost).to eq(20.25)
            end
            it "total cost" do
              expect(subject[2].total_cost).to eq(81.00)
            end
          end
          context "4" do
            it "description" do            
              expect(subject[3].description).to eq("Early Years Entitlement - 0 hours")
            end            
          end    
        end  
      end

      context "with term & non-term time 3 yr old" do
        before do
          nursery = FactoryGirl.create(:nursery)
          local_authority = nursery.local_authority
          term = FactoryGirl.create(:term, :term_1, local_authority_id: local_authority.id)
          account = FactoryGirl.create(:account, nursery_id: nursery.id)
          user = account.user
          role = FactoryGirl.create(:role)
          user_roles = user.roles << role
          permission = FactoryGirl.create(:permission, role_id: role.id)
          employment = FactoryGirl.create(:employment, nursery_id: nursery.id, user_id: user.id)
          carer = FactoryGirl.create(:carer, :shehzad, nursery_id: nursery.id)
          accountability = FactoryGirl.create(:accountability, account_id: account.id, carer_id: carer.id)
          child = FactoryGirl.create(:child, :mayya, carer_id: carer.id, nursery_id: nursery.id, account_id: account.id)
          child_relationship_type = FactoryGirl.create(:child_relationship_type, relationship_type: "Father")
          child_relationship = FactoryGirl.create(:child_relationship, child_id: child.id, carer_id: carer.id, child_relationship_type_id: child_relationship_type.id, pick_up_flag: true, drop_off_flag: true)
          billing_period = FactoryGirl.create(:billing_period, nursery_id: nursery.id, start_date: Date.new(2013,9,1), end_date: Date.new(2013,9,30))
          full_day_period = FactoryGirl.create(:period, :full_day_3_yr_olds, nursery_id: nursery.id)
          half_day_am_fe_period = FactoryGirl.create(:period, :half_day_am_3_yr_olds_fe, nursery_id: nursery.id)
          half_day_am_nofe_period = FactoryGirl.create(:period, :half_day_am_3_yr_olds_nofe, nursery_id: nursery.id)
          half_day_pm_period = FactoryGirl.create(:period, :half_day_pm_3_yr_olds, nursery_id: nursery.id)
          full_day_dates = [Date.new(2013,9,2),Date.new(2013,9,9),Date.new(2013,9,16),Date.new(2013,9,23),Date.new(2013,9,30)]
          full_day_dates.each do |date|
            timesheet = FactoryGirl.create(:timesheet, :full_day_3_yr_olds, nursery_id: nursery.id, child_id: child.id, period_id: full_day_period.id, term_id: term.id, date_attended: date)
          end
          half_day_am_fe_dates = [Date.new(2013,9,3),Date.new(2013,9,10),Date.new(2013,9,17),Date.new(2013,9,24)]
          half_day_am_fe_dates.each do |date|
            timesheet = FactoryGirl.create(:timesheet, :half_day_3_yr_olds, nursery_id: nursery.id, child_id: child.id, period_id: half_day_am_fe_period.id, term_id: term.id, date_attended: date)
          end
          half_day_pm_dates = [Date.new(2013,9,4),Date.new(2013,9,11),Date.new(2013,9,18),Date.new(2013,9,25)]
          half_day_pm_dates.each do |date|
            timesheet = FactoryGirl.create(:timesheet, :half_day_3_yr_olds, nursery_id: nursery.id, child_id: child.id, period_id: half_day_pm_period.id, term_id: term.id, date_attended: date)
          end        
          half_day_am_nofe_dates = [Date.new(2013,9,5),Date.new(2013,9,12),Date.new(2013,9,19),Date.new(2013,9,26)]
          half_day_am_nofe_dates.each do |date| 
            timesheet = FactoryGirl.create(:timesheet, :half_day_3_yr_olds, nursery_id: nursery.id, child_id: child.id, period_id: half_day_am_nofe_period.id, term_id: term.id, date_attended: date)
          end  
          children = carer.children
          session_discount = FactoryGirl.create(:discount, discountable_id: full_day_period.id, discountable_type: "Period") 
          account_discount = FactoryGirl.create(:discount, name: "Account Discount", amount: 10.00, discountable_id: account.id, discountable_type: "Account", applies_to: "Whole Invoice") 
          @invoice =  account.invoices.build(nursery_id: nursery.id, carer_id: carer.id, number: Invoice.next_number, issue: 1, state: "new", billing_period_id: billing_period.id)
          @line_items = @invoice.populate_line_items(account, children, billing_period)
          @invoice.save                    
        end

        describe "timesheets" do
          subject { Timesheet.count }
          it { should eq(17) }
        end
        
        describe "line items" do      
          subject { InvoiceLineItem.count }
          it { should eq(8) }
        end
        
        describe "charges" do
          subject { Charge.count }
          it { should eq(17) }
        end
        
        describe "line item" do
          subject { Invoice.last.invoice_line_items.order("id ASC") }
          context "1" do
            it "description" do            
              expect(subject[0].description).to eq( "Full Day 3 Year Olds Full Sessions")
            end
            it "quantity" do              
              expect(subject[0].quantity).to eq(5)
            end
            it "cost" do               
              expect(subject[0].cost).to eq(36.00)
            end
            it "total cost" do
              expect(subject[0].total_cost).to eq(108.00)
            end
          end
          context "2" do
            it "description" do            
              expect(subject[1].description).to eq("Half Day - AM 3 Year Olds FE Full Sessions")
            end
            it "quantity" do              
              expect(subject[1].quantity).to eq(4)
            end
            it "cost" do               
              expect(subject[1].cost).to eq(19.40)
            end
            it "total cost" do
              expect(subject[1].total_cost).to eq(19.4)
            end
          end
          context "3" do
            it "description" do            
              expect(subject[2].description).to eq("Half Day - PM 3 Year Olds Full Sessions")
            end
            it "quantity" do              
              expect(subject[2].quantity).to eq(4)
            end
            it "cost" do               
              expect(subject[2].cost).to eq(19.40)
            end
            it "total cost" do
              expect(subject[2].total_cost).to eq(19.40)
            end
          end 
          context "4" do
            it "description" do            
              expect(subject[3].description).to eq("Half Day - AM 3 Year Olds No FE Full Sessions")
            end
            it "quantity" do              
              expect(subject[3].quantity).to eq(4)
            end
            it "cost" do               
              expect(subject[3].cost).to eq(19.40)
            end
            it "total cost" do
              expect(subject[3].total_cost.to_f).to eq(77.60)
            end
          end  
          context "5" do
            it "description" do            
              expect(subject[4].description).to eq("Early Years Entitlement - 50 hours")
            end             
          end                   
        end          
      end
      Timecop.return
    end
  end  
end