require 'spec_helper'

describe "Term Pages" do
  let(:group) { FactoryGirl.create(:nursery_group) }  
  let(:user) { FactoryGirl.create(:user) }
  let(:role) { FactoryGirl.create(:role) } 
  let(:user_roles) { user.roles << role }
  let(:permission) { FactoryGirl.create(:permission, role_id: role.id) }
  let!(:local_authority) { FactoryGirl.create(:local_authority) }
  let!(:local_authority2) { FactoryGirl.create(:local_authority, name: "A different LA") }
  let(:region) { FactoryGirl.create(:region) }
  let(:contact_info) { FactoryGirl.create(:contact_info) }
  let!(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: group.id, region_id: region.id, local_authority_id: local_authority.id) }
  let!(:employment) { FactoryGirl.create(:employment, nursery_id: nursery.id, user_id: user.id) }    
  let!(:term) { FactoryGirl.create(:term, local_authority_id: local_authority.id) }
  let(:submit) { "Save" } 
  
  subject { page }
  
  describe "Terms Index Page" do
    context "for non-signed-in users" do
      before { get terms_path }
      specify { response.should redirect_to(root_path) }  
    end
    
    context "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit terms_path        
      end
      
      it { should have_selector('h1', text: 'Terms') }
      it { should have_selector('title', text: full_title('Terms')) } 
      it { should have_link('+ Term', href: new_term_path) }
      it { should have_selector('th', text: 'Local Authority') }
      it { should have_selector('th', text: 'Description') }
      it { should have_selector('th', text: 'Start Date') }
      it { should have_selector('th', text: 'End Date') }
      it { should have_selector('th', text: '...') }
      it { should have_selector('td', text: term.local_authority.name ) }
      it { should have_selector('td', text: term.description) }
      it { should have_selector('td', text: term.start_date.to_s) }   
      it { should have_selector('td', text: term.end_date.to_s) }
      it { should have_link('View', href: term_path(term)) }
      it { should have_link('Edit', href: edit_term_path(term)) } 
      it { should have_link('Delete', href: term_path(term)) }                           
    end     
  end

  describe "Terms New Page" do
    context "for non-signed-in users" do
      before { get new_term_path }
      specify { response.should redirect_to(root_path) } 
    end  
    
    context "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit new_term_path        
      end      
      
      it { should have_selector('h1', text: 'New Term') }
      it { should have_selector('title', text: full_title('New Term')) } 
      
      context "with valid information" do
        before do
          select "Lincolnshire",          from: 'term[local_authority_id]'
          fill_in 'term[description]',    with: "Term 2"
          select "October",               from: 'term[start_date(2i)]' 
          select "28",                    from: 'term[start_date(3i)]'
          select "2013",                  from: 'term[start_date(1i)]'
          select "December",              from: 'term[end_date(2i)]'  
          select "20",                    from: 'term[end_date(3i)]'
          select "2013",                  from: 'term[end_date(1i)]'             
        end
        
        it "should create a new term record" do
          expect { click_button submit }.to change(Term, :count).by(1)
        end
        
        it "should redirect to term index page" do
          click_button submit
          current_path.should == terms_path
        end
      end     

      context "with missing information" do
        let(:missing_local_authority) { "" }
        let(:missing_description) { "" }
        let(:missing_start_date) { "" }
        let(:missing_end_date) { "" }
        
        before do
          select missing_local_authority,          from: 'term[local_authority_id]'
          fill_in 'term[description]',             with: missing_description              
        end
        
        it "should not create a new term record" do
          expect { click_button submit }.not_to change(Term, :count)
        end

        it "should render new term page" do
          click_button submit
          current_path.should == terms_path
        end
        
        it { should have_selector('div', :id => "error_explanation") }
        it { should have_selector('div', :class => "control-group string required error") }          
      end
    end        
  end

  describe "Terms Edit Page" do
    context "for non-signed-in users" do
      before { get edit_term_path(term) }
      specify { response.should redirect_to(root_path) }  
    end  
    
    context "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit edit_term_path(term)        
      end  
      
      it { should have_selector('h1', text: 'Edit Term') }
      it { should have_selector('title', text: full_title('Edit Term')) }  
      
      context "with valid information" do
        let(:revised_local_authority) { local_authority2.name }
        let(:revised_description) { "Revised Term" }
        let(:revised_start_date_month) { "November" }
        let(:revised_start_date_month_integer) { 11 }
        let(:revised_start_date_day) { "1" }
        let(:revised_start_date_year) { "2014" }
        let(:revised_end_date_month) { "January" }
        let(:revised_end_date_month_integer) { 1 }
        let(:revised_end_date_day) { "1" }
        let(:revised_end_date_year) { "2015" }

        before do
          select revised_local_authority,   from: 'term[local_authority_id]'
          fill_in 'term[description]',      with: revised_description
          select revised_start_date_month,  from: 'term[start_date(2i)]' 
          select revised_start_date_day,    from: 'term[start_date(3i)]'
          select revised_start_date_year,   from: 'term[start_date(1i)]'
          select revised_end_date_month,    from: 'term[end_date(2i)]'  
          select revised_end_date_day,      from: 'term[end_date(3i)]'
          select revised_end_date_year,     from: 'term[end_date(1i)]'
          click_button submit             
        end        
        
        specify { term.reload.local_authority.name == local_authority2.name }
        specify { term.reload.description == revised_description }
        specify { term.reload.start_date == Date.new(revised_start_date_year.to_i,revised_start_date_month_integer,revised_start_date_day.to_i) }
        specify { term.reload.end_date == Date.new(revised_end_date_year.to_i,revised_end_date_month_integer,revised_end_date_day.to_i) }      
      
        it "should redirect to terms index page" do
          current_path.should == terms_path
        end
      end        

      context "with missing information" do
        let(:missing_local_authority) { "" }
        let(:missing_description) { "" }
        let(:missing_start_date) { "" }
        let(:missing_end_date) { "" }
        
        before do
          select missing_local_authority,          from: 'term[local_authority_id]'
          fill_in 'term[description]',             with: missing_description
          click_button submit             
        end   
        
        specify { term.reload.local_authority.name != missing_local_authority }
        specify { term.reload.description != missing_description } 
        
        it { should have_selector('div', :id => "error_explanation") }
        it { should have_selector('div', :class => "control-group string required error") }                 
           
        it "should redirect to edit term page" do
          current_path.should == term_path(term)
        end
      end
    end      
  end

  describe "Terms Show Page" do
    context "for non-signed-in users" do
      before { get term_path(term) }
      specify { response.should redirect_to(root_path) }  
    end
    
    context "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit term_path(term)        
      end 
      
      it { should have_selector('h1', text: 'Term') }
      it { should have_selector('title', text: full_title('Term')) } 
      it { should have_link('Edit', href: edit_term_path(term)) }
      it { should have_content(term.local_authority.name) }
      it { should have_content(term.description) }
      it { should have_content(term.start_date.strftime("%d %B %Y")) }
      it { should have_content(term.end_date.strftime("%d %B %Y")) }
       
    end
  end

  describe "Terms Destroy Page" do
    context "for non-signed-in users" do
      before { delete term_path(term) }
      specify { response.should redirect_to(root_path) } 
    end   
    
    context "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit terms_path      
      end 
      
      it "should display term index page" do
        current_path.should == terms_path
      end
      
      it { should have_link('Delete', href: term_path(term)) }
      
      it "should delete the term record" do
        expect { click_link('Delete'.to change(Term, :count)).by(-1) }
      end
      
      it "should redirect to term index page" do
        expect { response.to redirect_to(terms_path) } 
      end  
    end  
  end
end
