require 'spec_helper'

describe "ChildrenPages" do
  let(:group) { FactoryGirl.create(:nursery_group) }
  let(:user) { FactoryGirl.create(:user) }
  let(:role) { FactoryGirl.create(:role) } 
  let(:user_roles) { user.roles << role }
  let(:permission) { FactoryGirl.create(:permission, role_id: role.id) }
  let(:region) { FactoryGirl.create(:region) }
  let(:contact_info) { FactoryGirl.create(:contact_info) }
  let!(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: group.id, region_id: region.id) }
  let!(:employment) { FactoryGirl.create(:employment, nursery_id: nursery.id, user_id: user.id) }
  let!(:room) { FactoryGirl.create(:room, nursery_id: nursery.id, name: "Blue Room") } 
  let!(:room2) { FactoryGirl.create(:room, nursery_id: nursery.id, name: "Red Room") }  
  let!(:hair) { FactoryGirl.create(:hair) }
  let!(:hair2) { FactoryGirl.create(:hair, color: "Blond") }
  let!(:eye_color) { FactoryGirl.create(:eye_color) }
  let!(:eye_color2) { FactoryGirl.create(:eye_color, shade: "Brown") }
  let!(:religion) { FactoryGirl.create(:religion) }
  let!(:religion2) { FactoryGirl.create(:religion, name: "Muslim") }
  let!(:ethnicity) { FactoryGirl.create(:ethnicity) }
  let!(:ethnicity2) { FactoryGirl.create(:ethnicity, race: "Asian") }
  let!(:nationality) { FactoryGirl.create(:nationality) }
  let!(:nationality2) { FactoryGirl.create(:nationality, country: "American") }
  let!(:language) { FactoryGirl.create(:language) }
  let!(:language2) { FactoryGirl.create(:language, tongue: "Spanish") }
  let!(:status) { FactoryGirl.create(:status_type) }
  let!(:status2) { FactoryGirl.create(:status_type, status_name: "On Waiting List") }
  let!(:carer) { FactoryGirl.create(:carer, nursery_id: nursery.id) }   
  let!(:child) { FactoryGirl.create(:child, 
                                    nursery_id: nursery.id,
                                    carer_id: carer.id,
                                    room_id: room.id, 
                                    status_type_id: status.id, 
                                    preferred_name: "Preferred Name", 
                                    hair_id: hair.id,
                                    eye_color_id: eye_color.id,
                                    religion_id: religion.id,
                                    ethnicity_id: ethnicity.id,
                                    nationality_id: nationality.id,
                                    language_id: language.id ) }
  let!(:child_relationship_type) { FactoryGirl.create(:child_relationship_type) }
  let!(:child_relationship) { FactoryGirl.create(:child_relationship, 
                                                child_id: child.id, 
                                                carer_id: carer.id, 
                                                child_relationship_type_id: child_relationship_type.id,
                                                pick_up_flag: true,
                                                drop_off_flag: true) }
  

  let(:submit) { "Save" } 
  
  subject { page }
  
  describe "Children Index Page" do
    describe "for non-signed-in users" do
      before { get children_path }
      specify { response.should redirect_to(root_path) }  
    end 
    
    describe "for signed-in user" do
      before do        
        sign_in_user(user, user_roles, permission) 
        visit children_path 
      end
      
      it { should have_selector('h1', text: 'Nursery Children') }
      it { should have_selector('title', text: full_title('Nursery Children')) }
      it { should have_link('+ Child', href: new_child_path) }
      it { should have_selector('th', text: 'Name') }
      it { should have_selector('th', text: 'Room') }
      it { should have_selector('th', text: 'Age')  }
      it { should have_selector('th', text: 'Status') }
      it { should have_selector('th', text: '...') } 
      it { should have_content(child.full_name) }
      it { should have_content(child.room.name) }
      it { should have_content(child.age) }
      it { should have_content(child.status_type.status_name) }
      it { should have_link('View', href: child_path(child)) }
      it { should have_link('Edit', href: edit_child_path(child)) }
      it { should have_link('Delete', href: child_path(child)) }
    end    
  end

  describe "Child New Page" do
    describe "for non-signed-in users" do
      before { get new_child_path }
      specify { response.should redirect_to(root_path) } 
    end 
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit new_child_path    
      end      
      
      it { should have_selector('h1', text: 'Add a child') }
      it { should have_selector('title', text: full_title('Add Child')) }  
      
      describe "with valid information" do
        before do
          fill_in 'child[first_name]',                              with: "FirstName"
          fill_in 'child[middle_name]',                             with: "MiddleName"
          fill_in 'child[surname]',                                 with: "Surname"
          fill_in 'child[preferred_name]',                          with: "PreferredName"
          select 'male',                                            from: 'child[gender_id]'
          select 'September',                                       from: 'child[date_of_birth(2i)]'
          select '7',                                               from: 'child[date_of_birth(3i)]'
          select '2011',                                            from: 'child[date_of_birth(1i)]' 
          fill_in 'child[contact_info_attributes][address_line_1]', with: "Address Line 1" 
          fill_in 'child[contact_info_attributes][address_line_2]', with: "Address Line 2"
          fill_in 'child[contact_info_attributes][address_line_3]', with: "Address Line 3" 
          fill_in 'child[contact_info_attributes][town]',           with: "Faringdon"
          fill_in 'child[contact_info_attributes][county]',         with: "Oxon"
          fill_in 'child[contact_info_attributes][post_code]',      with: "SN7 7BP"
          fill_in 'child[contact_info_attributes][telephone_1]',    with: "01522511444"
          fill_in 'child[contact_info_attributes][email_1]',        with: "mynursery@example.com"
          select 'Registered',                                      from: 'child[status_type_id]'          
          select "Blue Room",                                       from: 'child[room_id]' 
          select 'January',                                         from: 'child[start_date(2i)]'
          select '1',                                               from: 'child[start_date(3i)]'
          select '2013',                                            from: 'child[start_date(1i)]'
          select 'September',                                       from: 'child[leave_date(2i)]'
          select '1',                                               from: 'child[leave_date(3i)]'
          select '2016',                                            from: 'child[leave_date(1i)]'
          fill_in 'child[AEN]',                                     with: 'Gifted & Talented'
          select 'Brown',                                           from: 'child[hair_id]'  
          select 'Blue',                                            from: 'child[eye_color_id]'
          select 'Christian',                                       from: 'child[religion_id]'
          select 'Caucasian',                                       from: 'child[ethnicity_id]'
          select 'British',                                         from: 'child[nationality_id]' 
          select 'British',                                         from: 'child[language_id]'                              
        end 
        
        it "should create a new child" do
          expect { click_button submit }.to change(Child, :count).by(1) 
        end
        
        it "should create a new contact_info record" do
          expect { click_button submit }.to change(ContactInfo, :count).by(1)          
        end         
      end     
    
      describe "with missing information" do
        let(:new_first_name) { "" }
        let(:new_middle_name) { "" }
        let(:new_surname) { "" }
        let(:new_preferred_name) { "" }
        let(:new_add_1) { "" }
        let(:new_add_2) { "" }
        let(:new_add_3) { "" }
        let(:new_town) { "" }
        let(:new_county) { "" }
        let(:new_post_code) { "" }
        let(:new_telephone) { "" }
        let(:new_email) { "" }
        before do
          fill_in 'child[first_name]',                              with: new_first_name
          fill_in 'child[middle_name]',                             with: new_middle_name
          fill_in 'child[surname]',                                 with: new_surname
          fill_in 'child[preferred_name]',                          with: new_preferred_name 
          select 'male',                                            from: 'child[gender_id]'
          select 'September',                                       from: 'child[date_of_birth(2i)]'
          select '7',                                               from: 'child[date_of_birth(3i)]'
          select '2011',                                            from: 'child[date_of_birth(1i)]' 
          fill_in 'child[contact_info_attributes][address_line_1]', with: new_add_1 
          fill_in 'child[contact_info_attributes][address_line_2]', with: new_add_2
          fill_in 'child[contact_info_attributes][address_line_3]', with: new_add_3 
          fill_in 'child[contact_info_attributes][town]',           with: new_town
          fill_in 'child[contact_info_attributes][county]',         with: new_county
          fill_in 'child[contact_info_attributes][post_code]',      with: new_post_code
          fill_in 'child[contact_info_attributes][telephone_1]',    with: new_telephone
          fill_in 'child[contact_info_attributes][email_1]',        with: new_email
          select 'Registered',                                      from: 'child[status_type_id]'          
          select "Blue Room",                                       from: 'child[room_id]' 
          select 'January',                                         from: 'child[start_date(2i)]'
          select '1',                                               from: 'child[start_date(3i)]'
          select '2013',                                            from: 'child[start_date(1i)]'
          select 'September',                                       from: 'child[leave_date(2i)]'
          select '1',                                               from: 'child[leave_date(3i)]'
          select '2016',                                            from: 'child[leave_date(1i)]'
          fill_in 'child[AEN]',                                     with: 'Gifted & Talented'
          select 'Brown',                                           from: 'child[hair_id]'  
          select 'Blue',                                            from: 'child[eye_color_id]'
          select 'Christian',                                       from: 'child[religion_id]'
          select 'Caucasian',                                       from: 'child[ethnicity_id]'
          select 'British',                                         from: 'child[nationality_id]' 
          select 'British',                                         from: 'child[language_id]'                 
        end
        
        it "should not create a new child" do
          expect { click_button submit }.not_to change(Child, :count)
        end

        it { should have_selector('div', :id => "error_explanation") }
        it { should have_selector('div', :class => "control-group string required error") }
                
        it "should redirect to new child page" do
          current_path.should == new_child_path
        end         
      end
    end       
  end

  describe "Child Edit Page" do
    describe "for non-signed-in users" do
      before { get edit_child_path(child) }
      specify { response.should redirect_to(root_path) } 
    end     
    
    describe "for signed-in users" do
       before do
        sign_in_user(user, user_roles, permission)
        visit edit_child_path(child)    
      end     
      
      it { should have_selector('h1', text: 'Edit Child') }
      it { should have_selector('title', text: full_title('Edit Child')) } 
      
      describe "with valid information" do
        let(:revised_first_name) { "RevisedFirstName" }
        let(:revised_middle_name) { "RevisedMiddleName" }
        let(:revised_surname) { "RevisedSurname" }
        let(:revised_preferred_name) { "RevisedPreferredName" }
        let(:revised_add_1) { "Revised Address 1" }
        let(:revised_add_2) { "Revised Address 2" }
        let(:revised_add_3) { "Revised Address 3" }
        let(:revised_town) { "Revised Town" }
        let(:revised_county) { "Revised County" }
        let(:revised_post_code) { "Revised PostCode" }
        let(:revised_telephone) { "01522511445" }
        let(:revised_email) { "revisednursery@example.com" }
        let(:revised_status) { "On Waiting List" }
        let(:revised_gender) { "female" }
        let(:revised_month) { "October" }
        let(:revised_day) { "8" }
        let(:revised_year) { "2012" }
        let(:revised_room) { "Red Room" }
        let(:revised_AEN) { "Revised AEN" }
        let(:revised_hair) { "Blond" }
        let(:revised_eyes) { "Brown" }
        let(:revised_religion) { "Muslim" }
        let(:revised_ethnicity) { "Asian" }
        let(:revised_nationality) { "American" }
        let(:revised_language) { "Spanish" }
        let(:revised_date) { Date.new(2012,10,8) }
        before do
          fill_in 'child[first_name]',                              with: revised_first_name
          fill_in 'child[middle_name]',                             with: revised_middle_name
          fill_in 'child[surname]',                                 with: revised_surname
          fill_in 'child[preferred_name]',                          with: revised_preferred_name 
          select revised_gender,                                    from: 'child[gender_id]'
          select revised_month,                                     from: 'child[date_of_birth(2i)]'
          select revised_day,                                       from: 'child[date_of_birth(3i)]'
          select revised_year,                                      from: 'child[date_of_birth(1i)]' 
          fill_in 'child[contact_info_attributes][address_line_1]', with: revised_add_1 
          fill_in 'child[contact_info_attributes][address_line_2]', with: revised_add_2
          fill_in 'child[contact_info_attributes][address_line_3]', with: revised_add_3 
          fill_in 'child[contact_info_attributes][town]',           with: revised_town
          fill_in 'child[contact_info_attributes][county]',         with: revised_county
          fill_in 'child[contact_info_attributes][post_code]',      with: revised_post_code
          fill_in 'child[contact_info_attributes][telephone_1]',    with: revised_telephone
          fill_in 'child[contact_info_attributes][email_1]',        with: revised_email
          select revised_status,                                    from: 'child[status_type_id]'
          select revised_room,                                      from: 'child[room_id]' 
          select revised_month,                                     from: 'child[start_date(2i)]'
          select revised_day,                                       from: 'child[start_date(3i)]'
          select revised_year,                                      from: 'child[start_date(1i)]'
          select revised_month,                                     from: 'child[leave_date(2i)]'
          select revised_day,                                       from: 'child[leave_date(3i)]'
          select revised_year,                                      from: 'child[leave_date(1i)]'
          fill_in 'child[AEN]',                                     with: revised_AEN
          select revised_hair,                                      from: 'child[hair_id]'  
          select revised_eyes,                                      from: 'child[eye_color_id]'
          select revised_religion,                                  from: 'child[religion_id]'
          select revised_ethnicity,                                 from: 'child[ethnicity_id]'
          select revised_nationality ,                              from: 'child[nationality_id]' 
          select revised_language,                                  from: 'child[language_id]'  
          click_button submit               
        end        
        
        specify { child.reload.first_name == revised_first_name }
        specify { child.reload.middle_name == revised_middle_name }
        specify { child.reload.surname == revised_surname }
        specify { child.reload.preferred_name == revised_preferred_name }
        specify { child.reload.gender_id == revised_gender }
        specify { child.reload.date_of_birth == revised_date } 
        specify { child.reload.status_type_id == revised_status } 
        specify { child.reload.room_id == room2.id }
        specify { child.reload.AEN == revised_AEN }                                      
        specify { child.reload.hair_id == hair2.id }   
        specify { child.reload.eye_color_id == eye_color2.id }          
        specify { child.reload.religion_id == religion2.id }
        specify { child.reload.ethnicity_id == ethnicity2.id }
        specify { child.reload.nationality_id == nationality2.id }
        specify { child.reload.language_id == language2.id }                                                           
      
        it "should redirect to nursery child index page" do
          current_path.should == children_path
        end      
      end         

      describe "missing information" do
        let(:new_first_name) { "" }
        let(:new_middle_name) { "" }
        let(:new_surname) { "" }
        let(:new_preferred_name) { "" }
        let(:new_add_1) { "" }
        let(:new_add_2) { "" }
        let(:new_add_3) { "" }
        let(:new_town) { "" }
        let(:new_county) { "" }
        let(:new_post_code) { "" }
        let(:new_telephone) { "" }
        let(:new_email) { "" }
        before do
          fill_in 'child[first_name]',                              with: new_first_name
          fill_in 'child[middle_name]',                             with: new_middle_name
          fill_in 'child[surname]',                                 with: new_surname
          fill_in 'child[preferred_name]',                          with: new_preferred_name 
          select 'male',                                            from: 'child[gender_id]'
          select 'September',                                       from: 'child[date_of_birth(2i)]'
          select '7',                                               from: 'child[date_of_birth(3i)]'
          select '2011',                                            from: 'child[date_of_birth(1i)]' 
          fill_in 'child[contact_info_attributes][address_line_1]', with: new_add_1 
          fill_in 'child[contact_info_attributes][address_line_2]', with: new_add_2
          fill_in 'child[contact_info_attributes][address_line_3]', with: new_add_3 
          fill_in 'child[contact_info_attributes][town]',           with: new_town
          fill_in 'child[contact_info_attributes][county]',         with: new_county
          fill_in 'child[contact_info_attributes][post_code]',      with: new_post_code
          fill_in 'child[contact_info_attributes][telephone_1]',    with: new_telephone
          fill_in 'child[contact_info_attributes][email_1]',        with: new_email
          select "Blue Room",                                       from: 'child[room_id]' 
          select 'January',                                         from: 'child[start_date(2i)]'
          select '1',                                               from: 'child[start_date(3i)]'
          select '2013',                                            from: 'child[start_date(1i)]'
          select 'September',                                       from: 'child[leave_date(2i)]'
          select '1',                                               from: 'child[leave_date(3i)]'
          select '2016',                                            from: 'child[leave_date(1i)]'
          fill_in 'child[AEN]',                                     with: 'Gifted & Talented'
          select 'Brown',                                           from: 'child[hair_id]'  
          select 'Blue',                                            from: 'child[eye_color_id]'
          select 'Christian',                                       from: 'child[religion_id]'
          select 'Caucasian',                                       from: 'child[ethnicity_id]'
          select 'British',                                         from: 'child[nationality_id]' 
          select 'British',                                         from: 'child[language_id]'                 
        end

        specify { child.reload.first_name != new_first_name }
        specify { child.reload.middle_name != new_middle_name }        
        specify { child.reload.surname != new_surname }
        specify { child.reload.preferred_name != new_preferred_name }       

        it { should have_selector('div', :id => "error_explanation") }
        it { should have_selector('div', :class => "control-group string required error") }
                
        it "should redirect to edit nursery page" do
          current_path.should == edit_child_path(child)
        end                 
      end
    end
  end

  describe "Child Show Page" do
    describe "for non-signed-in users" do
      before { get child_path(child) }
      specify { response.should redirect_to(root_path) } 
    end 
    
    describe "for signed-in users" do
       before do
        sign_in_user(user, user_roles, permission)
        visit child_path(child)    
      end    
      
      it { should have_selector('h1', text: "#{'Child: '}#{child.full_name}") }
      it { should have_selector('title', text: full_title('Child')) }
      it { should have_link('Back to List', href: children_path) }
      it { should have_link('Summary', href: child_path(child)) }  
      it { should have_link('Personal', href: detail_child_path(child)) }
      it { should have_link('Time & Attendance', href: child_bookings_path(child)) }
      it { should have_link('Learning Journey', href: '#') }
      it { should have_link('Finance', href: '#') } 
      it { should have_link('Notes', href: '#') }  
      it { should have_selector('h2', text: 'Child Summary View') }          
      it { should have_content("#{child.full_name} #{'('}#{child.preferred_name}#{')'}") }
      it { should have_content("#{'Status: '}")}
      it { should have_content("#{child.status_type.status_name}") }
      it { should have_content("#{'Account: '}") }
      it { should have_content("#{'Account number goes here'}")}
      it { should have_content("#{child.gender_id}") }
      it { should have_content("#{child.age} #{'years'} #{'('}#{child.age_in_months} #{'months'}#{')'}") }      
      it { should have_selector('h3', text: 'Related Carers') }
      it { should have_content("Staff Member full name goes here") }  
      it { should have_content("Staff Member role goes here") }
      it { should have_content(child.carer.display_name) }
      it { should have_content(child.carer.relationship(child.carer, child)) }
      it { should have_content(child.carer.contact_info.telephone_1) }
    end   
  end

  describe "Child Detail Page" do
    describe "for non-signed-in users" do
      before { get detail_child_path(child) }
      specify { response.should redirect_to(root_path) } 
    end     
    
    describe "for signed-in users" do
       before do
        sign_in_user(user, user_roles, permission)
        visit detail_child_path(child)  
      end      
      
      it { should have_selector('h1', text: "#{'Child: '}#{child.full_name}") }
      it { should have_selector('title', text: full_title('Child Details')) }
      it { should have_link('Back to List', href: children_path) }
      it { should have_link('Summary', href: child_path(child)) }  
      it { should have_link('Personal', href: detail_child_path(child)) }
      it { should have_link('Time & Attendance', href: child_bookings_path(child)) }
      it { should have_link('Learning Journey', href: '#') }
      it { should have_link('Finance', href: '#') } 
      it { should have_link('Notes', href: '#') }     
      it { should have_selector('h2', text: 'Child Detail View') }   
      it { should have_content("#{child.preferred_name}") } 
      it { should have_selector('h3', text: 'Contact Information') }  
      it { should have_selector('h3', text: 'Nursery Information') }
      it { should have_content("#{child.date_of_birth}") }  
      it { should have_content("#{child.contact_info.address_line_1}") }
      it { should have_content("#{child.start_date}") } 
      it { should have_content("#{child.contact_info.town}") }
      it { should have_content("#{child.leave_date}") }   
      it { should have_content("#{child.contact_info.post_code}") }
      it { should have_content("#{'Act. Leave:'}") }
      it { should have_content("#{child.contact_info.telephone_1}") }
      it { should have_content("#{child.contact_info.telephone_2}") }
      it { should have_selector('h3', text: 'Identifying Information') }
      it { should have_content("#{child.gender_id}") }   
      it { should have_content("#{child.AEN}") } 
      it { should have_content("#{child.hair.color}") } 
      it { should have_content("#{child.eye_color.shade}") }
      it { should have_content("#{child.religion.name}") } 
      it { should have_content("#{child.ethnicity.race}") }
      it { should have_content("#{child.nationality.country}") }
      it { should have_content("#{child.language.tongue}") }
      it { should have_link('Edit', href: edit_child_path(child)) } 
      it { should have_link('Add/Change Photo', href: "#") }           
    end
  end
end
