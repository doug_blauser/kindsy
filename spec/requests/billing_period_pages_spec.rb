require 'spec_helper'

describe "BillingPeriodPages" do
  let(:group) { FactoryGirl.create(:nursery_group) }
  let(:user) { FactoryGirl.create(:user) }
  let(:role) { FactoryGirl.create(:role) } 
  let(:user_roles) { user.roles << role }
  let(:permission) { FactoryGirl.create(:permission, role_id: role.id) }  
  let(:region) { FactoryGirl.create(:region) }  
  let!(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: group.id, region_id: region.id) }
  let!(:employment) { FactoryGirl.create(:employment, nursery_id: nursery.id, user_id: user.id) }
  let!(:account) { FactoryGirl.create(:account, nursery_id: nursery.id, user_id: user.id) }  
  let!(:carer) { FactoryGirl.create(:carer, nursery_id: nursery.id) } 
  let!(:accountability) { FactoryGirl.create(:accountability, account_id: account.id, carer_id: carer.id) }   
  let!(:status) { FactoryGirl.create(:status_type) }    
  let!(:child) { FactoryGirl.create(:child, nursery_id: nursery.id, status_type_id: status.id, account_id: account.id) } 
  let!(:child2) { FactoryGirl.create(:child, nursery_id: nursery.id, status_type_id: status.id, account_id: account.id, first_name: "Duce") }
  let!(:child_relationship_type) { FactoryGirl.create(:child_relationship_type) }
  let!(:child_relationship) { FactoryGirl.create(:child_relationship, child_id: child.id, carer_id: carer.id, child_relationship_type_id: child_relationship_type.id, pick_up_flag: true, drop_off_flag: true) }       
  let!(:child_relationship2) { FactoryGirl.create(:child_relationship, child_id: child2.id, carer_id: carer.id, child_relationship_type_id: child_relationship_type.id, pick_up_flag: true, drop_off_flag: true) }
  let!(:period) { FactoryGirl.create(:period, nursery_id: nursery.id) }
  let!(:period2) { FactoryGirl.create(:period, nursery_id: nursery.id, period_name: "PM") } 
  let!(:booking) { FactoryGirl.create(:booking, nursery_id: nursery.id, child_id: child.id, period_id: period.id, start_date: Date.today.beginning_of_month-1.month, end_date: Date.today.end_of_month+3.months, days_of_week: ["Monday", "Wednesday", "Tuesday"]) }
  let!(:child1_am_hourly_price1) { FactoryGirl.create(:timesheet, nursery_id: nursery.id,
                                                                child_id: child.id,
                                                                period_id: period.id,
                                                                date_attended: Date.today,
                                                                hourly_cost: period.period_hourly_cost,
                                                                period_cost: period.period_cost,
                                                                full_period: false) }                                                    
  let!(:child1_pm_hourly_price1) { FactoryGirl.create(:timesheet, nursery_id: nursery.id,
                                                                 child_id: child.id,
                                                                 period_id: period2.id,
                                                                 date_attended: Date.today,
                                                                 hourly_cost: period.period_hourly_cost,
                                                                 period_cost: period.period_cost, 
                                                                 full_period: false) }                                                     
  let!(:child2_am_hourly_price1) { FactoryGirl.create(:timesheet, nursery_id: nursery.id,
                                                                 child_id: child2.id,
                                                                 period_id: period.id,
                                                                 date_attended: Date.today,
                                                                 hourly_cost: period.period_hourly_cost,
                                                                 period_cost: period.period_cost,
                                                                 full_period: false) }
  let!(:child2_am_full_price1) { FactoryGirl.create(:timesheet, nursery_id: nursery.id,
                                                               child_id: child2.id,
                                                               period_id: period.id,
                                                               duration: 23400,
                                                               date_attended: Date.today,
                                                               hourly_cost: period.period_hourly_cost,
                                                               period_cost: period.period_cost, 
                                                               full_period: true) }  
  let!(:child1_am_hourly_price2) { FactoryGirl.create(:timesheet, nursery_id: nursery.id,
                                                                child_id: child.id,
                                                                period_id: period.id,
                                                                date_attended: Date.today,
                                                                hourly_cost: 7.75,
                                                                period_cost: 35.00,
                                                                full_period: false) } 
  let!(:child1_am_full_price2) { FactoryGirl.create(:timesheet, nursery_id: nursery.id,
                                                                       child_id: child.id,
                                                                       period_id: period.id,
                                                                       duration: 23400,
                                                                       date_attended: Date.today,
                                                                       hourly_cost: 7.75,
                                                                       period_cost: 35.00,
                                                                       full_period: true) }      
  let!(:billing_period) { FactoryGirl.create(:billing_period, nursery_id: nursery.id) }
  let!(:current_billing_period) { FactoryGirl.create(:billing_period, nursery_id: nursery.id, start_date: Date.today.beginning_of_month, end_date: Date.today.end_of_month) }
  let!(:invoice) { FactoryGirl.create(:invoice, nursery_id: nursery.id, carer_id: carer.id, account_id: account.id, billing_period_id: billing_period.id) }                                                                                                          
  let!(:line_item) { FactoryGirl.create(:invoice_line_item, invoice_id: invoice.id, account_id: account.id, child_id: child.id) }   
  
  let(:submit) { "Save" }
  
  subject { page }
  
  describe "Billing Period New Page" do
    describe "for non-signed-in users" do
      before { get new_billing_period_path }
      specify { response.should redirect_to(root_path) }
    end    
  end
  
  describe "Billing Period Index Page" do
    describe "for non-signed-in users" do
      before { get billing_periods_path }
      specify { response.should redirect_to(root_path) }
    end  
    
    describe "for signed-in users" do
      before do 
        sign_in_user(user, user_roles, permission)
        visit billing_periods_path
      end
      
      it { should have_selector('h1', text: "Billing List View") }
      it { should have_selector('title', text: full_title("Billing List View")) }
      it { should have_link('+ Billing Period', href: new_billing_period_path) }
      it { should have_selector('th', text: "") }
      it { should have_selector('th', text: "Date Range") }
      it { should have_selector('th', text: "Status") }
      it { should have_selector('th', text: "# Invoices") }
      it { should have_selector('th', text: "...") }
      it { should have_selector('td', text: billing_period.id.to_s) }
      it { should have_selector('td', text: "#{billing_period.start_date.day.ordinalize} #{billing_period.start_date.strftime("%B")} #{'-'} #{billing_period.end_date.day.ordinalize} #{billing_period.start_date.strftime("%B, %Y")}")}
      it { should have_selector('td', text: billing_period.status) }
      it { should have_link("#{billing_period.invoices.count}", href: invoices_path(billing_period_id: billing_period.id)) }
      it { should have_link('View', href: billing_period_path(billing_period)) }
      it { should have_link('Edit', href: "#") }
    end         
  end

  describe "Billing Period Show Page" do
    describe "for non-signed-in users" do
      before { get billing_period_path(billing_period) }
      specify { response.should redirect_to(root_path) }
    end
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit billing_period_path(billing_period)        
      end
      
      it { should have_selector('title', text: full_title("Billing Period")) }
      it { should have_selector('h1', text: "Billing Period") }
      it { should have_link('Edit', href: edit_billing_period_path(billing_period)) }
      it { should have_content("#{billing_period.start_date}") }
      it { should have_content("#{billing_period.end_date}") }
      it { should have_content("#{billing_period.status}") }
    end
  end
  
  describe "Billing Period Edit Page" do
    describe "for non-signed-in users" do
      before { get edit_billing_period_path(billing_period) }
      specify { response.should redirect_to(root_path) }
    end
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)   
        visit edit_billing_period_path(billing_period)     
      end
      
      it { should have_selector('title', text: full_title('Edit Billing Period')) }
      it { should have_selector('h1', text: 'Edit Billing Period') }
      
      describe "with valid information" do
        let(:revised_start_date_year) { "2014" }
        let(:revised_start_date_month) { "July" }
        let(:revised_start_date_day) { "5" }
        let(:revised_status) { "closed" }
        before do
          select revised_start_date_year,     from: 'billing_period[start_date(1i)]'
          select revised_start_date_month,    from: 'billing_period[start_date(2i)]'
          select revised_start_date_day,      from: 'billing_period[start_date(3i)]'
          select revised_status,              from: 'billing_period[status]'
          click_button submit
        end
        
        specify { billing_period.reload.start_date.should == Date.new(2014,07,05) }
        specify { billing_period.reload.status.should == revised_status }
        
        it "should redirect to billing period index page" do
          current_path.should == billing_periods_path
        end   
      end

      describe "with missing information" do
        let(:missing_status) { "" }
        
        before do
          select missing_status,          from: 'billing_period[status]'
          click_button submit
        end
        
        specify { billing_period.reload.status.should == "open" }
        
        it "should redirect to billing period edit page" do
          current_path.should == billing_period_path(billing_period)
        end
        
        it { should have_selector('div', :id => "error_explanation") }
        it { should have_selector('div', :class => "control-group string required error") }         
      end
      
    end
  end
end
