require 'spec_helper'

describe "BookingPages" do
  let(:group) { FactoryGirl.create(:nursery_group) }
  let(:user) { FactoryGirl.create(:user) }
  let(:role) { FactoryGirl.create(:role) } 
  let(:user_roles) { user.roles << role }
  let(:permission) { FactoryGirl.create(:permission, role_id: role.id) }
  let(:region) { FactoryGirl.create(:region) }
  let(:contact_info) { FactoryGirl.create(:contact_info) }
  let!(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: group.id, region_id: region.id) }
  let!(:employment) { FactoryGirl.create(:employment, nursery_id: nursery.id, user_id: user.id) }
  let!(:room) { FactoryGirl.create(:room, nursery_id: nursery.id, name: "Blue Room") } 
  let!(:room2) { FactoryGirl.create(:room, nursery_id: nursery.id, name: "Red Room") }  
  let!(:open_hour) { FactoryGirl.create(:open_hour, nursery_id: nursery.id) }
  let!(:open_hour2) { FactoryGirl.create(:open_hour, nursery_id: nursery.id, day_of_week: "Tuesday") }
  let!(:period) { FactoryGirl.create(:period, nursery_id: nursery.id) }
  let!(:period2) { FactoryGirl.create(:period, nursery_id: nursery.id, period_name: "PM") }  
  let!(:hair) { FactoryGirl.create(:hair) }
  let!(:hair2) { FactoryGirl.create(:hair, color: "Blond") }
  let!(:eye_color) { FactoryGirl.create(:eye_color) }
  let!(:eye_color2) { FactoryGirl.create(:eye_color, shade: "Brown") }
  let!(:religion) { FactoryGirl.create(:religion) }
  let!(:religion2) { FactoryGirl.create(:religion, name: "Muslim") }
  let!(:ethnicity) { FactoryGirl.create(:ethnicity) }
  let!(:ethnicity2) { FactoryGirl.create(:ethnicity, race: "Asian") }
  let!(:nationality) { FactoryGirl.create(:nationality) }
  let!(:nationality2) { FactoryGirl.create(:nationality, country: "American") }
  let!(:language) { FactoryGirl.create(:language) }
  let!(:language2) { FactoryGirl.create(:language, tongue: "Spanish") }
  let!(:status) { FactoryGirl.create(:status_type) }
  let!(:status2) { FactoryGirl.create(:status_type, status_name: "On Waiting List") }
  let!(:child) { FactoryGirl.create(:child, 
                                    nursery_id: nursery.id,
                                    room_id: room.id, 
                                    status_type_id: status.id, 
                                    preferred_name: "Preferred Name", 
                                    hair_id: hair.id,
                                    eye_color_id: eye_color.id,
                                    religion_id: religion.id,
                                    ethnicity_id: ethnicity.id,
                                    nationality_id: nationality.id,
                                    language_id: language.id ) }
  let!(:booking) { FactoryGirl.create(:booking, nursery_id: nursery.id,
                                                child_id: child.id,
                                                period_id: period.id,
                                                start_date: Date.today,
                                                end_date: Date.new(2016,11,01),
                                                every: "Week",
                                                days_of_week: ["Monday", "Wednesday", "Friday"]) }
  let!(:bow) { Date.today.beginning_of_week }
  let!(:eow) { Date.today.end_of_week }                                                
                                                
                                                
  let(:submit) { "Save" }                                               
                                                
  subject { page }
  
  describe "Booking Index Page" do
    describe "for non-signed-in users" do
      before { get child_bookings_path(child) }
      specify { response.should redirect_to(root_path) }
    end
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit child_bookings_path(child)
      end
      
      it { should have_selector('h1', text: "#{'Child: '}#{child.full_name}") }
      it { should have_selector('title', text: full_title('Bookings')) }
      it { should have_link('Back to List', href: children_path) }
      it { should have_link('Summary', href: child_path(child)) }  
      it { should have_link('Personal', href: detail_child_path(child)) }
      it { should have_link('Time & Attendance', href: child_bookings_path(child)) }
      it { should have_link('Learning Journey', href: '#') }
      it { should have_link('Finance', href: '#') } 
      it { should have_link('Notes', href: '#') }     
      it { should have_selector('h3', text: 'Time and Attendance') } 
      it { should have_link('Booking Pattern', href: child_bookings_path(child)) }  
      it { should have_link('Timesheets', href: child_timesheets_path(child)) }   
      it { should have_selector('h3', text: 'Booking Pattern') }
      it { should have_content("#{bow.strftime('%A')} #{bow.day.ordinalize} #{'to'} #{(eow-2).strftime('%A')} #{(eow-2).day.ordinalize} #{eow.strftime('%B %Y')}")}        
      it { should have_link('This Week', href: "#") }
      it { should have_link('Previous', href: "#") }
      it { should have_link('Next', href: "#") }
      it { should have_selector('th', text: 'Day') }
      it { should have_selector('th', text: nursery.periods[0].period_name) }
      it { should have_selector('td', text: nursery.open_hours[0].day_of_week) }
      it { should have_selector('td', text: "this is where bookings go") }
      it { should have_link('+Add Session', href: new_child_booking_path(child)) }
      it { should have_link('+Add Preset', href: "#") }
      it { should have_selector('h3', text: 'Session Plan') }
      it { should have_selector('th', text: 'Session') }
      it { should have_selector('th', text: 'Days') }
      it { should have_selector('th', text: 'Repetition') }
      it { should have_selector('th', text: '...') }
      it { should have_selector('td', text: booking.period.period_name) }
      it { should have_selector('td', text: booking.days_of_week.map { |day| "#{booking.abbr_day(day)}#{' '}" }.join) }
      it { should have_selector('td', text: booking.every) }
      it { should have_link('View', href: child_booking_path(child, booking)) }
      it { should have_link('Edit', href: edit_child_booking_path(child, booking)) }
      it { should have_link('Delete', href: child_booking_path(child, booking)) }
    end
  end                                                

  describe "Booking New Page" do

    describe "for non-signed-in users" do
      before { get new_child_booking_path(child) }
      specify { response.should redirect_to(root_path) }
    end    
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit new_child_booking_path(child)
      end      
      
      it { should have_selector('h3', text: 'Booking Pattern - Add to session plan') }
      it { should have_selector('title', text: full_title('Add Booking')) }
      it { should have_selector('h3', text: 'Repeat Pattern') }
      
      describe "with valid information" do
        before do
          select "AM",                        from: 'booking[period_id]'
          select "June",                      from: 'booking[start_date(2i)]'
          select "15",                        from: 'booking[start_date(3i)]'
          select "2013",                      from: 'booking[start_date(1i)]'
          select "July",                      from: 'booking[end_date(2i)]'
          select "31",                        from: 'booking[end_date(3i)]'
          select "2016",                      from: 'booking[end_date(1i)]'
          check('booking_days_of_week_monday')
          select "Week",                      from: 'booking[every]'
        end
        
        it "should create a new booking record for the child" do
          expect { click_button submit}.to change(Booking, :count).by(1)
        end 
        
        it "should redirect to booking index page" do
          click_button submit
          current_path.should == child_bookings_path(child)
        end               
      end
 
      describe "with missing information" do
        let(:missing_period) { "" }
        let(:missing_every) { "" }
        
        before do
          select missing_period,              from: 'booking[period_id]'
          select "June",                      from: 'booking[start_date(2i)]'
          select "15",                        from: 'booking[start_date(3i)]'
          select "2013",                      from: 'booking[start_date(1i)]'
          select "July",                      from: 'booking[end_date(2i)]'
          select "31",                        from: 'booking[end_date(3i)]'
          select "2016",                      from: 'booking[end_date(1i)]'
          select missing_every,               from: 'booking[every]'   
        end     
        
        it "should not create a new booking record for the child" do
          expect { click_button submit}.not_to change(Booking, :count) 
        end  
        
        it "should redirect to child booking new page" do
          click_button submit
          current_path.should == child_bookings_path(child)
        end   
        
        it { should have_selector('div', :id => "error_explanation") }
        it { should have_selector('div', :class => "control-group string required error") }                   
      end 
    end
  end

  describe "Booking Edit Page" do
    describe "for non-signed-in users" do
      before { get edit_child_booking_path(child, booking) }
      specify { response.should redirect_to(root_path) }
    end 
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission) 
        visit edit_child_booking_path(child, booking)      
      end
      
      it { should have_selector('h3', text: 'Booking Pattern - Add to session plan') }
      it { should have_selector('title', text: full_title('Edit Booking')) } 
      
      describe "with valid information" do
        let(:revised_period) { "PM" }
        let(:revised_start_date_year) { "2014"}
        let(:revised_start_date_month) { "January" }
        let(:revised_start_date_day) { "5" }
        let(:revised_end_date_year) { "2017" }
        let(:revised_end_date_month) { "August" }
        let(:revised_end_date_day) { "17" }
        let(:revised_days_of_week) { 'booking_days_of_week_tuesday' }
        let(:revised_every) { "Month" }
        before do
          select revised_period,              from: 'booking[period_id]'
          select revised_start_date_month,    from: 'booking[start_date(2i)]'
          select revised_start_date_day,      from: 'booking[start_date(3i)]'
          select revised_start_date_year,     from: 'booking[start_date(1i)]'
          select revised_end_date_month,      from: 'booking[end_date(2i)]'
          select revised_end_date_day,        from: 'booking[end_date(3i)]'
          select revised_end_date_year,       from: 'booking[end_date(1i)]'
          check(revised_days_of_week)
          select revised_every,               from: 'booking[every]'
          click_button submit
        end
        
        specify { booking.reload.period_id.should == period2.id }
        specify { booking.reload.start_date.should == Date.new(2014,01,05) }
        specify { booking.reload.end_date.should == Date.new(2017,8,17) }
        specify { booking.reload.days_of_week.should == ["Monday", "Tuesday"] }
        specify { booking.reload.every.should == "Month" }
        
        it "should redirect to child booking index page" do
          current_path.should == child_bookings_path(child) 
        end
      end     

      describe "with missing information" do
        let(:missing_period) { "" }
        let(:missing_every) { "" }
        
        before do
          select missing_period,              from: 'booking[period_id]'
          select "June",                      from: 'booking[start_date(2i)]'
          select "15",                        from: 'booking[start_date(3i)]'
          select "2013",                      from: 'booking[start_date(1i)]'
          select "July",                      from: 'booking[end_date(2i)]'
          select "31",                        from: 'booking[end_date(3i)]'
          select "2016",                      from: 'booking[end_date(1i)]'
          select missing_every,               from: 'booking[every]'   
          click_button submit
        end     
        

        specify { booking.reload.period_id.should == period.id }
        specify { booking.reload.every.should == "Week" }                    
        
        it "should redirect to child booking edit page" do
          current_path.should == child_booking_path(child, booking)
        end   
        
        it { should have_selector('div', :id => "error_explanation") }
        it { should have_selector('div', :class => "control-group string required error") }                           
      end
    end   
  end

  describe "Booking Show Page" do
    describe "for non-signed-in users" do
      before { get child_booking_path(child, booking) }
      specify { response.should redirect_to(root_path) }
    end     
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission) 
        visit child_booking_path(child, booking)      
      end
      
      it { should have_selector('h3', text: 'Booking Pattern - session plan') }
      it { should have_selector('title', text: full_title('Booking Pattern')) }
      it { should have_link('Edit', href: edit_child_booking_path(child, booking)) }
      it { should have_content(booking.period.period_name) }
      it { should have_content(booking.start_date.strftime('%d-%m-%Y')) }
      it { should have_content(booking.end_date.strftime('%d-%m-%Y')) } 
      it { should have_content(booking.days_of_week.map { |day| "#{booking.abbr_day(day)}#{' '}" }.join) }   
      it { should have_content(booking.every) }        
    end
  end

  describe "Booking Delete Page" do
    describe "for non-signed-in users" do
      before { delete child_booking_path(child, booking) }
      specify { response.should redirect_to(root_path) } 
    end  
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit child_bookings_path(child)
      end      
      
      it "should display child bookings index page" do
        current_path.should == child_bookings_path(child)
      end
      
      it { should have_link('Delete', href: child_booking_path(child, booking)) }
      
      it "should delete the child booking record" do
        expect { click_link('Delete'.to change(Booking, :count)).by(-1) }
        current_path.should == child_bookings_path(child)
      end      
    end  
  end
end
