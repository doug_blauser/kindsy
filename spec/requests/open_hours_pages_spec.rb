require 'spec_helper'

describe "OpenHoursPages" do
  let(:group) { FactoryGirl.create(:nursery_group) }
  let(:user) { FactoryGirl.create(:user) }
  let(:role) { FactoryGirl.create(:role) } 
  let(:user_roles) { user.roles << role }
  let(:permission) { FactoryGirl.create(:permission, role_id: role.id) }
  let(:region) { FactoryGirl.create(:region) }
  let(:contact_info) { FactoryGirl.create(:contact_info) }
  let!(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: group.id, region_id: region.id) }
  let!(:employment) { FactoryGirl.create(:employment, nursery_id: nursery.id, user_id: user.id) }  
  let!(:open_hour) { FactoryGirl.create(:open_hour, nursery_id: nursery.id, open_time: Time.new(2000,01,01,06,00,00), close_time: Time.new(2000,01,01,19,30,00)) }

  let(:submit) { "Save" }
  
  subject { page }
  
  describe "Open Hours New Page" do
    describe "for non-signed-in users" do
      before { get new_open_hour_path }
      specify { response.should redirect_to(root_path) } 
    end
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit new_open_hour_path
      end
      
      it { should have_selector('h1', text: 'Add Nursery Open Hours') }
      it { should have_selector('title', text: full_title('Add Open Hours')) }
   
      describe "with valid information" do
        before do
          select "Monday",                    from: 'open_hour[day_of_week]'
          select "06",                        from: 'open_hour[open_time(4i)]'
          select "45",                        from: 'open_hour[open_time(5i)]'
          select "18",                        from: 'open_hour[close_time(4i)]'
          select "30",                        from: 'open_hour[close_time(5i)]'
        end
        
        it "should create a new open hour record for the nursery" do
          expect { click_button submit}.to change(OpenHour, :count).by(1)
        end
        
        it "should redirect to nursery room index page" do
          click_button submit
          current_path.should == open_hours_path
        end        
      end
      
      describe "with missing information" do
        let(:missing_day_of_week) { "" }
        let(:missing_open_time_hour) { "" }
        let(:missing_open_time_minutes) { "" }
        let(:missing_close_time_hour) { "" }
        let(:missing_close_time_minutes) { "" }
        
        before do
          select missing_day_of_week,         from: 'open_hour[day_of_week]'
          select "00",                        from: 'open_hour[open_time(4i)]'
          select "00",                        from: 'open_hour[open_time(5i)]'
          select "00",                        from: 'open_hour[close_time(4i)]'
          select "00",                        from: 'open_hour[close_time(5i)]'
        end
          
        it "should redirect to nursery open hours new page" do
          click_button submit
          current_path.should == open_hours_path
        end   
        
        it { should have_selector('div', :id => "error_explanation") }
        it { should have_selector('div', :class => "control-group string required error") }              
      end
    end      
  end 

  describe "Open Hours Edit Page" do
    describe "for non-signed-in users" do
      before { get edit_open_hour_path(open_hour) }
      specify { response.should redirect_to(root_path) } 
    end    
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit edit_open_hour_path(open_hour)
      end    
      
      it { should have_selector('h1', text: 'Edit Nursery Open Hours') }
      it { should have_selector('title', text: full_title('Edit Nursery Open Hours')) }                 
      
      describe "with valid information" do
        let(:revised_day_of_week) { "Tuesday"}
        let(:revised_open_time_hours) { "05" }
        let(:revised_open_time_minutes) { "15" }
        let(:revised_close_time_hours) { "19" }
        let(:revised_close_time_minutes) { "15" }
        before do
          select revised_day_of_week,         from: 'open_hour[day_of_week]'
          select revised_open_time_hours,     from: 'open_hour[open_time(4i)]'
          select revised_open_time_minutes,   from: 'open_hour[open_time(5i)]'
          select revised_close_time_hours,    from: 'open_hour[close_time(4i)]'
          select revised_close_time_minutes,  from: 'open_hour[close_time(5i)]'
          click_button submit
        end
        
        specify { open_hour.reload.day_of_week.should == revised_day_of_week }
        specify { open_hour.reload.open_time.should == Time.utc(2000,01,01,revised_open_time_hours,revised_open_time_minutes) }
        specify { open_hour.reload.close_time.should == Time.utc(2000,01,01,revised_close_time_hours,revised_close_time_minutes) }
      
        it "should redirect to nursery index page" do
          current_path.should == open_hours_path
        end       
      end           
    end
    
  end

  describe "Open Hours Index Page" do
    describe "for non-signed-in users" do
      before { get open_hours_path }
      specify { response.should redirect_to(root_path) } 
    end    
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit open_hours_path
      end      
      
      it { should have_selector('h1', text: 'Opening Times') }
      it { should have_selector('title', text: full_title('Nursery Opening Times'))}
      it { should have_link('S') }
      it { should have_link('M') }
      it { should have_link('T') }
      it { should have_link('W') }
      it { should have_link('F') }
      it { should have_content('Day') }
      it { should have_content('Time Range') }
      it { should have_content('...') }
      it { should have_content(nursery.open_hours[0].day_of_week) } 
      it { should have_content("#{nursery.open_hours[0].open_time.strftime("%I:%M %P")} #{'-'} #{nursery.open_hours[0].close_time.strftime("%I:%M %P")}") }
    end
  end

  describe "Delete Open Hours Page" do
    describe "for non-signed-in users" do
      before { delete open_hour_path(open_hour) }
      specify { response.should redirect_to(root_path) } 
    end    
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit open_hours_path
      end 
      
      it "should display nursery index page" do
        current_path.should == open_hours_path
      end   
       
      it { should have_link('Delete', href: open_hour_path(open_hour))}        

      it "should delete the open_hour record" do
        expect { click_link('Delete'.to change(OpenHour, :count)).by(-1) } 
      end
    end    
  end

  describe "Show Open Hours Page" do
    describe "for non-signed-in users" do
      before { get open_hour_path(open_hour) }
      specify { response.should redirect_to(root_path) } 
    end
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit open_hour_path(open_hour)
      end       
      
      it { should have_selector('h1', text: 'Nursery Open Hours') }
      it { should have_selector('title', text: 'Nursery Open Hours') }                 
      it { should have_content(nursery.open_hours[0].day_of_week) }
      it { should have_content(nursery.open_hours[0].open_time.strftime("%I:%M %P")) }
      it { should have_content(nursery.open_hours[0].close_time.strftime("%I:%M %P")) }
      it { should have_link('Edit') }
    end      
  end
end
