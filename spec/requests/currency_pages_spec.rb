require 'spec_helper'

describe "CurrencyPages" do
  let(:group) { FactoryGirl.create(:nursery_group) }
  let(:user) { FactoryGirl.create(:user) }
  let(:role) { FactoryGirl.create(:role) } 
  let(:user_roles) { user.roles << role }
  let(:permission) { FactoryGirl.create(:permission, role_id: role.id) }
  let(:region) { FactoryGirl.create(:region) }
  let(:contact_info) { FactoryGirl.create(:contact_info) }
  let!(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: group.id, region_id: region.id) } 
  let!(:employment) { FactoryGirl.create(:employment, nursery_id: nursery.id, user_id: user.id) } 
  let!(:currency_type) { FactoryGirl.create(:currency_type) }
  let!(:currency_type2) { FactoryGirl.create(:currency_type, currency_name: "American Dollar") }
  let!(:currency) { FactoryGirl.create(:currency) }
  
  let(:submit) { "Save" }
  
  subject { page }
  
  describe "Currencies New Page" do
    describe "for non-signed-in users" do
      before { get new_currency_path }
      specify { response.should redirect_to(root_path) } 
    end    
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit new_currency_path
      end
      
      it { should have_selector('h1', text: 'Add Currency') }
      it { should have_selector('title', text: full_title('Add Currency')) }
      
      describe "with valid information" do
        before do
          select "Pound Sterling",  from: 'currency[currency_type_id]'          
        end

        it "should create new currency record" do
          expect { click_button submit}.to change(Currency, :count).by(1)            
        end
        
        it "should redirect to nursery default page" do
          click_button submit
          current_path.should == currencies_path
        end   
      end
    end 
  end
  
  describe "Currencies Edit Page" do
    describe "for non-signed-in users" do
      before { get edit_currency_path(currency) }
      specify { response.should redirect_to(root_path) } 
    end     
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit edit_currency_path(currency)       
      end
      
      it { should have_selector('h1', text: 'Edit Currency') }
      it { should have_selector('title', text: full_title('Edit Currency')) }
  
      describe "with valid information" do
        let(:revised_currency) { "American Dollar" }
        before do
          select "American Dollar",   from: 'currency[currency_type_id]'
          click_button submit 
        end
        
        describe "should redirect to the nursery defaults page" do
          it { should have_selector('h1', text: 'Defaults') }
          it { should have_selector('title', text: full_title("Nursery Defaults")) }          
        end
        
        specify { currency.reload.currency_type.currency_name.should == revised_currency }      
      end      
    end  
  end

  describe "Currency Index Page" do
    describe "for non-signed-in users" do
      before { get currencies_path }
      specify { response.should redirect_to(root_path) } 
    end 
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit currencies_path       
      end
      
      it { should have_selector('h1', text: 'Defaults') }
      it { should have_selector('title', text: full_title('Nursery Defaults')) }      
    end   
  end
end
