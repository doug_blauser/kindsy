require 'spec_helper'

describe "AccountPages" do
  let(:group) { FactoryGirl.create(:nursery_group) }
  let(:user) { FactoryGirl.create(:user) }
  let(:role) { FactoryGirl.create(:role) } 
  let(:user_roles) { user.roles << role }
  let(:permission) { FactoryGirl.create(:permission, role_id: role.id) }
  let(:region) { FactoryGirl.create(:region) }
  let(:contact_info) { FactoryGirl.create(:contact_info) }
  let!(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: group.id, region_id: region.id) }
  let!(:employment) { FactoryGirl.create(:employment, nursery_id: nursery.id, user_id: user.id) }
  let!(:status) { FactoryGirl.create(:status_type) }  
  let!(:account) { FactoryGirl.create(:account, nursery_id: nursery.id, user_id: user.id) }
  let!(:child) { FactoryGirl.create(:child, nursery_id: nursery.id, status_type_id: status.id, account_id: account.id) }
  let!(:child2) { FactoryGirl.create(:child, nursery_id: nursery.id, status_type_id: status.id, surname: "Addition") }    
  let!(:carer) { FactoryGirl.create(:carer, nursery_id: nursery.id) }
  let!(:accountability) { FactoryGirl.create(:accountability, account_id: account.id, carer_id: carer.id) }
  let(:submit) { "Save" }
  
  subject { page }
  
  describe "Account New Page" do
    describe "for non-signed-in users" do
      before { get new_account_path }
      specify { response.should redirect_to(root_path) } 
    end
    
    describe "for signed-in users" do
      before do 
        sign_in_user(user, user_roles, permission)  
        visit new_account_path
      end
      
      it { should have_selector('h1', text: 'Add an account') }
      it { should have_selector('title', text: full_title('Add Account')) }
      
      describe "with valid information" do
        before do
          fill_in 'account[account_name]',                        with: "Example Family"
          fill_in 'account[account_no]',                          with: "SMI65479"
          fill_in 'account[sage_account_reference]',              with: "a value that meets sage requirements"
        end
        
        it "should create a new account" do
          expect { click_button submit }.to change(Account, :count).by(1)
        end  
      end
      
      describe "with missing information" do
        let(:missing_account_name) { "" }
        let(:missing_account_no) { "" }
        
        before do
          fill_in 'account[account_name]',                        with: missing_account_name
          fill_in 'account[account_no]',                          with: missing_account_no
          fill_in 'account[sage_account_reference]',              with: "a value that meets sage requirements"
        end
        
        it "should not create a new account" do
          expect { click_button submit }.not_to change(Account, :count) 
        end
        
        it { should have_selector('div', :id => "error_explanation") }
        it { should have_selector('div', :class => "control-group string required error") }
                
        it "should redirect to new account page" do
          current_path.should == new_account_path
        end        
      end
    end
  end    

  describe "Account Index Page" do
    describe "for non-signed-in users" do
      before { get accounts_path }
      specify { response.should redirect_to(root_path) } 
    end    
    
    describe "for signed-in users" do
      before do 
        sign_in_user(user, user_roles, permission)  
        visit accounts_path
      end      
      
      it { should have_selector('h1', text: 'Nursery Accounts') }
      it { should have_selector('title', text: full_title('Nursery Accounts')) }  
      it { should have_link('+ Add Account', href: new_account_path) }
      it { should have_selector('th', text: 'Account Name') }
      it { should have_selector('th', text: 'Account Number') }
      it { should have_selector('th', text: 'Count of Related Children') }
      it { should have_selector('th', text: 'Status') }
      it { should have_selector('th', text: '...') }
      it { should have_selector('td', text: account.account_name) }
      it { should have_selector('td', text: account.account_no) }
      it { should have_selector('td', text: account.children.count.to_s) }   
      it { should have_selector('td', text: 'Account status goes here') }
      it { should have_link('View', href: account_path(account)) }
      it { should have_link('Edit', href: edit_account_path(account)) } 
      it { should have_link('Delete', href: account_path(account)) }      
    end
  end

  describe "Account Edit Page" do
    describe "for non-signed-in users" do
      before { get edit_account_path(account) }
      specify { response.should redirect_to(root_path) } 
    end    
    
    describe "for signed-in users" do
      before do 
        sign_in_user(user, user_roles, permission)  
        visit edit_account_path(account)
      end      
      
      it { should have_selector('h1', text: 'Edit this account') }
      it { should have_selector('title', text: full_title('Edit this account')) }      
    
      describe "with valid information" do
        let(:revised_account_name) { "Revised Family" }
        let(:revised_account_no) { "SMI65777" }
        let(:revised_sage_ref) { "a revised value that meets sage requirements" }
        
        before do
          fill_in 'account[account_name]',                        with: revised_account_name
          fill_in 'account[account_no]',                          with: revised_account_no
          fill_in 'account[sage_account_reference]',              with: revised_sage_ref
          click_button submit
        end
        
        specify { account.reload.account_name.should == revised_account_name }
        specify { account.reload.account_no.should == revised_account_no  }
        specify { account.reload.sage_account_reference.should == revised_sage_ref }
        
        it "should redirect to account index page" do
          current_path.should == accounts_path
        end  
      end    
    
      describe "with missing information" do
        let(:missing_account_name) { "" }
        let(:missing_account_no) { "" }
        let(:missing_sage_ref) { "" }
        
        before do
          fill_in 'account[account_name]',                        with: missing_account_name
          fill_in 'account[account_no]',                          with: missing_account_no
          fill_in 'account[sage_account_reference]',              with: missing_sage_ref
          click_button submit
        end
        
        specify { account.reload.account_name.should == account.account_name }
        specify { account.reload.account_no.should == account.account_no  }
        specify { account.reload.sage_account_reference.should == account.sage_account_reference }
        specify { account.reload.account_name.should_not == missing_account_name }
        specify { account.reload.account_no.should_not == missing_account_no  }
        specify { account.reload.sage_account_reference.should_not == missing_sage_ref }        

        it { should have_selector('div', :id => "error_explanation") }
        it { should have_selector('div', :class => "control-group string required error") }
        
        it "should redirect to account index page" do
          current_path.should == account_path(account)
        end          
      end
    end
  end

  describe "Account Show Page" do
    describe "for non-signed-in users" do
      before { get account_path(account) }
      specify { response.should redirect_to(root_path) } 
    end     
  
    describe "for signed-in users" do
      before do 
        sign_in_user(user, user_roles, permission)  
        visit account_path(account)
      end       
      
      it { should have_selector('h1', text: "Account Summary") }
      it { should have_selector('title', text: full_title('Account Summary')) }
      it { should have_link('Back to List', href: accounts_path) } 
      it { should have_link('Summary', href: account_path(account)) } 
      it { should have_link('Related Children', href: related_children_account_path(account)) }
      it { should have_link('Related Carers', href: related_carers_account_path(account)) }
      it { should have_link('Edit', href: edit_account_path(account)) }  
      it { should have_link('+ Add Child', href: search_children_account_path(account)) }
      it { should have_link('+ Add Carer', href: search_carers_account_path(account)) }
      it { should have_content('Account Name:') }
      it { should have_content(account.account_name) } 
      it { should have_content('Account Number:') }
      it { should have_content(account.account_no) }
      it { should have_content('Sage Reference:') }
      it { should have_content(account.sage_account_reference) }
      it { should have_content('Account Balance:') }
      it { should have_content("This is where the calculated account balance goes") }                
    end
  end

  describe "Account Related Children Page" do
    describe "for non-signed-in users" do
      before { get related_children_account_path(account) }
      specify { response.should redirect_to(root_path) } 
    end        
    
    describe "for signed-in users" do
       before do
        sign_in_user(user, user_roles, permission)
        visit related_children_account_path(account)  
      end      
      
      it { should have_selector('h1', text: 'Account Detail') }
      it { should have_selector('title', text: full_title('Related Children')) }
      it { should have_link('Back to List', href: accounts_path) } 
      it { should have_link('Summary', href: account_path(account)) } 
      it { should have_link('Related Children', href: related_children_account_path(account)) }
      it { should have_link('Related Carers', href: related_carers_account_path(account)) }
      it { should have_selector('h2', text: 'Related Children') }
      it { should have_link('Edit', href: edit_account_path(account)) }  
      it { should have_link('+ Add Child', href: search_children_account_path(account)) }
      it { should have_link('+ Add Carer', href: search_carers_account_path(account)) }
      it { should have_content(account.children[0].full_name) }
      it { should have_content("#{account.children[0].age} #{account.children[0].age > 1? 'years':'year'}") }
      it { should have_content("#{'('}#{account.children[0].age_in_months} #{account.children[0].age_in_months > 1? 'months':'month'}#{')'}") }
                 
    end 
  end

  describe "Account Remove Related Children Page" do
    describe "for non-signed-in users" do
      before { put remove_child_account_path(account, child) }
      specify { response.should redirect_to(root_path) } 
    end    
    
    describe "for-signed_in users" do
       before do
        sign_in_user(user, user_roles, permission)
        visit related_children_account_path(account)  
      end      
      
      it { should have_selector('h1', text: 'Account Detail') }
      it { should have_selector('title', text: full_title('Related Children')) }
      it { should have_link('Delete', href: remove_child_account_path(account, :child_id => child.id)) }  
      
      describe "when delete link is clicked" do
        before do
          click_link 'Delete'
        end
        
        specify { child.reload.account_id.should == nil }        
      end 
    end
  end

  describe "Account Search Children Page" do
    describe "for non-signed-in users" do
      before { get search_children_account_path(account) }
      specify { response.should redirect_to(root_path) } 
    end
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit search_children_account_path(account)  
      end   
      
      it { should have_selector('h1', text: 'Account Detail') }  
      it { should have_selector('title', text: full_title('Add Related Children')) }
      it { should have_link('Back to List', href: accounts_path) }      
      it { should have_selector('h2', text: 'Add Related Children') }
      
      describe "with valid information" do
        before do
          select "surname",                 from: 'field'
          fill_in 'query',                  with: 'sur' 
          click_button 'Search'         
        end
        
        it { should have_content(child.full_name) }
        it { should have_css('input#add_children', type: 'submit') }
      end              
    end       
  end

  describe "Account Add Children Page" do
    describe "for non-signed-in users" do
      before { put add_children_account_path(account) }
      specify { response.should redirect_to(root_path) } 
    end
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit search_children_account_path(account)  
      end 
      
      describe "with valid information" do
        before do
          select "surname",                 from: 'field'
          fill_in 'query',                  with: 'ad' 
          click_button 'Search'              
          check('child_ids[]')    
          click_button 'Add Children' 
        end        
        
        specify { child.reload.account_id.should == account.id }         
      end      
    end     
  end

  describe "Account Related Carers Page" do
    describe "for non-signed-in users" do
      before { get related_carers_account_path(account) }
      specify { response.should redirect_to(root_path) } 
    end      
  
    describe "for signed-in users" do
       before do
        sign_in_user(user, user_roles, permission)
        visit related_carers_account_path(account)  
      end        
      
      it { should have_selector('h1', text: 'Account Detail') }
      it { should have_selector('title', text: full_title('Related Carers')) }
      it { should have_link('Back to List', href: accounts_path) } 
      it { should have_link('Summary', href: account_path(account)) } 
      it { should have_link('Related Children', href: related_children_account_path(account)) }
      it { should have_link('Related Carers', href: related_carers_account_path(account)) }
      it { should have_selector('h2', text: 'Related Carers') }
      it { should have_link('Edit', href: edit_account_path(account)) }  
      it { should have_link('+ Add Child', href: search_children_account_path(account)) }
      it { should have_link('+ Add Carer', href: search_carers_account_path(account)) }       
      it { should have_content(account.carers[0].display_name) }
      it { should have_content(carer.display_relations(carer, carer.child_relationships).join(", "))  } 
      it { should have_content('Account balance goes here') }     
    end
  end

  describe "Account Search Carers Page" do
    describe "for non-signed-in users" do
      before { get search_carers_account_path(account) }
      specify { response.should redirect_to(root_path) } 
    end    
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit search_carers_account_path(account)  
      end   
      
      it { should have_selector('h1', text: 'Account Detail') }  
      it { should have_selector('title', text: full_title('Add Related Carers')) }
      it { should have_link('Back to List', href: accounts_path) }      
      it { should have_selector('h2', text: 'Add Related Carers') }
      
      describe "with valid information" do
        before do
          select "surname",                 from: 'field'
          fill_in 'query',                  with: 'rig' 
          click_button 'Search'         
        end
        
        it { should have_content(carer.display_name) }
        it { should have_css('input#add_carer', type: 'submit') }
      end                 
    end    
  end

  describe "Account Add Carers Page" do
    describe "for non-signed-in users" do
      before { put add_carers_account_path(account) }
      specify { response.should redirect_to(root_path) } 
    end 
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit search_carers_account_path(account)  
      end
      
      describe "with valid information" do
        before do
          select "surname",                 from: 'field'
          fill_in 'query',                  with: 'rig' 
          click_button 'Search'              
          check('carer_ids[]')   
        end        
        
        it "should create a new accountability" do
          expect { click_button 'Add Carer' }.to change(Accountability, :count).by(1)
        end
      end                
    end   
  end

  describe "Account Remove Related Carers Page" do
    describe "for non-signed-in users" do
      before { put remove_carer_account_path(account, carer) }
      specify { response.should redirect_to(root_path) } 
    end     
    
    describe "for signed-in users" do
       before do
        sign_in_user(user, user_roles, permission)
        visit related_carers_account_path(account)  
      end      
      
      it { should have_selector('h1', text: 'Account Detail') }
      it { should have_selector('title', text: full_title('Related Carers')) }
      it { should have_link('Delete', href: remove_carer_account_path(account, :carer_id => carer.id)) } 
      
      it "should remove accountability record when delete link is clicked" do
        expect { click_link 'Delete' }.to change(Accountability, :count).by(-1)     
      end             
    end
  end
end
