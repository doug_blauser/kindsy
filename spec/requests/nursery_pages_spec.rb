require 'spec_helper'

describe "NurseryPages" do
  let(:group) { FactoryGirl.create(:nursery_group) }
  let(:user) { FactoryGirl.create(:user) }
  let(:role) { FactoryGirl.create(:role) } 
  let(:user_roles) { user.roles << role }
  let(:permission) { FactoryGirl.create(:permission, role_id: role.id) }
  let(:region) { FactoryGirl.create(:region) }
  let(:contact_info) { FactoryGirl.create(:contact_info) }
  let(:local_authority) { FactoryGirl.create(:local_authority) }
  let(:local_authority2) { FactoryGirl.create(:local_authority, name: "Oxon") }
  let!(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: group.id, region_id: region.id, billing_cycle_start_day: 5, local_authority_id: local_authority.id) }
 
   
  let(:submit) { "Save" }
  
  subject { page }
  
  describe "Nurseries Index Page" do

    
    describe "for non-signed-in users" do
      before { get nurseries_path }
      specify { response.should redirect_to(root_path) } 
    end 
    
    describe "for signed-in user" do
      before do        
        sign_in_user(user, user_roles, permission) 
        visit nurseries_path 
      end
      
      describe "of correct account" do 
        it { should have_selector('h1', text: 'Our Nurseries') }
        it { should have_selector('title', text: full_title('Nursery List')) }
        it { should have_content(nursery.name) }
        it { should have_content(nursery.region.name) } 
        it { should have_content(nursery.contact_info.town) }
        it { should have_content(nursery.contact_info.post_code) }
        it { should have_selector('a', title: "View") }
        it { should have_selector('a', title: "Edit") }
      end
    end          
  end
  
  describe "Nurseries Show Page" do
    describe "for non-signed-in users" do
      before { get nursery_path(nursery) }
      specify { response.should redirect_to(root_path) } 
    end
    
    describe "for signed-in user" do
      before do
        sign_in_user(user, user_roles, permission)
        visit nursery_path(nursery)
      end
      
      it { should have_selector('h1', text: "Contact Information") }
      it { should have_selector('title', text: full_title("Nursery Contact Info"))}
      it { should have_link('Edit', href: edit_nursery_path(nursery)) }
      it { should have_content(nursery.name) }
      it { should have_content(nursery.contact_info.address_line_1) }
      it { should have_content(nursery.contact_info.address_line_2) } 
      it { should have_content(nursery.contact_info.address_line_3) }
      it { should have_content(nursery.contact_info.town) }
      it { should have_content(nursery.contact_info.county) }
      it { should have_content(nursery.contact_info.post_code)  }
      it { should have_content(nursery.contact_info.telephone_1)  }
      it { should have_content(nursery.contact_info.email_1) }                
    end
  end

  describe "New Nursery Page" do
   describe "for non-signed-in users" do
      before { get new_nursery_path }
      specify { response.should redirect_to(root_path) } 
    end 
    
    describe "for signed-in user" do
      before do
        sign_in_user(user, user_roles, permission)
        visit new_nursery_path        
      end
      
      it { should have_selector('h1', text: "Add a nursery") }
      it { should have_selector('title', text: full_title('Add a nursery')) }
      
      describe "with valid information" do
        before do
          fill_in 'nursery[name]',                                    with: "The Old Station Nursery Ltd - New Branch"
          select "South East",                                        from: 'nursery[region_id]'
          select local_authority.name,                                from: 'nursery[local_authority_id]'
          fill_in 'nursery[billing_cycle_start_day]',                 with: 5
          fill_in 'nursery[contact_info_attributes][address_line_1]', with: "Address Line 1" 
          fill_in 'nursery[contact_info_attributes][address_line_2]', with: "Address Line 2"
          fill_in 'nursery[contact_info_attributes][address_line_3]', with: "Address Line 3" 
          fill_in 'nursery[contact_info_attributes][town]',           with: "Faringdon"
          fill_in 'nursery[contact_info_attributes][county]',         with: "Oxon"
          fill_in 'nursery[contact_info_attributes][post_code]',      with: "SN7 7BP"
          fill_in 'nursery[contact_info_attributes][telephone_1]',    with: "01522511444"
          fill_in 'nursery[contact_info_attributes][email_1]',        with: "mynursery@example.com"
        end 
        
        it "should create a new nursery" do
          expect { click_button submit }.to change(Nursery, :count).by(1) 
        end
        
        it "should create a new contact_info record" do
          expect { click_button submit }.to change(ContactInfo, :count).by(1)          
        end         
      end
      
      describe "with missing information" do
        let(:new_name) { "" }
        let(:new_billing_cycle_start_day) { "" }
        let(:new_add1) { "" }
        let(:new_town) { "" }
        let(:new_post_code) { "" }  
        before do
          fill_in 'nursery[name]',                                    with: new_name
          select "South East",                                        from: 'nursery[region_id]'
          fill_in 'nursery[billing_cycle_start_day]',                 with: new_billing_cycle_start_day                    
          fill_in 'nursery[contact_info_attributes][address_line_1]', with: new_add1
          fill_in 'nursery[contact_info_attributes][address_line_2]', with: "Address Line 2"
          fill_in 'nursery[contact_info_attributes][address_line_3]', with: "Address Line 3"                      
          fill_in 'nursery[contact_info_attributes][town]',           with: new_town
          fill_in 'nursery[contact_info_attributes][county]',         with: "Oxon"
          fill_in 'nursery[contact_info_attributes][post_code]',      with: new_post_code
          fill_in 'nursery[contact_info_attributes][telephone_1]',    with: ""
          fill_in 'nursery[contact_info_attributes][email_1]',        with: ""          
        end 
        
        it "should not create a new nursery" do
          expect { click_button submit }.not_to change(Nursery, :count)
        end

        it { should have_selector('div', :id => "error_explanation") }
        it { should have_selector('div', :class => "control-group string required error") }
                
        it "should redirect to new nursery page" do
          current_path.should == new_nursery_path
        end 
      end      
    end
  end

  describe "Nursery Edit Page" do
   describe "for non-signed-in users" do
      before { get edit_nursery_path(nursery) }
      specify { response.should redirect_to(root_path) } 
    end  
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit edit_nursery_path(nursery)        
      end   
      
      it { should have_selector('h1', text: "Edit this nursery") }
      it { should have_selector('title', text: full_title('Edit this nursery')) }         
    
      describe "with valid information" do        
        let(:revised_name) { "New Name" }
        let(:revised_region) { FactoryGirl.create(:region, name: "East Midlands") } 
        let(:revised_billing_cycle_start_day) { 4 }
        let(:revised_add1) { "Revised Address Line 1" }
        let(:revised_add2) { "Revised Address Line 2" }
        let(:revised_add3) { "Revised Address Line 3" }
        let(:revised_town) { "Revised Town" }
        let(:revised_county) { "Revised County" }
        let(:revised_post_code) { "NT7 2DB"} 
        let(:revised_telephone) { "01522511445" }  
        let(:revised_email) { "revisednursery@example.com" }    
        before do
          fill_in 'nursery[name]',                                    with: revised_name
          fill_in 'nursery[billing_cycle_start_day]',                 with: revised_billing_cycle_start_day
          fill_in 'nursery[contact_info_attributes][address_line_1]', with: revised_add1 
          fill_in 'nursery[contact_info_attributes][address_line_2]', with: revised_add2
          fill_in 'nursery[contact_info_attributes][address_line_3]', with: revised_add3 
          fill_in 'nursery[contact_info_attributes][town]',           with: revised_town
          fill_in 'nursery[contact_info_attributes][county]',         with: revised_county
          fill_in 'nursery[contact_info_attributes][post_code]',      with: revised_post_code
          fill_in 'nursery[contact_info_attributes][telephone_1]',    with: revised_telephone
          fill_in 'nursery[contact_info_attributes][email_1]',        with: revised_email
          click_button submit
        end
        
        it { should have_selector('h1', text: "Our Nurseries" ) }
        it { should have_selector('title', text: full_title("Nursery List")) } 
        specify { nursery.reload.name.should == revised_name }
        specify { nursery.reload.billing_cycle_start_day.should == revised_billing_cycle_start_day }
        specify { nursery.reload.contact_info.address_line_1.should == revised_add1 }
        specify { nursery.reload.contact_info.address_line_2.should == revised_add2 } 
        specify { nursery.reload.contact_info.address_line_3.should == revised_add3 } 
        specify { nursery.reload.contact_info.town.should == revised_town }
        specify { nursery.reload.contact_info.county.should == revised_county }  
        specify { nursery.reload.contact_info.post_code.should == revised_post_code }
        specify { nursery.reload.contact_info.telephone_1.should == revised_telephone }
        specify { nursery.reload.contact_info.email_1.should == revised_email }                                         
      end          

      describe "with missing information" do
        let(:revised_name) { "" }
        let(:revised_billing_cycle_start_day) { "" }
        let(:revised_add1) { "" }
        let(:revised_town) { "" }
        let(:revised_post_code) { "" }  
        before do
          fill_in 'nursery[name]',                                    with: revised_name
          fill_in 'nursery[billing_cycle_start_day]',                 with: revised_billing_cycle_start_day         
          fill_in 'nursery[contact_info_attributes][address_line_1]', with: revised_add1 
          fill_in 'nursery[contact_info_attributes][town]',           with: revised_town
          fill_in 'nursery[contact_info_attributes][post_code]',      with: revised_post_code
          click_button submit
        end 
        
        it { should have_selector('h1', text: "Edit this nursery" ) }
        it { should have_selector('title', text: full_title("Edit this nursery")) } 
        specify { nursery.reload.name.should_not == revised_name }
        specify { nursery.reload.billing_cycle_start_day.should_not == revised_billing_cycle_start_day }
        specify { nursery.reload.contact_info.address_line_1.should_not == revised_add1 }
        specify { nursery.reload.contact_info.town.should_not == revised_town }               
      end
    end   
  end
end
