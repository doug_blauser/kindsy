require 'spec_helper'

describe "RoomPages" do
  let(:group) { FactoryGirl.create(:nursery_group) }
  let(:user) { FactoryGirl.create(:user) }
  let(:role) { FactoryGirl.create(:role) } 
  let(:user_roles) { user.roles << role }
  let(:permission) { FactoryGirl.create(:permission, role_id: role.id) }
  let(:region) { FactoryGirl.create(:region) }
  let(:contact_info) { FactoryGirl.create(:contact_info) }
  let!(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: group.id, region_id: region.id) } 
  let!(:employment) { FactoryGirl.create(:employment, nursery_id: nursery.id, user_id: user.id) }   
  let!(:room) { FactoryGirl.create(:room, nursery_id: nursery.id) }

  let(:submit) { "Save" }
  
  subject { page }
  
  describe "Rooms Index Page" do
    describe "for non-signed-in users" do
      before { get rooms_path }
      specify { response.should redirect_to(root_path) }
    end
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit rooms_path
      end  
      
      it { should have_selector('h1', text: 'Rooms') }   
      it { should have_selector('title', text: full_title('Nursery Rooms')) }
      it { should have_link('+ Room', href: new_room_path) }
      it { should have_content("Room Name") } 
      it { should have_content(room.name) } 
      it { should have_content("Capacity") }
      it { should have_content(room.capacity) }
      it { should have_content("Age Start") }
      it { should have_content(room.age_from) }
      it { should have_content("Age End") }
      it { should have_content(room.age_to) } 
      it { should have_link('View', href: room_path(room)) }
      it { should have_link('Edit', href: edit_room_path(room)) }
      it { should have_link('Delete', href: room_path(room)) }
    end
  end 
  
  describe "Rooms New Page" do
    describe "for non-signed-in users" do
      before { get new_room_path }
      specify { response.should redirect_to(root_path) }
    end  
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit new_room_path
      end  
      
      it { should have_selector('h1', text: 'Add Nursery Room') }
      it { should have_selector('title', text: 'Add Nursery Room') }           
    
      describe "with valid information" do
        before do
          fill_in 'room[name]',      with: "New Room"
          fill_in 'room[capacity]',  with: 16
          fill_in 'room[age_from]',  with: 25
          fill_in 'room[age_to]',    with: 35
        end
        
        it "should create a new nursery room" do
          expect { click_button submit }.to change(Room, :count).by(1)
        end
        
        it "should redirect to nursery room index page" do
          click_button submit
          current_path.should == rooms_path
        end
      end
      
      describe "with missing information" do
        let(:wrong_name) { "" }
        let(:wrong_capacity) { "" }
        let(:wrong_start_age) { "" }
        let(:wrong_end_age) { "" }
        
        before do
          fill_in 'room[name]',      with: wrong_name
          fill_in 'room[capacity]',  with: wrong_capacity
          fill_in 'room[age_from]',  with: wrong_start_age
          fill_in 'room[age_to]',    with: wrong_end_age
        end              
        
        it "should not create a new room" do
          expect { click_button submit }.not_to change(Room, :count)
        end

        it { should have_selector('div', :id => "error_explanation") }
        it { should have_selector('div', :class => "control-group string required error") }
                
        it "should redirect to new nursery page" do
          current_path.should == new_room_path
        end         
      end
    end  
  end

  describe "Rooms Edit Page" do
    describe "for non-signed-in users" do
      before { get edit_room_path(room) }
      specify { response.should redirect_to(root_path) }
    end   
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit edit_room_path(room)
      end
      
      it { should have_selector('h1', text: 'Edit Nursery Room') }
      it { should have_selector('title', text: 'Edit Nursery Room') }                 
    
      describe "with valid information" do
        let(:revised_name) { "Revised Room Name" }
        let(:revised_capacity) { 14 }
        let(:revised_start_age) { 24 }
        let(:revised_end_age)  { 36 } 
        
        before do
          fill_in 'room[name]',      with: revised_name
          fill_in 'room[capacity]',  with: revised_capacity
          fill_in 'room[age_from]',  with: revised_start_age
          fill_in 'room[age_to]',    with: revised_end_age
          click_button submit
        end     
        
        it { should have_selector('h1', text: 'Rooms')  }
        it { should have_selector('title', text: full_title('Nursery Rooms')) }          
        specify { room.reload.name.should == revised_name }
        specify { room.reload.capacity.should == revised_capacity } 
        specify { room.reload.age_from.should == revised_start_age }
        specify { room.reload.age_to.should == revised_end_age }     
      end
      
      describe "with missing information" do
        let(:wrong_name) { "" }
        let(:wrong_capacity) { "" }
        let(:wrong_start_age) { "" }
        let(:wrong_end_age) { "" }
        
        before do
          fill_in 'room[name]',      with: wrong_name
          fill_in 'room[capacity]',  with: wrong_capacity
          fill_in 'room[age_from]',  with: wrong_start_age
          fill_in 'room[age_to]',    with: wrong_end_age
        end              
        
        it { should have_selector('div', :id => "error_explanation") }
        it { should have_selector('div', :class => "control-group string required error") }
                
        it "should redirect to new nursery page" do
          current_path.should == edit_room_path(room)
        end  
        
        specify { room.reload.name.should_not == wrong_name }
        specify { room.reload.capacity.should_not == wrong_capacity } 
        specify { room.reload.age_from.should_not == wrong_start_age }
        specify { room.reload.age_to.should_not == wrong_end_age }               
      end      
    end      
  end
end
