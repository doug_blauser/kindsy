require 'spec_helper'

describe "InvoicePages" do
 
  let(:group) { FactoryGirl.create(:nursery_group) }
  let(:user) { FactoryGirl.create(:user) }
  let(:role) { FactoryGirl.create(:role) } 
  let(:user_roles) { user.roles << role }
  let(:permission) { FactoryGirl.create(:permission, role_id: role.id) }  
  let(:region) { FactoryGirl.create(:region) }  
  let!(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: group.id, region_id: region.id) }
  let!(:employment) { FactoryGirl.create(:employment, nursery_id: nursery.id, user_id: user.id) }  
  let!(:account) { FactoryGirl.create(:account, nursery_id: nursery.id, user_id: user.id) }  
  let!(:carer) { FactoryGirl.create(:carer, nursery_id: nursery.id) } 
  let!(:accountability) { FactoryGirl.create(:accountability, account_id: account.id, carer_id: carer.id) }   
  let!(:status) { FactoryGirl.create(:status_type) }    
  let!(:child) { FactoryGirl.create(:child, nursery_id: nursery.id, status_type_id: status.id, account_id: account.id) } 
  let!(:child2) { FactoryGirl.create(:child, nursery_id: nursery.id, status_type_id: status.id, account_id: account.id, first_name: "Duce") }
  let!(:child_relationship_type) { FactoryGirl.create(:child_relationship_type) }
  let!(:child_relationship) { FactoryGirl.create(:child_relationship, child_id: child.id, carer_id: carer.id, child_relationship_type_id: child_relationship_type.id, pick_up_flag: true, drop_off_flag: true) }       
  let!(:child_relationship2) { FactoryGirl.create(:child_relationship, child_id: child2.id, carer_id: carer.id, child_relationship_type_id: child_relationship_type.id, pick_up_flag: true, drop_off_flag: true) }
  let!(:period) { FactoryGirl.create(:period, nursery_id: nursery.id) }
  let!(:period2) { FactoryGirl.create(:period, nursery_id: nursery.id, period_name: "PM") } 
  let!(:booking) { FactoryGirl.create(:booking, nursery_id: nursery.id, child_id: child.id, period_id: period.id, start_date: Date.today.beginning_of_month-1.month, end_date: Date.today.end_of_month+3.months, days_of_week: ["Monday", "Wednesday", "Tuesday"]) } 
  let!(:child1_am_hourly_price1) { FactoryGirl.create(:timesheet, nursery_id: nursery.id,
                                                                child_id: child.id,
                                                                period_id: period.id,
                                                                date_attended: Date.today,
                                                                hourly_cost: period.period_hourly_cost,
                                                                period_cost: period.period_cost,
                                                                full_period: false) }                                                    
  let!(:child1_pm_hourly_price1) { FactoryGirl.create(:timesheet, nursery_id: nursery.id,
                                                                 child_id: child.id,
                                                                 period_id: period2.id,
                                                                 date_attended: Date.today,
                                                                 hourly_cost: period.period_hourly_cost,
                                                                 period_cost: period.period_cost, 
                                                                 full_period: false) }                                                     
  let!(:child2_am_hourly_price1) { FactoryGirl.create(:timesheet, nursery_id: nursery.id,
                                                                 child_id: child2.id,
                                                                 period_id: period.id,
                                                                 date_attended: Date.today,
                                                                 hourly_cost: period.period_hourly_cost,
                                                                 period_cost: period.period_cost,
                                                                 full_period: false) }
  let!(:child2_am_full_price1) { FactoryGirl.create(:timesheet, nursery_id: nursery.id,
                                                               child_id: child2.id,
                                                               period_id: period.id,
                                                               duration: 23400,
                                                               date_attended: Date.today,
                                                               hourly_cost: period.period_hourly_cost,
                                                               period_cost: period.period_cost, 
                                                               full_period: true) }  
  let!(:child1_am_hourly_price2) { FactoryGirl.create(:timesheet, nursery_id: nursery.id,
                                                                child_id: child.id,
                                                                period_id: period.id,
                                                                date_attended: Date.today,
                                                                hourly_cost: 7.75,
                                                                period_cost: 35.00,
                                                                full_period: false) } 
  let!(:child1_am_full_price2) { FactoryGirl.create(:timesheet, nursery_id: nursery.id,
                                                                       child_id: child.id,
                                                                       period_id: period.id,
                                                                       duration: 23400,
                                                                       date_attended: Date.today,
                                                                       hourly_cost: 7.75,
                                                                       period_cost: 35.00,
                                                                       full_period: true) }                                                                               
                                                                                                                                                                           
  let!(:billing_period) { FactoryGirl.create(:billing_period, start_date: Date.today.beginning_of_month, end_date: Date.today.end_of_month) }
  let!(:session_discount) { FactoryGirl.create(:discount, discountable_id: period2.id, discountable_type: "Period") }
  let!(:account_discount) { FactoryGirl.create(:discount, name: "Account Discount", amount: 10.00, discountable_id: account.id, discountable_type: "Account", applies_to: "Whole Invoice") }                                                   
  let!(:invoice) { FactoryGirl.create(:invoice, nursery_id: nursery.id, carer_id: carer.id, account_id: account.id, billing_period_id: billing_period.id) }                                                                                                          
  let!(:line_item) { FactoryGirl.create(:invoice_line_item, invoice_id: invoice.id, account_id: account.id, child_id: child.id) } 
  let!(:balance) { FactoryGirl.create(:invoice_line_item, invoice_id: invoice.id, account_id: account.id, child_id: nil, description: "Balance Brought Forward", quantity: 0, cost: 0.00, total_cost: 173.50) }

  
  let(:submit) { "Save" } 
  
  subject { page }            

  describe "Invoice create page" do
    describe "for non-signed-in users" do
      before { post invoices_path }
      specify { response.should redirect_to(root_path) }
    end 
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit related_carers_account_path(account)
      end      
      
      it { should have_selector('h2', text: "Related Carers") }
      it { should have_link('Create Invoice', href: invoices_path(account_id: account.id, carer_id: carer.id), action: :create, method: :post) }
      
      describe "with valid information" do  
        it "should create a new invoice" do
          expect { click_link "Create Invoice" }.to change(Invoice, :count).by(1) 
        end    
        
        it "should create invoice line items" do
          expect { click_link "Create Invoice" }.to change(InvoiceLineItem, :count).by(12)           
        end   
        
        it "should create charge record" do
          expect { click_link "Create Invoice" }.to change(Charge, :count).by(7)
        end 
      end
      
      describe "with no current billing period" do
        let!(:billing_period) { FactoryGirl.create(:billing_period, start_date: Date.today.beginning_of_month.prev_month, end_date: Date.today.end_of_month.prev_month) }  

        it "should create a new billing period" do
          expect { click_link "Create Invoice" }.to change(BillingPeriod, :count).by(1)
        end
      end

    end   
  end


  describe "Invoice index page" do
    describe "for non-signed-in users" do
      before { get invoices_path }
      specify { response.should redirect_to(root_path) }
    end   
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit invoices_path
      end
      
      it { should have_selector('title', text: full_title("Invoices List")) }
      it { should have_selector('h1', text: "Invoices List") }
      it { should have_selector('th', text: "Invoice Number") }
      it { should have_selector('th', text: "Issue Number") }
      it { should have_selector('th', text: "Invoice Date") }
      it { should have_selector('th', text: "Account Number") }
      it { should have_selector('th', text: "Invoice Amount") }
      it { should have_selector('th', text: "...") }
      it { should have_selector('td', text: invoice.number.to_s) }
      it { should have_selector('td', text: invoice.issue.to_s) }
      it { should have_selector('td', text: invoice.created_at.strftime("%D")) }
      it { should have_selector('td', text: invoice.account.account_no) }
      it { should have_selector('td', text: invoice.amount_due.to_s) }
      it { should have_link('View', href: invoice_path(invoice)) }      
    end   
  end
  
  describe "Invoice show page" do
    describe "for non-signed-in users" do
      before { get invoice_path(invoice) }
      specify { response.should redirect_to(root_path) }
    end  
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit invoice_path(invoice)
      end
      
      it { should have_selector('title', text: full_title("Invoice Detail")) } 
      it { should have_selector('h1', text: 'Invoice Detail') }
      it { should have_link('+ Add Line Item', href: new_invoice_invoice_line_item_path(invoice)) } 
      it { should have_content("#{invoice.carer.display_name}") }
      it { should have_content("#{invoice.carer.contact_info.address_line_1}") } 
      it { should have_content("#{invoice.carer.contact_info.address_line_2}") }  
      it { should have_content("#{invoice.carer.contact_info.address_line_3}") }
      it { should have_content("#{invoice.carer.contact_info.town}") }
      it { should have_content("#{invoice.carer.contact_info.county}") } 
      it { should have_content("#{invoice.carer.contact_info.post_code}") }  
      it { should have_content("#{invoice.number}") }
      it { should have_content("#{'Invoice Date:'} #{invoice.updated_at.strftime("%d %B %Y")}") }
      it { should have_content("#{invoice.account.account_no}") } 
      it { should have_content("#{invoice.nursery.name}") } 
      it { should have_content("#{invoice.nursery.contact_info.address_line_1}") }
      it { should have_content("#{invoice.nursery.contact_info.address_line_2}") }   
      it { should have_content("#{invoice.nursery.contact_info.address_line_3}") } 
      it { should have_content("#{invoice.nursery.contact_info.town}") }  
      it { should have_content("#{invoice.nursery.contact_info.county}") } 
      it { should have_content("#{invoice.nursery.contact_info.post_code}") } 
      it { should have_content("#{invoice.billing_period.start_date.strftime("%d/%m/%Y")} #{'to'} #{invoice.billing_period.end_date.strftime("%d/%m/%Y")}")}       
      it { should have_content("#{'Sessions'}") }
      it { should have_content("#{'Quantity'}") }
      it { should have_content("#{'Cost'}") }
      it { should have_content("#{'Total'}") }
      it { should have_content("#{line_item.child.full_name}") }
      it { should have_content("#{line_item.description}") }
      it { should have_content("#{line_item.quantity}") }
      it { should have_content("#{line_item.cost}") }
      it { should have_content("#{line_item.total_cost}") }
      it { should have_content("#{invoice.amount_due.to_s}") }
      it { should have_content("#{balance.description}") }
      it { should have_content("#{balance.quantity}") }
      it { should have_content("#{balance.cost}") }
      it { should have_content("#{balance.total_cost}") }
    end 
  end  

  describe "Invoice show page with editable line_item" do
   let!(:editable_line_item) { FactoryGirl.create(:invoice_line_item, invoice_id: invoice.id, account_id: account.id, child_id: child.id, editable: true) }    
  
    before do
      sign_in_user(user, user_roles, permission)
      visit invoice_path(invoice)
    end 
    
    it { should have_link('Edit', href: edit_invoice_invoice_line_item_path(invoice, editable_line_item))}   
  end

  describe "Invoice Update Page" do
    describe "for non-signed-in users" do
      before { put invoice_path(invoice) }
      specify { response.should redirect_to(root_path) }
    end   
    
    describe "for signed-in users" do
      describe "when invoice has 'New' status" do
        before do
          sign_in_user(user, user_roles, permission)
          visit related_carers_account_path(account)
          click_link 'Create Invoice'         
          new_invoice = Invoice.last 
          new_invoice.invoice_line_items.create(account_id: account.id, child_id: child.id, description: "Additional Charge", cost: 40.00, quantity: 1, total_cost: 40.00, editable: true)
          @non_editable_line_item_ids = new_invoice.invoice_line_items.non_editable.pluck(:id)
          @editable_line_item_ids = new_invoice.invoice_line_items.editable.pluck(:id)
          @number_of_line_items  = new_invoice.invoice_line_items.count
          @new_line_item_timesheet  = FactoryGirl.create(:timesheet, nursery_id: nursery.id, child_id: child.id, period_id: period.id, date_attended: Date.today, duration: 12600, hourly_cost: 8.50, period_cost: period.period_cost, full_period: false)
          @add_time_to_line_item_timesheet = FactoryGirl.create(:timesheet, nursery_id: nursery.id, child_id: child.id, period_id: period.id, date_attended: Date.today, duration: 12600, hourly_cost: period.period_hourly_cost, period_cost: period.period_cost, full_period: false )
          @orig_hourly_line_item_description = child1_pm_hourly_price1.charge.first.invoice_line_item.description
          @period_duration_changed_timesheet_id = child1_pm_hourly_price1.charge.first.chargeable_id
          child1_pm_hourly_price1.duration = 14400
          child1_pm_hourly_price1.full_period = true
          child1_pm_hourly_price1.save    
          @period_changed_timesheet_id = child1_am_hourly_price2.charge.first.chargeable_id
          @period_changed_line_item_id = child1_am_hourly_price2.charge.first.invoice_line_item_id
          child1_am_hourly_price2.period_id = period2.id
          child1_am_hourly_price2.save  
          @date_changed_timesheet_id = child1_am_full_price2.charge.first.chargeable_id
          @date_changed_line_item_id = child1_am_full_price2.charge.first.invoice_line_item_id
          child1_am_full_price2.date_attended = Date.today + 1.month
          child1_am_full_price2.save
          @orig_booking_quantity = booking.charges.first.invoice_line_item.quantity
          booking.days_of_week = ["Monday", "Tuesday"]
          booking.save
          @new_line_item_booking = FactoryGirl.create(:booking, nursery_id: nursery.id, child_id: child.id, period_id: period2.id, start_date: Date.today.beginning_of_month-1.month, end_date: Date.today.end_of_month+3.months, days_of_week: ["Monday", "Wednesday", "Tuesday"]) 
          visit invoices_path 
          within('#'"#{new_invoice.id}") do
            click_link "Edit"
          end
        end        
        describe "line item removal" do
          subject { Invoice.last.invoice_line_items.non_editable.pluck(:id) }
          context "it should remove non-editable line items" do        
            it { should_not eq(@non_editable_line_item_ids) }
          end
          
          subject { Invoice.last.invoice_line_items.editable.pluck(:id) }
          context "it should leave editable line items" do
            it { should eq(@editable_line_item_ids) }
          end
        end
        
        describe "line item re-creation" do
          describe "changing the period on a timesheet" do
            subject { Timesheet.find(@period_changed_timesheet_id).charge.first.invoice_line_item } 
            context "it should create new line item for revised period" do
              its(:description) { should eq('PM Hourly Sessions') }
            end  
            
            context "it should not have line item for original period" do
              its(:description) { should_not eq('AM Hourly Sessions') }
            end  
            
            describe "charge changes" do
              subject { Timesheet.find(@period_changed_timesheet_id).charge.first.invoice_line_item_id }              
              context "its line item should change" do
                it { should_not eq(@period_changed_line_item_id) }
              end   
            end    
          end
          
          describe "changing the date on a timesheet to future billing cycle" do
            subject { Invoice.last.invoice_line_items.pluck(:id) }
            context "it should remove the line item from the invoice" do
              it { should_not include(@date_changed_line_item_id) }
            end 
            
            describe "charges"
            subject { Charge.where("chargeable_id = ?", @date_changed_timesheet_id ) }
            context "it should not have a charge for the timesheet" do
              it { should be_empty }
            end
          end  
          
          describe "changing duration on timesheet from hourly to full period" do
            subject { Timesheet.find(@period_duration_changed_timesheet_id).charge.first.invoice_line_item }
            context "it should not show description as hourly sessions" do
              its(:description) { should_not eq(@orig_hourly_line_item_description) }
            end
            
            context "it should show description as full period" do
              its(:description) { should eq("PM Full Sessions") }
            end
            
            context "it should list period cost instead of hourly cost" do
              its(:cost) { should eq(period.period_cost)}
            end
            
            context "it should list the number of sessions instead of number of hours" do
              its(:quantity) { should eq(1) }
            end
          end                           
                    
          describe "adding a new timesheet that changes existing line item quantity" do
            subject { @add_time_to_line_item_timesheet.charge.first.invoice_line_item.quantity.to_s }
            context "it should increase quantity when new timesheets added for same child/duration/cost combo" do
             it { should eq("7.0") }
            end
          end
          
          describe "new line items" do
            subject { Invoice.last.invoice_line_items.pluck(:id) }            
            context "it should add a new line item for different child/duration/cost combo" do
              it { should include(@new_line_item_timesheet.charge.first.invoice_line_item_id) }             
            end
            
            describe "non_editable items" do
              subject { Invoice.last.invoice_line_items }              
              context "it should add new non-editable line items" do
                its(:non_editable) { should_not be_empty }
              end
            end
          end
        
          describe "booking changes" do
            describe "changing existing booking values" do
              subject { booking.charges.first.invoice_line_item.quantity }
              context "it should decrease the quantity" do
                it { should be < @orig_booking_quantity }
              end              
            end
            
            describe "adding new booking" do
              subject { Invoice.last.invoice_line_items.pluck(:id) }            
              context "it should add a new line item for different booking" do
                it { should include(@new_line_item_booking.charges.first.invoice_line_item_id) }             
              end              
            end
          end
        end

     
      end      
 
      describe "when invoice has 'Posted' status" do
        before do
          sign_in_user(user, user_roles, permission)
          visit related_carers_account_path(account)
          click_link 'Create Invoice'         
          @new_invoice = Invoice.last 
          @new_invoice.invoice_line_items.create(account_id: account.id, child_id: child.id, issue: @new_invoice.issue, description: "Additional Charge", cost: 40.00, quantity: 1, total_cost: 40.00, editable: true)
          @new_invoice.state = "posted"
          @new_invoice.save
          @pre_change_invoice_count = Invoice.count
          @issue = @new_invoice.issue
          @non_editable_line_item_ids = @new_invoice.invoice_line_items.non_editable.pluck(:id)
          @editable_line_item_ids = @new_invoice.invoice_line_items.editable.pluck(:id)
          @number_of_line_items  = @new_invoice.invoice_line_items.count
          @new_line_item_timesheet  = FactoryGirl.create(:timesheet, nursery_id: nursery.id, child_id: child.id, period_id: period.id, date_attended: Date.today, duration: 12600, hourly_cost: 8.50, period_cost: period.period_cost, full_period: false)
          @add_time_to_line_item_timesheet = FactoryGirl.create(:timesheet, nursery_id: nursery.id, child_id: child.id, period_id: period.id, date_attended: Date.today, duration: 12600, hourly_cost: period.period_hourly_cost, period_cost: period.period_cost, full_period: false )
          @orig_hourly_line_item_description = child1_pm_hourly_price1.charge.first.invoice_line_item.description
          @period_duration_changed_timesheet_id = child1_pm_hourly_price1.charge.first.chargeable_id
          child1_pm_hourly_price1.duration = 14400
          child1_pm_hourly_price1.full_period = true
          child1_pm_hourly_price1.save    
          @period_changed_timesheet_id = child1_am_hourly_price2.charge.first.chargeable_id
          @period_changed_line_item_id = child1_am_hourly_price2.charge.first.invoice_line_item_id
          child1_am_hourly_price2.period_id = period2.id
          child1_am_hourly_price2.save  
          @date_changed_timesheet_id = child1_am_full_price2.charge.first.chargeable_id
          @date_changed_line_item_id = child1_am_full_price2.charge.first.invoice_line_item_id
          child1_am_full_price2.date_attended = Date.today + 1.month
          child1_am_full_price2.save
          @orig_booking_quantity = booking.charges.first.invoice_line_item.quantity
          booking.days_of_week = ["Monday", "Tuesday"]
          booking.save
          @new_line_item_booking = FactoryGirl.create(:booking, nursery_id: nursery.id, child_id: child.id, period_id: period2.id, start_date: Date.today.beginning_of_month-1.month, end_date: Date.today.end_of_month+3.months, days_of_week: ["Monday", "Wednesday", "Tuesday"]) 
          visit invoices_path 
          within('#'"#{@new_invoice.id}") do
            click_link "Edit"
          end
          @dup_invoice = Invoice.last
          @post_change_invoice_count = Invoice.count
        end

        describe "it should create a new invoice record" do
          subject { @post_change_invoice_count }
          context "it should change record count by 1" do
            it { should eq(@pre_change_invoice_count + 1) }
          end
        end

        describe "it should update invoice settings" do
          subject { @dup_invoice }
          context "it should have a state of Posted" do
            its(:state) { should eq("posted") }
          end
          
          context "it should increment the issue number by 1" do
            its(:issue) { should eq(@issue + 1) }
          end
          
          context "its invoice number should not change" do
            its(:number) { should eq(@new_invoice.number) }
          end
        end
        
        describe "it should flag line items" do
          describe "for original items" do
            subject { @new_invoice.invoice_line_items.pluck(:issue).uniq }
            context "it should_not change line item issue number" do
              it { should eq([1]) }
            end
          end          
            
          describe "for new items" do
            subject { @dup_invoice.invoice_line_items.pluck(:issue).uniq }            
            context "it should include issue number 2" do
              it { should eq([2]) }
            end    
          end
        end        
        
        describe "it should re-create line items" do
          describe "for original items" do
            subject { @new_invoice.invoice_line_items.non_editable.pluck(:id) }
            context "it should not remove non-editable line items" do        
              it { should eq(@non_editable_line_item_ids) }
            end             
          end
         
          describe "for new items" do
            subject { @dup_invoice.invoice_line_items.non_editable.pluck(:id) }
            context "it should add new non_editable line items" do
              its(:count) { should be > 0 }
            end
          end
          
          describe "for editable items" do
            subject { @dup_invoice.invoice_line_items.editable }
            context "its count should equal those on new invoice" do
              its(:count) { should eq(@editable_line_item_ids.count) }
            end
          end

          describe "changing the period on a timesheet" do
            subject { Timesheet.find(@period_changed_timesheet_id).charge.last.invoice_line_item } 
            context "it should create new line item for revised period" do
              its(:description) { should eq('PM Hourly Sessions') }
            end  
            
            context "it should not have line item for original period" do
              its(:description) { should_not eq('AM Hourly Sessions') }
            end  
            
            describe "charge changes" do
              subject { Timesheet.find(@period_changed_timesheet_id).charge.last.invoice_line_item_id }              
              context "its line item should change" do
                it { should_not eq(@period_changed_line_item_id) }
              end   
            end    
          end
          
          describe "changing the date on a timesheet to future billing cycle" do
            subject { @dup_invoice.invoice_line_items.pluck(:id) }
            context "it should remove the line item from the invoice" do
              it { should_not include(@date_changed_line_item_id) }
            end 
            
            describe "charges"
              line_item_ids = Charge.where("chargeable_id = ?", @date_changed_timesheet_id ).pluck(:invoice_line_item_id)
              subject { line_item_ids  }
              context "it should not have a charge for the timesheet" do
                its(:count) { should_not eq(2) }
            end
          end  
          
          describe "changing duration on timesheet from hourly to full period" do
            subject { Timesheet.find(@period_duration_changed_timesheet_id).charge.last.invoice_line_item }
            context "it should not show description as hourly sessions" do
              its(:description) { should_not eq(@orig_hourly_line_item_description) }
            end
            
            context "it should show description as full period" do
              its(:description) { should eq("PM Full Sessions") }
            end
            
            context "it should list period cost instead of hourly cost" do
              its(:cost) { should eq(period.period_cost)}
            end
            
            context "it should list the number of sessions instead of number of hours" do
              its(:quantity) { should eq(1) }
            end
          end

          describe "adding a new timesheet that changes existing line item quantity" do
            subject { @add_time_to_line_item_timesheet.charge.last.invoice_line_item.quantity.to_s }
            context "it should increase quantity when new timesheets added for same child/duration/cost combo" do
             it { should eq("7.0") }
            end
          end
        
          describe "new line item timesheet" do
            subject { @dup_invoice.invoice_line_items.pluck(:id) }            
            context "it should add a new line item for different child/duration/cost combo" do
              it { should include(@new_line_item_timesheet.charge.last.invoice_line_item_id) }             
            end            
          end
        
          describe "booking changes" do
            describe "changing existing booking values" do
              subject { booking.charges.second.invoice_line_item.quantity }
              context "it should decrease the quantity" do
                it { should be < @orig_booking_quantity }
              end              
            end
            
            describe "adding new booking" do
              subject { @dup_invoice.invoice_line_items.pluck(:id) }            
              context "it should add a new line item for different booking" do
                 it { should include(@new_line_item_booking.charges.first.invoice_line_item_id) }             
              end              
            end
          end          
        end
      end 
    end       
  end
end
