require 'spec_helper'

describe "TimesheetPages" do
  let(:group) { FactoryGirl.create(:nursery_group) }
  let(:user) { FactoryGirl.create(:user) }
  let(:role) { FactoryGirl.create(:role) } 
  let(:user_roles) { user.roles << role }
  let(:permission) { FactoryGirl.create(:permission, role_id: role.id) }
  let(:region) { FactoryGirl.create(:region) }
  let(:contact_info) { FactoryGirl.create(:contact_info) }
  let!(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: group.id, region_id: region.id) }
  let!(:employment) { FactoryGirl.create(:employment, nursery_id: nursery.id, user_id: user.id) }
  let!(:room) { FactoryGirl.create(:room, nursery_id: nursery.id, name: "Blue Room") } 
  let!(:room2) { FactoryGirl.create(:room, nursery_id: nursery.id, name: "Red Room") }  
  let!(:open_hour) { FactoryGirl.create(:open_hour, nursery_id: nursery.id) }
  let!(:open_hour2) { FactoryGirl.create(:open_hour, nursery_id: nursery.id, day_of_week: "Tuesday") }
  let!(:period) { FactoryGirl.create(:period, nursery_id: nursery.id) }
  let!(:period2) { FactoryGirl.create(:period, nursery_id: nursery.id, period_name: "PM", period_hourly_cost: 8.00, period_cost: 35.25) }  
  let!(:hair) { FactoryGirl.create(:hair) }
  let!(:hair2) { FactoryGirl.create(:hair, color: "Blond") }
  let!(:eye_color) { FactoryGirl.create(:eye_color) }
  let!(:eye_color2) { FactoryGirl.create(:eye_color, shade: "Brown") }
  let!(:religion) { FactoryGirl.create(:religion) }
  let!(:religion2) { FactoryGirl.create(:religion, name: "Muslim") }
  let!(:ethnicity) { FactoryGirl.create(:ethnicity) }
  let!(:ethnicity2) { FactoryGirl.create(:ethnicity, race: "Asian") }
  let!(:nationality) { FactoryGirl.create(:nationality) }
  let!(:nationality2) { FactoryGirl.create(:nationality, country: "American") }
  let!(:language) { FactoryGirl.create(:language) }
  let!(:language2) { FactoryGirl.create(:language, tongue: "Spanish") }
  let!(:status) { FactoryGirl.create(:status_type) }
  let!(:status2) { FactoryGirl.create(:status_type, status_name: "On Waiting List") }
  let!(:child) { FactoryGirl.create(:child, 
                                    nursery_id: nursery.id,
                                    room_id: room.id, 
                                    status_type_id: status.id, 
                                    preferred_name: "Preferred Name", 
                                    hair_id: hair.id,
                                    eye_color_id: eye_color.id,
                                    religion_id: religion.id,
                                    ethnicity_id: ethnicity.id,
                                    nationality_id: nationality.id,
                                    language_id: language.id ) }
  let!(:timesheet) { FactoryGirl.create(:timesheet, nursery_id: nursery.id,
                                                    child_id: child.id,
                                                    period_id: period.id, 
                                                    hourly_cost: period.period_hourly_cost,
                                                    period_cost: period.period_cost) }
  let!(:timesheet2) { FactoryGirl.create(:timesheet, nursery_id: nursery.id,
                                                     child_id: child.id,
                                                     period_id: period2.id,
                                                     hourly_cost: period.period_hourly_cost,
                                                     period_cost: period.period_cost) }
  let!(:timesheet3) { FactoryGirl.create(:timesheet, nursery_id: nursery.id,
                                                     child_id: child.id,
                                                     period_id: period.id,
                                                     hourly_cost: period.period_hourly_cost,
                                                     period_cost: period.period_cost,                                                     
                                                     date_attended: Date.today.beginning_of_week+1) }                                                                                                             
  let!(:hours_attended) { child.timesheets.where(date_attended: timesheet.date_attended, period_id: period.id) }
  let!(:totals_attended) { child.timesheets.where(date_attended: timesheet.date_attended) }
  let!(:totals_per_day) { totals_attended.map { |period| period.duration_hours(period.duration) } }
  let!(:timesheets_this_week) { child.timesheets.where(date_attended: timesheet.date_attended..timesheet3.date_attended) }
  let!(:totals_this_week) { timesheets_this_week.map { |timesheet| timesheet.duration_hours(timesheet.duration) } }
  let!(:bow) { Date.today.beginning_of_week }
  let!(:eow) { Date.today.end_of_week }
                                                    
  let(:submit) { "Save" } 
  
  subject { page }            
  
  describe "Timesheet Index Page" do
    describe "for non-signed-in users" do
      before { get child_timesheets_path(child) }
      specify { response.should redirect_to(root_path) }
    end
    
    describe "for signed-in-users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit child_timesheets_path(child)
      end 
      
      it { should have_selector('h1', text: "#{'Child: '}#{child.full_name}") }
      it { should have_selector('title', text: full_title('Timesheets')) }
      it { should have_link('Back to List', href: children_path) }
      it { should have_link('Summary', href: child_path(child)) }  
      it { should have_link('Personal', href: detail_child_path(child)) }
      it { should have_link('Time & Attendance', href: child_bookings_path(child)) }
      it { should have_link('Learning Journey', href: '#') }
      it { should have_link('Finance', href: '#') } 
      it { should have_link('Notes', href: '#') }     
      it { should have_selector('h3', text: 'Time and Attendance') } 
      it { should have_link('Booking Pattern', href: child_bookings_path(child)) }  
      it { should have_link('Timesheets', href: child_timesheets_path(child)) }   
      it { should have_selector('h3', text: 'Timesheets') }
      it { should have_content("#{bow.strftime('%A')} #{bow.day.ordinalize} #{'to'} #{(eow-2).strftime('%A')} #{(eow-2).day.ordinalize} #{eow.strftime('%B %Y')}")}        
      it { should have_link('This Week', href: "#") }
      it { should have_link('Previous', href: "#") }
      it { should have_link('Next', href: "#") }
      it { should have_selector('th', text: 'Session') }
      it { should have_selector('th', text: nursery.open_hours[0].day_of_week) }  
      it { should have_selector('td', text: nursery.periods[0].period_name) } 
      it { should have_selector('td', content: hours_attended.first.duration_hours(hours_attended.first.duration)) }  
      it { should have_selector('td', text: 'Totals') }
      it { should have_selector('td', content: totals_per_day.sum) } 
      it { should have_content("Total for the week:") }  
      it { should have_content(totals_this_week.sum) }          
    end
  end                                            

  describe "Timesheet New Page" do
    describe "for non-signed-in users" do
      before { get new_child_timesheet_path(child) }
      specify { response.should redirect_to(root_path) }
    end    
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit new_child_timesheet_path(child)
      end
      
      it { should have_selector('h3', text: 'Timesheet - Add time') }
      it { should have_selector('title', text: full_title('Add Timesheet')) }
    
      describe "with valid information" do
        before do
          select "AM",                        from: 'timesheet[period_id]'
          select "June",                      from: 'timesheet[date_attended(2i)]'
          select "15",                        from: 'timesheet[date_attended(3i)]'
          select "2013",                      from: 'timesheet[date_attended(1i)]'
          select "3.5",                       from: 'timesheet[duration]'              
        end
        
        it "should create a new timesheet record for the child" do
          expect { click_button submit}.to change(Timesheet, :count).by(1)
        end 
        
        it "should redirect to timesheet index page" do
          click_button submit
          current_path.should == child_timesheets_path(child)
        end            
      end

      describe "with missing information" do
        let(:missing_period) { "" } 
        let(:missing_duration) { "" }
        
        before do
          select missing_period,              from: 'timesheet[period_id]'
          select "June",                      from: 'timesheet[date_attended(2i)]'
          select "15",                        from: 'timesheet[date_attended(3i)]'
          select "2013",                      from: 'timesheet[date_attended(1i)]'
          select missing_duration,            from: 'timesheet[duration]'             
        end       
        
        it "should not create a new timesheet record for the child" do
          expect { click_button submit}.not_to change(Timesheet, :count) 
        end  
        
        it "should redirect to child booking new page" do
          click_button submit
          current_path.should == child_timesheets_path(child)
        end   
        
        it { should have_selector('div', :id => "error_explanation") }
        it { should have_selector('div', :class => "control-group string required error") }                           
      end
    end
  end

  describe "Timesheet Edit Page" do
    describe "for non-signed-in users" do
      before { get edit_child_timesheet_path(child, timesheet) }
      specify { response.should redirect_to(root_path) }
    end        
    
    describe "for signed-in users" do
      before do
        sign_in_user(user, user_roles, permission)
        visit edit_child_timesheet_path(child, timesheet)
      end      
      
      it { should have_selector('h3', text: 'Timesheet - Edit time') }
      it { should have_selector('title', text: full_title('Edit Timesheet')) }   
      
      describe "with valid information" do
        let(:revised_period) { "PM" }
        let(:revised_date_attended_year) { "2014" }
        let(:revised_date_attended_month) { "February" }
        let(:revised_date_attended_day) { "14" }
        let(:revised_duration) { "3.0" }  

        before do
          select revised_period,              from: 'timesheet[period_id]'
          select revised_date_attended_year,  from: 'timesheet[date_attended(1i)]'
          select revised_date_attended_month, from: 'timesheet[date_attended(2i)]'
          select revised_date_attended_day,   from: 'timesheet[date_attended(3i)]'
          select revised_duration,            from: 'timesheet[duration]'
          click_button submit
        end      
        
        specify { timesheet.reload.period_id.should == period2.id }
        specify { timesheet.reload.date_attended.should == Date.new(2014,02,14) }
        specify { timesheet.reload.duration.should == 10800 }
        specify { timesheet.reload.hourly_cost.should == period2.period_hourly_cost}
        specify { timesheet.reload.period_cost.should == period2.period_cost }
        specify { timesheet.reload.full_period.should == false }
        
        it "should redirect to child timesheet index page" do
          current_path.should == child_timesheets_path(child)
        end
      end   

      describe "with missing information" do
        let(:missing_period) { "" } 
        let(:missing_duration) { "" }

        before do
          select missing_period,              from: 'timesheet[period_id]'
          select "January",                   from: 'timesheet[date_attended(2i)]'
          select "15",                        from: 'timesheet[date_attended(3i)]'
          select "2013",                      from: 'timesheet[date_attended(1i)]'
          select missing_duration,            from: 'timesheet[duration]'             
          click_button submit
        end        
        
        specify { timesheet.reload.period_id.should == period.id } 
        specify { timesheet.reload.duration.should == 12600 }      
        
        it "should redirect to child timesheet edit page" do
          current_path.should == child_timesheet_path(child, timesheet)
        end                
        
        it { should have_selector('div', :id => "error_explanation") }
        it { should have_selector('div', :class => "control-group string required error") }        
      end
    end
  end
end
