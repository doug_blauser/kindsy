include ApplicationHelper
 
  def sign_in_user(user, user_roles, permission)
    visit "/users/sign_in"
    fill_in "user[username]",        :with => user.username
    fill_in "user[password]",        :with => user.password
    click_button "Sign in"
   end