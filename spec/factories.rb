FactoryGirl.define do
  factory :currency_type do
    currency_name                        "Pound Sterling"    
  end
  
  factory :currency do
    currency_type     
    nursery                  
  end  
  
  factory :region do
    name                        "South East"
  end
  
  factory :nursery_group do
    name                        "Old Station Nursery Group"
    billing_period_start_date   Date.new(2012, 8, 1)
  end
  
  factory :user do
    username                    "exampleuser"
    first_name                  "Example"
    last_name                   "User"
    email                       "euser@example.com"
    password                    "foobar2"
    password_confirmation       "foobar2"    
  end
  
  factory :role do
    name                        "admin"
    resource_type               nil
  end
  
  factory :permission do
    action                      "access"
    condition                   "all"
    role                  
  end
  
  factory :contact_info do
    address_line_1             "The Marina, Lincolnshire, LN1 1RD"
    address_line_2             "The Marina, Lincolnshire, LN1 1RD"
    address_line_3             "The Marina, Lincolnshire, LN1 1RD"
    town                       "Somewhere"
    county                     "Lincolnshire"
    country                    "England"
    post_code                  "LN1 1RD"
    telephone_1                "01522511333"
    telephone_2                "123123123"
    telephone_3                "123456789"
    telephone_4                "012345678"
    skype                      "marinanursery"
    email_1                    "marina@theoldstationnursery.com"
    email_2                    "info@theoldstationnursery.com"
    website                    "theoldstationnursery.com"    
  end

  factory :local_authority_contact_info, class: ContactInfo do
    address_line_1             "123 Local Authority Place"
    address_line_2             "Local Authority line 2"
    address_line_3             "Local Authority line 3"
    town                       "Nice Town"
    county                     "Lincolnshire"
    country                    "England"
    post_code                  "LN1 1RD"
    telephone_1                "01522511331"
    telephone_2                "123123122"
    telephone_3                "123456783"
    telephone_4                "012345674"
    skype                      "myskype"
    email_1                    "localauthority@example.com"
    email_2                    "localauthority2@example.com"
    website                    "localauthority.com"  
  end 
  
  factory :local_authority do
    name                    "Lincolnshire" 
    association :contact_info, factory: :local_authority_contact_info
  end  

  factory :nursery do 
    name                       "The Old Station Nursery @ The Marina"
    nursery_group
    region   
    billing_cycle_start_day    1
    local_authority
    association :contact_info, factory: :contact_info    
  end

  factory :staff_contact_info, class: ContactInfo do
    address_line_1             "123 Where I live"
    address_line_2             "Where I live line 2"
    address_line_3             "Where I live line 3"
    town                       "Overhere"
    county                     "Lincolnshire"
    country                    "England"
    post_code                  "LN1 1RD"
    telephone_1                "01522511333"
    telephone_2                "123123123"
    telephone_3                "123456789"
    telephone_4                "012345678"
    skype                      "myskype"
    email_1                    "mywork@example.com"
    email_2                    "myhome@example.com"
    website                    "theoldstationnursery.com"    
  end   
  
  factory :carer_contact_info, class: ContactInfo do
    address_line_1             "123 My Home"
    address_line_2             "My Home line 2"
    address_line_3             "My Home line 3"
    town                       "Next Store"
    county                     "Lincolnshire"
    country                    "England"
    post_code                  "LN1 1RD"
    telephone_1                "01522511333"
    telephone_2                "123123123"
    telephone_3                "123456789"
    telephone_4                "012345678"
    skype                      "myskype"
    email_1                    "carerwork@example.com"
    email_2                    "carerhome@example.com"
    website                    "theoldstationnursery.com"    
  end       

  factory :child_contact_info, class: ContactInfo do
    address_line_1             "123 My Parent's Home"
    address_line_2             "My Parent's Home line 2"
    address_line_3             "My Parent's Home line 3"
    town                       "Nice Town"
    county                     "Lincolnshire"
    country                    "England"
    post_code                  "LN1 1RD"
    telephone_1                "01522511333"
    telephone_2                "123123123"
    telephone_3                "123456789"
    telephone_4                "012345678"
    skype                      "myskype"
    email_1                    "childwork@example.com"
    email_2                    "childhome@example.com"
    website                    "theoldstationnursery.com"  
  end 
  
  factory :account do
    user
    nursery
    account_name               "Account Owner"
    account_no                 "SMI65479"
    invoice_type               "paper"
    sage_account_reference     "a value that meets sage requirements"
  end
  
  factory :staff_member do
    nursery
    association :contact_info, factory: :staff_contact_info
    first_name                  "Jill"
    middle_name                 "Child"
    surname                     "Worker"
    title                       "Ms"
    date_of_birth               Date.new(1986,3,10)
    medical_conditions          "none"
    start_date                  Date.new(2011,9,1)
    leave_date                  Date.new(2012,8,15)
    payroll_number              "XYZ987"
    hours_per_week_contracted   30
    hourly_rate                 10
    start_of_holiday_year       Date.new(2012,3,1)
    holiday_entitlement         10.5
  end
  
  factory :room do
    nursery
    name                        "blue room"
    capacity                    7
    age_from                    12
    age_to                      18
  end
  
  factory :carer do
    association :contact_info, factory: :carer_contact_info
    title                       "Mrs"
    first_name                  "Elenore"
    surname                     "Rigby"
    
    trait :shehzad do
      title                     "Mr"
      first_name                "Shehzad"
      surname                   "Najib"
    end
  end
  
  factory :accountability do
    account
    carer
  end
  
  factory :open_hour  do
    nursery
    day_of_week                 "Monday"
    open_time                   Time.new(2000,01,01,07,00,00)
    close_time                  Time.new(2000,01,01,18,30,00)   
  end
  
  factory :period  do
    nursery
    period_name                 "AM"
    period_start_time           Time.new(2000,01,01,06,30,00)
    period_end_time             Time.new(2000,01,01,13,00,00)
    period_hourly_cost          7.50
    period_cost                 34.50
    days_of_week                [1,3,5]
    fe_limit                    2.5
    
    trait :full_day_1_yr_olds do
      period_name               "Full Day 1 Year Olds"
      period_start_time         Time.new(2000,01,01,8,00,00)
      period_end_time           Time.new(2000,01,01,18,00,00)
      period_cost               38.00
      days_of_week              [1,2,3,4,5]
      fe_limit                  5.0
    end

    trait :full_day_3_yr_olds do
      period_name               "Full Day 3 Year Olds"
      period_start_time         Time.new(2000,01,01,8,00,00)
      period_end_time           Time.new(2000,01,01,18,00,00)
      period_hourly_cost        3.60
      period_cost               36.00
      days_of_week              [1,2,3,4,5]
      fe_limit                  5.0
    end
    
    trait :half_day_am_1_yr_olds do
      period_name               "Half Day - AM 1 Year Olds"
      period_start_time         Time.new(2000,01,01,8,00,00)
      period_end_time           Time.new(2000,01,01,13,00,00)
      period_cost               20.25
      days_of_week              [1,2,3,4,5]
      fe_limit                  5.0      
    end
    
    trait :half_day_pm_1_yr_olds do
      period_name               "Half Day - PM 1 Year Olds"
      period_start_time         Time.new(2000,01,01,13,00,00)
      period_end_time           Time.new(2000,01,01,18,00,00)
      period_hourly_cost        3.88
      period_cost               20.25
      days_of_week              [1,2,3,4,5]
      fe_limit                  5.0      
    end    
    
    trait :half_day_am_3_yr_olds_fe do
      period_name               "Half Day - AM 3 Year Olds FE"
      period_start_time         Time.new(2000,01,01,8,00,00)
      period_end_time           Time.new(2000,01,01,13,00,00)
      period_hourly_cost        3.88      
      period_cost               19.40
      days_of_week              [1,2,3,4,5]
      fe_limit                  5.0      
    end    
    
    trait :half_day_am_3_yr_olds_nofe do
      period_name               "Half Day - AM 3 Year Olds No FE"
      period_start_time         Time.new(2000,01,01,8,00,00)
      period_end_time           Time.new(2000,01,01,13,00,00)
      period_hourly_cost        3.88      
      period_cost               19.40
      days_of_week              [1,2,3,4,5]
      fe_limit                  0.0      
    end     
    
    trait :half_day_pm_3_yr_olds do
      period_name               "Half Day - PM 3 Year Olds"
      period_start_time         Time.new(2000,01,01,13,00,00)
      period_end_time           Time.new(2000,01,01,18,00,00)
      period_hourly_cost        3.88      
      period_cost               19.40
      days_of_week              [1,2,3,4,5]
      fe_limit                  5.0      
    end        
  end
  
  factory :periodation do
    period
    room
  end
  
  factory :preset do
    nursery
    preset_name                 "Full Day"
  end

  factory :period_automation do
    period
    preset
  end
  
  factory :child do 
    nursery
    status_type
    first_name                  "First"
    middle_name                 "Middle"
    surname                     "Surname"
    date_of_birth               Date.new(2010,05,19)
    start_date                  Date.new(2012,06,01) 
    association :contact_info, factory: :child_contact_info

    trait :zaynab do
      first_name                "Zaynab"
      surname                   "Najib"    
      date_of_birth             Date.new(2012,8,01)
    end
    
    trait :mayya do
      first_name                "Mayya"
      surname                   "Najib"
      date_of_birth             Date.new(2010,6,15)
    end
  end
  
  factory :hair do
    color                       "Brown"
  end
  
  factory :eye_color do
    shade                       "Blue"
  end
  
  factory :religion do
    name                        "Christian"
  end
  
  factory :ethnicity do
    race                        "Caucasian"
  end
  
  factory :nationality do
    country                     "British"
  end
  
  factory :language  do
    tongue                      "British"
  end
  
  factory :status_type   do
    status_name                "Registered"
  end
  
  factory :booking do
    nursery
    child
    period
    start_date                  Date.today
    end_date                    Date.new(2016,11,01)
    every                       "Week"
    days_of_week                [1,3,5]
end

  factory :employment do
    nursery
    user
  end
  
  factory :timesheet do
    nursery
    child
    period
    date_attended         Date.today.beginning_of_week
    duration              12600
    hourly_cost           7.25
    period_cost           18.75
    full_period           true
    before(:create) do |timesheet|
      timesheet.term_id = timesheet.current_term  
      timesheet.fe_time = timesheet.populate_fe_time.to_i    
    end    
    
    trait :full_day_1_yr_olds do
      duration              36000
      hourly_cost           7.25
      period_cost           38.00
      full_period           true      
    end
    
    trait :full_day_3_yr_olds do
      duration              36000
      hourly_cost           3.60
      period_cost           36.00
      full_period           true      
    end    
    
    trait :half_day_1_yr_olds do
      duration              18000
      hourly_cost           7.25
      period_cost           20.25
      full_period           true      
    end    
    
    trait :half_day_3_yr_olds do
      duration              18000
      hourly_cost           3.88
      period_cost           19.40
      full_period           true      
    end    
  end
  
  factory :child_relationship_type do
    relationship_type     "Mother"
  end
  
  factory :child_relationship do
    child
    carer
  end
  
  factory :invoice do
    nursery
    carer
    account
    number                1000
    issue                 1
    state                 "new"
  end
  
  factory :period_price do
    period
    start_timestamp         Time.now
    price                   18.75
  end  
  
  factory :billing_period do
    nursery
    start_date              Date.new(2013,06,01)
    end_date                Date.new(2013,06,30)
    status                  "open"
    
    trait :september do
    start_date              Date.new(2013,9,1)
    end_date                Date.new(2013,9,30)      
    end
  end
  
  factory :invoice_line_item do
    invoice
    account
    child
    issue                   1
    description             "PM Hourly Sessions"
    quantity                3.75
    cost                    7.35
    total_cost              27.56 
  end
  
  factory :discount do
    nursery
    name                    "All Day Discount"
    discount_type           "Percent"
    amount                  15.0
    discountable_id         1
    discountable_type       "Room"
    start_date              Date.today - 1.year
    end_date                Date.today + 1.year
    applies_to              "Session"
  end   
  
  factory :term  do
    local_authority
    description             "Term 2"
    start_date              Date.new(2013,10,28)
    end_date                Date.new(2013,12,20)
    
    trait :term_1 do
      description           "Term 1"
      start_date            Date.new(2013,9,5)
      end_date              Date.new(2013,10,17)
    end
  end
end