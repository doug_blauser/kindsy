# == Schema Information
#
# Table name: roles
#
#  id            :integer         not null, primary key
#  name          :string(255)
#  resource_id   :integer
#  resource_type :string(255)
#  created_at    :datetime        not null
#  updated_at    :datetime        not null
#

require 'spec_helper'

describe Role do
  before { @role = Role.new(name: "admin", resource_id: 1, resource_type: "some string") }
  
  subject { @role }
  
  tables = [:users]
  tables.each do |table|
    describe "#{table} association" do
      it { should have_and_belong_to_many(table) }
    end
  end
  
  tables = [:resource] 
  tables.each do |table|
    describe "#{table} associations" do
      it { should belong_to(table) }
    end
  end
  
  tables = [:permissions]
  tables.each do |table|
    describe "#{table} associations" do
      it { should have_many(table)}
    end
  end
  
  fields = [:name, :resource_id, :resource_type]
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass-assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end
  end
  
  fields = [:name]
  fields.each do |field|
    describe "it should require #{field}" do
      it { should validate_presence_of(field) }
    end    
  end
  
  it { should be_valid }
end
