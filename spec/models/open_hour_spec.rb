# == Schema Information
#
# Table name: open_hours
#
#  id          :integer         not null, primary key
#  nursery_id  :integer
#  day_of_week :string(255)
#  open_time   :time
#  close_time  :time
#  created_at  :datetime        not null
#  updated_at  :datetime        not null
#

require 'spec_helper'

describe OpenHour do
  let(:nursery_group) { FactoryGirl.create(:nursery_group) }
  let(:contact_info) { FactoryGirl.create(:contact_info) }
  let(:region) { FactoryGirl.create(:region) }
  let(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: nursery_group.id, region_id: region.id) }  
  before { @open_hour = OpenHour.new(nursery_id: nursery.id, day_of_week: "Monday", open_time: '07:00:00', close_time: "18:00:00") }
  
  subject { @open_hour }
  
  tables = [:nursery] 
  tables.each do |table|
    describe "#{table} association" do
      it { should belong_to table }
    end
  end
  
  fields = [:nursery_id, :day_of_week, :open_time, :close_time]
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end
  end
  
  fields = [:nursery_id, :day_of_week, :open_time, :close_time] 
  fields.each do |field|
    describe "it should require #{field}" do
      it { should validate_presence_of(field) }
    end
  end  

  
  fields = [:open_time, :close_time]
  fields.each do |field|

    valid_times = %w[07:00:00, "10AM", "5PM", 18:15:00, 6]
    describe "when #{field} field format is valid" do
      valid_times.each do |valid_time|
        it { should allow_value(valid_time).for(field) }        
      end
    end
    
    invalid_times = ["AM", "PM", ""]
    describe "when #{field} field format is invalid" do
      invalid_times.each do |invalid_time|
        it { should_not allow_value(invalid_time).for(field) }        
      end
    end    
  end 

  it { should be_valid }
  
end
