# == Schema Information
#
# Table name: regions
#
#  id         :integer         not null, primary key
#  name       :string(255)
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

require 'spec_helper'

describe Region do
  before { @region = Region.new(name: "South East") }
  
  subject { @region }

  tables = [:nurseries]
  tables.each do |table|
    describe "#table association" do
      it { should have_many(table).dependent(:destroy) }
    end
  end
  
  fields = [:name]
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end
    
    describe "it should require #{field}" do
      it { should validate_presence_of(field) }
    end    
  end
  
  it  { should be_valid }
end
