# == Schema Information
#
# Table name: currencies
#
#  id               :integer         not null, primary key
#  currency_type_id :integer
#  nursery_id       :integer
#  created_at       :datetime        not null
#  updated_at       :datetime        not null
#

require 'spec_helper'

describe Currency do
  let(:currency_type) { FactoryGirl.create(:currency_type) }
  let(:contact_info) {FactoryGirl.create(:contact_info) }
  let(:group) { FactoryGirl.create(:nursery_group) }
  let(:region) { FactoryGirl.create(:region) }
  let(:nursery) {FactoryGirl.create(:nursery, nursery_group_id: group.id, region_id: region.id)}  
  before { @currency = Currency.new(currency_type_id: currency_type.id, nursery_id: nursery.id) }
  
  subject { @currency }  
  
  tables = [:currency_type, :nursery] 
  tables.each do |table|
    describe "#{table} association" do
      it { should belong_to table }
    end
  end  
  
  fields = [:currency_type_id, :nursery_id]
  fields.each do |field|
    describe "it should respond to {#field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end
    
    describe "it should require #{field} " do
      it { should validate_presence_of(field) }
    end     
  end
  
  it { should be_valid }  
end
