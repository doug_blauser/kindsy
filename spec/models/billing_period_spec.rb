# == Schema Information
#
# Table name: billing_periods
#
#  id         :integer         not null, primary key
#  nursery_id :integer
#  start_date :date
#  end_date   :date
#  status     :string(255)
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

require 'spec_helper'

describe BillingPeriod do
  let(:group) { FactoryGirl.create(:nursery_group) }
  let(:region) { FactoryGirl.create(:region) }
  let!(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: group.id, region_id: region.id) }
  before { @billing_period = BillingPeriod.new(nursery_id: nursery.id,
                                               start_date: Date.new(2013,06,01),
                                               end_date: Date.new(2013,06,30),
                                               status: "open")} 
  subject { @billing_period }
   
  tables = [:nursery]
  tables.each do |table|
    describe "#{table} associations" do
      it { should belong_to table } 
    end    
  end
  
  tables = [:invoices]
  tables.each do |table|
    describe "#{table} associations" do
      it { should have_many(table).dependent(:destroy) }
    end
  end
  
  fields = [:nursery_id, :start_date, :end_date, :status] 
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end
    
    describe "it should require #{field}" do
      it { should validate_presence_of(field) }
    end    
  end      

  fields = [:nursery_id]
  fields.each do |field|
    describe "it should require #{field} value be an integer" do
      it { should validate_numericality_of(field).only_integer }
    end
  end  

 
  describe "Add New Period Method" do
    subject { BillingPeriod.add_new_period(nursery) } 
    
    it "should have nursery_id == nursery.id" do
      subject.nursery_id.should == nursery.id
    end
    
    it "should have start_date with current year, month, and nursery's billing_cycle_start_day" do
      subject.start_date.should == Date.new(Date.today.year, Date.today.month, nursery.billing_cycle_start_day)
    end
    
    it "should have end_date one month less one day from the start_date" do
      subject.end_date.should == subject.start_date + 1.month - 1.day
    end
    
    describe "if no current billing period exists" do
      before do
        @billing_period_count = BillingPeriod.count 
        @new_billing_period = BillingPeriod.add_new_period(nursery)  
      end
      
      it "should increase billing period count by 1" do
        BillingPeriod.count.should == @billing_period_count + 1
      end
    end
    
    describe "if current billing period exists" do
      before do        
        start_date = Date.today
        end_date = start_date + 1.month - 1.day         
        @current_period = nursery.billing_periods.create(nursery_id: nursery.id, start_date: start_date, end_date: end_date, status: "open")  
        @billing_period_count = BillingPeriod.count
        @new_billing_period = BillingPeriod.add_new_period(nursery)          
      end 
      
      it "should be nil" do         
         @new_billing_period.should == nil
      end    
      
      it "should not increase billing period count by 1" do
        BillingPeriod.count.should_not == @billing_period_count + 1
      end             
    end   
  end

                                                      
end
