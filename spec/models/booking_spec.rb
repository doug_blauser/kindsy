# == Schema Information
#
# Table name: bookings
#
#  id           :integer         not null, primary key
#  nursery_id   :integer
#  child_id     :integer
#  period_id    :integer
#  start_date   :date
#  end_date     :date
#  every        :string(255)
#  days_of_week :text
#  created_at   :datetime        not null
#  updated_at   :datetime        not null
#

require 'spec_helper'

describe Booking do
  let!(:nursery_group) { FactoryGirl.create(:nursery_group) }
  let!(:nursery_contact_info) { FactoryGirl.create(:contact_info) }
  let!(:region) { FactoryGirl.create(:region) }
  let!(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: nursery_group.id, region_id: region.id) }
  let!(:user) { FactoryGirl.create(:user) }
  let!(:child_contact_info) { FactoryGirl.create(:child_contact_info) }
  let!(:status_type) { FactoryGirl.create(:status_type) } 
  let!(:room) { FactoryGirl.create(:room, nursery_id: nursery.id, name: "Blue Room") } 
  let!(:hair) { FactoryGirl.create(:hair) }
  let!(:eye_color) { FactoryGirl.create(:eye_color) }
  let!(:religion) { FactoryGirl.create(:religion) }
  let!(:ethnicity) { FactoryGirl.create(:ethnicity) }
  let!(:nationality) { FactoryGirl.create(:nationality) }
  let!(:language) { FactoryGirl.create(:language) }
  let!(:status) { FactoryGirl.create(:status_type) }
  let!(:period) { FactoryGirl.create(:period) }
  let!(:child) { FactoryGirl.create(:child, 
                                    nursery_id: nursery.id,
                                    room_id: room.id, 
                                    status_type_id: status.id, 
                                    preferred_name: "Preferred Name", 
                                    hair_id: hair.id,
                                    eye_color_id: eye_color.id,
                                    religion_id: religion.id,
                                    ethnicity_id: ethnicity.id,
                                    nationality_id: nationality.id,
                                    language_id: language.id ) } 
  before { @booking = nursery.bookings.build(nursery_id: nursery.id, 
                                             child_id:child.id,
                                             period_id:period.id,
                                             start_date: Date.today,
                                             end_date: Date.new(2016,11,01),
                                             every: "Week",
                                             days_of_week: [1,3,5]) }
                                         
  subject { @booking }
  
  tables = [:nursery, :child, :period] 
  tables.each do |table|
    describe "#{table} association" do
      it { should belong_to table }
    end
  end    
  
  tables = [:charges]
  tables.each do |table|
    describe "#{table} association" do
      it { should have_many table }  
    end    
  end  
  
  fields = [:child_id, :days_of_week, :end_date, :every, :nursery_id, :period_id, :start_date]
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end
    
    describe "it should require #{field}" do
      it { should validate_presence_of(field) }
    end      
  end                              

  fields = [:nursery_id, :child_id, :period_id]
  fields.each do |field|
    describe "when #{field} field format is valid" do
      it { should validate_numericality_of(field) }
    end
  end
  

  fields = [:start_date, :end_date]
  fields.each do |field|
    valid_date = /\d{2}\/\d{2}\/\d{4}/
    describe "when #{field} field format is valid" do
      it { should allow_value(valid_date).for(field) }
    end
    
    invalid_date = "this is not a date"     
    describe "when #{field} field format is invalid" do
      it { should_not allow_value(invalid_date).for(field) }
    end   
  end  
      
  it { should be_valid }   
end
