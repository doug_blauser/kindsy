# == Schema Information
#
# Table name: nurseries
#
#  id                      :integer         not null, primary key
#  nursery_group_id        :integer
#  name                    :string(255)
#  created_at              :datetime        not null
#  updated_at              :datetime        not null
#  region_id               :integer
#  billing_cycle_start_day :integer
#  local_authority_id      :integer
#

require 'spec_helper'

describe Nursery do
  let(:currency_type) { FactoryGirl.create(:currency_type) }
  let(:currency) { FactoryGirl.create(:currency) }
  let(:contact_info) {FactoryGirl.create(:contact_info) }
  let(:group) { FactoryGirl.create(:nursery_group) }
  let(:region) { FactoryGirl.create(:region) } 
  let(:local_authority) { FactoryGirl.create(:local_authority) } 

  before { @nursery = group.nurseries.build(name: "The Old Station Nursery @ The Marina", region_id: region.id, billing_cycle_start_day: 5, local_authority_id: local_authority.id) }                                  

  subject { @nursery } 
  
  tables = [:nursery_group, :region, :local_authority]  
  tables.each do |table| 
    describe "#{table} association" do
      it { should belong_to table }  
    end
  end   
 
  tables = [:contact_info, :currency] 
  tables.each do |table| 
    describe "#{table} association" do
      it { should have_one table }  
    end
  end     
  
  tables = [:rooms, :staff_members, :children, :open_hours, :periods, :presets, :bookings, :timesheets, :accounts, :carers, :invoices, :billing_periods, :discounts]
  tables.each do |table|
    describe "#{table} association" do
      it { should have_many table}
    end
  end
  
  it { should have_many(:users).through(:employments) }
                   
  fields = [:name, :region_id, :billing_cycle_start_day, :local_authority_id]                               
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end
  end
  
  fields = [:nursery_group_id]
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) } 
    end
    
    describe "it should not allow mass assignment of #{field}" do
      it { should_not allow_mass_assignment_of(field) }
    end    
  end
    
  fields = [:nursery_group_id, :name, :region_id, :billing_cycle_start_day]  
  fields.each do |field|
    describe "it should require #{field}" do
      it { should validate_presence_of(field) }
    end
  end
  
  fields = [:nursery_group_id, :region_id, :billing_cycle_start_day, :local_authority_id]
  fields.each do |field|
    describe "it should require #{field} value be an integer" do
      it { should validate_numericality_of(field).only_integer }
    end
  end   

  it { should be_valid }
  
end
