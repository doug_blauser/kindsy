# == Schema Information
#
# Table name: currency_types
#
#  id            :integer         not null, primary key
#  currency_name :string(255)
#  created_at    :datetime        not null
#  updated_at    :datetime        not null
#

require 'spec_helper'

describe CurrencyType do
  before { @currency_type = CurrencyType.new(currency_name: "Pound Sterling") }
  
  subject { @currency_type }  
  
  tables = [:currencies]
  tables.each do |table|
    describe "#{table} association" do
      it { should have_many table }
    end
  end  
  
  fields = [:currency_name]
  fields.each do |field|
    describe "it should respond to {#field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end
    
    describe "it should require #{field} " do
      it { should validate_presence_of(field) }
    end     
  end
  
  it { should be_valid }
end
