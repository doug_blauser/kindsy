# == Schema Information
#
# Table name: nursery_groups
#
#  id                        :integer         not null, primary key
#  name                      :string(255)
#  billing_period_start_date :date
#  created_at                :datetime        not null
#  updated_at                :datetime        not null
#

require 'spec_helper'

describe NurseryGroup do
  before { @group = NurseryGroup.new(name: "Old Station Nursery Group", billing_period_start_date: Date.new(2012, 07, 23)) }
  
  subject { @group }
  
  tables = [:nurseries]
  tables.each do |table|
    describe "#{table} association" do
      it { should have_many(table).dependent(:destroy) } 
    end
  end
  
  tables = [:nurseries]
  tables.each do |table|
    describe "#{table} association" do
      it { should have_many table } 
    end
  end  
  
  fields = [:billing_period_start_date, :name]  
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end
    
    describe "it should require #{field}" do
      it { should validate_presence_of(field) }
    end
  end
  
  it { should be_valid }
end
