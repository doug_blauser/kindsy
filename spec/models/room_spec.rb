# == Schema Information
#
# Table name: rooms
#
#  id              :integer         not null, primary key
#  nursery_id      :integer
#  staff_member_id :integer
#  name            :string(255)
#  capacity        :integer
#  age_from        :integer
#  age_to          :integer
#  created_at      :datetime        not null
#  updated_at      :datetime        not null
#

require 'spec_helper'

describe Room do
  let(:nursery_group) { FactoryGirl.create(:nursery_group) }
  let(:contact_info) { FactoryGirl.create(:contact_info) }
  let(:region) { FactoryGirl.create(:region) }
  let(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: nursery_group.id, region_id: region.id) }  
  let(:staff_contact_info) { FactoryGirl.create(:staff_contact_info) }
  let(:staff_member) { FactoryGirl.create(:staff_member, nursery_id: nursery.id, contact_info_id: staff_contact_info.id) } 
  before { @room = Room.new(nursery_id: nursery.id, name: "Red Room", capacity: 12, age_from: 12, age_to: 18, staff_member_id: staff_member.id) }
  
  subject { @room }
  
  tables = [:nursery, :staff_member] 
  tables.each do |table|
    describe "#{table} association" do
      it { should belong_to table }
    end
  end
  
  tables = [:periods, :periodations, :discounts]
  tables.each do |table|
    describe "#{table} association" do
      it { should have_many table}
    end
  end  
  
  fields = [:age_from, :age_to, :capacity, :name, :nursery_id, :staff_member_id]
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end
  end
  
  fields = [:name, :nursery_id]
  fields.each do |field|
    describe "it should require #{field}" do
      it { should validate_presence_of(field) }
    end
  end
  
  fields = [:nursery_id, :capacity, :age_from, :age_to, :staff_member_id]
  fields.each do |field|
    describe "it should require #{field} value be an integer" do
      it { should validate_numericality_of(field).only_integer}
    end
  end
  
  it { should be_valid }
  
end
