# == Schema Information
#
# Table name: accountabilities
#
#  id         :integer         not null, primary key
#  account_id :integer
#  carer_id   :integer
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

require 'spec_helper'

describe Accountability do
  let(:nursery) { FactoryGirl.create(:nursery) }
  let(:account) { FactoryGirl.create(:account, nursery_id: nursery.id) }
  let(:carer) { FactoryGirl.create(:carer, nursery_id: nursery.id) }
  before { @accountability = Accountability.new(account_id: account.id, carer_id: carer.id) }
  
  subject { @accountability }
  
  tables = [:account, :carer]
  tables.each do |table|
    describe "#{table} associations" do
      it { should belong_to table }
    end
  end
  
  fields = [:account_id, :carer_id]
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end
    
    describe "it should require #{field}" do
      it { should validate_presence_of(field) }
    end    
    
    describe "when #{field} field format is valid" do
      it { should validate_numericality_of(field).only_integer }
    end    
  end  
  
  it { should be_valid }
    
end
