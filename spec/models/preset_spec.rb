# == Schema Information
#
# Table name: presets
#
#  id          :integer         not null, primary key
#  nursery_id  :integer
#  preset_name :string(255)
#  start_time  :time
#  end_time    :time
#  created_at  :datetime        not null
#  updated_at  :datetime        not null
#

require 'spec_helper'

describe Preset do
  let(:nursery_group) { FactoryGirl.create(:nursery_group) }
  let(:contact_info) { FactoryGirl.create(:contact_info) }
  let(:region) { FactoryGirl.create(:region) }
  let(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: nursery_group.id, region_id: region.id) }  
  let(:room) { FactoryGirl.create(:room) }
  let(:period) { FactoryGirl.create(:period, nursery_id: nursery.id) }
  let(:period_room) { FactoryGirl.create(:periodation, period_id: period.id, room_id: room.id) }

  before { @preset = nursery.presets.build(nursery_id: nursery.id, preset_name: "Full Day") }
  
  subject { @preset }
  
  tables = [:nursery] 
  tables.each do |table|
    describe "#{table} association" do
      it { should belong_to table }
    end
  end    
  
  tables = [:periods]
  tables.each do |table|
    describe "#{table} association" do
      it { should have_many table }
    end
  end
  
  tables = [:periods_automations]
  tables.each do |table|
    describe "#{table} association" do
      it { should have_many(:period_automations).dependent(:destroy) }
    end
  end
  
  fields = [:nursery_id, :preset_name]
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end 
    
    describe "it should require #{field}" do
      it { should validate_presence_of(field) }
    end    
  end
  
  it { should be_valid }          
end
