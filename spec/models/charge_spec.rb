# == Schema Information
#
# Table name: charges
#
#  id                   :integer         not null, primary key
#  invoice_line_item_id :integer
#  chargeable_id        :integer
#  chargeable_type      :string(255)
#  created_at           :datetime        not null
#  updated_at           :datetime        not null
#

require 'spec_helper'

describe Charge do
  let(:group) { FactoryGirl.create(:nursery_group) }
  let(:region) { FactoryGirl.create(:region) }
  let!(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: group.id, region_id: region.id) }
  let(:nursery_contact_info) { FactoryGirl.create(:contact_info) } 
  let(:user) { FactoryGirl.create(:user) }
  let(:account) { FactoryGirl.create(:account, user_id: user.id, nursery_id: nursery.id) } 
  let(:staff_contact_info) { FactoryGirl.create(:staff_contact_info) }
  let(:staff_member) { FactoryGirl.create(:staff_member, nursery_id: nursery.id, contact_info_id: staff_contact_info.id) }
  let(:room) { FactoryGirl.create(:room, nursery_id: nursery.id, staff_member_id: staff_member.id) }
  let(:carer_contact_info) { FactoryGirl.create(:carer_contact_info) }
  let(:carer) { FactoryGirl.create(:carer, nursery_id: nursery.id) }  
  let(:child) { FactoryGirl.create(:child, nursery_id: nursery.id, staff_member_id: staff_member.id, room_id: room.id, carer_id: carer.id) }  
  let(:billing_period) { FactoryGirl.create(:billing_period) }
  let(:invoice) { FactoryGirl.create(:invoice, nursery_id: nursery.id, carer_id: carer.id, account_id: account.id, billing_period_id: billing_period.id) }
  let(:line_item) { FactoryGirl.create(:invoice_line_item, invoice_id: invoice.id, account_id: account.id, child_id: child.id) }  
  let(:timesheet) { FactoryGirl.create(:timesheet) }
  before { @charge = Charge.new(invoice_line_item_id: line_item.id,
                                chargeable_id: timesheet.id,
                                chargeable_type: "Timesheet") }
  subject { @charge }         
  
  tables = [:chargeable, :invoice_line_item]
  tables.each do |table|
    describe "#{table} association" do
      it { should belong_to table }
    end
  end    
  
  fields = [:chargeable_id, :chargeable_type, :invoice_line_item_id]  
    fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
  end
  
  fields = [:chargeable_id, :chargeable_type]  
    fields.each do |field|
    describe "it should require #{field}" do
      it { should validate_presence_of(field) }
    end
  end  
  
  fields = [:chargeable_id]
  fields.each do |field|
    describe "it should require #{field} value be an integer" do
      it { should validate_numericality_of(field).only_integer }
    end    
  end

  it { should be_valid }                         
end
