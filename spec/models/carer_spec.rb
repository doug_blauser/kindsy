# == Schema Information
#
# Table name: carers
#
#  id         :integer         not null, primary key
#  title      :string(255)
#  first_name :string(255)
#  surname    :string(255)
#  created_at :datetime        not null
#  updated_at :datetime        not null
#  nursery_id :integer
#

require 'spec_helper'

describe Carer do
  let(:group) { FactoryGirl.create(:nursery_group) }
  let(:region) { FactoryGirl.create(:region) }
  let!(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: group.id, region_id: region.id) }      
  let(:contact_info) { FactoryGirl.create(:carer_contact_info) }
  
  before { @carer = nursery.carers.build(title: "Mr", first_name: "George", surname: "Jones") }
  
  subject { @carer }
  
  tables = [:nursery]
  tables.each do |table|
    describe "#{table} association" do
      it { should belong_to table }
    end
  end

  tables = [:contact_info] 
  tables.each do |table| 
    describe "#{table} association" do
      it { should have_one table }  
    end
  end  
  
  tables = [:children]
  tables.each do |table|
    describe "#{table} association" do
      it { should have_many(table).through(:child_relationships) }
    end
  end
    
  tables = [:accountabilities, :child_relationships, :invoices]
  tables.each do |table|
    describe "#{table} association" do
      it { should have_many(table).dependent(:destroy)}
    end
  end   
  
  tables = [:accounts]
  tables.each do |table|
    describe "#{table} associations" do
      it { should have_many(table).through(:accountabilities) }
    end
  end  
  

  fields = [:title, :first_name, :surname, :nursery_id]  
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end
  end 
  
  fields = [:first_name, :surname, :nursery_id]  
  fields.each do |field|
    describe "it should require #{field}" do
      it { should validate_presence_of(field) }
    end
  end 
  
  fields = [:nursery_id]  
  fields.each do |field|
    describe "it should require #{field} value be an integer" do
      it { should validate_numericality_of(field).only_integer}
    end
  end    
  
  it { should be_valid }
end
