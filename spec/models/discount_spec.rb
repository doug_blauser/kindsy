# == Schema Information
#
# Table name: discounts
#
#  id                :integer         not null, primary key
#  nursery_id        :integer
#  name              :string(255)
#  discount_type     :string(255)
#  amount            :decimal(6, 3)
#  created_at        :datetime        not null
#  updated_at        :datetime        not null
#  discountable_id   :integer
#  discountable_type :string(255)
#  start_date        :date
#  end_date          :date
#  applies_to        :string(255)
#

require 'spec_helper'

describe Discount do
  let(:group) { FactoryGirl.create(:nursery_group) }
  let(:region) { FactoryGirl.create(:region) }
  let!(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: group.id, region_id: region.id) }
  let(:nursery_contact_info) { FactoryGirl.create(:contact_info) } 
  let(:user) { FactoryGirl.create(:user) }
  let(:staff_contact_info) { FactoryGirl.create(:staff_contact_info) }
  let(:staff_member) { FactoryGirl.create(:staff_member, nursery_id: nursery.id, contact_info_id: staff_contact_info.id) }  
  let(:room) { FactoryGirl.create(:room, nursery_id: nursery.id, staff_member_id: staff_member.id) }
  let!(:period) { FactoryGirl.create(:period) }    
  before { @discount = Discount.new(nursery_id: nursery.id,
                                    name: "Early Birds",
                                    discount_type: "Percent",
                                    amount: 10.000,
                                    discountable_id: room.id,
                                    discountable_type: "Room",
                                    start_date: Date.today,
                                    applies_to: "Session") }
                                    
  subject { @discount }
  
  tables = [:nursery, :discountable]
  tables.each do |table|
    describe "#{table} associations" do
      it { should belong_to table } 
    end
  end   
  

  fields = [:amount, :discount_type, :name, :nursery_id, :discountable_id, :discountable_type, :start_date, :end_date, :applies_to]                                              
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end   
  end   
  
  fields = [:amount, :discount_type, :name, :nursery_id, :discountable_id, :discountable_type, :start_date, :applies_to] 
  fields.each do |field|
    describe "it should require #{field}" do
      it { should validate_presence_of(field) }
    end 
  end  
  
  fields = [:nursery_id, :discountable_id]
  fields.each do |field|
    describe "it should require #{field} value be an integer" do
      it { should validate_numericality_of(field).only_integer }
    end  
  end

  fields = [:amount]
  fields.each do |field|
    describe "it should require #{field} value to be a number" do
      it { should validate_numericality_of(field) }
    end
  end
  
  describe "amount should be between 0 and 100" do
    it { should ensure_inclusion_of(:amount).in_range(0..100) }
  end
  
  it { should be_valid }
end
