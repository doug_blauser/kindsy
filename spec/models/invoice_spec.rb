# == Schema Information
#
# Table name: invoices
#
#  id                :integer         not null, primary key
#  nursery_id        :integer
#  carer_id          :integer
#  account_id        :integer
#  issue             :integer
#  state             :string(255)
#  created_at        :datetime        not null
#  updated_at        :datetime        not null
#  billing_period_id :integer
#  number            :integer
#

require 'spec_helper'

describe Invoice do
  let(:group) { FactoryGirl.create(:nursery_group) }
  let(:region) { FactoryGirl.create(:region) }
  let!(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: group.id, region_id: region.id) }
  let(:nursery_contact_info) { FactoryGirl.create(:contact_info) } 
  let(:user) { FactoryGirl.create(:user) }
  let(:account) { FactoryGirl.create(:account, user_id: user.id, nursery_id: nursery.id) } 
  let(:staff_contact_info) { FactoryGirl.create(:staff_contact_info) }
  let(:staff_member) { FactoryGirl.create(:staff_member, nursery_id: nursery.id, contact_info_id: staff_contact_info.id) }
  let(:room) { FactoryGirl.create(:room, nursery_id: nursery.id, staff_member_id: staff_member.id) }
  let(:carer_contact_info) { FactoryGirl.create(:carer_contact_info) }
  let(:carer) { FactoryGirl.create(:carer, nursery_id: nursery.id) }  
  let(:child) { FactoryGirl.create(:child, nursery_id: nursery.id, staff_member_id: staff_member.id, room_id: room.id, carer_id: carer.id) }  
  let(:billing_period) { FactoryGirl.create(:billing_period) }
                          
  before { @invoice = Invoice.new(nursery_id: nursery.id,
                                  carer_id: carer.id,
                                  account_id: account.id,
                                  number: 1000,
                                  issue: 1,
                                  state: "new",
                                  billing_period_id: billing_period.id) }                                  
  subject { @invoice }    
  
  it { should be_valid }      
  
  tables = [:nursery, :carer, :account, :billing_period]  
  tables.each do |table|
    describe "#{table} associations" do
      it { should belong_to table } 
    end
  end          
  
  tables = [:invoice_line_items] 
  tables.each do |table|
    describe "#{table} associations" do
      it { should have_many(table).dependent(:destroy) }
    end
  end               

  fields = [:nursery_id, :carer_id, :account_id, :issue, :state, :billing_period_id, :number]  
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end
    
    describe "it should require #{field}" do
      it { should validate_presence_of(field) }
    end    
  end   
  
  fields = [:nursery_id, :carer_id, :account_id, :issue, :billing_period_id, :number]
  fields.each do |field|
    describe "it should require #{field} value be an integer" do
      it { should validate_numericality_of(field).only_integer }
    end
  end      

end
