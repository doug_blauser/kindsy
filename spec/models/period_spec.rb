# == Schema Information
#
# Table name: periods
#
#  id                 :integer         not null, primary key
#  nursery_id         :integer
#  period_name        :string(255)
#  period_start_time  :time
#  period_end_time    :time
#  period_hourly_cost :decimal(5, 2)
#  created_at         :datetime        not null
#  updated_at         :datetime        not null
#  period_cost        :decimal(5, 2)
#  days_of_week       :text
#  fe_limit           :decimal(6, 3)
#

require 'spec_helper'

describe Period do
  let(:nursery_group) { FactoryGirl.create(:nursery_group) }
  let(:contact_info) { FactoryGirl.create(:contact_info) }
  let(:region) { FactoryGirl.create(:region) }
  let(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: nursery_group.id, region_id: region.id) }  
  before { @period = nursery.periods.build(nursery_id: nursery.id, period_name: "AM", period_start_time: Time.new(2000,01,01,06,30), period_end_time: Time.new(2000,01,01,13,00), period_hourly_cost: 7.50, period_cost: 34.5, days_of_week: [1,3,5], fe_limit: 2.5) }
  
  subject { @period }
  
  tables = [:nursery] 
  tables.each do |table|
    describe "#{table} association" do
      it { should belong_to table }
    end
  end  
  
  tables = [:rooms, :presets, :timesheets, :discounts]
  tables.each do |table|
    describe "#{table} association" do
      it { should have_many table}
    end
  end
  
  tables = [:periodations, :period_automations, :period_prices]
  tables.each do |table|
    describe "#{table} association" do
      it { should have_many(table).dependent(:destroy)}
    end
  end  
  
  fields = [:nursery_id, :period_name, :period_start_time, :period_end_time, :period_hourly_cost, :period_cost, :days_of_week, :fe_limit]
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end
    
    describe "it should require #{field}" do
      it { should validate_presence_of(field) }
    end    
  end
  
  fields = [:period_start_time, :period_end_time]
  fields.each do |field|

    valid_times = %w[07:00:00, "10AM", "5PM", 18:15:00, 6]
    describe "when #{field} field format is valid" do
      valid_times.each do |valid_time|
        it { should allow_value(valid_time).for(field) }        
      end
    end
    
    invalid_times = ["AM", "PM", ""]
    describe "when #{field} field format is invalid" do
      invalid_times.each do |invalid_time|
        it { should_not allow_value(invalid_time).for(field) }        
      end
    end    
  end   
  
  fields = [:period_hourly_cost, :period_cost, :fe_limit]
  fields.each do |field|
    describe "when #{field} field format is valid" do
      it { should validate_numericality_of(field) }
    end
  end
  
  it { should be_valid }  
end
