# == Schema Information
#
# Table name: staff_members
#
#  id                        :integer         not null, primary key
#  nursery_id                :integer
#  contact_info_id           :integer
#  first_name                :string(255)
#  middle_name               :string(255)
#  surname                   :string(255)
#  title                     :string(255)
#  date_of_birth             :date
#  medical_conditions        :text
#  start_date                :date
#  leave_date                :date
#  payroll_number            :string(255)
#  hours_per_week_contracted :integer
#  hourly_rate               :float
#  start_of_holiday_year     :date
#  holiday_entitlement       :float
#  created_at                :datetime        not null
#  updated_at                :datetime        not null
#

require 'spec_helper'

describe StaffMember do
  let(:nursery_group) { FactoryGirl.create(:nursery_group) }
  let(:region) { FactoryGirl.create(:region) }
  let(:contact_info) { FactoryGirl.create(:contact_info) }
  let(:staff_contact_info) { FactoryGirl.create(:staff_contact_info) }
  let(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: nursery_group.id, region_id: region.id) }
  
  before { @staff_member = nursery.staff_members.build(nursery_id: nursery.id,
                                          contact_info_id: staff_contact_info.id, 
                                          first_name: "Beverly", 
                                          middle_name: "Jean", 
                                          surname: "Jones",
                                          title: "Ms",
                                          date_of_birth: Date.new(1986,3,10),
                                          medical_conditions: "none",
                                          start_date: Date.new(2010,9,1),
                                          leave_date: Date.new(2012,8,15),
                                          payroll_number: "XYZ123",
                                          hours_per_week_contracted: 35,
                                          start_of_holiday_year: Date.new(2011,9,1),
                                          holiday_entitlement: 14.5 ) }
  
  subject { @staff_member }
  
  tables = [:nursery, :contact_info]
  tables.each do |table|
    describe "#{table} association" do
      it { should belong_to table }
    end
  end
  
  tables = [:children]
  tables.each do |table|
    describe "#{table} association" do
      it { should have_many table }
    end
  end
  
  fields = [:nursery_id, :contact_info_id, :first_name, :middle_name, :surname, :title, :date_of_birth, :medical_conditions, :start_date, :leave_date, :payroll_number, :hours_per_week_contracted, :hourly_rate, :start_of_holiday_year, :holiday_entitlement]                              
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end
  end 
  
  fields = [:nursery_id, :contact_info_id, :first_name, :surname]
  fields.each do |field|
    describe "it should require #{field}" do
      it { should validate_presence_of(field) }
    end
  end   
  
  fields = [:nursery_id, :contact_info_id]
  fields.each do |field|
    describe "it should require #{field} value be an integer" do
      it { should validate_numericality_of(field).only_integer}
    end
  end  
end

