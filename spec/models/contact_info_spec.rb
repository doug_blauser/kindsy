# == Schema Information
#
# Table name: contact_infos
#
#  id               :integer         not null, primary key
#  address_line_1   :string(255)
#  address_line_2   :string(255)
#  address_line_3   :string(255)
#  town             :string(255)
#  county           :string(255)
#  country          :string(255)
#  post_code        :string(255)
#  telephone_1      :string(255)
#  telephone_2      :string(255)
#  telephone_3      :string(255)
#  telephone_4      :string(255)
#  skype            :string(255)
#  email_1          :string(255)
#  email_2          :string(255)
#  website          :string(255)
#  created_at       :datetime        not null
#  updated_at       :datetime        not null
#  addressable_id   :integer
#  addressable_type :string(255)
#

require 'spec_helper'

describe ContactInfo do

  before { @contact_info = ContactInfo.new(address_line_1: "The Marina, Lincolnshire, LN1 1RD",
                                           address_line_2: "The Marina, Lincolnshire, LN1 1RD",
                                           address_line_3: "The Marina, Lincolnshire, LN1 1RD",
                                           town:           "Somewhere",
                                           county:         "Lincolnshire",
                                           country:        "England",
                                           post_code:      "LN1 1RD",
                                           telephone_1:    "01522511333",
                                           telephone_2:    "123123123",
                                           telephone_3:    "123456789",
                                           telephone_4:    "012345678",
                                           skype:          "marinanursery",
                                           email_1:        "marina@theoldstationnursery.com",
                                           email_2:        "info@theoldstationnursery.com",
                                           website:         "theoldstationnursery.com")}      

  subject { @contact_info }
  
  tables = [:staff_members]        
  tables.each do |table|
    describe "#{table} association" do
      it { should have_many table}
    end
  end
  
  tables = [:addressable]    
  tables.each do |table|
    describe "#{table} association" do
      it { should belong_to table }
    end
  end  
  
  fields = [:address_line_1, :address_line_2, :address_line_3, :town, :county, :country, :post_code, :telephone_1, :telephone_2, :telephone_3, :telephone_4,  :skype, :email_1, :email_2, :website]                                                            
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end
  end
  
  fields = [:email_1, :email_2]
  invalid_addresses = %w[user@foo,com user_at_foo.org example.user@foo.]
  valid_addresses = %w[user@foo.com A_USER@f.b.org frst.lst@foo.jp a+b@baz.cn]
  fields.each do |field|
    describe "when #{field} field format is invalid" do
      invalid_addresses.each do |invalid_address|
        it { should_not allow_value(invalid_address).for(field) }
      end
    end
    
    describe "when #{field} field format is valid" do
      valid_addresses.each do |valid_address|
        it { should allow_value(valid_address).for(field) }
      end  
    end
  end
  
  fields = [:telephone_1, :telephone_2, :telephone_3, :telephone_4]
  invalid_telephone_numbers = ["0208 123-4567", "+44 0 1234 567-890", "this is not valid"]
  valid_telephone_numbers = ["02081234567","0208 123 4567", "+44 208 123 4567", "+44 (0) 208 123 4567"]
  fields.each do |field|
      describe "when #{field} field format is invalid" do
        invalid_telephone_numbers.each do |invalid_telephone_number|
          it { should_not allow_value(invalid_telephone_number).for(field) }
        end    
      end
      
      describe "when #{field} field format is valid" do
        valid_telephone_numbers.each do |valid_telephone_number|
          it { should allow_value(valid_telephone_number).for(field) }
        end
      end
  end

  invalid_websites = ["--this is invalid.gov", "newwebsite.123", "somewebsite.com+"] 
  valid_websites = %w[google.com theoldstationnursery.com] 
  describe "when website field format is invalid" do
    invalid_websites.each do |invalid_website|
      it { should_not allow_value(invalid_website).for(:website) }    
    end
  end
  
  describe "when website field format is valid" do
    valid_websites.each do |valid_website|
      it { should allow_value(valid_website).for(:website) }
    end
  end 
  
  it { should be_valid }   
end


