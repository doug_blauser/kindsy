# == Schema Information
#
# Table name: permissions
#
#  id         :integer         not null, primary key
#  role_id    :integer
#  action     :string(255)
#  condition  :string(255)
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

require 'spec_helper'

describe Permission do
  let(:user) { FactoryGirl.create(:user) }
  let(:role) { FactoryGirl.create(:role) }
  let(:user_role) { FactoryGirl.create(:users_roles) }
  
  before { @permission = role.permissions.build(role_id: role.id, action: "access") }
  
  subject { @permission }
  
  tables = [:role]
  tables.each do |table|
    describe "#{table} association" do
      it { should belong_to(table) } 
    end
  end
  
  fields = [:role_id, :action, :condition]
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass_assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end
  end
  
  fields = [:role_id, :action]
  fields.each do |field|
    describe "it should require #{field}" do
      it { should validate_presence_of(field) }
    end
  end
  
  it { should be_valid }
end
