require "spec_helper"
require "cancan/matchers"

describe Ability do
  subject { ability }
  
  describe "as guest" do
    let(:ability) { Ability.new(nil) }
    


    context "cannot create a user" do
      it { should_not be_able_to(:access, :users) }
    end

    tables = ActiveRecord::Base.connection.tables
    tables.each do |table|
      context "cannot access #{table}" do
        it { should_not be_able_to(:access, table) }
      end     
    end

  end
  
  describe "as logged-in user" do
   
    context "with a role of admin" do 
      let(:admin_user) { FactoryGirl.create(:user, :email => "auser@example.com") }     
      let(:role) { FactoryGirl.create(:role) } 
      before { admin_user.roles << role }
      before { @permission = Permission.create(role_id: role.id, action: "access", condition: "all") }      
      let(:ability) { Ability.new(admin_user) }     

      tables = ActiveRecord::Base.connection.tables     
      tables.each do |table|
        it { should be_able_to(:access, table) } 
      end
    end
    
    context "with a role of manager" do
      let(:manager) { FactoryGirl.create(:user, :username => "manageruser", :email => "manager@example.com") }     
      let(:role) { FactoryGirl.create(:role, name: "manager", resource_type: "Nursery") } 
      before { manager.roles << role }
      before { @permission = Permission.create(role_id: role.id, action: "show", condition: nil) }
      before { @permission2 = Permission.create(role_id: role.id, action: "update", condition: nil)}      
      let(:ability) { Ability.new(manager) }     
      
      it { should be_able_to(:show, :nursery) }  
      it { should be_able_to(:update, :nursery) }
    end
    
    context "with a role of employee" do
      let(:employee) { FactoryGirl.create(:user, :username => "employeeuser", :email => "employee@example.com") }     
      let(:role) { FactoryGirl.create(:role, name: "employee", resource_type: "User") } 
      let(:role2) {FactoryGirl.create(:role, name: "carer", resource_type: "Nursery")}
      before { employee.roles << role }
      before { employee.roles << role2 }
      before { @permission = Permission.create(role_id: role.id, action: "update", condition: nil) } 
      before { @permission2 = Permission.create(role_id: role2.id, action: "show", condition: nil) }
      let(:ability) { Ability.new(employee) }     
       
      it { should be_able_to(:update, :user) }
      it { should be_able_to(:show, :nursery) }
    end
  end
end
