# == Schema Information
#
# Table name: timesheets
#
#  id            :integer         not null, primary key
#  nursery_id    :integer
#  child_id      :integer
#  period_id     :integer
#  date_attended :date
#  duration      :integer
#  created_at    :datetime        not null
#  updated_at    :datetime        not null
#  hourly_cost   :decimal(5, 2)
#  period_cost   :decimal(5, 2)
#  full_period   :boolean
#  fe_time       :integer
#  term_id       :integer
#

require 'spec_helper'

describe Timesheet do
  let!(:nursery_group) { FactoryGirl.create(:nursery_group) }
  let!(:nursery_contact_info) { FactoryGirl.create(:contact_info) }
  let!(:region) { FactoryGirl.create(:region) }
  let!(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: nursery_group.id, region_id: region.id) }
  let!(:user) { FactoryGirl.create(:user) }
  let!(:child_contact_info) { FactoryGirl.create(:child_contact_info) }
  let!(:status_type) { FactoryGirl.create(:status_type) } 
  let!(:room) { FactoryGirl.create(:room, nursery_id: nursery.id, name: "Blue Room") } 
  let!(:hair) { FactoryGirl.create(:hair) }
  let!(:eye_color) { FactoryGirl.create(:eye_color) }
  let!(:religion) { FactoryGirl.create(:religion) }
  let!(:ethnicity) { FactoryGirl.create(:ethnicity) }
  let!(:nationality) { FactoryGirl.create(:nationality) }
  let!(:language) { FactoryGirl.create(:language) }
  let!(:status) { FactoryGirl.create(:status_type) }
  let!(:period) { FactoryGirl.create(:period) }
  let!(:child) { FactoryGirl.create(:child, 
                                    nursery_id: nursery.id,
                                    room_id: room.id, 
                                    status_type_id: status.id, 
                                    preferred_name: "Preferred Name", 
                                    hair_id: hair.id,
                                    eye_color_id: eye_color.id,
                                    religion_id: religion.id,
                                    ethnicity_id: ethnicity.id,
                                    nationality_id: nationality.id,
                                    language_id: language.id ) } 
  before { @timesheet = child.timesheets.build(nursery_id: nursery.id,
                                               child_id: child.id,
                                               period_id: period.id,
                                               date_attended: Date.new(2013,9,16),
                                               duration: 12600,
                                               hourly_cost: 7.25,
                                               period_cost: 18.50,
                                               full_period: true,
                                               fe_time: 5400) }  
  subject { @timesheet }     
  
  tables = [:nursery, :child, :period, :term]
  tables.each do |table|
    describe "#{table} association" do
      it { should belong_to table }
    end
  end
  
  tables = [:charge]
  tables.each do |table|
    describe "#{table} association" do
      it { should have_many table }      
    end    
  end
  
  fields = [:nursery_id, :child_id, :period_id, :date_attended, :duration, :hourly_cost, :period_cost, :full_period, :fe_time]   
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end    
  end                                                                               

  fields = [:nursery_id, :child_id, :period_id, :duration]
  fields.each do |field|
    describe "when #{field} field format is valid" do
      it { should validate_numericality_of(field).only_integer }
    end
    
    describe "it should require #{field}" do
      it { should validate_presence_of(field) }
    end      
  end
  
  fields = [:hourly_cost, :period_cost]
  fields.each do |field|
    describe "when #{field} field format is valid" do
      it { should validate_numericality_of(field) }
    end
    
    describe "it should require #{field}" do
      it { should validate_presence_of(field) }
    end      
  end  
  
  fields = [:fe_time]
  fields.each do |field|
    describe "when #{field} format is valid" do
      it { should validate_numericality_of(field).only_integer }
    end
  end

  fields = [:date_attended]
  fields.each do |field|
    
    describe "it should require #{field}" do
      it { should validate_presence_of(field) }
    end      
    
    valid_date = /\d{2}\/\d{2}\/\d{4}/
    describe "when #{field} field format is valid" do
      it { should allow_value(valid_date).for(field) }
    end
        
    invalid_date = "this is not a date"     
    describe "when #{field} field format is invalid" do
      it { should_not allow_value(invalid_date).for(field) }
    end   
  end  
  
  fields = [:full_period] 
  fields.each do |field|
    describe "it should require #{field}" do
      it { should validate_presence_of(field).with_message(/requires a true or false value/) }
    end      
  end  
  
  it { should be_valid }  
end
