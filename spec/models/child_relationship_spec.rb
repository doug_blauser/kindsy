# == Schema Information
#
# Table name: child_relationships
#
#  id                         :integer         not null, primary key
#  child_id                   :integer
#  carer_id                   :integer
#  child_relationship_type_id :integer
#  created_at                 :datetime        not null
#  updated_at                 :datetime        not null
#  pick_up_flag               :boolean
#  drop_off_flag              :boolean
#

require 'spec_helper'

describe ChildRelationship do
  let(:nursery_group) { FactoryGirl.create(:nursery_group) }
  let(:region) { FactoryGirl.create(:region) }
  let(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: nursery_group.id, region_id: region.id) }  
  let(:user) { FactoryGirl.create(:user) }
  let(:account) { FactoryGirl.create(:account, user_id: user.id, nursery_id: nursery.id) }
  let(:carer_contact_info) { FactoryGirl.create(:carer_contact_info) }  
  let(:carer) { FactoryGirl.create(:carer, nursery_id: nursery.id)}
  let(:status_type) { FactoryGirl.create(:status_type) } 
  let(:child) { FactoryGirl.create(:child, nursery_id: nursery.id,
                                   carer_id: carer.id,
                                   gender_id: 1,
                                   status_type_id: status_type.id) }
  let(:relationship_type) { FactoryGirl.create(:child_relationship_type) }                                
  before { @child_relationship = ChildRelationship.new(child_id: child.id, carer_id: carer.id, child_relationship_type_id: relationship_type.id, pick_up_flag: true, drop_off_flag: true) }

  subject { @child_relationship }
  
  tables = [:child, :carer, :child_relationship_type]
  tables.each do |table|
    describe "#{table} associations" do
      it { should belong_to table} 
    end
  end  

  fields = [:child_id, :carer_id, :child_relationship_type_id, :pick_up_flag, :drop_off_flag]
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end
  end  
  
  fields = [:child_id, :carer_id, :child_relationship_type_id]
  fields.each do |field| 
    describe "when #{field} field format is valid" do
      it { should validate_numericality_of(field) }
    end  
    
    describe "it should require #{field} " do
      it { should validate_presence_of(field) }
    end      
  end
  
  fields = [:pick_up_flag, :drop_off_flag]
  fields.each do |field|
    describe "it should require #{field}" do
      its(field) { should eq(true) }
    end
  end  
    
  it { should be_valid }  
end
