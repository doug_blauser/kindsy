# == Schema Information
#
# Table name: period_prices
#
#  id              :integer         not null, primary key
#  period_id       :integer
#  start_timestamp :datetime
#  end_timestamp   :datetime
#  price           :decimal(5, 2)
#  created_at      :datetime        not null
#  updated_at      :datetime        not null
#

require 'spec_helper'

describe PeriodPrice do
  let(:nursery_group) { FactoryGirl.create(:nursery_group) }
  let(:contact_info) { FactoryGirl.create(:contact_info) }
  let(:region) { FactoryGirl.create(:region) }
  let(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: nursery_group.id, region_id: region.id) }  
  let(:period) { FactoryGirl.create(:period, nursery_id: nursery.id) }
  before { @period_price = period.period_prices.build(period_id: period.id,
                                                      start_timestamp: Time.now,
                                                      end_timestamp: Time.now,
                                                      price: 34.5) }   
  
  subject { @period_price }
  
  it { should be_valid }   
  
  tables = [:period]  
  tables.each do |table|
    describe "#{table} associations" do
      it { should belong_to table } 
    end
  end 
  
  fields = [:period_id, :start_timestamp, :end_timestamp, :price]
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end
    
  end      
  
  fields = [:period_id, :start_timestamp, :price]
  fields.each do |field|             
    describe "it should require #{field}" do
      it { should validate_presence_of(field) }
    end    
  end 
  
  fields = [:period_id]
  fields.each do |field|
    describe "it should require #{field} value be an integer" do
      it { should validate_numericality_of(field).only_integer }
    end
  end   
  
  fields = [:price]
  fields.each do |field|
    describe "it should require #{field} value to be a number" do
      it { should validate_numericality_of(field) }
    end
  end                                           
end
