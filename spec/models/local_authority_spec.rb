# == Schema Information
#
# Table name: local_authorities
#
#  id         :integer         not null, primary key
#  name       :string(255)
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

require 'spec_helper'

describe LocalAuthority do
  before { @local_authority = LocalAuthority.new(name: "Lincolnshire") }
  
  subject { @local_authority }
  
  it { should be_valid }
  
  tables = [:contact_info] 
  tables.each do |table| 
    describe "#{table} association" do
      it { should have_one table }  
    end
  end   
  
  tables = [:nurseries, :terms]
  tables.each do |table| 
    describe "#{table} association" do
      it { should have_many(table) }  
    end
  end 
    
  fields = [:name]
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end     
  end

  fields = [:name]
  fields.each do |field|
    describe "it should require #{field} " do
      it { should validate_presence_of(field) }
    end
  end    
end
