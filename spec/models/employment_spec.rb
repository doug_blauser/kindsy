# == Schema Information
#
# Table name: employments
#
#  id         :integer         not null, primary key
#  nursery_id :integer
#  user_id    :integer
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

require 'spec_helper'

describe Employment do
  let(:currency_type) { FactoryGirl.create(:currency_type) }
  let(:currency) { FactoryGirl.create(:currency) }
  let(:contact_info) {FactoryGirl.create(:contact_info) }
  let(:group) { FactoryGirl.create(:nursery_group) }
  let(:region) { FactoryGirl.create(:region) } 
  let(:nursery) {FactoryGirl.create(:nursery) }
  let(:user) { FactoryGirl.create(:user) }
  before { @employment = Employment.new(nursery_id: nursery.id, user_id: user.id) }
  
  subject { @employment }
  
  tables = [:nursery, :user]
  tables.each do |table|
    describe "#{table} association" do
      it { should belong_to table }
    end  
  end  
  
  fields = [:nursery_id, :user_id]
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end
    
    describe "it should require #{field}" do
      it { should validate_presence_of(field) }
    end    
    
    describe "when #{field} field format is valid" do
      it { should validate_numericality_of(field).only_integer }
    end    
  end   
  
  it { should be_valid }  
end
