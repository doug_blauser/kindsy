# == Schema Information
#
# Table name: invoice_line_items
#
#  id          :integer         not null, primary key
#  invoice_id  :integer
#  account_id  :integer
#  description :string(255)
#  cost        :decimal(5, 2)
#  total_cost  :decimal(6, 2)
#  created_at  :datetime        not null
#  updated_at  :datetime        not null
#  quantity    :decimal(5, 2)
#  child_id    :integer
#  editable    :boolean
#  issue       :integer
#  fe_amount   :decimal(5, 2)
#

require 'spec_helper'

describe InvoiceLineItem do
  let(:group) { FactoryGirl.create(:nursery_group) }
  let(:region) { FactoryGirl.create(:region) }
  let!(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: group.id, region_id: region.id) }
  let(:nursery_contact_info) { FactoryGirl.create(:contact_info) } 
  let(:user) { FactoryGirl.create(:user) }
  let(:account) { FactoryGirl.create(:account, user_id: user.id, nursery_id: nursery.id) } 
  let(:staff_contact_info) { FactoryGirl.create(:staff_contact_info) }
  let(:staff_member) { FactoryGirl.create(:staff_member, nursery_id: nursery.id, contact_info_id: staff_contact_info.id) }
  let(:room) { FactoryGirl.create(:room, nursery_id: nursery.id, staff_member_id: staff_member.id) }
  let(:carer_contact_info) { FactoryGirl.create(:carer_contact_info) }
  let(:carer) { FactoryGirl.create(:carer, nursery_id: nursery.id) }  
  let(:child) { FactoryGirl.create(:child, nursery_id: nursery.id, staff_member_id: staff_member.id, room_id: room.id, carer_id: carer.id) }  
  let(:billing_period) { FactoryGirl.create(:billing_period) }
  let(:invoice) { FactoryGirl.create(:invoice, nursery_id: nursery.id, carer_id: carer.id, account_id: account.id, billing_period_id: billing_period.id) }
  before { @line_item = invoice.invoice_line_items.build(account_id: account.id,
                                                         issue: invoice.issue,
                                                         description: "38 Sessions",
                                                         quantity: 38,
                                                         cost: 18.65,
                                                         total_cost: 708.70,
                                                         child_id: child.id) }
                                                         
  subject { @line_item }      
  
  tables = [:invoice, :account, :child]
  tables.each do |table|
    describe "#{table} associations" do
      it { should belong_to table }
    end
  end   
  
  tables = [:charges] 
  tables.each do |table|
    describe "#{table} associations" do
      it { should have_many(table).dependent(:destroy) }  
    end
  end           
  
  it { should be_valid } 
  
  fields = [:account_id, :cost, :description, :total_cost, :quantity, :issue]  
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end
    
    describe "it should require #{field}" do
      it { should validate_presence_of(field) }
    end        
  end

  fields = [:invoice_id, :child_id, :editable, :fe_amount]  
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end
  end    
  
  fields = [:account_id, :child_id, :issue]
  fields.each do |field|
    describe "it should require #{field} value be an integer" do
      it { should validate_numericality_of(field).only_integer }
    end
  end  
  
  fields = [:cost, :total_cost, :quantity, :fe_amount]
  fields.each do |field|
    describe "it should require #{field} value to be a number" do
      it { should validate_numericality_of(field) }
    end
  end                                   
end
