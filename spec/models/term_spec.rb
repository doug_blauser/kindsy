# == Schema Information
#
# Table name: terms
#
#  id                 :integer         not null, primary key
#  local_authority_id :integer
#  description        :string(255)
#  start_date         :date
#  end_date           :date
#  created_at         :datetime        not null
#  updated_at         :datetime        not null
#

require 'spec_helper'

describe Term do
  let(:local_authority) { FactoryGirl.create(:local_authority) }
  before { @term = local_authority.terms.build(local_authority_id: local_authority.id, description: "Term2", start_date: Date.new(2013,10,28), end_date: Date.new(2013,12,20))}
  
  subject { @term }
  
  tables = [:local_authority]
  tables.each do |table|
    describe "#{table} association" do
      it { should belong_to table }
    end 
  end
  
  tables = [:timesheets]
  tables.each do |table|
    describe "#{table} association" do
      it { should have_many(table) }      
    end     
  end
  
  fields = [:description, :start_date, :end_date, :local_authority_id]
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end
    
    describe "it should require #{field}" do
      it { should validate_presence_of(field) }
    end      
  end

  fields = [:local_authority_id]
  fields.each do |field| 
    describe "when #{field} field format is valid" do
      it { should validate_numericality_of(field).only_integer }
    end    
  end
  
  it { should be_valid }  
end
