# == Schema Information
#
# Table name: children
#
#  id              :integer         not null, primary key
#  nursery_id      :integer
#  account_id      :integer
#  staff_member_id :integer
#  room_id         :integer
#  carer_id        :integer
#  first_name      :string(255)
#  middle_name     :string(255)
#  surname         :string(255)
#  preferred_name  :string(255)
#  gender_id       :string(255)
#  date_of_birth   :date
#  start_date      :date
#  leave_date      :date
#  created_at      :datetime        not null
#  updated_at      :datetime        not null
#  AEN             :string(255)
#  hair_id         :integer
#  eye_color_id    :integer
#  religion_id     :integer
#  ethnicity_id    :integer
#  nationality_id  :integer
#  language_id     :integer
#  status_type_id  :integer
#

require 'spec_helper'

describe Child do
  let(:nursery_group) { FactoryGirl.create(:nursery_group) }
  let(:region) { FactoryGirl.create(:region) }
  let(:nursery_contact_info) { FactoryGirl.create(:contact_info) }
  let(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: nursery_group.id, region_id: region.id) }  
  let(:user) { FactoryGirl.create(:user) }
  let(:account) { FactoryGirl.create(:account, user_id: user.id, nursery_id: nursery.id) }
  let(:staff_contact_info) { FactoryGirl.create(:staff_contact_info) }
  let(:staff_member) { FactoryGirl.create(:staff_member, nursery_id: nursery.id, contact_info_id: staff_contact_info.id) }
  let(:room) { FactoryGirl.create(:room, nursery_id: nursery.id, staff_member_id: staff_member.id) }
  let(:carer_contact_info) { FactoryGirl.create(:carer_contact_info) }
  let(:carer) { FactoryGirl.create(:carer, nursery_id: nursery.id) }
  let(:child_contact_info) { FactoryGirl.create(:child_contact_info) }
  let(:status_type) { FactoryGirl.create(:status_type) }
  before { @child = Child.new(nursery_id: nursery.id, 
                              account_id: account.id,
                              staff_member_id: staff_member.id,
                              room_id: room.id,
                              carer_id: carer.id,
                              first_name: "Frank",
                              surname: "Roosevelt",
                              preferred_name: "Frankie",
                              gender_id: 1,
                              date_of_birth: Date.new(2011,5,15),
                              start_date: Date.new(2011,8,7),
                              status_type_id: status_type.id) }         

  subject { @child }
  
  it { should be_valid }
  
  tables = [:nursery, :account, :staff_member, :room, :carer, :hair, :eye_color, :religion, :ethnicity, :nationality, :language, :status_type]  
  tables.each do |table|
    describe "#{table} associations" do
      it { should belong_to table} 
    end
  end
  
  tables = [:contact_info] 
  tables.each do |table| 
    describe "#{table} association" do
      it { should have_one table }  
    end
  end  
  
  tables = [:bookings, :timesheets, :child_relationships, :invoice_line_items] 
  tables.each do |table| 
    describe "#{table} association" do
      it { should have_many(table).dependent(:destroy) }  
    end
  end 
  
  tables = [:discounts]
  tables.each do |table|
    describe "#{table} association" do
      it { should have_many table }
    end
  end        
  
  fields = [:nursery_id, :account_id, :staff_member_id, :room_id, :carer_id, :first_name, :middle_name, :surname, :preferred_name, :date_of_birth, :start_date, :leave_date, :AEN, :hair_id, :eye_color_id, :religion_id, :ethnicity_id, :nationality_id, :language_id, :status_type_id]
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end     
  end
  
  fields = [:nursery_id, :first_name, :surname, :date_of_birth, :start_date, :status_type_id]
  fields.each do |field|
    describe "it should require #{field} " do
      it { should validate_presence_of(field) }
    end
  end
  
  it { should be_valid }         
end
