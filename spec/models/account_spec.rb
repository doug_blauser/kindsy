# == Schema Information
#
# Table name: accounts
#
#  id                     :integer         not null, primary key
#  user_id                :integer
#  invoice_type           :string(255)
#  sage_account_reference :string(255)
#  created_at             :datetime        not null
#  updated_at             :datetime        not null
#  account_no             :string(255)
#  nursery_id             :integer
#  account_name           :string(255)
#

require 'spec_helper'

describe Account do
  let(:user) { FactoryGirl.create(:user) }
  let(:nursery) { FactoryGirl.create(:nursery) }
  before { @account = user.accounts.build(account_no: "SMI65479", invoice_type: "paper", sage_account_reference: "formatted to meet sage requirements", nursery_id: nursery.id, account_name: "Account Owner") }
  
  subject { @account }
  
  tables = [:user, :nursery]
  tables.each do |table|
    describe "#{table} associations" do
      it { should belong_to table }
    end
  end
  
  tables = [:children]
  tables.each do |table|
    describe "#{table} associations" do
      it { should have_many table }
    end
  end
  
  tables = [:discounts]
  tables.each do |table|
    describe "#{table} associations" do
#      it { should have_and_belong_to_many table }
      it { should have_many table }
    end
  end  
  
  tables = [:accountabilities, :invoices]
  tables.each do |table|
    describe "#{table} association" do
      it { should have_many(table).dependent(:destroy)}
    end
  end   
  
  tables = [:carers]
  tables.each do |table|
    describe "#{table} associations" do
      it { should have_many(table).through(:accountabilities) }
    end
  end
  
  fields = [:account_no, :invoice_type, :sage_account_reference, :user_id, :account_name, :nursery_id]
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end
  end
  
  fields = [:user_id, :account_no, :nursery_id, :account_name]
  fields.each do |field|
    describe "it should require #{field}" do
      it { should validate_presence_of(field) }
    end
  end

  fields = [:user_id, :nursery_id]
  fields.each do |field|
    describe "it should require #{field} value be an integer" do
      it { should validate_numericality_of(field).only_integer}
    end
  end
   
  it { should be_valid }  
end
