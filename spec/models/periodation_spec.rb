# == Schema Information
#
# Table name: periodations
#
#  id         :integer         not null, primary key
#  period_id  :integer
#  room_id    :integer
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

require 'spec_helper'

describe Periodation do
  let(:nursery_group) { FactoryGirl.create(:nursery_group) }
  let(:contact_info) { FactoryGirl.create(:contact_info) }
  let(:region) { FactoryGirl.create(:region) }
  let(:nursery) { FactoryGirl.create(:nursery, nursery_group_id: nursery_group.id, region_id: region.id) }
  let(:room) { FactoryGirl.create(:room, nursery_id: nursery.id )}
  let(:period) {FactoryGirl.create(:period, nursery_id: nursery.id) }
  before { @periodation = Periodation.new(period_id: period.id, room_id: room.id)}

  subject { @periodation }
 
  tables = [:period, :room] 
  tables.each do |table|
    describe "#{table} association" do
      it { should belong_to table }
    end
  end    
 
  fields = [:period_id, :room_id]
  fields.each do |field|
    describe "it should respond to #{field}" do
      it { should respond_to(field) }
    end
    
    describe "it should allow mass assignment of #{field}" do
      it { should allow_mass_assignment_of(field) }
    end
    
    describe "it should require #{field}" do
      it { should validate_presence_of(field) }
    end    
    
    describe "when #{field} field format is valid" do
      it { should validate_numericality_of(field).only_integer }
    end    
  end   
  
  it { should be_valid }

end
