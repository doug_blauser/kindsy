class InvoiceLineItemsController < ApplicationController
  enable_authorization
    
  def new
    @invoice = Invoice.find(params[:invoice_id])
    @invoice_line_item = @invoice.invoice_line_items.build
    @top_tab = "Finance"
    @bottom_menu = "tab6" 
    @bottom_tab = 4    
  end
  
  def create 
    @invoice = Invoice.find(params[:invoice_id])
    @invoice_line_item = @invoice.invoice_line_items.build(params[:invoice_line_item])
    @invoice_line_item.account_id = @invoice.account_id
    @invoice_line_item.issue = @invoice.issue
    @invoice_line_item.total_cost = (params[:invoice_line_item][:quantity].to_f) * (params[:invoice_line_item][:cost].to_f)
    @invoice_line_item.editable = true
    if @invoice_line_item.save
      redirect_to invoice_path(@invoice)
      flash[:notice] = "Line item was added to this invoice"
    else
      flash[:notice] = "Line item was not added to this invoice"
      render :action => 'new'
    end
  end
  
  def edit
    @invoice = Invoice.find(params[:invoice_id])
    @invoice_line_item = InvoiceLineItem.find(params[:id])
    redirect_to(invoice_path(@invoice)) unless @invoice_line_item.editable
  end
  
  def update
    @invoice = Invoice.find(params[:invoice_id])
    @invoice_line_item = InvoiceLineItem.find(params[:id])
    if @invoice_line_item.update_attributes(params[:invoice_line_item])
      redirect_to invoice_path(@invoice)
      flash[:notice] = "Line item was updated on this invoice"  
    else
      flash[:notice] = "Line item was not updated this invoice"
      render :action => 'edit'           
    end

  end
end
