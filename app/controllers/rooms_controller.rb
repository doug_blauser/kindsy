class RoomsController < ApplicationController
  enable_authorization 

  def show
    @room = Room.find(params[:id])
  end
  
  def index
    @nursery = current_user.current_nursery
    @rooms = @nursery.rooms
    @top_tab = "Nursery Settings"
    @bottom_menu = "tab9" 
    @bottom_tab = 9    
  end
  
  def new
    @nursery = current_user.current_nursery
    @room = @nursery.rooms.build
    @top_tab = "Nursery Settings"
    @bottom_menu = "tab9" 
    @bottom_tab = 9     
  end
  
  def create
    @nursery = current_user.current_nursery
    @room = @nursery.rooms.build(params[:room])
    if @room.save
      redirect_to rooms_path
      flash[:notice] = "Room was added to nursery"  
    else
      flash[:notice] = "Room was not added to nursery"
      render :action => 'new'
    end
  end
  
  def edit
    @nursery = current_user.current_nursery
    @room = Room.find(params[:id])
    @top_tab = "Nursery Settings"
    @bottom_menu = "tab9" 
    @bottom_tab = 9    
  end
  
  def update
    @nursery = current_user.current_nursery
    @room = Room.find(params[:id])
    if @room.update_attributes(params[:room])
      redirect_to rooms_path
      flash[:notice] = "Room values were changed" 
    else
      flash[:notice] = "Room values were not changed"
      render :action => 'edit' 
    end
  end
end
