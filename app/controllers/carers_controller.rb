class CarersController < ApplicationController
  enable_authorization  
  
  def new
    @nursery = current_user.current_nursery
    @carer = @nursery.carers.build
    @contact_info = @carer.build_contact_info
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 4    
  end
  
  def create
    @nursery = current_user.current_nursery
    @carer = @nursery.carers.build(params[:carer])
    if @carer.save
      redirect_to carers_path
      flash[:notice] = "Carer was added"
    else
      flash[:notice] = "No carer was added"
      render :action => 'new'
    end
  end
  
  def index
    @nursery = current_user.current_nursery
    @carers = @nursery.carers
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 4    
  end
  
  def edit
    @nursery = current_user.current_nursery
    @carer = Carer.find(params[:id])
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 4    
  end
  
  def update
    @nursery = current_user.current_nursery
    @carer = Carer.find(params[:id])
    if @carer.update_attributes(params[:carer])
      redirect_to carers_path
      flash[:notice] = "Carer values were changed"
    else
      flash[:notice] = "No Carer values were changed"
      render :action => 'edit'
    end
  end

  def show 
    @carer = Carer.find(params[:id])
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 4    
    @sidetab = "Summary"
  end
  
  def detail
    @carer = Carer.find(params[:id])
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 4
    @sidetab = "Personal Information"    
  end
  
  def relationships
    @carer = Carer.find(params[:id])
    @child_relations = @carer.child_relationships
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 4
    @sidetab = "Relationships"    
  end
  
  def remove_relationship
    @carer = Carer.find(params[:id])
    @child = Child.find(params[:child_id])
    ChildRelationship.destroy_all(carer_id: @carer.id, child_id: @child.id)
    redirect_to relationships_carer_path
    flash[:notice] = "Relationship was removed from carer"
  end
  
  def search_children
    @nursery = current_user.current_nursery
    @carer = Carer.find(params[:id])
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 4    
    if params.has_key?(:query)
      selected_field = params[:field]
      query = params[:query]
    else
      selected_field = 'surname'
      query = nil
    end
    @results = Child.search(selected_field, query, @nursery.id)    
    @relationship_types = ChildRelationshipType.all.map { |relationship| [relationship.relationship_type, relationship.id] }    
  end
  
  def add_children
    @carer = Carer.find(params[:id])
    @relations = params[:child_relationship_type_ids].delete_if { |relationship| relationship.empty? }
    @pick_up_flags = params[:pick_up_flag].delete_if { |flag| flag.empty? }
    @drop_off_flags = params[:drop_off_flag].delete_if { |flag| flag.empty? }
    params[:child_ids].each do |child|
      @child_relationship = ChildRelationship.create(child_id: child, carer_id: @carer.id, child_relationship_type_id: @relations[params[:child_ids].index(child)], pick_up_flag: @pick_up_flags[params[:child_ids].index(child)], drop_off_flag: @drop_off_flags[params[:child_ids].index(child)] )
    end
    redirect_to relationships_carer_path(@carer)
    flash[:notice] = "Children were related to this carer"
  end
  
  def search_accounts
    @nursery = current_user.current_nursery
    @carer = Carer.find(params[:id])
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 4    
    if params.has_key?(:query)
      selected_field = params[:field]
      query = params[:query]
    else
      selected_field = 'account_name'
      query = nil
    end    
    @results = Account.search(selected_field, query, @nursery.id)        
  end
  
  def assign_accounts
    logger.info "starting to assign accounts"
    @carer = Carer.find(params[:id])
    logger.info "account_ids #{params[:account_ids]}"
    params[:account_ids].each do |account|
      @accountability = Accountability.create(carer_id: @carer.id, account_id: account)
    end        
    redirect_to carer_path(@carer)
    flash[:notice] = "Accounts were added to this carer"    
  end
end
