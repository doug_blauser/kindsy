class CurrenciesController < ApplicationController
  enable_authorization  
  
  def new
    @nursery = current_user.current_nursery
    @currency = @nursery.build_currency
  end
  
  def create
    @nursery = current_user.current_nursery
    @currency_type = CurrencyType.find_by_currency_name(params[:currency][:currency_type_id])
    @currency = @nursery.build_currency(currency_type_id: @currency_type.id)
    if @currency.save
      redirect_to currencies_path
      flash[:notice] = "Currency was added for nursery"
    else
      flash[:notice] = "Currency was not added to nursery"
      render :action => 'new'
    end
  end
  
  def edit
    @nursery = current_user.current_nursery
    @currency = Currency.find(params[:id])
  end
  
  def update
    @nursery = current_user.current_nursery
    @currency = Currency.find(params[:id])
    @currency_type = CurrencyType.find_by_currency_name(params[:currency][:currency_type_id])
    if @currency.update_attributes(currency_type_id: @currency_type.id)
      redirect_to currencies_path
      flash[:notice] = "Currency value was changed"
    else
      flash[:notice] = "Currency value was not changed"
      render :action => 'edit'
    end     
  end
  
  def index
    @nursery = current_user.current_nursery
    @currency = @nursery.currency
    @local_authority = @nursery.local_authority
    @top_tab = "Nursery Settings"
    @bottom_menu = "tab9" 
    @bottom_tab = 5    
  end
end
