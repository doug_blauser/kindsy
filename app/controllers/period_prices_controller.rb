class PeriodPricesController < ApplicationController
  enable_authorization
    
  def new
    @period = Period.find(params[:period_id])
    @period_price = @period.period_prices.build
  end
  
  def create
    @period = Period.find(params[:period_price][:period_id])
    @period_price = @period.period_prices.build(params[:period_price])
    if @period_price.save
      redirect_to periods_path
      flash[:notice] = "New Period Price added"
    else
      flash[:notice] = "Period Price was not added" 
      render :action => 'new'  
    end
  end
  
  def edit
    @nursery = current_user.current_nursery    
    @period_price = PeriodPrice.find(params[:id])
  end
  
  def update
    @nursery = current_user.current_nursery    
    @period_price = PeriodPrice.find(params[:id])
    if @period_price.update_attributes(params[:period_price]) 
      redirect_to periods_path
      flash[:notice] = "Period price values were changed"
    else
      flash[:notice] = "Period price values were not changed"
      render :action => 'edit'
    end   
  end
end
