class BillingPeriodsController < ApplicationController
  enable_authorization
    
  def new
    @nursery = current_user.current_nursery
    @billing_period = @nursery.billing_periods.build
    @current_billing_period = BillingPeriod.current_billing_period
  end
  
  def create
    logger.info "#{'started billing period create'}"
    binding.pry
    @nursery = current_user.current_nursery
    @billing_period = @nursery.billing_periods.build
    @billing_period.start_date = Date.new(Date.today.year, Date.today.month, @nursery.billing_cycle_start_day)
    @billing_period.end_date = @billing_period.start_date + 1.month - 1.day
    @billing_period.save    
  end
  
  def edit
    @billing_period = BillingPeriod.find(params[:id])
    @current_billing_period = BillingPeriod.current_billing_period    
  end
  
  def update
    @billing_period = BillingPeriod.find(params[:id])
    @billing_period.end_date = @billing_period.set_end_date(@billing_period.start_date)    
    if @billing_period.update_attributes(params[:billing_period])
      redirect_to billing_periods_path
      flash[:notice] = "Billing period values were changed"
    else
      @current_billing_period = BillingPeriod.current_billing_period
      flash[:notice] = "Billing period values were not changed"
      render :action => 'edit'
    end
  end
  
  def index
    @nursery = current_user.current_nursery
    @billing_periods = @nursery.billing_periods
    @top_tab = "Finance"
    @bottom_menu = "tab6" 
    @bottom_tab = 1     
  end
  
  def show
    @billing_period = BillingPeriod.find(params[:id])
  end
end
