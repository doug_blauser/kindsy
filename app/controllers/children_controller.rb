class ChildrenController < ApplicationController
  enable_authorization
  
  def index
    @nursery = current_user.current_nursery
    @children = @nursery.children
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 1    
  end
  
  def new
    @nursery = current_user.current_nursery
    @child = @nursery.children.build
    @contact_info = @child.build_contact_info
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 1     
  end
  
  def create
    @nursery = current_user.current_nursery
    @child = @nursery.children.build(params[:child])
    if @child.save
      redirect_to children_path
      flash[:notice] = "Child was added to the nursery"
    else
      flash[:notice] = "Child was not added to the nursery"
      render :action => 'new'
    end    
  end
  
  def edit
    @nursery = current_user.current_nursery
    @child = Child.find(params[:id])
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 1 
  end
  
  def update
    @nursery = current_user.current_nursery
    @child = Child.find(params[:id])
    if @child.update_attributes(params[:child])
      redirect_to children_path
      flash[:notice] = "Child values were changed"
    else
      flash[:notice] = "Child values were not changed"
      render :action => 'edit'
    end
  end
  
  def show
    @nursery = current_user.current_nursery
    @child = Child.find(params[:id])
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 1    
    @sidetab = "Summary"
  end
  
  def detail
    @nursery = current_user.current_nursery 
    @child = Child.find(params[:id])
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 1    
    @sidetab = "Personal"
  end
end
