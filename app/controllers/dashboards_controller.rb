class DashboardsController < ApplicationController
  
  has_widgets do |root|
    root << widget(:currency)
    root << widget(:nursery_group)
    root << widget(:nursery)
    root << widget(:room)
    root << widget(:staff_member)
    root << widget(:carer)
    root << widget(:child)
    root << widget(:contact_info)
  end  
  
  def index
    dashlet_array = [["nursery", 1, 2], ["nursery_group", 1, 1], ["currency", 1, 3], ["room", 2, 2], ["staff_member", 2, 1], ["carer", 3, 1], ["child", 3, 2]]
    @dashlets = dashlet_array.sort_by { |dashlet| dashlet[1..2] }.map
    @top_tab = "Home"
    @bottom_menu = "tab1" 
    @bottom_tab = 1
  end
end
