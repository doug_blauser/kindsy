class TermsController < ApplicationController
  enable_authorization
  
  def index
    @nursery = current_user.current_nursery
    if params[:local_authority].nil?
      @terms = Term.all
    else
      @local_authority = LocalAuthority.find(params[:local_authority])
      @terms = @local_authority.terms
    end
    @top_tab = "Group Settings"
    @bottom_menu = "tab10" 
    @bottom_tab = 6    
  end
  
  def new
    @term = Term.new
    @top_tab = "Group Settings"
    @bottom_menu = "tab10" 
    @bottom_tab = 6
  end
  
  def create
    @term = Term.new(params[:term])
    if @term.save
      redirect_to terms_path
      flash[:notice] = "Term was added for Local Authority"
    else
      flash[:notice] = "Term was not added for Local Authority"
      render :action => 'new'
    end
  end
  
  def edit
    @term = Term.find(params[:id])
    @top_tab = "Group Settings"
    @bottom_menu = "tab10" 
    @bottom_tab = 6
  end
  
  def update
    @term = Term.find(params[:id])
    if @term.update_attributes(params[:term])
      redirect_to terms_path
      flash[:notice] = "Term values were changed" 
    else
      flash[:notice] = "Term values were not changed" 
      render :action => 'edit'
    end
  end
  
  def show
    @term = Term.find(params[:id])
    @top_tab = "Group Settings"
    @bottom_menu = "tab10" 
    @bottom_tab = 6
  end
  
  def destroy
    @term = Term.find(params[:id]).destroy
    flash[:success] = "Term record was deleted"
    redirect_to terms_path
  end
end
