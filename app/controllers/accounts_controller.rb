class AccountsController < ApplicationController
  enable_authorization
  
  def new
    @nursery = current_user.current_nursery
    @account = @nursery.accounts.build
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 2    
  end
  
  def create
    @nursery = current_user.current_nursery
    @account = @nursery.accounts.build(params[:account])
    @account.user_id = current_user.id
    if @account.save
      redirect_to accounts_path
      flash[:notice] = "Account was added to nursery"
    else
      flash[:notice] = "Account was not added to nursery"
      render :action => 'new'
    end
  end
  
  def index
    @nursery = current_user.current_nursery
    @accounts = @nursery.accounts
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 2    
  end
  
  def edit
    @nursery = current_user.current_nursery
    @account = Account.find(params[:id])
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 2      
  end
  
  def update
    @account = Account.find(params[:id])
    if @account.update_attributes(params[:account])
      redirect_to accounts_path
      flash[:notice] = "Account values were changed"
    else
      flash[:notice] = "Account values were not changed" 
      render :action => 'edit'
    end
  end

  def show
    @account = Account.find(params[:id])
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 2    
    @sidetab = "Summary"
  end
  
  def related_children
    @account = Account.find(params[:id])
    @children = @account.children
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 2    
    @sidetab = "Related Children"    
  end
  
  def remove_child
    @account = Account.find(params[:id])
    @child = Child.find(params[:child_id])
    @child.account_id = nil
    if @child.save
      redirect_to related_children_account_path(@account)
      flash[:notice] = "Child was removed from account"
    end
  end
  
  def search_children
    @nursery = current_user.current_nursery
    @account = Account.find(params[:id])
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 2    
    if params.has_key?(:query)
      selected_field = params[:field]
      query = params[:query]
    else
      selected_field = 'surname'
      query = nil
    end
    @results = Child.search(selected_field, query, @nursery.id)
  end
  
  def add_children
    @account = Account.find(params[:id])
    Child.update_all(["account_id=?", @account.id], :id => params[:child_ids]) 
    redirect_to related_children_account_path(@account)
    flash[:notice] = "Children were added to the account"
  end

  def related_carers
    @account = Account.find(params[:id])
    @carers = @account.carers
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 2    
    @sidetab = "Related Carers"     
  end
  
  def search_carers
    @nursery = current_user.current_nursery    
    @account = Account.find(params[:id])
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 2 
    if params.has_key?(:query)
      selected_field = params[:field]
      query = params[:query]
    else
      selected_field = 'surname'
      query = nil
    end
    @results = Carer.search(selected_field, query, @nursery.id)        
  end
  
  def add_carers
    @account = Account.find(params[:id])
    params[:carer_ids].each do |carer|
      @accountability = Accountability.create(account_id: @account.id, carer_id: carer)
    end
    redirect_to related_carers_account_path(@account)
    flash[:notice] = "Carers were added to the account"    
  end
  
  def remove_carer
    @account = Account.find(params[:id])
    @carer = Carer.find(params[:carer_id])
    Accountability.destroy_all(account_id: @account.id, carer_id: @carer.id) 
    redirect_to related_carers_account_path(@account)
    flash[:notice] = "Carer was removed from the account"
  end
end
