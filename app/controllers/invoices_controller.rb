class InvoicesController < ApplicationController
    enable_authorization
    
  def new
    @nursery = current_user.current_nursery
    @account = Account.find(params[:account_id])
    @carer = Carer.find(params[:carer_id])
    @children = @carer.children
    @billing_periods = [Date.new(2013,06,01), Date.new(2013,06,30)]
    @invoice = @account.invoices.build(nursery_id: @nursery.id, carer_id: @carer.id)
    @line_item = @invoice.invoice_line_items.build
    @periods_billed = @invoice.periods_billed(@children, @billing_periods)
    @line_item.description = "#{@periods_billed.to_s} #{'Sessions'}"
    @cost = 18.75
    @line_item.cost = "#{@cost}"
    @line_item.total_cost = "#{(@periods_billed * @cost).round(2)}"
  end
  
  def create
    @nursery = current_user.current_nursery
    @billing_period = BillingPeriod.current_billing_period.empty?? BillingPeriod.add_new_period(@nursery):BillingPeriod.current_billing_period.first    
    @account = Account.find(params[:account_id])    
    @carer = Carer.find(params[:carer_id])    
    @children = @carer.children
    @invoice = @account.invoices.build(nursery_id: @nursery.id, carer_id: @carer.id, number: Invoice.next_number, issue: 1, state: "new", billing_period_id: @billing_period.id)
    @line_items = @invoice.populate_line_items(@account, @children, @billing_period)      
    if @invoice.save
      redirect_to carer_path(@carer)
      flash[:notice] ="Invoice was added for this bill payer"
    else
      flash[:notice] = "No Invoice was added for this bill payer"
      redirect_to carer_path(@carer)
    end
  end

  def index
    @nursery = current_user.current_nursery
    if !params[:billing_period_id].nil?
      @billing_period = BillingPeriod.find(params[:billing_period_id])
      @invoices = @billing_period.invoices
    elsif !params[:carer_id].nil?
      @carer = Carer.find(params[:carer_id])
      @invoices = @carer.invoices
    else
      @invoices = @nursery.invoices  
    end
  end

  def show
    @invoice = Invoice.find(params[:id])
    @line_items = @invoice.invoice_line_items
    @balance_due_item = @line_items.balance_due
    @children = @line_items.pluck(:child_id).reject{ |child_id| child_id == nil }.uniq
  end  
  
  def update
    @invoice = Invoice.find(params[:id])
    @billing_period = @invoice.billing_period
    @children = @invoice.carer.children
    @line_items = @invoice.change_invoice(@invoice, @billing_period, @children)
    if @invoice.save
      redirect_to invoices_path
      flash[:notice] ="Invoice was updated"
    else
      flash[:notice] = "Invoice was not updated"
      redirect_to invoices_path
    end    
  end
  
end
