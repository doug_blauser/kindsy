class TimesheetsController < ApplicationController
  enable_authorization
  
  def new
    @nursery = current_user.current_nursery
    @child = Child.find(params[:child_id])
    @timesheet = @child.timesheets.build
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 1    
  end
  
  def create
    @nursery = current_user.current_nursery
    @child = Child.find(params[:child_id])
    @timesheet = @child.timesheets.build(params[:timesheet])
    @timesheet.nursery_id = @nursery.id 
    @timesheet.term_id = @timesheet.current_term
    @timesheet.populate_costs
    @timesheet.populate_fe_time
    if @timesheet.save
      redirect_to child_timesheets_path(@child)
      flash[:notice] = "Timesheet was added to this child"
    else
      flash[:notice] = "Timesheet was not added to this child"
      render :action => 'new'      
    end
  end
  
  def index 
    @bow = Date.today.beginning_of_week
    @eow = Date.today.end_of_week
    @nursery = current_user.current_nursery
    @child = Child.find(params[:child_id])    
    @timesheets = Timesheet.by_child(@child)
    open_days = @nursery.open_hours.map { |day| day.day_of_week.downcase.to_sym }
    open_dates_this_week = Recurrence.new(:starts => @bow, :every => :week, :on => open_days, :until => @eow) 
    @dates_this_week = open_dates_this_week.events    
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 1   
    @sidetab = "Time & Attendance"    
  end
  
  def edit
    @child = Child.find(params[:child_id])
    @timesheet = Timesheet.find(params[:id])
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 1      
  end
  
  def update
    @nursery = current_user.current_nursery
    @child = Child.find(params[:child_id])
    @timesheet = Timesheet.find(params[:id])
    @timesheet.period_id = params[:timesheet][:period_id]       
    if @timesheet.update_attributes(params[:timesheet])
      @timesheet.populate_costs
      @timesheet.save
      redirect_to child_timesheets_path(@child)
      flash[:notice] = "Timesheet values for this child were changed"
    else
      flash[:notice] = "Timesheet values for this child were not changed"
      render :action => 'edit'
    end
  end
end
