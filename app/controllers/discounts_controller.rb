class DiscountsController < ApplicationController
  enable_authorization
    
  def index
    @nursery = current_user.current_nursery
    @discounts = @nursery.discounts    
  end
  
  def new
    @nursery = current_user.current_nursery
    @accounts = @nursery.accounts
    @periods = @nursery.periods
    @discount = @nursery.discounts.build
  end
  
  def create
    @nursery = current_user.current_nursery
    @discount = @nursery.discounts.build(params[:discount])
    @partial_type = params[:partial]    
    if @discount.save
      redirect_to discounts_path
      flash[:notice] = "Discount was added to the nursery"
    else
      if @partial_type == "attendance"
        params[:partial] = "attendance"
      else
        params[:partial] = "relationship"
      end
      flash[:notice] = "Discount was not added to the nursery"
      render :action => 'new'  
    end
  end
  
  def edit
    @nursery = current_user.current_nursery
    @accounts = @nursery.accounts
    @periods = @nursery.periods
    @discount = Discount.find(params[:id])
  end
  
  def update
    @nursery = current_user.current_nursery
    @discount = Discount.find(params[:id])
    @partial_type = params[:partial]    
    if @discount.update_attributes(params[:discount])
      redirect_to discounts_path
      flash[:notice] = "Nursery discount values were changed"
    else
      if @partial_type == "attendance"
        params[:partial] = "attendance"
      else
        params[:partial] = "relationship"
      end
      flash[:notice] = "Nursery discount values were not changed"
      render :action => 'edit'
    end
  end
  
  def show
    @nursery = current_user.current_nursery
    @discount = Discount.find(params[:id])
    @partial_type = @discount.discountable_type == "Preset"? "attendance":"relationship"
  end
  
  def destroy
    @nursery = current_user.current_nursery
    Discount.find(params[:id]).destroy
    flash[:success] = "Nursery Discount record deleted"
    redirect_to discounts_path
  end
end
