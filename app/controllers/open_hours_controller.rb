class OpenHoursController < ApplicationController
  enable_authorization

  def new
    @nursery = current_user.current_nursery
    @open_hours = @nursery.open_hours.build
    @top_tab = "Nursery Settings"
    @bottom_menu = "tab9" 
    @bottom_tab = 4    
  end
  
  def create
    @nursery = current_user.current_nursery
    @open_hours = @nursery.open_hours.build(params[:open_hour])
    if @open_hours.save
      redirect_to open_hours_path
      flash[:notice] = "Open Hours were added for the nursery"
    else
      flash[:notice] = "Open Hours were not added for the nursery"
      render :action => 'new'
    end
  end
  
  def edit
    @nursery = current_user.current_nursery
    @open_hours = OpenHour.find(params[:id])
    @top_tab = "Nursery Settings"
    @bottom_menu = "tab9" 
    @bottom_tab = 4    
  end
  
  def update
    @nursery = current_user.current_nursery
    @open_hours = OpenHour.find(params[:id])
    if @open_hours.update_attributes(params[:open_hour])
      redirect_to open_hours_path
      flash[:notice] = "Nursery Open Hours were changed"
    else
      flash[:notice] = "Nursery Open Hours were not changed"
      render :action => 'edit'
    end
  end
  
  def index
    @nursery = current_user.current_nursery
    @open_hours = @nursery.open_hours
    @open_days = @open_hours.map { |a| a.day_of_week }
    @top_tab = "Nursery Settings"
    @bottom_menu = "tab9" 
    @bottom_tab = 4    
  end
  
  def destroy
    @nursery = current_user.current_nursery
    OpenHour.find(params[:id]).destroy
    flash[:success] = "Nursery Open Hours record deleted"
    redirect_to open_hours_path
  end
  
  def show
    @nursery = current_user.current_nursery
    @open_hours = OpenHour.find(params[:id])
    @top_tab = "Nursery Settings"
    @bottom_menu = "tab9" 
    @bottom_tab = 4      
  end
  
end
