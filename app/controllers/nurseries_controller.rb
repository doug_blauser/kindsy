class NurseriesController < ApplicationController
  enable_authorization

  def index
    @group = NurseryGroup.first
    @nurseries = @group.nurseries
    @top_tab = "Group Settings"
    @bottom_menu = "tab10" 
    @bottom_tab = 1
  end
  
  def show
    @nursery = Nursery.find(params[:id])
    @top_tab = "Nursery Settings"
    @bottom_menu = "tab9" 
    @bottom_tab = 1    
  end
  
  def new
    @group = NurseryGroup.first
    @nursery = @group.nurseries.build
    @contact_info = @nursery.build_contact_info  
    @top_tab = "Group Settings"
    @bottom_menu = "tab10" 
    @bottom_tab = 1          
  end
  
  def create
    @group = NurseryGroup.first
    @nursery = @group.nurseries.build(params[:nursery]) 
    if @nursery.save
      redirect_to nurseries_path
      flash[:notice] = "New nursery was added!"      
    else
      flash[:notice] = "Nursery addition failed!"
      render :action => 'new'
    end
  end
  
  def edit
    @nursery = Nursery.find(params[:id])
    @top_tab = "Group Settings"
    @bottom_menu = "tab10" 
    @bottom_tab = 1     
  end
  
  def update
    @nursery = Nursery.find(params[:id])
    if @nursery.update_attributes(params[:nursery])
      redirect_to nurseries_path
      flash[:notice] = "Nursery was updated!"  
    else
      flash[:notice] = "Nursery update failed!"
      render :action => 'edit'
    end
  end
end
