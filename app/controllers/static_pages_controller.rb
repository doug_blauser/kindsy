class StaticPagesController < ApplicationController
  enable_authorization
  
  has_widgets do |root|
    root << widget(:currency)
    root << widget(:nursery_group)
    root << widget(:nursery)
    root << widget(:room)
    root << widget(:staff_member)
    root << widget(:carer)
    root << widget(:child)
    root << widget(:contact_info)
  end
  
  def home
    @top_tab = "Home"
    @bottom_menu = "tab1"
    @bottom_tab = 1
  end
end
