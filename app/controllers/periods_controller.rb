class PeriodsController < ApplicationController
  enable_authorization
  
  def index
    @nursery = current_user.current_nursery
    @periods = @nursery.periods
    @top_tab = "Nursery Settings"
    @bottom_menu = "tab9" 
    @bottom_tab = 3    
  end
  
  def new
    @nursery = current_user.current_nursery
    @period = @nursery.periods.build
    @top_tab = "Nursery Settings"
    @bottom_menu = "tab9" 
    @bottom_tab = 3     
  end
  
  def create
    @nursery = current_user.current_nursery
    @period = @nursery.periods.build(params[:period])
    if @period.save
      redirect_to periods_path
      flash[:notice] = "Period was added to the nursery"
    else
      flash[:notice] = "Period was not added to the nursery"
      render :action => 'new'
    end
  end
  
  def edit
    @nursery = current_user.current_nursery
    @period = Period.find(params[:id])
    @top_tab = "Nursery Settings"
    @bottom_menu = "tab9" 
    @bottom_tab = 3    
  end
  
  def update
    @nursery = current_user.current_nursery
    @period = Period.find(params[:id])
    if @period.update_attributes(params[:period])
      redirect_to periods_path
      flash[:notice] = "Nursery period values were changed"
    else
      flash[:notice] = "Nursery period values were not changed"
      render :action => 'edit'
    end
  end
  
  def show
    @nursery = current_user.current_nursery
    @period = Period.find(params[:id])
    @top_tab = "Nursery Settings"
    @bottom_menu = "tab9" 
    @bottom_tab = 3   
  end
  
  def destroy
    @nursery = current_user.current_nursery
    Period.find(params[:id]).destroy
    flash[:success] = "Nursery Period record deleted"
    redirect_to nursery_periods_path(@nursery)
  end
end
