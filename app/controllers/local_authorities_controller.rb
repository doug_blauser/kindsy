class LocalAuthoritiesController < ApplicationController
  enable_authorization
  
  def new
    @local_authority = LocalAuthority.new 
    @contact_info = @local_authority.build_contact_info 
    @top_tab = "Group Settings"
    @bottom_menu = "tab10" 
    @bottom_tab = 6      
  end
  
  def create
    @local_authority = LocalAuthority.new(params[:local_authority])
    if @local_authority.save
      redirect_to local_authorities_path
      flash[:notice] = "New Local Authority was added"
    else
      flash[:notice] = "Local Authority addition failed" 
      render :action => 'new'
    end
  end
  
  def index
    if params[:local_authority].nil?
      @local_authorities = LocalAuthority.all
    else
      @local_authorities = LocalAuthority.where("id = ?", params[:local_authority])
    end
    @top_tab = "Group Settings"
    @bottom_menu = "tab10" 
    @bottom_tab = 6    
  end
  
  def edit
    @local_authority = LocalAuthority.find(params[:id])  
    @top_tab = "Group Settings"
    @bottom_menu = "tab10" 
    @bottom_tab = 6 
  end
  
  def update
    @local_authority = LocalAuthority.find(params[:id])
    if @local_authority.update_attributes(params[:local_authority])
      redirect_to local_authorities_path
      flash[:notice] = "Local Authority values were changed"
    else
      flash[:notice] = "Local Authority values were not changed"
      render :action => 'edit'
    end
  end
  
  def show
    @local_authority = LocalAuthority.find(params[:id])
    @top_tab = "Group Settings"
    @bottom_menu = "tab10" 
    @bottom_tab = 6
  end
  
  def destroy
    
  end
end
