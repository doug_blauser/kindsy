class PresetsController < ApplicationController
  enable_authorization
  
  def new
    @nursery = current_user.current_nursery
    @periods = @nursery.periods
    @preset = @nursery.presets.build
  end
  
  def create
    @nursery = current_user.current_nursery
    @preset = @nursery.presets.build(params[:preset])
    params[:preset][:period_ids] = params[:preset][:period_ids].reject{ |period_id| period_id == ""}
    if @preset.save
      redirect_to presets_path
      flash[:notice] = "Preset was added to the nursery"
    else
      flash[:notice] = "Preset was not added to the nursery"
      render :action => 'new'
    end
  end
  
  def index
    @nursery = current_user.current_nursery
    @presets = @nursery.presets
  end
  
  def edit
    @nursery = current_user.current_nursery
    @preset = Preset.find(params[:id])
  end
  
  def update
    @nursery= current_user.current_nursery
    @preset = Preset.find(params[:id])
    if @preset.update_attributes(params[:preset])
      redirect_to presets_path
      flash[:notice] = "Nursery preset values were changed"
    else
      flash[:notice] = "Nursery preset values were not changed"
      render :action => 'edit'      
    end
  end
  
  def show
    @nursery = current_user.current_nursery
    @preset = Preset.find(params[:id])
  end
  
  def destroy
    @nursery = current_user.current_nursery
    Preset.find(params[:id]).destroy
    flash[:success] = "Nursery Preset record was deleted"
    redirect_to presets_path
  end
end
