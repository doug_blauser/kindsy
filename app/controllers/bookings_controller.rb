class BookingsController < ApplicationController
  enable_authorization

  def index
    @bow = Date.today.beginning_of_week
    @eow = Date.today.end_of_week
    @nursery = current_user.current_nursery
    @child = Child.find(params[:child_id])
    @bookings = @child.bookings
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 1   
    @sidetab = "Time & Attendance"  
  end
  
  def new
    @nursery = current_user.current_nursery
    @child = Child.find(params[:child_id])
    @booking = @child.bookings.build
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 1
  end
  
  def create
    @nursery = current_user.current_nursery
    @child = Child.find(params[:child_id])
    @booking = @child.bookings.build(params[:booking])
    @booking.nursery_id = @nursery.id
    if @booking.save
      redirect_to child_bookings_path(@child)
      flash[:notice] = "Booking was added to this child"
    else
      flash[:notice] = "Booking was not added to this child"
      render :action => 'new'
    end
  end
  
  def edit
    @nursery = current_user.current_nursery
    @child = Child.find(params[:child_id])
    @booking = Booking.find(params[:id])
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 1    
  end
  
  def update
    @nursery = current_user.current_nursery
    @child = Child.find(params[:child_id])
    @booking = Booking.find(params[:id])
    if @booking.update_attributes(params[:booking])
      redirect_to child_bookings_path(@child)
      flash[:notice] = "Booking values for this child were changed"
    else
      flash[:notice] = "Booking values for this child were not changed"
      render :action => 'edit'
    end
  end

  def show
    @child = Child.find(params[:child_id])
    @booking = Booking.find(params[:id])
    @top_tab = "Children"
    @bottom_menu = "tab3" 
    @bottom_tab = 1    
  end
  
  def destroy
    @nursery = current_user.current_nursery
    @child = Child.find(params[:child_id])
    Booking.find(params[:id]).destroy
    flash[:success] = "Child booking record deleted"
    redirect_to child_bookings_path(@child)    
  end
end
