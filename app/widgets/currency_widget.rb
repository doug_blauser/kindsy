class CurrencyWidget < Apotomo::Widget

  def display
    @currencies = Currency.all
    render
  end

end
