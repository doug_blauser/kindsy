class NurseryGroupWidget < Apotomo::Widget

  def display
    @nursery_groups = NurseryGroup.all
    render
  end

end
