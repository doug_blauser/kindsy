class StaffMemberWidget < Apotomo::Widget

  def display
    @staff_members = StaffMember.all
    render
  end

end
