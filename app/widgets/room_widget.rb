class RoomWidget < Apotomo::Widget

  def display
    @rooms = Room.all
    render
  end

end
