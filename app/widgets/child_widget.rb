class ChildWidget < Apotomo::Widget

  def display
    @children = Child.all
    render
  end

end
