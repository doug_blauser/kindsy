class NurseryWidget < Apotomo::Widget

  def display
    @nurseries = Nursery.all
    render
  end

end
