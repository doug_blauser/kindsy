# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/

jQuery ->

  $(".modal_display").click (e) ->
    url = $(this).attr("href")
    dialog_form = $("<div id=\"dialog-form\"><p class=\"loading-msg\">Loading...</p></div>").dialog(
      autoOpen: false
      width: 350
      position: ['center', 125]
      modal: true
      open: ->
        $(this).load url + " #modal_content"

      close: ->
        $("#dialog-form").remove()
    )
    dialog_form.dialog "open"
    e.preventDefault()