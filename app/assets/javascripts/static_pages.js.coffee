# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->
  $(".column").sortable connectWith: ".column"
  $(".portlet").addClass("ui-widget ui-widget-content ui-helper-clearfix ui-corner-all").find(".portlet-header").addClass("ui-widget-header ui-corner-all").prepend("<span class='ui-icon ui-icon-minusthick ui'></span>").prepend("<span class='ui-icon ui-icon-closethick ui'></span>").end().find ".portlet-content"
  $(".portlet-header .ui-icon").click ->
    $(this).toggleClass("ui-icon-minusthick").toggleClass "ui-icon-plusthick"
    $(this).parents(".portlet:first").find(".portlet-content").toggle()

  $(".column").disableSelection()
  
  $(".remove_link").click ->
    $(this).parents(".portlet:first").remove()

  $(".ui-icon-closethick").click ->
    $(this).parents(".portlet:first").remove()