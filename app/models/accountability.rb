# == Schema Information
#
# Table name: accountabilities
#
#  id         :integer         not null, primary key
#  account_id :integer
#  carer_id   :integer
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

class Accountability < ActiveRecord::Base
  attr_accessible :account_id, :carer_id
  
  belongs_to :account
  belongs_to :carer
  
  validates :account_id, :carer_id, presence: true, :numericality => { :only_integer => true }
end
