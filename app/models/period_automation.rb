# == Schema Information
#
# Table name: period_automations
#
#  id         :integer         not null, primary key
#  period_id  :integer
#  preset_id  :integer
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

class PeriodAutomation < ActiveRecord::Base
  attr_accessible :period_id, :preset_id
  
  belongs_to :period
  belongs_to :preset

  validates :period_id, :preset_id, presence: true, :numericality => { :only_integer => true }

end
