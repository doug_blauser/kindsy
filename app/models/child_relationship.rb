# == Schema Information
#
# Table name: child_relationships
#
#  id                         :integer         not null, primary key
#  child_id                   :integer
#  carer_id                   :integer
#  child_relationship_type_id :integer
#  created_at                 :datetime        not null
#  updated_at                 :datetime        not null
#  pick_up_flag               :boolean
#  drop_off_flag              :boolean
#

class ChildRelationship < ActiveRecord::Base
  attr_accessible :carer_id, :child_id, :child_relationship_type_id, :pick_up_flag, :drop_off_flag
  
  belongs_to :child
  belongs_to :carer
  belongs_to :child_relationship_type
  
  validates :carer_id, presence: true, :numericality => { :only_integer => true }
  validates :child_id, presence: true, :numericality => { :only_integer => true }
  validates :child_relationship_type_id, presence: true, :numericality => { :only_integer => true }
  validates_inclusion_of :pick_up_flag,  :in => [true, false] 
  validates_inclusion_of :drop_off_flag,  :in => [true, false] 
end
