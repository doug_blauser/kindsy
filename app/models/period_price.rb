# == Schema Information
#
# Table name: period_prices
#
#  id              :integer         not null, primary key
#  period_id       :integer
#  start_timestamp :datetime
#  end_timestamp   :datetime
#  price           :decimal(5, 2)
#  created_at      :datetime        not null
#  updated_at      :datetime        not null
#

class PeriodPrice < ActiveRecord::Base
  attr_accessible :end_timestamp, :period_id, :price, :start_timestamp
  
  belongs_to :period
  
  validates :period_id, presence: true, :numericality => { :only_integer => true }
  validates :start_timestamp, presence: true
  validates :price, presence: true, :numericality => true
  
end
