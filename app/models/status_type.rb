# == Schema Information
#
# Table name: status_types
#
#  id          :integer         not null, primary key
#  status_name :string(255)
#  created_at  :datetime        not null
#  updated_at  :datetime        not null
#

class StatusType < ActiveRecord::Base
  attr_accessible :status_name
  
  has_many :children
  
  validates :status_name, presence: true
end
