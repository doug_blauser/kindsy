# == Schema Information
#
# Table name: rooms
#
#  id              :integer         not null, primary key
#  nursery_id      :integer
#  staff_member_id :integer
#  name            :string(255)
#  capacity        :integer
#  age_from        :integer
#  age_to          :integer
#  created_at      :datetime        not null
#  updated_at      :datetime        not null
#

class Room < ActiveRecord::Base
  attr_accessible :age_from, :age_to, :capacity, :name, :nursery_id, :staff_member_id

  belongs_to :nursery
  belongs_to :staff_member
  has_many :periodations
  has_many :periods, :through => :periodations
  has_many :discounts, as: :discountable
    
  validates :name, presence: true
  validates :nursery_id, presence: true, :numericality => { :only_integer => true }
  validates :staff_member_id, :numericality => { :only_integer => true }, allow_nil: true
  validates :capacity, :numericality => { :only_integer => true }
  validates :age_from, :numericality => { :only_integer => true }
  validates :age_to, :numericality => { :only_integer => true }
end
