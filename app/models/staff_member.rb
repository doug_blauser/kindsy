# == Schema Information
#
# Table name: staff_members
#
#  id                        :integer         not null, primary key
#  nursery_id                :integer
#  contact_info_id           :integer
#  first_name                :string(255)
#  middle_name               :string(255)
#  surname                   :string(255)
#  title                     :string(255)
#  date_of_birth             :date
#  medical_conditions        :text
#  start_date                :date
#  leave_date                :date
#  payroll_number            :string(255)
#  hours_per_week_contracted :integer
#  hourly_rate               :float
#  start_of_holiday_year     :date
#  holiday_entitlement       :float
#  created_at                :datetime        not null
#  updated_at                :datetime        not null
#

class StaffMember < ActiveRecord::Base
  attr_accessible :nursery_id, :contact_info_id, :first_name, :middle_name, :surname, :title, :date_of_birth, :medical_conditions, :start_date, :leave_date, :payroll_number, :hours_per_week_contracted, :hourly_rate, :start_of_holiday_year, :holiday_entitlement
  
  belongs_to :nursery
  belongs_to :contact_info
  
  has_many :children
  
  validates :nursery_id, presence: true, :numericality => { :only_integer => true }
  validates :contact_info_id, presence: true, :numericality => { :only_integer => true }
  validates :first_name, presence: true
  validates :surname, presence: true
  
  def display_name
    "#{title} #{first_name} #{middle_name} #{surname}"
  end
  
end
