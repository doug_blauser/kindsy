class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new 
    roles = user.roles
    unless user.roles.size == 0
      can :access, :static_pages
      cannot [:manage, :create, :update, :destroy], :static_pages
      roles.each do |role|
        role.permissions.each do |permission|
          if permission.condition.nil?
            can permission.action.to_sym, role.resource_type.downcase.to_sym
          else
            if permission.condition == "all"
              can permission.action.to_sym, permission.condition.to_sym
            else
              can permission.action.to_sym, role.resource_type.downcase.to_sym, permission.condition              
            end
          end
        end
      end
    end
  end
end
