# == Schema Information
#
# Table name: accounts
#
#  id                     :integer         not null, primary key
#  user_id                :integer
#  invoice_type           :string(255)
#  sage_account_reference :string(255)
#  created_at             :datetime        not null
#  updated_at             :datetime        not null
#  account_no             :string(255)
#  nursery_id             :integer
#  account_name           :string(255)
#

class Account < ActiveRecord::Base
  attr_accessible :account_no, :invoice_type, :sage_account_reference, :user_id, :nursery_id, :account_name

  belongs_to :user
  belongs_to :nursery
  
  has_many :children
  has_many :carers, :through => :accountabilities
  has_many :accountabilities, :dependent => :destroy
  has_many :invoices, :dependent => :destroy
  has_many :discounts, as: :discountable
  
  
  validates :user_id, presence: true, :numericality => { :only_integer => true }
  validates :nursery_id, presence: true, :numericality => { :only_integer => true }
  validates :account_no, presence: true
  validates :account_name, presence: true
  
  def to_label
    "#{account_no}"
  end
  
  def display_name
    self.account_no
  end
  
  def self.search(field, query, id)
    if query
      where("#{'upper('}#{field}#{')'} #{'LIKE upper(?) AND nursery_id = ?'}", "%#{query}%", id)       
    else
      "Enter a value in Contains field"
    end    
  end  
  
end
