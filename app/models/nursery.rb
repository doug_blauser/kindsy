# == Schema Information
#
# Table name: nurseries
#
#  id                      :integer         not null, primary key
#  nursery_group_id        :integer
#  name                    :string(255)
#  created_at              :datetime        not null
#  updated_at              :datetime        not null
#  region_id               :integer
#  billing_cycle_start_day :integer
#  local_authority_id      :integer
#

class Nursery < ActiveRecord::Base
  attr_accessible :name, :region_id, :billing_cycle_start_day, :contact_info_attributes, :currency_attributes, :local_authority_id  

  belongs_to :nursery_group

  has_one :contact_info, :as => :addressable
  has_one :currency
  belongs_to :region
  belongs_to :local_authority
  
  has_many :accounts
  has_many :rooms
  has_many :staff_members
  has_many :children 
  has_many :carers
  has_many :open_hours
  has_many :periods
  has_many :presets
  has_many :bookings
  has_many :timesheets
  has_many :employments
  has_many :users, :through => :employments
  has_many :invoices  
  has_many :billing_periods
  has_many :discounts
  
  accepts_nested_attributes_for :contact_info
  accepts_nested_attributes_for :currency
  
  
  validates :nursery_group_id, presence: true, numericality: { only_integer: true }
  validates :name, presence: true
  validates :region_id, presence: true, numericality: { only_integer: true }
  validates :billing_cycle_start_day, presence: true, numericality: { only_integer: true }
  validates :local_authority_id, presence: true, numericality: { only_integer: true }


end
