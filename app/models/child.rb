# == Schema Information
#
# Table name: children
#
#  id              :integer         not null, primary key
#  nursery_id      :integer
#  account_id      :integer
#  staff_member_id :integer
#  room_id         :integer
#  carer_id        :integer
#  first_name      :string(255)
#  middle_name     :string(255)
#  surname         :string(255)
#  preferred_name  :string(255)
#  gender_id       :string(255)
#  date_of_birth   :date
#  start_date      :date
#  leave_date      :date
#  created_at      :datetime        not null
#  updated_at      :datetime        not null
#  AEN             :string(255)
#  hair_id         :integer
#  eye_color_id    :integer
#  religion_id     :integer
#  ethnicity_id    :integer
#  nationality_id  :integer
#  language_id     :integer
#  status_type_id  :integer
#

class Child < ActiveRecord::Base
  attr_accessible :account_id, :carer_id, :date_of_birth, :first_name, :gender_id, :leave_date, :middle_name, :nursery_id, :preferred_name, :room_id, :staff_member_id, :start_date, :surname, :AEN, :hair_id, :eye_color_id, :religion_id, :ethnicity_id, :nationality_id, :language_id, :status_type_id, :contact_info_attributes 

  belongs_to :nursery
  belongs_to :account
  belongs_to :staff_member
  belongs_to :room
  belongs_to :carer
  belongs_to :hair
  belongs_to :eye_color
  belongs_to :religion
  belongs_to :ethnicity
  belongs_to :nationality
  belongs_to :language
  belongs_to :status_type
  
  has_one :contact_info, as: :addressable
  
  has_many :bookings, dependent: :destroy
  has_many :timesheets, dependent: :destroy
  has_many :child_relationships, dependent: :destroy
  has_many :invoice_line_items, dependent: :destroy
  has_many :discounts, as: :discountable
  
  accepts_nested_attributes_for :contact_info 
  
  validates :nursery_id, presence: true, numericality: { only_integer: true } 
  validates :first_name, presence: true
  validates :surname, presence: true
  validates :date_of_birth, presence: true
  validates :start_date, presence: true
  validates :status_type_id, presence: true, numericality: { only_integer: true }   
  
  def full_name
    "#{first_name}#{" #{middle_name}" if middle_name} #{surname}"
  end
  
  def age
    now = Time.now.utc.to_date
    dob = date_of_birth
    now.year - dob.year - ((now.month > dob.month || (now.month == dob.month && now.day >= dob.day)) ? 0 : 1)
  end
  
  def age_in_months
    now = Time.now.utc.to_date
    dob = date_of_birth
    (now.year * 12 + now.month) - (dob.year * 12 + dob.month)    
  end
  
  def age_at_term_start(term)
    term_start = term.start_date
    dob = date_of_birth
    term_start.year - dob.year - ((term_start.month > dob.month || (term_start.month == dob.month && term_start.day >= dob.day))? 0:1)   
  end
  
  def fe_allocation(term)
    fe_age = self.age_at_term_start(term)
    fe_used_elsewhere = 0 * 3600
    if fe_age > 2 && fe_age < 6
      15 * 3600 - fe_used_elsewhere
    elsif fe_age == 2
      10 * 3600 - fe_used_elsewhere
    else 
      0
    end
  end
  
  def self.search(field, query, id)
    if query
      where("#{'upper('}#{field}#{')'} #{'LIKE upper(?) AND nursery_id = ?'}", "%#{query}%", id)       
    else
      "Enter a value in Contains field"
    end    
  end
end
