# == Schema Information
#
# Table name: billing_periods
#
#  id         :integer         not null, primary key
#  nursery_id :integer
#  start_date :date
#  end_date   :date
#  status     :string(255)
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

class BillingPeriod < ActiveRecord::Base
  attr_accessible :end_date, :nursery_id, :start_date, :status
  
  scope :current_billing_period, lambda { where("start_date <= ? AND end_date >= ?", Date.today, Date.today) }
  scope :previous_billing_periods, lambda { where("end_date <= ?", Date.today).order('end_date asc') }
  
  belongs_to :nursery
  has_many :invoices, dependent: :destroy
  
  validates :nursery_id, presence: true, numericality: { only_integer: true }
  validates :start_date, presence: true
  validates :end_date, presence: true
  validates :status, presence: true  
  
  def set_end_date(start_date)
    self.end_date = start_date+1.month-1.day
  end
   
  def previous_billing_period
    BillingPeriod.previous_billing_periods.last
  end
  
  def next_start_date
    BillingPeriod.current_billing_period.first.end_date+1.day
  end  

  
  def self.add_new_period(nursery)
    if BillingPeriod.current_billing_period.empty?
      start_date = Date.new(Date.today.year, Date.today.month, nursery.billing_cycle_start_day)
      end_date = start_date + 1.month - 1.day 
      nursery.billing_periods.create(start_date: start_date, end_date: end_date, status: "open" )
    end
  end

end
