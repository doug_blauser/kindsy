# == Schema Information
#
# Table name: currency_types
#
#  id            :integer         not null, primary key
#  currency_name :string(255)
#  created_at    :datetime        not null
#  updated_at    :datetime        not null
#

class CurrencyType < ActiveRecord::Base
  attr_accessible :currency_name
  
  has_many :currencies
  
  validates :currency_name, presence: true
end
