# == Schema Information
#
# Table name: ethnicities
#
#  id         :integer         not null, primary key
#  race       :string(255)
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

class Ethnicity < ActiveRecord::Base
  attr_accessible :race
  
  has_many :children
  
  validates :race, presence: true
end
