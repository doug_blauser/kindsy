# == Schema Information
#
# Table name: languages
#
#  id         :integer         not null, primary key
#  tongue     :string(255)
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

class Language < ActiveRecord::Base
  attr_accessible :tongue
  
  has_many :children
  
  validates :tongue, presence: true
end
