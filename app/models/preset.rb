# == Schema Information
#
# Table name: presets
#
#  id          :integer         not null, primary key
#  nursery_id  :integer
#  preset_name :string(255)
#  start_time  :time
#  end_time    :time
#  created_at  :datetime        not null
#  updated_at  :datetime        not null
#

class Preset < ActiveRecord::Base
  attr_accessible :end_time, :nursery_id, :preset_name, :start_time, :period_ids, :period_automation_attributes

  belongs_to :nursery
  
  has_many :periods, :through => :period_automations
  has_many :period_automations, :dependent => :destroy
  
  validates :nursery_id, presence: true
  validates :preset_name, presence: true

end
