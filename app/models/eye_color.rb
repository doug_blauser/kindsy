# == Schema Information
#
# Table name: eye_colors
#
#  id         :integer         not null, primary key
#  shade      :string(255)
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

class EyeColor < ActiveRecord::Base
  attr_accessible :shade
  
  has_many :children
  
  validates :shade, presence: true
end
