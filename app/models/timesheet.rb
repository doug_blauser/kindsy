# == Schema Information
#
# Table name: timesheets
#
#  id            :integer         not null, primary key
#  nursery_id    :integer
#  child_id      :integer
#  period_id     :integer
#  date_attended :date
#  duration      :integer
#  created_at    :datetime        not null
#  updated_at    :datetime        not null
#  hourly_cost   :decimal(5, 2)
#  period_cost   :decimal(5, 2)
#  full_period   :boolean
#  fe_time       :integer
#  term_id       :integer
#

class Timesheet < ActiveRecord::Base
  attr_accessible :child_id, :date_attended, :duration, :nursery_id, :period_id, :hourly_cost, :period_cost, :full_period, :fe_time
  
  scope :by_child, lambda { |child| where(child_id: child) }
  scope :by_date, lambda { |date| where(date_attended: date) }
  scope :by_period, lambda { |period| where(period_id: period) }
  scope :by_week, lambda { |bow, eow| where(date_attended: bow..eow) } 
  scope :by_date_range, lambda { |start, stop| where(date_attended: start..stop) }
  scope :full_periods, lambda { |full_period_value| where(full_period: full_period_value) }
  scope :by_period_cost, lambda { |period_cost| where(period_cost: period_cost) }
  scope :by_hourly_cost, lambda { |hourly_cost| where(hourly_cost: hourly_cost) }
  scope :updated_after, lambda { |updated_at| where("updated_at > ?", updated_at) }
   
  belongs_to :nursery
  belongs_to :child
  belongs_to :period
  belongs_to :term
  
  has_many :charge, as: :chargeable  
  
  validates :nursery_id, presence: true, numericality: { only_integer: true }
  validates :child_id, presence: true, numericality: { only_integer: true }
  validates :period_id, presence: true, numericality: { only_integer: true }
  validates :date_attended, presence: true
  validates :duration, presence: true, numericality: { only_integer: true }
  validates :full_period, inclusion: { in: [true, false], message: 'requires a true or false value' }
  validates :hourly_cost, presence: true, numericality: true
  validates :period_cost, presence: true, numericality: true
  validates :fe_time, numericality: { only_integer: true }
    
  def duration_hours(elapsed)
    mm, ss = elapsed.divmod(60)
    hh, mm = mm.divmod(60)
    hh+(mm/60.to_f)
  end
  
  def full_period?
    self.period_id.nil?? false:self.period.period_end_time - self.period.period_start_time <= self.duration
  end
  
  def populate_costs
    self.hourly_cost = self.period.nil?? 0:self.period.period_hourly_cost
    self.period_cost = self.period.nil?? 0:self.period.period_cost    
    self.full_period = self.period.nil?? false:self.period.period_end_time - self.period.period_start_time <= self.duration   
  end
  
  def fe_used
    bow = self.date_attended.beginning_of_week
    eow = self.date_attended.end_of_week
    fe_this_week = Timesheet.by_child(self.child).by_week(bow, eow).pluck(:fe_time).sum
  end
  
  def fe_left
    self.child.fe_allocation(self.term) - self.fe_used
  end
  
  def calculate_fe_time
    unless self.term.nil?
      fe_left = self.fe_left    
      if self.duration <= fe_left
        self.duration
      else
        fe_left
      end
    else
      0
    end  
  end
  
  def populate_fe_time
    unless self.period.nil?  
      period_fe_limit = self.period.fe_limit * 3600
      fe_time_calculated = self.calculate_fe_time
      if period_fe_limit < fe_time_calculated
        self.fe_time = period_fe_limit.to_i
      else
        self.fe_time = fe_time_calculated
      end
    end
  end
  
  def current_term
    result = Term.where('start_date <= ? AND end_date >= ?', self.date_attended, self.date_attended).first
    unless result.nil?
      result.id
    else
      nil
    end
  end
end
