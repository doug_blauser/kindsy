# == Schema Information
#
# Table name: charges
#
#  id                   :integer         not null, primary key
#  invoice_line_item_id :integer
#  chargeable_id        :integer
#  chargeable_type      :string(255)
#  created_at           :datetime        not null
#  updated_at           :datetime        not null
#

class Charge < ActiveRecord::Base
  attr_accessible :chargeable_id, :chargeable_type, :invoice_line_item_id
  
  belongs_to :invoice_line_item
  belongs_to :chargeable, polymorphic: true
  
  validates :chargeable_id, presence: true, numericality: { only_integer: true }
  validates :chargeable_type, presence: true
  
  def self.ids(charges)
    charges.pluck(:chargeable_id)
  end  
  
  def period
    self.chargeable.period
  end
  
  def child
    self.chargeable.child
  end
  
  def full_period
    self.chargeable.full_period
  end
  
  def cost(full_period)
    if full_period
      self.chargeable.period_cost
    else
      self.chargeable.hourly_cost
    end
  end
end
