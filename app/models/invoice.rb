# == Schema Information
#
# Table name: invoices
#
#  id                :integer         not null, primary key
#  nursery_id        :integer
#  carer_id          :integer
#  account_id        :integer
#  issue             :integer
#  state             :string(255)
#  created_at        :datetime        not null
#  updated_at        :datetime        not null
#  billing_period_id :integer
#  number            :integer
#

class Invoice < ActiveRecord::Base
  attr_accessible :account_id, :number, :carer_id, :state, :issue, :nursery_id, :billing_period_id, :invoice_line_items_attributes
    
  belongs_to :nursery
  belongs_to :carer
  belongs_to :account
  belongs_to :billing_period
  has_many :invoice_line_items, dependent: :destroy
     
  accepts_nested_attributes_for :invoice_line_items, allow_destroy: true
  
  validates :nursery_id, presence: true, numericality: { only_integer: true }
  validates :carer_id, presence: true, numericality: { only_integer: true }
  validates :account_id, presence: true, numericality: { only_integer: true }
  validates :issue, presence: true, numericality: { only_integer: true }
  validates :state, presence: true
  validates :number, presence: true, numericality: { only_integer: true }
  validates :billing_period_id, presence: true, numericality: { only_integer: true }
  
  def populate_line_items(account, children, billing_period)
    children.each do |child|
      self.attendance_line_items(child)
      self.future_bookings_line_items(child)
      unless child.timesheets.by_date_range(billing_period.start_date, billing_period.end_date).empty?
        self.entitlement_line_item(child)
      end
    end    
    
    self.calculate_discounts
    self.balance_line_item   
  end  
  
  def attendance_line_items(child)
    periods = child.timesheets.by_date_range(billing_period.start_date, billing_period.end_date).pluck(:period_id).uniq
    periods.each do |period|
      full_period_values = child.timesheets.by_date_range(billing_period.start_date, billing_period.end_date).by_period(period).pluck(:full_period).uniq
        full_period_values.each do |full_period_value|
          if full_period_value
            costs = child.timesheets.by_date_range(billing_period.start_date, billing_period.end_date).by_period(period).full_periods(full_period_value).pluck(:period_cost).uniq
            description = "#{Period.find(period).period_name} #{'Full Sessions'}"
          else  
            costs = child.timesheets.by_date_range(billing_period.start_date, billing_period.end_date).by_period(period).full_periods(full_period_value).pluck(:hourly_cost).uniq
            description = "#{Period.find(period).period_name} #{'Hourly Sessions'}"
          end
          costs.each do |cost|
            if full_period_value
              timesheets = child.timesheets.by_date_range(billing_period.start_date, billing_period.end_date).by_period(period).full_periods(full_period_value).by_period_cost(cost)
              quantity = timesheets.count 
            else
              timesheets = child.timesheets.by_date_range(billing_period.start_date, billing_period.end_date).by_period(period).full_periods(full_period_value).by_hourly_cost(cost)              
              quantity = (timesheets.sum(:duration)/3600.to_f).round(2)
            end  
            fe_amount_calc = timesheets.pluck(:fe_time).sum/3600 * Period.find(period).period_hourly_cost  
            total_cost = (quantity * cost) - fe_amount_calc              
            line_item = self.invoice_line_items.build(account_id: account.id, 
                                                     issue: self.issue,
                                                     child_id: child.id,
                                                     description: description,
                                                     quantity: quantity,
                                                     cost: cost,
                                                     total_cost: total_cost,
                                                     fe_amount: fe_amount_calc)
            line_item.save
            timesheets.each do |timesheet|
              charge = line_item.charges.create(invoice_line_item_id: line_item.id, chargeable_id: timesheet.id, chargeable_type: timesheet.class.name)
            end                                                     
          end
        end
    end      
  end  
  
  def future_bookings_line_items(child)
    future_bookings = child.bookings.future_end_date(Date.today)
    future_bookings.each do |future_booking|
      interval = future_booking.every.downcase.to_sym
      frequency = future_booking.days_of_week.map { |day_of_week| day_of_week.downcase.to_sym }
      bookings = Recurrence.new(:every => interval, :on => frequency, :until => billing_period.end_date)
      bookings_events = bookings.events
      quantity = bookings_events.count
      period = Period.find(future_booking.period)
      description = "#{period.period_name} #{'Booked Full Sessions'}"
      cost = period.period_cost
      total_cost = quantity * cost
      line_item = self.invoice_line_items.build(account_id: account.id, 
                                               child_id: child.id,
                                               issue: self.issue,
                                               description: description,
                                               quantity: quantity,
                                               cost: cost,
                                               total_cost: total_cost)
      line_item.save  
      charge = line_item.charges.create(invoice_line_item_id: line_item.id, chargeable_id: future_booking.id, chargeable_type: future_booking.class.name)       
    end
  end

  def entitlement_line_item(child)
    fe_hours =  child.timesheets.by_date_range(billing_period.start_date, billing_period.end_date).pluck(:fe_time).sum / 3600
    description = "#{'Early Years Entitlement - '}#{fe_hours}#{' hours'}"
    quantity = 0
    cost = 0
    total_cost = 0.00     
    @line_item = self.invoice_line_items.build(account_id: account.id, 
                                               child_id: child.id,
                                               issue: self.issue,
                                               description: description,
                                               quantity: quantity,
                                               cost: cost,
                                               total_cost: total_cost)
    @line_item.save              
  end  
  
  def discount_line_item(discount_params)
    description = discount_params[:description]
    quantity = 0
    cost = 0.00
    total_cost = discount_params[:total_cost]
    @line_item = self.invoice_line_items.build(account_id: account.id, 
                                               issue: self.issue,
                                               description: description,
                                               quantity: quantity,
                                               cost: cost,
                                               total_cost: total_cost)                                              
    @line_item.save       
  end
  
  def balance_line_item
    description = "Balance Brought Forward"
    quantity = 0                
    cost = 0
    total_cost = 0.00 
    @line_item = self.invoice_line_items.build(account_id: account.id, 
                                               issue: self.issue,
                                               description: description,
                                               quantity: quantity,
                                               cost: cost,
                                               total_cost: total_cost )
    @line_item.save       
  end 
 
  def amount_due
    self.invoice_line_items.sum("total_cost")
  end

  
  def self.next_number
    self.pluck(:number).empty?? 0:self.pluck(:number).sort.last+1
  end
  
  def change_invoice(invoice, billing_period, children)
    case invoice.state
    when "new"  
      line_items_to_delete = invoice.invoice_line_items.non_editable
      line_items_to_delete.each do |line_item_to_delete|
        line_item_to_delete.destroy
      end 
      self.populate_line_items(account, children, billing_period)       
    when "posted"
      editable_line_items = self.invoice_line_items.editable
      dup_invoice = self.dup
      dup_invoice.issue += dup_invoice.issue
      dup_invoice.save
      dup_invoice.populate_line_items(account, children, billing_period)
      editable_line_items.each do |editable_line_item|
        dup_editable_line_item = editable_line_item.dup
        dup_editable_line_item.invoice_id = dup_invoice.id
        dup_editable_line_item.issue = dup_invoice.issue
        dup_editable_line_item.save
      end
    end
  end
  
  def calculate_discounts
    self.nursery_discounts
    self.customer_discounts  
  end
  
  def nursery_discounts
    self.invoice_line_items.each do |line_item|
      if line_item.charges.pluck(:chargeable_type).uniq == ["Timesheet"]
        line_item.timesheet_period_ids.each do |period_id|
          period = Period.find(period_id)
          unless period.discounts.empty?
            period.discounts.each do |discount|
              if discount.in_force?(billing_period)
                discount_params = {}
                discount_params[:description] = discount.name
                if discount.discount_type == "Fixed"
                  discount_params[:total_cost] = discount.amount * -1.0
                else
                  discount_params[:total_cost] = (discount.amount/100) * line_item.total_cost * -1.0
                end
                self.discount_line_item(discount_params)
              end
            end
          end
        end        
      end
    end
  end

  def customer_discounts
    unless account.discounts.empty?
      subtotal = self.invoice_line_items.sum("total_cost")
      account.discounts.each do |discount|
        if discount.in_force?(billing_period)
          discount_params = {}
          discount_params[:description] = discount.name
          if discount.discount_type == "Fixed"
            discount_params[:total_cost] = discount.amount * -1.0
          else
            discount_params[:total_cost] = (discount.amount/100) * subtotal * -1.0
          end
          self.discount_line_item(discount_params)          
        end
      end
    end
  end
end
