# == Schema Information
#
# Table name: bookings
#
#  id           :integer         not null, primary key
#  nursery_id   :integer
#  child_id     :integer
#  period_id    :integer
#  start_date   :date
#  end_date     :date
#  every        :string(255)
#  days_of_week :text
#  created_at   :datetime        not null
#  updated_at   :datetime        not null
#

class Booking < ActiveRecord::Base
  attr_accessible :child_id, :days_of_week, :end_date, :every, :nursery_id, :period_id, :start_date, :invoice_line_item_id
  before_save :remove_empty_days_of_week
  
  scope :by_child, lambda { |child| where(child_id: child) }
  scope :future_end_date, lambda { |stop| where("end_date >=?", stop) }  
  
  serialize :days_of_week
  
  belongs_to :nursery
  belongs_to :child
  belongs_to :period
  belongs_to :invoice_line_item
  
  has_many :charges, as: :chargeable
  
  validates :nursery_id, presence: true, :numericality => { :only_integer => true }
  validates :child_id, presence: true, :numericality => { :only_integer => true }
  validates :period_id, presence: true, :numericality => { :only_integer => true }
  validates :start_date, presence: true
  validates :end_date, presence: true
  validates :days_of_week, presence: true
  validates :every, presence:true
  
  def abbr_day(day)
     Date::ABBR_DAYNAMES[Date::DAYS_INTO_WEEK[day.downcase.to_sym]+1]
  end
  
  protected
  def remove_empty_days_of_week
    days_of_week.delete_if{ |x| x.empty? }
  end
  
end
