# == Schema Information
#
# Table name: terms
#
#  id                 :integer         not null, primary key
#  local_authority_id :integer
#  description        :string(255)
#  start_date         :date
#  end_date           :date
#  created_at         :datetime        not null
#  updated_at         :datetime        not null
#

class Term < ActiveRecord::Base
  attr_accessible :description, :end_date, :local_authority_id, :start_date
  
  belongs_to :local_authority
  
  has_many :timesheets
  
  validates :description, presence: true
  validates :start_date, presence: true
  validates :end_date, presence: true
  validates :local_authority_id, presence: true, numericality: { only_integer: true }
  
  def self.current_term
    where('start_date <= ? AND end_date >= ?', Date.today, Date.today).first
  end 
  
  def self.for_timesheet(timesheet)
    where('start_date <= ? AND end_date >= ?', timesheet.date_attended, timesheet.date_attended).first
  end
  
  def self.timesheet_in_term?(timesheet)
    if self.for_timesheet(timesheet).nil?
      false
    else
      true
    end
  end  
end
