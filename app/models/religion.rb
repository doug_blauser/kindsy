# == Schema Information
#
# Table name: religions
#
#  id         :integer         not null, primary key
#  name       :string(255)
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

class Religion < ActiveRecord::Base
  attr_accessible :name
  
  has_many :children
  
  validates :name, presence: true
end
