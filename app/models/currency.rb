# == Schema Information
#
# Table name: currencies
#
#  id               :integer         not null, primary key
#  currency_type_id :integer
#  nursery_id       :integer
#  created_at       :datetime        not null
#  updated_at       :datetime        not null
#

class Currency < ActiveRecord::Base
  attr_accessible :currency_type_id, :nursery_id
  
  belongs_to :currency_type
  belongs_to :nursery
  
  validates :currency_type_id, presence:true
  validates :nursery_id, presence: true
end
