# == Schema Information
#
# Table name: contact_infos
#
#  id               :integer         not null, primary key
#  address_line_1   :string(255)
#  address_line_2   :string(255)
#  address_line_3   :string(255)
#  town             :string(255)
#  county           :string(255)
#  country          :string(255)
#  post_code        :string(255)
#  telephone_1      :string(255)
#  telephone_2      :string(255)
#  telephone_3      :string(255)
#  telephone_4      :string(255)
#  skype            :string(255)
#  email_1          :string(255)
#  email_2          :string(255)
#  website          :string(255)
#  created_at       :datetime        not null
#  updated_at       :datetime        not null
#  addressable_id   :integer
#  addressable_type :string(255)
#

class ContactInfo < ActiveRecord::Base
  attr_accessible :address_line_1, :address_line_2, :address_line_3, :country, :county, :email_1, :email_2, :post_code, :skype, :telephone_1, :telephone_2, :telephone_3, :telephone_4, :town, :website, :addressable_id, :addressable_type

  belongs_to :addressable, :polymorphic => true
  has_many :staff_members
  
  valid_email_regex = /\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,4}\b/
  valid_telephone_regex = /^[+\/\() 0-9 \s*]+$/
  valid_website_regex = /^([a-zA-Z0-9]([a-zA-Z0-9\-]{0,61}[a-zA-Z0-9])?\.)+[a-zA-Z]{2,6}$/

  validates :address_line_1, presence: true
  validates :town, presence: true
  validates :post_code, presence: true
  validates :email_1, format: { with: valid_email_regex }, allow_nil: true
  validates :email_2, format: { with: valid_email_regex }, allow_nil: true
  validates :telephone_1, format: { with: valid_telephone_regex }, allow_nil: true
  validates :telephone_2, format: { with: valid_telephone_regex }, allow_nil: true  
  validates :telephone_3, format: { with: valid_telephone_regex }, allow_nil: true
  validates :telephone_4, format: { with: valid_telephone_regex }, allow_nil: true
  validates :website, format: { with: valid_website_regex }, allow_nil: true

  def to_label
    "#{address_line_1} #{address_line_2} #{address_line_3} #{town} #{county} #{country}"
  end
  
  def display_name
    "#{address_line_1} #{address_line_2} #{address_line_3} #{town} #{county} #{country}"
  end  
end
