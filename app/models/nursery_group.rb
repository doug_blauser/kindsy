# == Schema Information
#
# Table name: nursery_groups
#
#  id                        :integer         not null, primary key
#  name                      :string(255)
#  billing_period_start_date :date
#  created_at                :datetime        not null
#  updated_at                :datetime        not null
#

class NurseryGroup < ActiveRecord::Base
  attr_accessible :billing_period_start_date, :name  

  has_many :nurseries, dependent: :destroy

  
  validates :name, presence: true
  validates :billing_period_start_date, presence: true

end
