# == Schema Information
#
# Table name: hairs
#
#  id         :integer         not null, primary key
#  color      :string(255)
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

class Hair < ActiveRecord::Base
  attr_accessible :color
  
  has_many :children
  
  validates :color, presence: true
end
