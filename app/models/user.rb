# == Schema Information
#
# Table name: users
#
#  id                     :integer         not null, primary key
#  username               :string(255)     default(""), not null
#  first_name             :string(255)     default(""), not null
#  last_name              :string(255)     default(""), not null
#  email                  :string(255)     default(""), not null
#  encrypted_password     :string(255)     default(""), not null
#  reset_password_token   :string(255)
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer         default(0)
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string(255)
#  last_sign_in_ip        :string(255)
#  failed_attempts        :integer         default(0)
#  unlock_token           :string(255)
#  locked_at              :datetime
#  created_at             :datetime        not null
#  updated_at             :datetime        not null
#

class User < ActiveRecord::Base
  devise :database_authenticatable, :recoverable, :rememberable, :trackable, :validatable, :timeoutable, :lockable, :registerable

  attr_accessible :email, :password, :password_confirmation, :remember_me, :username, :first_name, :last_name
  
  has_many :accounts
  has_and_belongs_to_many :roles, :join_table => :users_roles  
  has_many :employments
  has_many :nurseries, :through => :employments
  
  validates :username, presence: true
  validates :first_name, presence: true
  validates :last_name, presence: true
  validates :email, uniqueness: { case_sensitive: false }
  
  def current_nursery
    self.nurseries.first
  end
end
