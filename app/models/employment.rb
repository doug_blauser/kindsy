# == Schema Information
#
# Table name: employments
#
#  id         :integer         not null, primary key
#  nursery_id :integer
#  user_id    :integer
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

class Employment < ActiveRecord::Base
  attr_accessible :nursery_id, :user_id
  
  belongs_to :nursery
  belongs_to :user
  
  validates :nursery_id, presence: true, :numericality => { :only_integer => true }
  validates :user_id, presence: true, :numericality => { :only_integer => true }
  
end
