# == Schema Information
#
# Table name: permissions
#
#  id         :integer         not null, primary key
#  role_id    :integer
#  action     :string(255)
#  condition  :string(255)
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

class Permission < ActiveRecord::Base
  attr_accessible :action, :condition, :role_id

  belongs_to :role
  
  validates :role_id, presence: true
  validates :action, presence: true
end
