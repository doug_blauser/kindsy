# == Schema Information
#
# Table name: local_authorities
#
#  id         :integer         not null, primary key
#  name       :string(255)
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

class LocalAuthority < ActiveRecord::Base
  attr_accessible :name, :contact_info_attributes
  
  has_one :contact_info, as: :addressable
  has_many :nurseries
  has_many :terms
  
  accepts_nested_attributes_for :contact_info  

  validates :name, presence: true
end
