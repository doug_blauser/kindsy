# == Schema Information
#
# Table name: periodations
#
#  id         :integer         not null, primary key
#  period_id  :integer
#  room_id    :integer
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

class Periodation < ActiveRecord::Base
  attr_accessible :period_id, :room_id
  
  belongs_to :period
  belongs_to :room
  
  validates :period_id, :room_id, presence: true, :numericality => { :only_integer => true }
end
