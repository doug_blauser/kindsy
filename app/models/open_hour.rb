# == Schema Information
#
# Table name: open_hours
#
#  id          :integer         not null, primary key
#  nursery_id  :integer
#  day_of_week :string(255)
#  open_time   :time
#  close_time  :time
#  created_at  :datetime        not null
#  updated_at  :datetime        not null
#

class OpenHour < ActiveRecord::Base
  attr_accessible :close_time, :day_of_week, :nursery_id, :open_time

  belongs_to :nursery
  

  validates :nursery_id, presence: true
  validates :day_of_week, presence: true
  validates :open_time, presence: true
  validates :close_time, presence: true
  
end
