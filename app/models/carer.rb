# == Schema Information
#
# Table name: carers
#
#  id         :integer         not null, primary key
#  title      :string(255)
#  first_name :string(255)
#  surname    :string(255)
#  created_at :datetime        not null
#  updated_at :datetime        not null
#  nursery_id :integer
#

class Carer < ActiveRecord::Base
  attr_accessible :contact_info_id, :title, :first_name, :surname, :nursery_id, :contact_info_attributes

  belongs_to :nursery

  has_one :contact_info, :as => :addressable
  
  has_many :children, :through => :child_relationships
  has_many :child_relationships, :dependent => :destroy
  has_many :accounts, :through => :accountabilities
  has_many :accountabilities, :dependent => :destroy
  has_many :invoices, :dependent => :destroy
  
  accepts_nested_attributes_for :contact_info  
  
  validates :first_name, presence: true
  validates :surname, presence: true
  validates :nursery_id, presence: true, :numericality => { :only_integer => true }
  
  def display_name
    "#{title} #{first_name} #{surname}"
  end
  
  def self.search(field, query, id)
    if query
      where("#{'upper('}#{field}#{')'} #{'LIKE upper(?) AND nursery_id = ?'}", "%#{query}%", id)       
    else
      "Enter a value in Contains field"
    end    
  end  

  def relationship(carer, child)
    ChildRelationship.where('child_id = ? AND carer_id = ?', child.id, carer.id).first.child_relationship_type.relationship_type
  end
  
  def display_relations(carer, relationships)
    relationships.map { |related| carer.relationship(carer, related.child) }
  end
end
