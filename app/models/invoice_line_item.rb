# == Schema Information
#
# Table name: invoice_line_items
#
#  id          :integer         not null, primary key
#  invoice_id  :integer
#  account_id  :integer
#  description :string(255)
#  cost        :decimal(5, 2)
#  total_cost  :decimal(6, 2)
#  created_at  :datetime        not null
#  updated_at  :datetime        not null
#  quantity    :decimal(5, 2)
#  child_id    :integer
#  editable    :boolean
#  issue       :integer
#  fe_amount   :decimal(5, 2)
#

class InvoiceLineItem < ActiveRecord::Base
  attr_accessible :account_id, :cost, :description, :invoice_id, :total_cost, :quantity, :child_id, :editable, :issue, :fe_amount

  scope :by_child, lambda { |child| where(child_id: child) }
  scope :non_editable, lambda { where("editable is null") }
  scope :editable, lambda { where("editable = ?", true) }
  
  belongs_to :invoice
  belongs_to :account
  belongs_to :child
  
  has_many :charges, dependent: :destroy
  
  validates :account_id, presence: true, numericality: { only_integer: true }
  validates :description, presence: true
  validates :cost, presence: true, numericality: true
  validates :total_cost, presence: true, numericality: true
  validates :quantity, presence: true, numericality: true
  validates :child_id, numericality: { only_integer: true }, allow_nil: true 
  validates :issue, presence: true, numericality: { only_integer: true }
  validates :fe_amount, numericality: true, allow_nil: true
  

  def self.balance_due
    self.where("description = ?", "Balance Brought Forward").last
  end
  
  def timesheet_ids
    self.charges.pluck(:chargeable_id)
  end
  
  def timesheets
    Timesheet.find(timesheet_ids)
  end
  
  def timesheet_period_ids
    self.timesheets.map { |timesheet| timesheet.period_id }.uniq
  end
  
end
