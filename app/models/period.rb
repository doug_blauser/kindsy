# == Schema Information
#
# Table name: periods
#
#  id                 :integer         not null, primary key
#  nursery_id         :integer
#  period_name        :string(255)
#  period_start_time  :time
#  period_end_time    :time
#  period_hourly_cost :decimal(5, 2)
#  created_at         :datetime        not null
#  updated_at         :datetime        not null
#  period_cost        :decimal(5, 2)
#  days_of_week       :text
#  fe_limit           :decimal(6, 3)
#

class Period < ActiveRecord::Base
  attr_accessible :nursery_id, :period_end_time, :period_hourly_cost, :period_cost, :period_name, :period_start_time, :periodation_attributes, :room_ids, :days_of_week, :fe_limit

  belongs_to :nursery
  has_many :periodations, :dependent => :destroy
  has_many :rooms, :through => :periodations
  has_many :period_automations, :dependent => :destroy
  has_many :presets, :through => :period_automations
  has_many :timesheets
  has_many :period_prices, :dependent => :destroy
  has_many :discounts, as: :discountable
    
  accepts_nested_attributes_for :periodations
  
  validates :nursery_id, presence: true
  validates :period_name, presence: true
  validates :period_start_time, presence: true
  validates :period_end_time, presence: true
  validates :period_hourly_cost, presence: true, numericality: true
  validates :period_cost, presence: true, numericality: true
  validates :days_of_week, presence: true
  validates :fe_limit, presence: true, numericality: true
  
end
