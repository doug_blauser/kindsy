# == Schema Information
#
# Table name: discounts
#
#  id                :integer         not null, primary key
#  nursery_id        :integer
#  name              :string(255)
#  discount_type     :string(255)
#  amount            :decimal(6, 3)
#  created_at        :datetime        not null
#  updated_at        :datetime        not null
#  discountable_id   :integer
#  discountable_type :string(255)
#  start_date        :date
#  end_date          :date
#  applies_to        :string(255)
#

class Discount < ActiveRecord::Base
  attr_accessible :amount, :discount_type, :name, :nursery_id, :discountable_id, :discountable_type, :start_date, :end_date, :applies_to
  
  belongs_to :nursery
  belongs_to :discountable, polymorphic: true
  
  validates :nursery_id, presence: true, numericality: { only_integer: true }
  validates :name, presence: true
  validates :discount_type, presence: true
  validates :amount, presence: true, numericality: true, if: :percentage_discount?, inclusion: 0..100
  validates :discountable_id, presence: true, numericality: { only_integer: true }
  validates :discountable_type, presence: true
  validates :start_date, presence: true
  validates :applies_to, presence: true
  
  def to_partial_path(name)
    "discounts/#{name}"
  end
  
  def percentage_discount?
    discount_type == "Percent"
  end
  
  def in_force?(billing_period)
    self.start_date <= billing_period.start_date && self.end_date >= billing_period.end_date
  end
  
end
