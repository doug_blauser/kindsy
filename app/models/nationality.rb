# == Schema Information
#
# Table name: nationalities
#
#  id         :integer         not null, primary key
#  country    :string(255)
#  created_at :datetime        not null
#  updated_at :datetime        not null
#

class Nationality < ActiveRecord::Base
  attr_accessible :country
  
  has_many :children
  
  validates :country, presence: true
end
