# == Schema Information
#
# Table name: child_relationship_types
#
#  id                :integer         not null, primary key
#  relationship_type :string(255)
#  created_at        :datetime        not null
#  updated_at        :datetime        not null
#

class ChildRelationshipType < ActiveRecord::Base
  attr_accessible :relationship_type
  
  has_many :child_relationships
  
  validates :relationship_type, presence: true  
end
